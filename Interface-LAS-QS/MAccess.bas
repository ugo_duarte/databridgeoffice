Attribute VB_Name = "MAccess"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MAccess"
Sub Upload_DMS_Dealprintout()
    Const SUB_NAME As String = "Upload DMS Dealprintout"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Table As String
    Dim aHeader() As Variant
    Dim aData() As Variant
    Dim wksData As Worksheet
    Dim Model As String
    Dim i As Long

    If MInterface.Message_Confirm("Deseja fazer o upload dos Dealprintout's?") <> vbYes Then End
    
    Call MGlobal.Global_Initialize
    
    Set wksData = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")

    Table = "DMS_DealPrintout"
    aHeader = MStructure.Get_Model_Range("Models_DMS_DealPrintout_Access")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    aData = MStructure.Get_Data(aHeader, aHeader, aData, , True)

    Call Insert_Timeserial_To_Array(aData)
    Call Insert_UserCHG_To_Array(aData)
    
    Call DMS_Dealprintout_Insert_To_Access(aHeader, aData)

Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

    
End Sub
Sub DMS_Deals()
    Const SUB_NAME As String = "Update Access DMS Deals"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Table As String
    Dim aHeader() As Variant
    Dim aData() As Variant
    Dim wksData As Worksheet
    Dim Model As String
    Dim i As Long

    If MInterface.Message_Confirm("Deseja atualizar a Tabela: 'DMS Deals'?") <> vbYes Then End
    
    Call MGlobal.Global_Initialize
    
    Set wksData = Get_Worksheet_By_Codename("wksData_DMSDeals")
    
    If MInterface.Message_Confirm("Import DMS Deals Report?", SUB_NAME) = vbYes Then
    
        Call MStructure.Clean_Worksheet(wksData)
        Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Models_DMS_Deals_Original")
        Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Models_DMS_Deals_Header")
    
    End If

    Table = "DMS_Deals"
    aHeader = MStructure.Get_Model_Range("Models_DMS_Deals_Access")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    aData = MStructure.Get_Data(aHeader, aHeader, aData, , True)

    Call Delete_From_Access("DELETE FROM " & Table)
    
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_UserCHG_To_Array(aData)
    
    Call Insert_To_Access_Recordset(Table, aHeader, aData)

Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

End Sub
Sub LAS_Adm_InstallationCertificate()
    Const SUB_NAME As String = "Update Access LAS Adm Installation Certificate"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Table As String

    Call MGlobal.Global_Initialize
    
    Table = "LAS_Adm_InstallationCertificate"
    
    Call Access_Delete_And_Upload_Data(Table)
    
    Call MInterface.Message_Information("Tabela '" & Table & "' atualizada", SUB_NAME)
    
Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
    
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

End Sub
Sub DMS_FollowUp_EmittedDeals()
    Const SUB_NAME As String = "Update Access LAS Adm Lease Schedules"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Table As String

    Call MGlobal.Global_Initialize

    Table = "DMS_FollowUp_EmittedDeals"
    
    Call Access_Delete_And_Upload_Data(Table)
    
    Call MInterface.Message_Information("Tabela '" & Table & "' atualizada", SUB_NAME)
    
Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

End Sub
Sub LAS_Adm_LeaseSchedules()
    Const SUB_NAME As String = "Update Access LAS Adm Lease Schedules"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Table As String

    Call MGlobal.Global_Initialize

    Table = "LAS_Adm_LeaseSchedules"
    
    Call Access_Delete_And_Upload_Data(Table)
    
    Call MInterface.Message_Information("Tabela '" & Table & "' atualizada", SUB_NAME)
    
Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

End Sub
Sub DMS_Addresses()
    Const SUB_NAME As String = "Update Access DMS Addresses"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Table As String

    Call MGlobal.Global_Initialize

    Table = "DMS_Addresses"
    
    Call Access_Delete_And_Upload_Data(Table)
    
    Call MInterface.Message_Information("Tabela '" & Table & "' atualizada", SUB_NAME)
    
Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

End Sub
Sub Access_Delete_And_Upload_Data(Table As String)
    Const SUB_NAME As String = "Update Access LAS Adm Installation Certificate"
    Call Log_Step(SUB_NAME)

    Dim aHeader() As Variant
    Dim aData() As Variant
    Dim wksData As Worksheet
    Dim Model As String
    Dim i As Long
    Dim ReportMessage As String
    Dim TableReport As String
    
    If MInterface.Message_Confirm("Deseja atualizar a Tabela: '" & Table & "'?") <> vbYes Then End
    
    Set wksData = Get_Worksheet_By_Codename("wksTemporary")
        
    Call MStructure.Clean_Worksheet(wksData)
    TableReport = REPORTS_FOLDER & Table & ".xlsx"
    
    'Usuario confirma se escolhe um arquivo ou se deseja atualizar com o da pasta de relatorios
    If File_Exists(TableReport) Then
        ReportMessage = "Update with the latest report? " & vbCrLf & "Report Date: " & Format(FileDateTime(TableReport), "dd/mm/yyyy hh:mm:ss")
        If Not MInterface.Message_Confirm(ReportMessage) = vbYes Then
            TableReport = vbNullString
        End If
    Else
        TableReport = vbNullString
    End If
    
    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Models_" & Table & "_Original", fileName:=TableReport)
    Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Models_" & Table & "_Header")

    aHeader = MStructure.Get_Model_Range("Models_" & Table & "_Access")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    aData = MStructure.Get_Data(aHeader, aHeader, aData, , True)
    
    If UBound(aData) > 1 Then
        Call Delete_From_Access("DELETE FROM " & Table)
        
        Call Insert_Timeserial_To_Array(aData)
        Call Insert_UserCHG_To_Array(aData)
        Call MData.Correct_TaxID(aData, Table)
        Call Insert_To_Access_Recordset(Table, aHeader, aData)
    End If

End Sub
Sub Change_Access_Database(CurrentPath As String, NewPath As String)
    Const SUB_NAME As String = "Change Access Database"
    Dim Qs As Queries
    Dim Q As WorkbookQuery
    
    Call Log_Step(SUB_NAME)
    
    Dim QFormula As String
    'QFormula = gdicGlobalParameters.Item("DEVELOPERQUERY")
    'QFormula = gdicGlobalParameters.Item("CLOUDQUERY")
    
    Set Qs = ThisWorkbook.Queries
    
    For Each Q In Qs
        QFormula = Q.Formula
        QFormula = Replace(QFormula, "C:\Users\udu\Documents\CHG-Meridian\SourceCode\databridgeoffice\DBCHGBrazil.accdb", NewPath)
        'QFormula = Replace(QFormula, ACCESS_DATABASE_PATH, ACCESS_DATABASE_PATH_CLOUD)
        'QFormula = Replace(QFormula, ACCESS_DATABASE_PATH_CLOUD, ACCESS_DATABASE_PATH)
        QFormula = Replace(QFormula, CurrentPath, NewPath)

        'Debug.Print Q.Name
        'Debug.Print Q.Description
        'Debug.Print Q.Formula

        Q.Formula = QFormula

    Next Q



    ' ActiveWorkbook.Queries.Add Name:="DMS_FollowUp_EmittedDeals", Formula:= _
    '     "let" & Chr(13) & "" & Chr(10) & "    Source = Access.Database(File.Contents(""C:\Users\udu\Documents\CHG-Meridian\SourceCode\databridgeoffice\DBCHGBrazil.accdb""), [CreateNavigationProperties=true])," & Chr(13) & "" & Chr(10) & "    _DMS_FollowUp_EmittedDeals = Source{[Schema="""",Item=""DMS_FollowUp_EmittedDeals""]}[Data]" & Chr(13) & "" & Chr(10) & "in" & Chr(13) & "" & Chr(10) & "    _DMS_FollowUp_EmittedDeals"
    ' ActiveWorkbook.Worksheets.Add
    ' With ActiveSheet.ListObjects.Add(SourceType:=0, Source:= _
    '        "OLEDB;Provider=Microsoft.Mashup.OleDb.1;Data Source=$Workbook$;Location=DMS_FollowUp_EmittedDeals;Extended Properties=""""" _
    '        , Destination:=Range("$A$1")).QueryTable
    '        .CommandType = xlCmdSql
    '        .CommandText = Array("SELECT * FROM [DMS_FollowUp_EmittedDeals]")
    '        .RowNumbers = False
    '        .FillAdjacentFormulas = False
    '        .PreserveFormatting = True
    '        .RefreshOnFileOpen = False
    '        .BackgroundQuery = True
    '        .RefreshStyle = xlInsertDeleteCells
    '        .SavePassword = False
    '        .SaveData = True
    '        .AdjustColumnWidth = True
    '        .RefreshPeriod = 0
    '        .PreserveColumnInfo = True
    '        .ListObject.DisplayName = "DMS_FollowUp_EmittedDeals"
    '        .Refresh BackgroundQuery:=False
    '    End With
End Sub

Sub Update_Data()
    Dim wks As Worksheet
    Set wks = ActiveSheet
    
    Select Case wks.Codename
        Case "wksList_QS_Assets"
            Call Access_Insert_Table("FromTo_LAS_QS_Assets", wks.Range("A1"), Get_Model_Range("Models_Gerar_FromTo_LAS_QS_Assets_Access"), Get_Model_Range("Models_Gerar_FromTo_LAS_QS_Assets_DataType"))
            
        Case "wksFromTo_LAS_QS_Assets"
            Call Access_Update_Table("FromTo_LAS_QS_Assets", wks.Range("A1"))
    End Select
    
    
End Sub
Sub Access_Insert_Table(TableName As String, rngData As Range, rngDataHeader As Range, rngDataType As Range)
    Const SUB_NAME As String = "Insert To Access"
    On Error GoTo ErrorHandler
    'Declaring the necessary variables.
    Dim con As ADODB.Connection
    Dim aData() As Variant
    Dim aDataHeader() As Variant
    Dim aDataType() As Variant
    Dim SQL As String
    Dim i As Long
    Dim Headers As String
    Dim Values As String
    Dim SQLExec As String
    
    aData = Get_Current_Region(rngData)
    aDataHeader = rngDataHeader
    aDataType = rngDataType
    
    Headers = Get_String_Headers(aData)
    Set con = New_ADODB_Connection(enmConnectionType_Access)
    SQL = ACCESS_INSERT_STATEMENT
    SQL = Replace(SQL, "@TABLE", TableName)
    SQL = Replace(SQL, "@HEADERS", Headers)
    
    For i = LBound(aData) + 1 To UBound(aData)
        Values = Get_String_Values_DataType(aDataHeader, aData, aDataType, i)
        
        SQLExec = Replace(SQL, "@VALUES", Values)
        con.Execute SQLExec
    Next i
    

Finish:
    If Not con Is Nothing Then
        con.Close
        Set con = Nothing
    End If

    Exit Sub

ErrorHandler:
    
    If InStr(1, err.Description, "create duplicate values in the index") > 0 Then
        Call MInterface.Message_Error(err.Description)
        Resume Next
    Else
        Call MInterface.Message_Error(err.Number & vbNewLine & err.Description)

    End If
    
    Set con = Nothing
    
    GoTo Finish

End Sub
Sub Access_Update_Table(TableName As String, rngData As Range, Optional IDColumnName As String = "ID")
    Const SUB_NAME As String = "Access_Update_Table"
    On Error GoTo ErrorHandler
    Call MInterface.Log_Step(SUB_NAME)

    Dim con As ADODB.Connection
    Dim rs As ADODB.Recordset
    Dim rstFiltered As ADODB.Recordset
    
    Dim SQL As String
    Dim aData() As Variant
    Dim dicColumns As Dictionary
    
    
    Dim i As Long
    Dim j As Long
    
    Dim jData As Long
    Dim Column As Long
    Dim CurrentField As String
    Dim ColumnID As Long
    Dim Header As String
    Dim Value As String
    
    'Create the ADODB connection object.
    Set con = New_ADODB_Connection(enmConnectionType_Access)
    SQL = "Select * from " & TableName
    
    Set rs = New ADODB.Recordset
    rs.CursorType = adOpenDynamic
    rs.LockType = adLockOptimistic

    Call rs.Open(SQL, con)
    
    aData = Get_Current_Region(rngData)
    Set dicColumns = Load_Dictionary_Columns(aData)
    ColumnID = dicColumns(IDColumnName)
    
    If ColumnID > 0 Then
    
        For i = LBound(aData) + 1 To UBound(aData)
            If aData(i, ColumnID) <> "" Then
                rs.Filter = IDColumnName & "=" & aData(i, ColumnID)
                Do Until rs.EOF
                    For j = 0 To rs.Fields.Count - 1
                        CurrentField = rs.Fields(j).Name
                        Column = dicColumns(CurrentField)
                        
                        If Column > 0 And CurrentField <> IDColumnName Then
                            rs(j) = aData(i, Column)
                        End If
                         
                    Next j 'For j = 0 To rs.Fields.Count - 1
                    rs.Update
                    rs.MoveNext
                Loop
                
                rs.Filter = 0
            End If
nextI:
        Next i
        
    End If 'If ColumnID > 0 Then
    
Fim:
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not con Is Nothing Then
        con.Close
        Set con = Nothing
    End If

    Exit Sub

ErrorHandler:
    If InStr(1, err.Description, "create duplicate values in the index") > 0 Then
        Resume Next
    Else
        Call MInterface.Message_Error(err.Number & vbNewLine & err.Description)
        Call err.Raise(err.Number, err.Source, err.Description)
    End If
    
    Set con = Nothing
    Set rs = Nothing
    
    GoTo Fim

End Sub
