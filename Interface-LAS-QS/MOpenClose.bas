Attribute VB_Name = "MOpenClose"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MOpenClose"

Private Sub Auto_Close()
    Const SUB_NAME As String = "Auto Close"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksMenu")
    Call MSecurity.Hide_All_Worksheets(visibleWorksheet:=wks)
End Sub
Private Sub Auto_Open()
    Const SUB_NAME As String = "Auto Open"
    Call Log_Step(SUB_NAME)

    Call MSecurity.Login_Environ
    
End Sub

