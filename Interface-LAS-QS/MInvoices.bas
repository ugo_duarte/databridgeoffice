Attribute VB_Name = "MInvoices"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MInvoices"

Const TAG_CONTRATO_VIGENTE As String = "ContratoVigente"
Const TAG_CHAVE_NFE As String = "ChaveNFE"
Const TAG_FILE_PATH As String = "Path"
Const TAG_INVOICE_NO As String = "InvoiceNo"
Const TAG_QUANTIDADE_ITEMS As String = "QuantidadeItems"
Const TAG_TOTAL_VALUE As String = "ValorTotalNF"
Const TAG_FOLDER_PATH As String = "Pasta"
Const TAG_ACTIVE As String = "Active"
Sub Upload_Invoices_To_Access()
    Dim wksDataFiles As Worksheet
    Dim aData() As Variant
    Dim i As Long
    
    Dim ColumnContratoVigente As Long
    Dim ColumnPath As Long
    Dim ColumnChaveNFE As Long
    Dim ColumnInvoiceNo As Long
    Dim ColumnQuantidadeItems As Long
    Dim ColumnValorTotal As Long
    Dim aHeader() As Variant
    
    
    Set wksDataFiles = Get_Worksheet_By_Codename("wksData_Files_NFE")
    aData = Get_Current_Region(wksDataFiles.Range("A1"))
    ColumnContratoVigente = Get_Column_Number(aData, TAG_CONTRATO_VIGENTE)
    ColumnPath = Get_Column_Number(aData, TAG_FILE_PATH)
    ColumnChaveNFE = Get_Column_Number(aData, TAG_CHAVE_NFE)
    ColumnInvoiceNo = Get_Column_Number(aData, TAG_INVOICE_NO)
    ColumnQuantidadeItems = Get_Column_Number(aData, TAG_QUANTIDADE_ITEMS)
    ColumnValorTotal = Get_Column_Number(aData, TAG_TOTAL_VALUE)
    
    For i = LBound(aData) + 1 To UBound(aData)
        Call Insert_To_Access_Recordset("NFE_COVER", aHeader, aData)
    Next i
    
End Sub
Sub Invoice_Activate_Deactivate(rng As Range)
    Dim wksDataFiles As Worksheet
    Dim ColumnActivate As Long
    Dim ColumnContratoVigente As Long
    Dim aModel() As Variant
    
    If rng.Row > 1 Then
        Set wksDataFiles = Get_Worksheet_By_Codename("wksData_Files_NFE")
        
        If rng.Worksheet.Codename = wksDataFiles.Codename Then
            
            aModel = Get_Model_Range("Model_Arquivos_NFE")
            ColumnContratoVigente = Get_Column_Number(aModel, TAG_CONTRATO_VIGENTE)
            ColumnActivate = Get_Column_Number(aModel, TAG_ACTIVE)
            
            If rng.Worksheet.Cells(rng.Row, ColumnContratoVigente).Text = vbNullString Then End
            
            If rng.Worksheet.Cells(rng.Row, ColumnActivate) = "0" Then
                rng.Worksheet.Cells(rng.Row, ColumnActivate) = "1"
            Else
                rng.Worksheet.Cells(rng.Row, ColumnActivate) = "0"
            End If
            
        Else
            Call MInterface.Message_Error("Can only Activate/Deactivate in the '" & wksDataFiles.Name & "' Sheet!")
        End If
        
    End If 'If rng.Row > 1 Then
    
End Sub

Sub Capture_Invoices_From_Folders()
    
    Dim aData() As Variant
    Dim i As Long
    Dim ColumnContratoVigente As Long
    Dim ColumnFolderPath As Long
    Dim CurrentFolder As String
    Dim ContratoVigente As String
    Dim wksDataFiles As Worksheet
    
    Call MGlobal.Global_Initialize
    
    aData = Get_Current_Region(Get_Worksheet_Range("wksData_Folders_NFE"))
    ColumnContratoVigente = Get_Column_Number(aData, TAG_CONTRATO_VIGENTE)
    ColumnFolderPath = Get_Column_Number(aData, TAG_FOLDER_PATH)
    Set wksDataFiles = Get_Worksheet_By_Codename("wksData_Files_NFE")
    gFileID = 0
    
    Call Generate_Worksheet_From_Model(wksDataFiles, True, "Model_Arquivos_NFE")
    
    For i = LBound(aData) + 1 To UBound(aData)
        ContratoVigente = aData(i, ColumnContratoVigente)
        CurrentFolder = aData(i, ColumnFolderPath)
        
        Call MStructure.FilesPaths_To_Collection(FolderPath:=CurrentFolder)
        
        If Not gdicPaths.Count = 0 Then
            Call List_Invoices(gdicPaths, ContratoVigente, wksDataFiles)
        End If
        
    Next i
    wksDataFiles.Activate
    wksDataFiles.Cells.Columns.AutoFit
    
    Call MGlobal.Global_Finalize
    
End Sub
Sub List_Invoices(dicPaths As Dictionary, ContratoVigente As String, wksData As Worksheet)
    Dim key As Variant
    Dim xDoc As MSXML2.DOMDocument
    'IMPORT: XML
    
    For Each key In dicPaths.Keys
        gFileID = gFileID + 1
        
        'XML object
        Set xDoc = New MSXML2.DOMDocument
        xDoc.Load (key)
        
        If Not xDoc Is Nothing Then
            Call List_Invoices_XML_Data(xDoc, key, ContratoVigente, wksData)
        End If
        
    Next key
    
End Sub
Public Sub List_Invoices_XML_Data(xDoc As MSXML2.DOMDocument, ByVal fileName As String, ByVal ContratoVigente As String, wksData As Worksheet, Optional ArrayBase As Long = 1)
    On Error GoTo ErrorHandler
    
    Dim xNodeChaveNFE           As IXMLDOMNode
    Dim xNodeInvoiceNo          As IXMLDOMNode
    Dim xNodeListItems          As IXMLDOMNodeList
    Dim xNodeValorTotal         As IXMLDOMNode
    

    
    Dim xNode       As IXMLDOMNode
    
    Dim xNodeList   As IXMLDOMNodeList
    
    Dim xMainNode   As IXMLDOMNode
    Dim aModel() As Variant
    Dim aModelData() As Variant
    
    Const XPATH_CHAVE_NFE       As String = "//protNFe/infProt/chNFe"
    Const XPATH_INVOICE_NO      As String = "//NFe/infNFe/ide/nNF"
    Const XPATH_ITEM            As String = "//NFe/infNFe/det"
    Const XPATH_VALOR_TOTAL     As String = "//NFe/infNFe/total/ICMSTot/vNF"
    
    
    aModel = Get_Model_Range("Model_Arquivos_NFE")
    ReDim aModelData(1 To 1, 1 To UBound(aModel, 2))
    
    Dim ColumnContratoVigente As Long
    Dim ColumnPath As Long
    Dim ColumnChaveNFE As Long
    Dim ColumnInvoiceNo As Long
    Dim ColumnQuantidadeItems As Long
    Dim ColumnValorTotal As Long
    
    ColumnContratoVigente = Get_Column_Number(aModel, TAG_CONTRATO_VIGENTE)
    ColumnPath = Get_Column_Number(aModel, TAG_FILE_PATH)
    ColumnChaveNFE = Get_Column_Number(aModel, TAG_CHAVE_NFE)
    ColumnInvoiceNo = Get_Column_Number(aModel, TAG_INVOICE_NO)
    ColumnQuantidadeItems = Get_Column_Number(aModel, TAG_QUANTIDADE_ITEMS)
    ColumnValorTotal = Get_Column_Number(aModel, TAG_TOTAL_VALUE)
    
    Set xNodeChaveNFE = xDoc.SelectSingleNode(XPATH_CHAVE_NFE)
    If Not xNodeChaveNFE Is Nothing Then
        Set xNodeInvoiceNo = xDoc.SelectSingleNode(XPATH_INVOICE_NO)
        Set xNodeListItems = xDoc.SelectNodes(XPATH_ITEM)
        Set xNodeValorTotal = xDoc.SelectSingleNode(XPATH_VALOR_TOTAL)
    
        aModelData(1, ColumnContratoVigente) = ContratoVigente
        aModelData(1, ColumnPath) = fileName
        aModelData(1, ColumnChaveNFE) = xNodeChaveNFE.nodeTypedValue
        aModelData(1, ColumnInvoiceNo) = xNodeInvoiceNo.nodeTypedValue
        aModelData(1, ColumnQuantidadeItems) = xNodeListItems.Length
        aModelData(1, ColumnValorTotal) = CDbl(Convert_Number(xNodeValorTotal.nodeTypedValue, "0.00"))
        
        
        wksData.Range("A1").Offset(Get_Last_Row(wksData, ColumnContratoVigente)).Resize(, UBound(aModelData, 2)) = aModelData
        
    End If
    
    
    Exit Sub
ErrorHandler:
    Stop
    Resume
    Call MInterface.Message_Error(err.Description)
End Sub
