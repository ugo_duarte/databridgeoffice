Attribute VB_Name = "MMain"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MMain"

Const MODEL_DATA_LAS As String = "Model_Data_LAS"
Sub Unlock_All_Click()
    Const SUB_NAME As String = "Unlock All Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim wksActive As Worksheet
    Set wksActive = ActiveSheet
    Call MGlobal.Global_Initialize

    Select Case UCase(gProjectName)
        Case UCase(PROJECT_DEAL_DETAILS)
            If MSecurity.Is_Master_User Then
                Call MSecurity.Unprotect_All_Worksheet
            Else
                Call err.Raise(999, , "Permission denied for user: " & Get_Windows_UserName)
            End If

        Case Else
            Call MSecurity.Unprotect_All_Worksheet
    End Select

    Call MInterface.Message_Information(MSG_SUCCESS)

Finish:
    Call MGlobal.Global_Finalize(False)
    wksActive.Activate

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Add_Click()
    Const SUB_NAME As String = "Add Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)
    
    Dim wksActive As Worksheet

    If Check_Excel_Template Then
        Call err.Raise(901, , "Cant use the original template!" & vbNewLine & "Create a New Deal!")
    End If
    
    Set wksActive = ActiveSheet
    
    Call MGlobal.Global_Initialize

    Select Case UCase(gProjectName)
        Case UCase(PROJECT_DEAL_DETAILS)
            Call MDealDetails.Add_Lines(wksActive)
    End Select

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    wksActive.Activate

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Clean_Click()
    Const SUB_NAME As String = "Clean Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)
    
    If MInterface.Message_Confirm("Clean solution?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    'Pre
    Call MStructure.Remove_All_Filters_Workbook

    Call MSetup.Setup_Workbook

    Call MSetup.Clean_Workbook

    'Post
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MInterface.Activate_Main_Menu

    Call MGlobal.Global_Finalize

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Copy_DMS_Description_Click()
    Const SUB_NAME As String = "Copy DMS Description Click"
    Call Log_Step(SUB_NAME)
    
    Dim DMS_LastRow As Long
    Dim rngCopy As Range
    Dim rngGenerated As Range
    Dim LastGenerated As Date
    Dim TimeDifference As Double

    If Check_Excel_Template Then
        Call err.Raise(901, , "Cant use the original template!" & vbNewLine & "Create a New Deal!")
    End If
    
    Set rngCopy = Get_Model_Range("DMS_Description_Range_Copy")
    
    DMS_LastRow = MStructure.Get_Last_Row(rngCopy.Worksheet, rngCopy.Column) - 4
    
    Set rngGenerated = Get_Model_Range("DMS_Description_Range_Generated")
    
    LastGenerated = rngGenerated.Value
    TimeDifference = DateDiff("S", LastGenerated, Now)
    If TimeDifference < 60 Then
        If DMS_LastRow <= 0 Then
            DMS_LastRow = 1
        End If

        rngCopy.Resize(DMS_LastRow).Copy

    Else
        Call MInterface.Message_Error("Generate again to copy!" & vbNewLine & "Time Difference has to be less then 60! " & vbNewLine & "TD:" & TimeDifference, SUB_NAME)

    End If


End Sub

Sub Export_Data_Purchases_Click()
    Const SUB_NAME As String = "Export Data Purchases Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)
    
    Dim rng As Range
    Dim wks As Worksheet
    
    If MInterface.Message_Confirm("Export: Data Purchases?") <> vbYes Then End

    Call MGlobal.Global_Initialize
    Set wks = Get_Worksheet_By_Codename("wksData_Purchases")
    Set rng = wks.Range("A1").CurrentRegion
    Call MExport.Export_Range_To_XLSX("Data_Purchases", rng)

Finish:
    Call MInterface.Activate_Worksheet_By_Codename("wksMenu_Reports")
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Export_DMS_Deals_Emitted_Click()
    Const SUB_NAME As String = "Export DMS Deals Emitted Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    If MInterface.Message_Confirm("Export: DMS Deals Emitted?") <> vbYes Then End

    Call MGlobal.Global_Initialize

    Call MDMSDealFollowUp.Export_DMS_Deals_Emitted

Finish:
    Call MInterface.Activate_Worksheet_By_Codename("wksMenu_Reports")
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub

Sub Export_Deal_Details_Click()
    Const SUB_NAME As String = "Export DMS Deals Details Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim rng As Range
    Dim wks As Worksheet

    If MInterface.Message_Confirm("Export: DMS Deal Details?") <> vbYes Then End

    Call MGlobal.Global_Initialize

    Call MDealDetails.Export_Deal_Details_to_XLSM

Finish:
    Call MInterface.Activate_Worksheet_By_Codename("wksMenu_Reports")
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub

Sub Export_Interface_LAS_Click()
    Const SUB_NAME As String = "Export Interface LAS Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim rng As Range
    Dim wks As Worksheet

    If MInterface.Message_Confirm("Export: Interface LAS ?") <> vbYes Then End

    Call MGlobal.Global_Initialize
    Set wks = Get_Worksheet_By_Codename("wksReport_Interface_XMLLAS")
    Set rng = wks.Range("A1").CurrentRegion
    Call MExport.Export_Range_To_XLSX("LAS_Assets", rng)

Finish:
    Call MInterface.Activate_Worksheet_By_Codename("wksMenu_Reports")
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Export_Interface_Qualisoft_Assets_Click()
    Const SUB_NAME As String = "Export Interface Qualisoft Assets Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim rng As Range
    Dim wks As Worksheet
    If MInterface.Message_Confirm("Export: Interface Qualisoft Assets?") <> vbYes Then End

    Call MGlobal.Global_Initialize
    
    Set wks = Get_Worksheet_By_Codename("wksReport_Interface_QS_Assets")
    Set rng = wks.Range("A1").CurrentRegion
    Call MExport.Export_CSV_Semicolon(rng, "Interface QS Assets", True)

Finish:
    Call MInterface.Activate_Worksheet_By_Codename("wksMenu_Reports")
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Export_Interface_Qualisoft_Schedules_Click()
    Const SUB_NAME As String = "Export Interface QS Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim wksReport As Worksheet
    If MInterface.Message_Confirm("Export Report QS Schedules?") <> vbYes Then End

    Call MGlobal.Global_Initialize
    Set wksReport = Get_Worksheet_By_Codename("wksReport_QS_Schedules")
    If Get_Last_Row(wksReport, 1) = 1 Then
        Call MInterface.Message_Error("No Data!", SUB_NAME)
        GoTo Finish
    End If

    Call MExport.Export_CSV_Semicolon(wksReport.Range("A1").CurrentRegion, "Interface-QS-Schedules", False)

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Export_Interface_LAS_Cashflow(Table As String, Codename As String)
    Const SUB_NAME As String = "Export_Interface_LAS_Cashflow"
    Dim wksReport As Worksheet
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)
    If MInterface.Message_Confirm("Export: Interface Cashflow " & Table & "?") <> vbYes Then End
    
    Call MGlobal.Global_Initialize
    
    Set wksReport = Get_Worksheet_By_Codename(Codename)
    
    If Get_Last_Row(wksReport, 1) = 1 Then
        Call MInterface.Message_Error("No Data!", SUB_NAME)
        GoTo Finish
    End If

    Call MExport.Export_CSV_Semicolon(wksReport.Range("A1").CurrentRegion, "Interface-LAS-Cashflow-" & Table, True)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Export_Interface_LAS_Cashflow_Beneficiaries_Click()
    Const SUB_NAME As String = "Export Interface Cashflow Beneficiaries Click"
    Call Export_Interface_LAS_Cashflow("Beneficiaries", "wksReport_Interface_Beneficiary")
    
End Sub
Sub Export_Interface_LAS_Cashflow_Schedules_Click()
    Const SUB_NAME As String = "Export Interface Cashflow Schedules Click"
    Call Export_Interface_LAS_Cashflow("Schedules", "wksReport_Interface_Schedules")
    
End Sub

Sub Export_Interface_LAS_Cashflow_Invoices_Click()
    Const SUB_NAME As String = "Export Interface Cashflow Invoices Click"
    Call Export_Interface_LAS_Cashflow("Invoices", "wksReport_Interface_Invoices")
End Sub
Sub Generate_DMS_Description_Click()
    Const SUB_NAME As String = "Generate DMS Description"
    Call Log_Step(SUB_NAME)

    If Check_Excel_Template Then
        Call err.Raise(901, , "Cant use the original template!" & vbNewLine & "Create a New Deal!")
    End If

    If MInterface.Message_Confirm("Generate DMS Description?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Call MSecurity.Check_Cloud_Enviroment

    Call MDealDetails.Refresh_All_Data
    Call MSetup.Setup_DMS_Description
    Call MDealDetails.Generate_DMS_Description

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Worksheet_By_Codename("wksReportDMSDescription")

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbNewLine & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Birthdays_Emails()
    Const SUB_NAME As String = "Generate Birthdays Emails"
    Call Log_Step(SUB_NAME)

    If MInterface.Message_Confirm("Generate Birthdays Emails?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Call MRHBirthdays.Generate_Birthday_Email

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbNewLine & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_DMS_Deals_Approved_Click()
    Const SUB_NAME As String = "Generate DMS Deals Approved"
    Call Log_Step(SUB_NAME)

    If Check_Excel_Template Then
        Call err.Raise(901, , "Cant use the original template!" & vbNewLine & "Create a New Deal!")
    End If

    If MInterface.Message_Confirm("Generate DMS Deals Approved?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    'Call MSecurity.Check_Cloud_Enviroment

    Call MDMSDealFollowUp.Generate_DMS_Deals_Approved_SQL
    
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DMSDeals_Approved")
    
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbNewLine & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_XML_LAS_Click()
    Const SUB_NAME As String = "Generate Interface XML-LAS Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String
    If MInterface.Message_Confirm("Generate: Interface XML-LAS Assets?") <> vbYes Then End
    Call MInterface.Message_Information("Gera os dados no formato do layout de importac�o de Assets do LAS." & vbCrLf & "Conferir!" & "Aba: Purchases x Invoices [Roxo]" & vbCrLf & "Caso modifique qualquer configuracao, gere novamente!.")
    
    
    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook

    'Process
    Call MInterfaceXMLLAS.Interface_LAS_Assets

    gTimerFinish = Now
    msg = "Generate Interface XML-LAS" & vbNewLine & _
          "Start: " & gTimerStart & vbNewLine & _
          "Finish: " & gTimerFinish & vbNewLine & _
          "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    
    Call MInterface.Message_Information(msg, SUB_NAME)
    
Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_Qualisoft_Click()
    Const SUB_NAME As String = "Generate Interface Qualisoft Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Generate: Interface Qualisoft Assets?") <> vbYes Then End

    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook

    'Process
    Call MInterfaceXMLLAS.Interface_QS_Assets

    gTimerFinish = Now
    msg = "Generate Interface Qualisoft" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_Framing_Click()
    Const SUB_NAME As String = "Generate Interface Framing Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Generate: Interface Framing?") <> vbYes Then End

    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook
    
    'Process
    Call MInterfaceLASQS.Interface_Framing
        
    gTimerFinish = Now
    msg = "Generate Interface Framing" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_LAS_Cashflow_Beneficiaries_Click()
    Const SUB_NAME As String = "Generate Interface LAS-Cashflow Beneficiaries Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Generate: Interface LAS-Cashflow Beneficiaries?") <> vbYes Then End

    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook
    
    'Process
    Call MInterfaceLASCashflow.Generate_Interface_LAS_Cashflow_Beneficiaries
        
    gTimerFinish = Now
    msg = "Generate Interface LAS-Cashflow Beneficiaries" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_LAS_Cashflow_Invoices_Click()
    Const SUB_NAME As String = "Generate Interface LAS-Cashflow Invoices Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Generate: Interface LAS-Cashflow Invoices?") <> vbYes Then End

    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook
    
    'Process
    Call MInterfaceLASCashflow.Generate_Interface_LAS_Cashflow_Invoices
        
    gTimerFinish = Now
    msg = "Generate Interface LAS-Cashflow Invoices" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_LAS_Cashflow_Schedules_Click()
    Const SUB_NAME As String = "Generate Interface LAS-Cashflow Schedules Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Generate: Interface LAS-Cashflow Schedules?") <> vbYes Then End

    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook

    'Process
    Call MInterfaceLASCashflow.Generate_Interface_LAS_Cashflow_Schedules

    gTimerFinish = Now
    msg = "Generate Interface LAS-Cashflow Schedules" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize

    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Schedules_For_Interface_QS_Assets()
    Const SUB_NAME As String = "Generate_Schedules_For_Interface_QS_Assets"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Generate: Schedules for Interface Qualisoft Assets?") <> vbYes Then End

    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook

    'Process

    Call MInterfaceLASQS.Generate_Schedules_Interface_LAS_QS_Assets

    gTimerFinish = Now
    msg = "Generate Schedles for Interface Qualisoft" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

    ThisWorkbook.RefreshAll

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize

    Exit Sub

ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_Qualisoft_Assets_Click()
    Const SUB_NAME As String = "Generate Interface Qualisoft Assets Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String
    
    If MInterface.Message_Confirm("Generate: Interface Qualisoft Assets?") <> vbYes Then End
    
    'Pre
    Call MGlobal.Global_Initialize
    Call MStructure.Remove_All_Filters_Workbook
    
    'Process
    
    Select Case UCase(gProjectName)
        Case UCase(PROJECT_LAS_QS_ASSETS)
            Call MInterfaceLASQS.Interface_LAS_QS_Assets
        Case UCase(PROJECT_XML_LAS_ASSETS)
            Call MInterfaceXMLLAS.Interface_QS_Assets
    End Select
    
    
    
    gTimerFinish = Now
    msg = "Generate Interface Qualisoft" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_Data_Purchases_Click()
    Const SUB_NAME As String = "Import Data Purchases Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Data Purchases?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize


    Set wksData = Get_Worksheet_By_Codename("wksData_Purchases")
    If MInterface.Message_Confirm("Clean Data Purchases?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If
    
    
    Select Case UCase(gProjectName)
        
        Case UCase(PROJECT_LAS_CASHFLOW)
            Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_LAS_Purchases_Header")
        Case Else
            Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Models_RPA_Purchases_Data_Header")
    End Select
    
    

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_DMS_Deals_Emitted_Click()
    Const SUB_NAME As String = "Import DMS Deals Emitted Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import DMS Deals Emitted?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize
    Call ThisWorkbook.RefreshAll
    'Set wksData = Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    'If MInterface.Message_Confirm("Clean DMS Deals Emitted Data?", SUB_NAME) = vbYes Then
    '    Call MStructure.Clean_Worksheet(wksData)
    'End If
    'Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_DMS_Deals_Emitted")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub

Sub Import_DMS_Deals_FollowUp_Click()
    Const SUB_NAME As String = "Import DMS Deals Follow Up Report Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import DMS Deals Report?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Set wksData = Get_Worksheet_By_Codename("wksData_DMSDeals")
    If MInterface.Message_Confirm("Clean DMS Deals Data?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_DMS_Deals")

    Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Model_Data_DMS_Deals_Header")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_DMS_TXT_DealPrintout_Click()
    Const SUB_NAME As String = "Import DMS TXT DealPrintout Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim TextCollection As Collection
    Dim TextCollectionNames As Collection

    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import: DMS Deals Printout's ?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Set wksData = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")
    If MInterface.Message_Confirm("Clean DMS Data?", SUB_NAME) = vbYes Then
        Call MStructure.Generate_Worksheet_From_Model(wksData, True, "Model_Data_DMS")
    Else
        Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Model_Data_DMS")
    End If
    
    'Call MStructure.Import_XLSX
    Set TextCollection = New Collection
    Set TextCollectionNames = New Collection
    Call MStructure.Import_TXT(TextCollection, TextCollectionNames)
    Call MDealDetails.Map_Model_DMS_Data(TextCollection, TextCollectionNames)


    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DMS_DealPrintout")
    
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_Deal_Details_Click()
    Const SUB_NAME As String = "Import Deal Details Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim TextCollection As Collection
    Dim TextCollectionNames As Collection

    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Deal Printout DMS?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Call MDMSDealFollowUp.Import_Deal_Details


    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_LAS_InstallCertificate_Administration_Click()
    Const SUB_NAME As String = "Import LAS Installation Administration Report Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import: Report LAS Installation Certificate?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize


    Select Case UCase(gProjectName)
        Case UCase(PROJECT_DMS_DEALS_FOLLOW_UP)
            Set wksData = Get_Worksheet_By_Codename("wksData_LAS_InstCert")
            Model = "Model_Data_LAS_IC"

        Case Else
            Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
            Model = "Model_Data_LAS_Installation_Certificate"

    End Select

    If MInterface.Message_Confirm("Clean LAS Installation Certificate Data?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:=Model)


    Select Case UCase(gProjectName)
        Case UCase(PROJECT_DMS_DEALS_FOLLOW_UP)
            Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Model_Data_LAS_IC_Header")

    End Select

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
    
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_LAS_Assets_Position_Purchases_Click()
    Const SUB_NAME As String = "Import LAS Assets Position Purchases Report Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Report LAS Assets Position Purchases?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize


    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Assets_Pos_Purchase")
    If MInterface.Message_Confirm("Clean LAS Assets Position Purchases Data?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_LAS_Assets_Position_Purchases")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_LAS_Schedules_Administration_Click()
    Const SUB_NAME As String = "Import LAS Schedules Administration Report Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import: Report LAS Adm Schedules?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize


    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
    
    If MInterface.Message_Confirm("Clean: " & wksData.Name & "?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_LAS")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    
    Call MInterface.Activate_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
    
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_Database_Data_Click()
    Const SUB_NAME As String = "Import Database Schedules Data Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Database Schedules Data?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Set wksData = Get_Worksheet_By_Codename("wksData_Database")
    Call MStructure.Clean_Worksheet(wksData)

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_Database")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_Database_NF_Payable_Click()
    Const SUB_NAME As String = "Import Database NF Payable Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Database NF Payable?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Set wksData = Get_Worksheet_By_Codename("wksData_Database_NF_Payable")
    Call MStructure.Clean_Worksheet(wksData)

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_Database_NF_Payable")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_LAS_Assets_Overview_Administration_Click()
    Const SUB_NAME As String = "Import LAS Assets Overview Administration Report Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import LAS Assets Overview?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Assets")
    
    If MInterface.Message_Confirm("Clean '" & wksData.Name & "' ?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_LAS_Assets")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Call MGlobal.Global_Finalize

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_LAS_Purchases_IC_Click()
    Const SUB_NAME As String = "Import LAS Purchases Installation Certificate Report Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import LAS Purchases Installation Certificate?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize

    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Purchases_IC")
    
    If MInterface.Message_Confirm("Clean '" & wksData.Name & "' ?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Data_LAS_Purchases_IC")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_SerialNumbers_Click()
    Const SUB_NAME As String = "Import Serial Numbers Excel"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    'Project
    'PROJECT_XML_LAS_ASSETS


    'Confirm
    If MInterface.Message_Confirm("Import: Serial Numbers ?", SUB_NAME) <> vbYes Then End
    
    Dim wksData As Worksheet
    Set wksData = Get_Worksheet_By_Codename("wksData_SerialNumbers")
    Call MInterface.Message_Information("Selecione N arquivos Excel para importar." & vbCrLf & _
                                        "Caso seja apresentada uma mensagem de erro ao importar, validar cabe�alho no arquivo!" & vbCrLf & _
                                        "Aba: " & wksData.Name & " [Laranja]", SUB_NAME)
    
    Call MGlobal.Global_Initialize

    Call MInterfaceXMLLAS.Import_SerialNumbers

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_PurchasesInvoices_Click()
    Const SUB_NAME As String = "Import Purchases Invoices Excel"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    'Project
    'PROJECT_XML_LAS_ASSETS

    'Confirm
    If MInterface.Message_Confirm("Import: Purchases Invoices ?", SUB_NAME) <> vbYes Then End
    
    Dim wksData As Worksheet
    Set wksData = Get_Worksheet_By_Codename("wksList_PurchasesInvoices")
    
    Call MGlobal.Global_Initialize

    Call MStructure.Clean_Worksheet(wksData)
    
    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Purchase_Invoice")
    Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Model_Purchase_Invoice_Header")
    
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_XML_NFE_Click()
    Const SUB_NAME As String = "Import XML NFE Folder"
    Call Log_Step(SUB_NAME)

    Dim msg As String
    Dim wks As Worksheet

    'Project
    'PROJECT_XML_LAS_ASSETS

    If MInterface.Message_Confirm("Import: NFE XML Files ?", SUB_NAME) <> vbYes Then End
    

    Set wks = Get_Worksheet_By_Codename("wksList_Model_SerialNumber")
    Call MInterface.Message_Information("Selecione uma pasta para importar." & vbCrLf & _
                                        "Esta rotina vai importar todos arquivos XML presentes na Pasta selecionada e respectivas Sub-Pastas." & vbCrLf & _
                                        "Ap�s importar, valide a configurac�o dos equipamentos!" & vbCrLf & _
                                        "Aba: " & wks.Name & " [Amarela]" & vbCrLf & _
                                        "Caso modifique a coluna [VALID] � necessario re-importar.")


    Call MGlobal.Global_Initialize

    Call MInterfaceXMLLAS.Import_XML_NFE

    gTimerFinish = Now
    msg = "Concluido: " & gFileID & " File(s) lido(s)" & vbNewLine & _
          "Start: " & gTimerStart & vbNewLine & _
          "Finish: " & gTimerFinish & vbNewLine & _
          "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
          
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_XML_NFE_Cover_Click()
    Const SUB_NAME  As String = "Import XML NFE Folder"
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Import: NFE XML Files ?", SUB_NAME) <> vbYes Then End
    
    Call MGlobal.Global_Initialize

    Call MInterfaceXMLLAS.Import_XML_NFE_Cover

    gTimerFinish = Now
    msg = "Concluido: " & gFileID & " File(s) lido(s)" & vbNewLine & _
          "Start: " & gTimerStart & vbNewLine & _
          "Finish: " & gTimerFinish & vbNewLine & _
          "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
          
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_XML_NFS_Click()
    Const SUB_NAME  As String = "Import XML NFS Folder"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If MInterface.Message_Confirm("Import: NFS XML Files ?", SUB_NAME) <> vbYes Then End
    
    Call MGlobal.Global_Initialize

    Call MInterfaceXMLLAS.Import_XML_NFS


    gTimerFinish = Now
    msg = "Concluido: " & gFileID & " File(s)" & vbNewLine & "Start: " & gTimerStart & vbNewLine & "Finish: " & gTimerFinish & vbNewLine & "Seconds: " & DateDiff("S", CDate(gTimerStart), CDate(gTimerFinish))
    Call MInterface.Message_Information(msg, SUB_NAME)

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Public Sub New_Deal_Click()
    Const SUB_NAME As String = "New Deal"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    'PROJECT_DEAL_DETAILS

    If MInterface.Message_Confirm("New Deal?", "New Deal") <> vbYes Then End

    Call MGlobal.Global_Initialize

    Call MDealDetails.NewDeal_Create
    
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu

    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbNewLine & err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Refresh_Click()
    Const SUB_NAME As String = "Refresh Click"
    Call Log_Step(SUB_NAME)

    Dim wksActive As Worksheet
    Dim aModel() As Variant
    Dim columnNumber As Long

    If Check_Excel_Template Then
        Call err.Raise(901, , "Cant use the original template!" & vbNewLine & "Create a New Deal!")
    End If
    
    Set wksActive = ActiveSheet
    
    Select Case wksActive.Codename
        Case "wksMenu", "wksMenu_Reports"
            ThisWorkbook.RefreshAll
        Case Else
            Call MGlobal.Global_Initialize
            ThisWorkbook.RefreshAll
    
            wksActive.Columns.AutoFit
            wksActive.Cells.WrapText = False
            wksActive.UsedRange.EntireColumn.AutoFit
            
            If wksActive.Codename = "wksData_Assets" Then
                aModel = Get_Model_Range("Assets_Range_Header")
                columnNumber = Get_Column_Number(aModel, "Asset Description") + 1
                If wksActive.Columns(columnNumber).ColumnWidth > 150 Then
                   wksActive.Columns(columnNumber).ColumnWidth = 150
                   wksActive.Columns(columnNumber).WrapText = True
                End If
            End If
            
            Call MGlobal.Global_Finalize
            
            wksActive.Activate
    End Select
    
    
End Sub
Sub Refresh_ModelsSerialNumbers_Click()
    Const SUB_NAME As String = "Refresh List: Models / Serial Numbers"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    If MInterface.Message_Confirm("Refresh Models / Serial Numbers?") <> vbYes Then End
    Call MGlobal.Global_Initialize


    Call MInterfaceXMLLAS.Validate_Models_Serial_Numbers

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Call MInterface.Activate_Worksheet_By_Codename("wksList_Model_SerialNumber")
    Call MGlobal.Global_Finalize
    Exit Sub
    
ErrorHandler:
    gTimerFinish = Now
    Call MInterface.Message_Error(err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Interface_Qualisoft_Schedules_Click()
    Const SUB_NAME As String = "Generate Interface Qualisoft Schedules Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Generate Interface QS Schedules?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize
    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
    If Get_Last_Row(wksData, 1) = 1 Then
        Call MInterface.Message_Error("No Data!", SUB_NAME)
        GoTo Finish
    End If

    Call MInterfaceLASQS.Interface_LAS_QS_Schedules

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Pipeline_Click()
    Const SUB_NAME As String = "Generate Pipeline Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Generate Pipeline?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize
    
    Set wksData = Get_Worksheet_By_Codename("wksData_Database")
    If Get_Last_Row(wksData, 1) = 1 Then
        Call MInterface.Message_Error("No Data!", SUB_NAME)
        GoTo Finish
    End If

    Call MPipeline.Generate_Pipeline

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Generate_Purchases_Click()
    Const SUB_NAME As String = "Generate Purchases Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Generate Purchases?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize
    
    Call MAddPurchases.Generate_Purchases

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Worksheet_By_Codename("wksData_Purchases")
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub

Sub Update_Data_Click()
    Const SUB_NAME As String = "Update_Data_Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Update Data?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize
    
    Call MAccess.Update_Data

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_SUCCESS & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_Data_Click()
    Const SUB_NAME As String = "Import_Data_Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Data?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize


    Set wksData = Get_Worksheet_By_Codename("wksData_Invoices")
    If MInterface.Message_Confirm("Clean Worksheet Data?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Input_Data")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub
Sub Import_Data_Click()
    Const SUB_NAME As String = "Import_Data_Click"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim Model As String
    Dim wksData As Worksheet

    If MInterface.Message_Confirm("Import Data?", SUB_NAME) <> vbYes Then End

    Call MGlobal.Global_Initialize


    Set wksData = Get_Worksheet_By_Codename("wksData_Invoices")
    If MInterface.Message_Confirm("Clean Worksheet Data?", SUB_NAME) = vbYes Then
        Call MStructure.Clean_Worksheet(wksData)
    End If

    Call MStructure.Import_XLSX_Model(wksData:=wksData, DataModel:="Model_Input_Data")

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(MSG_FAIL & vbCrLf & err.Number & vbCrLf & err.Description, SUB_NAME)
    GoTo Finish
End Sub