Attribute VB_Name = "MDealDetails"
Option Explicit
Option Private Module
Const MODULE_NAME                   As String = "MDealDetails"

'   /$$$$$$$                      /$$       /$$$$$$$              /$$               /$$ /$$
'  | $$__  $$                    | $$      | $$__  $$            | $$              |__/| $$
'  | $$  \ $$  /$$$$$$   /$$$$$$ | $$      | $$  \ $$  /$$$$$$  /$$$$$$    /$$$$$$  /$$| $$  /$$$$$$$
'  | $$  | $$ /$$__  $$ |____  $$| $$      | $$  | $$ /$$__  $$|_  $$_/   |____  $$| $$| $$ /$$_____/
'  | $$  | $$| $$$$$$$$  /$$$$$$$| $$      | $$  | $$| $$$$$$$$  | $$      /$$$$$$$| $$| $$|  $$$$$$
'  | $$  | $$| $$_____/ /$$__  $$| $$      | $$  | $$| $$_____/  | $$ /$$ /$$__  $$| $$| $$ \____  $$
'  | $$$$$$$/|  $$$$$$$|  $$$$$$$| $$      | $$$$$$$/|  $$$$$$$  |  $$$$/|  $$$$$$$| $$| $$ /$$$$$$$/
'  |_______/  \_______/ \_______/|__/      |_______/  \_______/   \___/   \_______/|__/|__/|_______/
'-----------------------------------------------------------------------------------------------------

Global Const DMS_DESCRIPTION_DATA_CUSTOMER_REQUIREMENT As String = "@Data_Customer_Requirement"
Global Const DMS_DESCRIPTION_DATA_CONCEPT As String = "@Data_Concept"
Global Const DMS_DESCRIPTION_DATA_CHANCES As String = "@Data_Chances"
Global Const DMS_DESCRIPTION_DATA_STATUS As String = "@Data_Status"

Function Is_Deal_Details_Data_Worksheet(wksActive As Worksheet) As Boolean
    Is_Deal_Details_Data_Worksheet = False
    Select Case wksActive.Codename
        Case "wksData_DeliveryAddresses", "wksData_Assets", "wksData_Suppliers"
            Is_Deal_Details_Data_Worksheet = True
    End Select
End Function

Sub Add_Lines(wksActive As Worksheet)
    Const SUB_NAME As String = "Add Lines"
        
    Dim wksData As Worksheet
    Dim Model As String
    Dim UserInput As String
    Dim iAddQuantity As Long
    Dim columnNumber As Long
    Dim i As Long
    
    If Is_Deal_Details_Data_Worksheet(wksActive) Then
    
        If MInterface.Message_Confirm("Do you wish to add new lines?", SUB_NAME) <> vbYes Then End
        
        UserInput = InputBox("How many lines", SUB_NAME, 1)
        iAddQuantity = 0
        If IsNumeric(UserInput) Then
            iAddQuantity = CLng(UserInput)
        Else
            Call err.Raise(999, , "Not a number!")
        End If
        
        wksActive.UsedRange.EntireColumn.AutoFit
        Set wksData = wksActive
        
        Select Case wksActive.Codename
            Case "wksData_Assets"
                Model = MODEL_DATA_ASSETS
            Case "wksData_Suppliers"
                Model = MODEL_DATA_SUPPLIERS
            Case "wksData_DeliveryAddresses"
                Model = MODEL_DATA_DELIVERYADDRESSES
        End Select
        
        For i = 1 To iAddQuantity
            Application.StatusBar = "Adding lines: " & i & " / " & iAddQuantity
            DoEvents
            Call MModels.Add_Data(wksData, Model)
        Next i
    Else
        Call err.Raise(999, , "Can't add lines to this sheet!")
    End If
End Sub

Sub Insert_DealDetails_To_Access(Text As String)
    Const SUB_NAME As String = "Deal Details to Access"
    On Error GoTo ErrorHandler
    
    Call Insert_To_Access_DealDetails_Description(Text)
    Call MDealDetails.Insert_To_Access_DealDetails_Details
    Call MDealDetails.Insert_To_Access_DealDetails_Assets
    Call MDealDetails.Insert_To_Access_DealDetails_Suppliers
    Call MDealDetails.Insert_To_Access_DealDetails_DeliveryAddresses
    
    Call MInterface.Message_Information("Deal Details Generated!", "Access")
Finish:
    Exit Sub
ErrorHandler:
    Call Delete_DealDetails_From_Access
End Sub
Sub Insert_Deal_To_Array(aData() As Variant)
    Const SUB_NAME As String = "Insert Deal To Array"
    Dim Text As String
    Text = Get_Model_Range("DealDetails_Overview_Deal").Text
    Call Insert_Field_To_Array(aData, "Deal", Text)
End Sub
Sub Insert_OldLeaseNo_To_Array(aData() As Variant)
    Const SUB_NAME As String = "Insert OldLeaseNo To Array"
    Dim Text As String
    Text = Get_Model_Range("DealDetails_Overview_OldLeaseNumber").Text
    Call Insert_Field_To_Array(aData, "OldLeaseNo", Text)
End Sub
Sub Insert_UserCHG_To_Array(aData() As Variant)
    Const SUB_NAME As String = "Insert UserCHG To Array"
    Dim Text As String
    Text = MSecurity.Get_Windows_UserName
    Call Insert_Field_To_Array(aData, "UserCHG", Text)
End Sub
Sub Insert_XML_B64_To_Array(aData() As Variant, XMLB64 As String)
    Const SUB_NAME As String = "Insert XML B64 To Array"
    Call Insert_Field_To_Array(aData, "XML", XMLB64)
End Sub
Sub Insert_To_Access_DealDetails_Description(XMLB64 As String)
    Const SUB_NAME As String = "Insert to Access Deal Details: Description"
    
    Dim Table As String
    Dim wksData As Worksheet
    Dim aHeader() As Variant
    Dim aDataType() As Variant
    Dim aData() As Variant
    
    Table = "DealDetails"
    aHeader = MStructure.Get_Model_Range("Model_DealDetails_Description")
    aData = MStructure.Get_Model_Range("Model_DealDetails_Description").Resize(2)
    
    Call Insert_Deal_To_Array(aData)
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_OldLeaseNo_To_Array(aData)
    Call Insert_UserCHG_To_Array(aData)
    Call Insert_XML_B64_To_Array(aData, XMLB64)
    Call Insert_To_Access_Recordset(Table, aHeader, aData)
    
End Sub

Sub Insert_To_Access_DealDetails_Details()
    Const SUB_NAME As String = "Insert to Access Deal Details: Details"
    
    Dim Table As String
    Dim wksData As Worksheet
    Dim aHeader() As Variant
    Dim aDataType() As Variant
    Dim aData() As Variant
    
    Table = "DealDetails_Details"
    aHeader = MStructure.Get_Model_Range("Model_DealDetails_Details")
    aDataType = MStructure.Get_Model_Range("Model_DealDetails_Details_DataType")
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_DealDetails_Details")
    aData = wksData.Range("A1").Resize(Get_Last_Row(wksData, 1), Get_Last_Column(wksData, 1))
    
    Call Insert_Deal_To_Array(aData)
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_OldLeaseNo_To_Array(aData)
    Call Insert_UserCHG_To_Array(aData)
    Call Insert_To_Access_Recordset(Table, aHeader, aData)
    
End Sub
Sub Insert_To_Access_DealDetails_DeliveryAddresses()
    Const SUB_NAME As String = "Insert to Access Deal Details: Delivery Addresses"
    
    Dim Table As String
    Dim wksData As Worksheet
    Dim aHeader() As Variant
    Dim aData() As Variant
    
    Table = "DealDetails_DeliveryAddresses"
    aHeader = MStructure.Get_Model_Range("Model_DealDetails_DeliveryAddresses")
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_DeliveryAddresses")
    
    aData = wksData.Range("B5").Resize(Get_Last_Row(wksData, 5) - 5 + 1, 6)
    aData = Get_Data(aHeader, aHeader, aData)
    
    Call Insert_Deal_To_Array(aData)
    Call Insert_OldLeaseNo_To_Array(aData)
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_To_Access_Recordset(Table, aHeader, aData)
    
End Sub
Sub Insert_To_Access_DealDetails_Suppliers()
    Const SUB_NAME As String = "Insert to Access Deal Details: Suppliers"
    
    Dim Table As String
    Dim wksData As Worksheet
    Dim aHeader() As Variant
    Dim aData() As Variant
    
    Table = "DealDetails_Suppliers"
    aHeader = MStructure.Get_Model_Range("Model_DealDetails_Suppliers")
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Suppliers")
    
    aData = wksData.Range("B5").Resize(Get_Last_Row(wksData, 5) - 5 + 1, 6)
    aData = Get_Data(aHeader, aHeader, aData)
    
    Call Insert_Deal_To_Array(aData)
    Call Insert_OldLeaseNo_To_Array(aData)
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_To_Access_Recordset(Table, aHeader, aData)
    
End Sub
Sub Insert_To_Access_DealDetails_Assets()
    Const SUB_NAME As String = "Insert to Access Deal Details: Assets"
    
    Dim Table As String
    Dim wksData As Worksheet
    Dim aHeader() As Variant
    Dim aData() As Variant
    
    Table = "DealDetails_Assets"
    aHeader = MStructure.Get_Model_Range("Model_DealDetails_Assets")
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Assets")
    
    aData = wksData.Range("B5").Resize(Get_Last_Row(wksData, 5) - 5 + 1, 12)
    aData = Get_Data(aHeader, aHeader, aData)
    
    Call Insert_Deal_To_Array(aData)
    Call Insert_OldLeaseNo_To_Array(aData)
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_To_Access_Recordset(Table, aHeader, aData)
    
End Sub
Sub Delete_DealDetails_From_Access()
    Call Delete_Deal_Timeserial("DealDetails")
    Call Delete_Deal_Timeserial("DealDetails_Details")
    Call Delete_Deal_Timeserial("DealDetails_Assets")
    Call Delete_Deal_Timeserial("DealDetails_DeliveryAddresses")
    Call Delete_Deal_Timeserial("DealDetails_Suppliers")
    
End Sub
Sub Delete_Deal_Timeserial(Table As String)
    Const SQL_DELETE As String = "DELETE FROM @TABLE WHERE Deal = '@Deal' AND Timeserial = '@Timeserial'"
    Dim SQL As String
    Dim Deal As String
    Dim Timeserial As String
    
    Deal = Get_Model_Range("DealDetails_Overview_Deal").Text
    SQL = SQL_DELETE
    SQL = Replace(SQL, "@TABLE", Table)
    SQL = Replace(SQL, "@Deal", Deal)
    SQL = Replace(SQL, "@Timeserial", gTimeserial)

    Call Delete_From_Access(SQL)
    
End Sub
Sub Export_Deal_Details_To_Cloud(EncryptedText As String)
    Const SUB_NAME As String = "Export Deal Details To Cloud"
    Dim Text As String
    Text = ENCRYPT_SEPARATOR & EncryptedText & ENCRYPT_SEPARATOR
    Call ThisWorkbook.SaveCopyAs(Generate_DealDetails_Filename_XLSM)
    Call MExport.Export_TXT(Text, Generate_DealDetails_Filename)
    
End Sub
Function Generate_DealDetails_Filename()
    Const SUB_NAME As String = "Generate_DealDetails_Filename"
    Dim dealDetailsFileName As String
    dealDetailsFileName = MSecurity.Get_CHG_Cloud_TI_Projects_Deal_Details_Path & "\@user-DealDetails-@leaseNo-@timeSerial.txt"
    dealDetailsFileName = Replace(dealDetailsFileName, "@user", Get_Windows_UserName)
    dealDetailsFileName = Replace(dealDetailsFileName, "@leaseNo", Get_DealDetails_OldLeaseNo)
    dealDetailsFileName = Replace(dealDetailsFileName, "@timeSerial", MFunctions.Get_TimeSerial)
    Generate_DealDetails_Filename = dealDetailsFileName
End Function
Function Generate_DealDetails_Filename_XLSM()
    Const SUB_NAME As String = "Generate_DealDetails_Filename_XLSM"
    Dim dealDetailsFileName As String
    dealDetailsFileName = MSecurity.Get_CHG_Cloud_TI_Projects_Deal_Details_Path & "\@user-DealDetails-@leaseNo-@timeSerial.xlsm"
    dealDetailsFileName = Replace(dealDetailsFileName, "@user", Get_Windows_UserName)
    dealDetailsFileName = Replace(dealDetailsFileName, "@leaseNo", Get_DealDetails_OldLeaseNo)
    dealDetailsFileName = Replace(dealDetailsFileName, "@timeSerial", MFunctions.Get_TimeSerial)
    Generate_DealDetails_Filename_XLSM = dealDetailsFileName
End Function
Public Sub Generate_DMS_Description()
    Const SUB_NAME As String = "Generate DMS Description"
    
    Dim aData() As Variant
    Dim aEncryptedData() As Variant
    
    Dim DataString As String
    Dim DataConcept As String
    Dim DataChances As String
    Dim DataStatus As String
    
    Dim B64Result As String
    Dim i As Long

    Dim rngReportDMSBase64 As Range

    Dim result As String
    Dim Text As String
    Dim EncryptedText As String
    
    Dim wksTemp As Worksheet
    Dim wksReport As Worksheet
    Dim wksModel As Worksheet
    Dim wksData As Worksheet

    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
    Set wksData = Get_Worksheet_By_Codename("wksData_DealDetailsTable")
    Set wksReport = Get_Worksheet_By_Codename("wksReportDMSDescription")
    Set wksModel = Get_Worksheet_By_Codename("wksModels")
    
    Set rngReportDMSBase64 = Get_Model_Range("Report_DMS_Base64")
    
    'Adjust the Char(10) to |
    Get_Model_Range("DealDetails_CustomerRequirements_Details").Value = Replace(Get_Model_Range("DealDetails_CustomerRequirements_Details").Value, Chr(10), " | ")

    result = wksModel.Range("Model_DMS_Description").Value
    aData = wksData.Range("A1").CurrentRegion
    DataString = DataString

    For i = LBound(aData) + 1 To UBound(aData)
        DataString = DataString & aData(i, 2) & ": " & MStructure.Add_Percentage_To_String(aData(i, 2), aData(i, 3)) & vbNewLine
    Next i

    result = Replace(result, DMS_DESCRIPTION_DATA_CUSTOMER_REQUIREMENT, vbNewLine & DataString)
    result = Replace(result, DMS_DESCRIPTION_DATA_CONCEPT, vbNewLine & DataConcept)
    result = Replace(result, DMS_DESCRIPTION_DATA_CHANCES, vbNewLine & DataChances)
    result = Replace(result, DMS_DESCRIPTION_DATA_STATUS, vbNewLine & DataStatus)

    
    B64Result = Get_Data_To_XML_DealDetails
    B64Result = Replace(B64Result, "&", "&amp;")
    EncryptedText = MCryptoAES.Encrypt(B64Result)
    
    If Is_Cloud_Enviroment Then
        Call MDealDetails.Export_Deal_Details_To_Cloud(EncryptedText)
    End If
    
    'EncryptedText = MCriptografia.EncodeBase64(B64Result)
    'Call MExport.Export_TXT(ENCRYPT_SEPARATOR & EncryptedText & ENCRYPT_SEPARATOR, Generate_DealDetails_Filename)
    
    
    aEncryptedData = MCryptoAES.Break_Encrypted_Data(ENCRYPT_SEPARATOR & EncryptedText)
    
    With rngReportDMSBase64
        .Resize(Get_Last_Row(rngReportDMSBase64.Worksheet, rngReportDMSBase64.Column)).ClearContents
        .Resize(Get_Last_Row(rngReportDMSBase64.Worksheet, rngReportDMSBase64.Column)).Style = "CHGInvisible"
        .Resize(UBound(aEncryptedData), UBound(aEncryptedData, 2)).Font.ThemeColor = xlThemeColorDark1
        .Resize(UBound(aEncryptedData), UBound(aEncryptedData, 2)).Font.TintAndShade = 0
        .Resize(UBound(aEncryptedData), UBound(aEncryptedData, 2)) = aEncryptedData
        '.Value = "|B64:" & EncryptedText '& vbNewLine & B64Result '"|Decoded: " & MCriptografia.DecodeBase64(EncryptedText)
    End With

    Call MModels.Model_Report_DMS_Description
    
    Get_Model_Range("DMS_Description_Range_Generated").Value = Now
    Get_Model_Range("DealDetails_Overview_Timeserial").Value = gTimeserial
    
    Text = ENCRYPT_SEPARATOR & EncryptedText & ENCRYPT_SEPARATOR
    Call MDealDetails.Insert_DealDetails_To_Access(Text)
    
    
    
End Sub
Function Get_DealDetails_OldLeaseNo()
    Get_DealDetails_OldLeaseNo = Get_Model_Range("DealDetails_Overview_OldLeaseNumber").Text
End Function
Function Get_Data_To_XML_DealDetails() As String
    Const SUB_NAME As String = "Get Data To XML DealDetails"

    Dim B64Result As String
    Dim rng As Range
    Dim wksData As Worksheet
    Dim lResize As Long

    B64Result = B64Result & vbNewLine & "<DO>"
    Set wksData = Get_Worksheet_By_Codename("wksData_DealDetailsTable")
    Set rng = wksData.Range("A1").CurrentRegion
    B64Result = B64Result & vbNewLine & Range_To_XML_String(rng, "DEALDETAILS")
    
    
    Set rng = Get_Model_Range(MODEL_DATA_SUPPLIERS & TAG_CURRENT_RANGE)
    lResize = (Get_Last_Row(rng.Worksheet, rng.Column) - rng.Row) + 2
    B64Result = B64Result & vbNewLine & Range_To_XML_String(rng.Offset(-1).Resize(lResize), "SUPPLIERS")
    
    Set rng = Get_Model_Range(MODEL_DATA_DELIVERYADDRESSES & TAG_CURRENT_RANGE)
    lResize = (Get_Last_Row(rng.Worksheet, rng.Column) - rng.Row) + 2
    B64Result = B64Result & vbNewLine & Range_To_XML_String(rng.Offset(-1).Resize(lResize), "DELIVERYADDRESSES")
    
    Set rng = Get_Model_Range(MODEL_DATA_ASSETS & TAG_CURRENT_RANGE)
    lResize = (Get_Last_Row(rng.Worksheet, rng.Column) - rng.Row) + 2
    B64Result = B64Result & vbNewLine & Range_To_XML_String(rng.Offset(-1).Resize(lResize), "ASSETS")


    B64Result = B64Result & vbNewLine & "</DO>"

    Get_Data_To_XML_DealDetails = B64Result

End Function
Public Function Get_Table_XML_DMS_Description(Text As String) As Variant
    Dim xDoc As MSXML2.DOMDocument
    Dim xNode As MSXML2.IXMLDOMNode
    Dim xNodeChild As MSXML2.IXMLDOMNode
    Dim xNodeD As MSXML2.IXMLDOMNode
    Dim xNodeList As MSXML2.IXMLDOMNodeList
    Dim aData() As Variant
    Dim aLayout() As Variant
    Dim XXPR As XMLXPathReader
    Dim CountImportXPath As Long
    Dim i As Long
    Dim j As Long
    Dim jNodes As Long
    Dim aXMLNodes() As Variant
    Dim ElementN As String
    Dim ElementD As String
    Dim wksLayout As Worksheet


    XXPR = New_Type_XMLXPathReader("Model_Layout_XML_DMS_Deal_Description")
    Set wksLayout = Get_Worksheet_By_Codename("wksLayout_XMLDMSDealDescription")
    aLayout = Get_Current_Region(wksLayout.Range("A1"))

    CountImportXPath = 0
    For i = LBound(aLayout) To UBound(aLayout)
        If CStr(aLayout(i, XXPR.Column_Import) = "1") Then
            CountImportXPath = CountImportXPath + 1
        End If
    Next i
    Set xDoc = New MSXML2.DOMDocument
    xDoc.LoadXML (Text)

    ReDim aXMLNodes(1 To 4, 1 To CountImportXPath)
    jNodes = 1
    For i = LBound(aLayout) + 1 To UBound(aLayout)
        If CStr(aLayout(i, XXPR.Column_Import)) = "1" Then
            aXMLNodes(XML_ROW_XPATH, jNodes) = Replace(aLayout(i, XXPR.Column_XPath), "\\", "\")
            aXMLNodes(XML_ROW_XPATH, jNodes) = Replace(aXMLNodes(XML_ROW_XPATH, jNodes), "\", "/")
            aXMLNodes(XML_ROW_TYPENODE, jNodes) = aLayout(i, XXPR.Column_XPathType)
            aXMLNodes(XML_ROW_CAPTION, jNodes) = aLayout(i, XXPR.Column_Tag)
            aXMLNodes(XML_ROW_ATTRIB, jNodes) = CStr(aLayout(i, XXPR.Column_Attribute))

            If aXMLNodes(XML_ROW_TYPENODE, jNodes) = "N" Then
                ElementN = aXMLNodes(XML_ROW_XPATH, jNodes)
                Set xNodeList = xDoc.SelectNodes(ElementN)
                ReDim aData(1 To xNodeList.Length + 1, 1 To UBound(aLayout) - 1)



            End If
            jNodes = jNodes + 1

        End If
    Next i
    j = 1
    For i = LBound(aLayout) + 1 To UBound(aLayout)
        If CStr(aLayout(i, XXPR.Column_Import)) = "1" Then
            aData(1, j) = aLayout(i, XXPR.Column_Tag)
            j = j + 1
        End If
    Next i

    i = 2

    For Each xNode In xNodeList

        For j = LBound(aXMLNodes, 2) To UBound(aXMLNodes, 2)
            
            Set xNodeChild = MXML.Get_Single_Node(xNode, aXMLNodes(XML_ROW_XPATH, j))
            If Regx_Match(aXMLNodes(XML_ROW_XPATH, j), "(" & ElementN & ")/?(.*)") Then
                ElementD = regExMatches(0).SubMatches(1)
                If ElementD = vbNullString Then
                    aData(i, j) = xNode.Attributes(0).nodeTypedValue
                Else
                    Set xNodeD = xNode.SelectSingleNode(ElementD)
                    If Not xNodeD Is Nothing Then
                        If aXMLNodes(XML_ROW_ATTRIB, j) = "1" Then
                            aData(i, j) = xNodeD.Attributes(0).nodeTypedValue
                        Else
                            aData(i, j) = xNodeD.nodeTypedValue

                        End If
                    End If
                End If
            End If 'If Regx_match(aXMLNodes(1, j), "(" & sElementN & ")/(.+)") Then


        Next j
        i = i + 1
        xNodeList.NextNode

    Next xNode
    
    Get_Table_XML_DMS_Description = aData
    
End Function
Sub Import_Deal_Details_Data_From_String(DataString As String, Model As String, wksData As Worksheet)
    Const SUB_NAME As String = "Import Data"

    Dim Pattern As String
    Dim aDataHeader() As Variant
    Dim aData() As Variant
    Dim RawX As Variant
    Dim RawData As String
    Dim i As Long
    Dim NamedRange As String
    Dim Dif As Long

    Dim xDoc As MSXML2.DOMDocument
    Dim xNode As MSXML2.IXMLDOMNode
    Dim xNodeChild As MSXML2.IXMLDOMNode
    Dim xNodeList As MSXML2.IXMLDOMNodeList
    Dim xNodeParameter As MSXML2.IXMLDOMNode
    Dim xNodeParameterText As MSXML2.IXMLDOMNode
    Dim xNodeParameterValue As MSXML2.IXMLDOMNode
    Dim xNodeParameterImport As MSXML2.IXMLDOMNode
    Dim ColumnData As Long
    Dim ValueNode As Variant
    Dim NumberFormat As String
    Dim Basename As String
    
    
    Dim rngData As Range
    Dim rngDataFooter As Range

    Dim Parameter As String
    Dim ParameterText As String
    Dim ParameterValue As String
    Dim j As Long
    Dim LastRow As Long
    Dim wksDataOriginal As Worksheet

    DataString = Replace(DataString, Chr(13), " ")
    DataString = Replace(DataString, Chr(10), " ")

    Pattern = Replace(PATTERN_XML, "TAG", UCase(Model))
    RawData = ""
    If Regx_Match(DataString, Pattern) Then
        RawData = regExMatches(0).SubMatches(0)
        Set xDoc = New MSXML2.DOMDocument
        RawData = Replace(RawData, "> <", "><")
        xDoc.LoadXML (RawData)
        Set xNodeList = xDoc.SelectNodes("//DATA")
        'aData = RawData_To_Array(RawData)
    End If

    Select Case Model
        Case MODEL_DATA_DELIVERYADDRESSES, MODEL_DATA_SUPPLIERS, MODEL_DATA_ASSETS
            aDataHeader = Get_Model_Range(Model & TAG_RANGE & TAG_HEADER)
            Set rngData = Get_Model_Range(Model & TAG_RANGE & TAG_HEADER).Resize(1, 1)
            Set rngDataFooter = Get_Model_Range(Model & TAG_RANGE & TAG_FOOTER).Resize(1, 1)
            
            For j = LBound(aDataHeader, 2) To UBound(aDataHeader, 2)
                aDataHeader(1, j) = MRegex.Regx_Replace(aDataHeader(1, j), "\W", "")
            Next j
    End Select

    If Not xNodeList Is Nothing Then
        For Each xNode In xNodeList

            If Model = MODEL_DATA_DEALDETAILS Then
                
                Set xNodeParameter = MXML.Get_Single_Node(xNode, "Parameter")
                Set xNodeParameterImport = MXML.Get_Single_Node(xNode, "Import")
                Set xNodeParameterValue = MXML.Get_Single_Node(xNode, "Value")
                Set xNodeParameterText = MXML.Get_Single_Node(xNode, "Text")
                
                If UCase(xNodeParameterImport.nodeTypedValue) = "SIM" Or UCase(xNodeParameterImport.nodeTypedValue) = "SIM" = "YES" Then

                    Parameter = xNodeParameter.nodeTypedValue
                    ParameterText = xNodeParameterText.nodeTypedValue
                    ParameterValue = xNodeParameterValue.nodeTypedValue
                    
                    If MRegex.Regx_Match(ParameterText, "\%|Value") Then
                        Get_Model_Range(Parameter).Value = MStructure.Convert_Number(ParameterValue, Check_Number_Format)
                    Else
                        Get_Model_Range(Parameter).Value = ParameterValue
                    End If
                    
                    
                End If
            Else
                ReDim aData(LBound(aDataHeader) To UBound(aDataHeader), LBound(aDataHeader, 2) To UBound(aDataHeader, 2))

                For Each xNodeChild In xNode.ChildNodes
                    'Debug.Print xNodeChild.Basename
                    'Debug.Print xNodeChild.nodeTypedValue
                    
                    Basename = xNodeChild.Basename
                    
                    If UCase(Basename) = UCase("CustomerNameBranch") Then
                        Basename = "CustomerName"
                    End If
                    
                    ColumnData = Get_Column_Number(aDataHeader, Basename)
                    
                    If UCase(xNodeChild.Basename) = "UNITYVALUE" Then
                        NumberFormat = Check_Number_Format
                        ValueNode = xNodeChild.nodeTypedValue
                        ValueNode = CDbl(Convert_Number(ValueNode, NumberFormat))
                        
                    Else
                        ValueNode = xNodeChild.nodeTypedValue
                        
                    End If
                    
                    aData(1, ColumnData) = ValueNode

                Next xNodeChild

                LastRow = Get_Last_Row(rngData.Worksheet, 2)
                If LastRow >= rngDataFooter.Row - 2 Then
                    Call Add_Data(wksData, Model, False)
                End If
                LastRow = Get_Last_Row(rngData.Worksheet, 2)
                rngData.Offset(LastRow - 4).Resize(, UBound(aData, 2)) = aData

            End If

        Next xNode
    End If

    Select Case Model
        
        Case MODEL_DATA_ASSETS
            Call Formula_To_Range("ModelDataAssets_Row_Assets_Range_Total", "Assets_Range_Total")
            Call Formula_To_Range("ModelDataAssets_Row_Assets_Range_Supplier_TaxID", "Assets_Range_Supplier_TaxID")
            Call Formula_To_Range("ModelDataAssets_Row_Assets_Range_Delivery_Address_TaxID", "Assets_Range_Delivery_Address_TaxID")

        Case MODEL_DATA_DEALDETAILS
            Set wksDataOriginal = Get_Worksheet_By_Codename("wksData_DealDetails")
            Call Modeling_Data(MODEL_DATA_DEALDETAILS, wksData, "Deal Details and Asset Forecast", True)

    End Select


End Sub
Sub Import_Deal_Details_Data_From_String_B64(DataStringB64 As String)
    Const SUB_NAME As String = "Import Data B64"

    Dim Decoded As String
    Dim wks As Worksheet
    Dim ProjectName As String

    ProjectName = MSecurity.Get_Project_Name

    Select Case UCase(ProjectName)
        Case UCase(PROJECT_DMS_DEALS_FOLLOW_UP)
            Call MSetup.Clean_Workbook("ImportDealDetail")
        Case UCase(PROJECT_DEAL_DETAILS)
            Call MSetup.Clean_Workbook
    End Select
    
    'Decoded = DecodeBase64(DataStringB64)
    Decoded = Decrypt(DataStringB64)
    
    Set wks = Get_Worksheet_By_Codename("wksData_DealDetails")
    Call Import_Deal_Details_Data_From_String(Decoded, MODEL_DATA_DEALDETAILS, wks)

    Set wks = Get_Worksheet_By_Codename("wksData_DeliveryAddresses")
    Call Import_Deal_Details_Data_From_String(Decoded, MODEL_DATA_DELIVERYADDRESSES, wks)

    Set wks = Get_Worksheet_By_Codename("wksData_Suppliers")
    Call Import_Deal_Details_Data_From_String(Decoded, MODEL_DATA_SUPPLIERS, wks)

    Set wks = Get_Worksheet_By_Codename("wksData_Assets")
    Call Import_Deal_Details_Data_From_String(Decoded, MODEL_DATA_ASSETS, wks)
End Sub
Public Sub Import_Data_From_Encrypted_String(Optional b64 As String = vbNullString)
    Const SUB_NAME As String = "Import Data From Encrypted String"

    Dim b64String As String

    b64String = Get_EncriptedString_Data(b64)

    If b64String <> vbNullString Then
        Call Import_Deal_Details_Data_From_String_B64(b64String)
    Else
        Call err.Raise(999, , "No valid data!")
    End If


End Sub
Sub Map_Model_DMS_Data(TextCollection As Collection, TextCollectionNames As Collection)
    Const SUB_NAME As String = "Map Model DMS Data"
    Dim FileNo As Long
    Dim TotalFiles As Long
    Dim Item As Variant
    Dim EncriptedString As String
    Dim DecriptedString As String
    
    TotalFiles = TextCollection.Count

    For FileNo = 1 To TotalFiles
        On Error GoTo nextFileNo
        Item = TextCollection.Item(FileNo)
        EncriptedString = Get_EncriptedString_Data(Item)
        DecriptedString = MCryptoAES.Decrypt(EncriptedString)
        
        'Item & DecriptedString
        Call Extract_DMS_Data(Item, DecriptedString)
nextFileNo:
        If err.Number > 0 Then
            Call MInterface.Message_Error(MSG_FAIL & vbCrLf & _
                                          err.Number & vbCrLf & _
                                          err.Description & vbCrLf & _
                                          "File: " & TextCollectionNames.Item(FileNo), SUB_NAME)
        End If
    Next FileNo
    
End Sub
Sub NewDeal_ChangeDetails(Deal As String, OldLeaseNumber As String)
    Const SUB_NAME As String = "NewDeal Change Details"
NewDealInfo:
    Dim rng As Range
    
    'Sales Person
    Set rng = Get_Model_Range("DealDetails_Overview_Salesperson")
    rng.Value = Get_Office_UserName
    
    'DMS Deal
    Set rng = Get_Model_Range("DealDetails_Overview_Deal")
    rng.Value = Deal
    
    'Old Deal
    Set rng = Get_Model_Range("DealDetails_Overview_OldLeaseNumber")
    rng.Value = OldLeaseNumber
End Sub
Sub NewDeal_Create(Optional CanSave As Boolean = True)
    Const SUB_NAME As String = "NewDeal Create"
    
    Dim Deal As String
    Dim OldLeaseNumber As String
    
    Dim FolderDealDetails As String
    Dim NewDealFilePath As String
    Dim NewDealFilePathFinal As String
    
    
    
    If CanSave Then
        Call Check_Cloud_Enviroment
        
        FolderDealDetails = Get_CHG_Cloud_Desktop_Path & "\" & FOLDER_DEALDETAILS
        Call MStructure.Create_Folder(FolderDealDetails)
        
Deal:
        Deal = Trim(InputBox("Insert the DMS Deal No", "DMS Deal No"))
        If Deal = vbNullString Then
            Call err.Raise(999, , "Deal is blank!")
        ElseIf Not MRegex.Regx_Match(Deal, "^\d+$") Then
            Call err.Raise(999, , "Deal No invalid!" & "Ex. 012345")
        End If
        
LeaseNo:
        OldLeaseNumber = Trim(InputBox("Insert the Old Lease No", "DMS Old Lease No"))
        If OldLeaseNumber = vbNullString Then
            Call err.Raise(999, , "Lease Number is blank!")
    
        ElseIf Not MRegex.Regx_Match(OldLeaseNumber, "^\d+\-\d+\-\d+$") Then
            Call err.Raise(999, , "Lease No invalid!" & "Ex. 0000-111-222")
        End If
        
EditFileName:
        NewDealFilePath = "Deal Details and Asset Forecast - Deal " & Deal & " - " & OldLeaseNumber
        NewDealFilePath = Trim(InputBox("Do you wish to edit the file name?", "Rename the file", NewDealFilePath))
                
        NewDealFilePathFinal = FolderDealDetails & "\" & NewDealFilePath & ".xlsm"
        
        Call ThisWorkbook.SaveAs(NewDealFilePathFinal, xlOpenXMLWorkbookMacroEnabled)
        
        Call MInterface.Message_Information("New deal saved to:" & vbNewLine & NewDealFilePathFinal, SUB_NAME)
    End If 'If CanSave Then
Process:
    'Process
    If MInterface.Message_Confirm("Clean the current data?") = vbYes Then
        Call MSetup.Setup_Workbook
        Call MSetup.Clean_Workbook
    End If

    Call MDealDetails.NewDeal_ChangeDetails(Deal, OldLeaseNumber)
    ThisWorkbook.Save
    Call MInterface.Activate_Main_Menu

End Sub
Public Sub Refresh_All_Data()
    Const SUB_NAME As String = "Refresh All Data"

    Dim Model As String
    Dim wksData As Worksheet

    Model = MODEL_DATA_ASSETS
    Set wksData = Get_Worksheet_By_Codename("wksData_Assets")
    Call MStructure.Refresh_NamedRanges_Between_NamedRanges(wksData, "B", 4, 6, wksData.Range(Model & TAG_RANGE & TAG_HEADER), wksData.Range(Model & TAG_RANGE & TAG_FOOTER))
    Call MStructure.Formula_To_Range("ModelDataAssets_Row_Assets_Range_Total", "Assets_Range_Total")

    Model = MODEL_DATA_DELIVERYADDRESSES
    Set wksData = Get_Worksheet_By_Codename("wksData_DeliveryAddresses")
    Call MStructure.Refresh_NamedRanges_Between_NamedRanges(wksData, "B", 4, 6, wksData.Range(Model & TAG_RANGE & TAG_HEADER), wksData.Range(Model & TAG_RANGE & TAG_FOOTER))

    Model = MODEL_DATA_SUPPLIERS
    Set wksData = Get_Worksheet_By_Codename("wksData_Suppliers")
    Call MStructure.Refresh_NamedRanges_Between_NamedRanges(wksData, "B", 4, 6, wksData.Range(Model & TAG_RANGE & TAG_HEADER), wksData.Range(Model & TAG_RANGE & TAG_FOOTER))

    ThisWorkbook.RefreshAll

End Sub

Sub Export_Deal_Details_to_XLSM()
    Const SUB_NAME As String = "Export Deal Details to XLSM"
    Dim ExportedFile As String
    Dim guid As String
    Dim tempWB As Workbook
    Dim tempWKS As Worksheet
    Dim OldLeaseNo As String
    Dim SalesPerson As String
    
    Const NAME_REPORT As String = "DMS Deal Details - @OldLeaseNo - @SalesPerson - @Guid.xlsm"
    Application.DisplayAlerts = False
    
    OldLeaseNo = MStructure.Get_Model_Range("DealDetails_Overview_OldLeaseNumber").Text
    SalesPerson = Replace(MStructure.Get_Model_Range("DealDetails_Overview_Salesperson").Text, ",", "")
    guid = Format(Now, "yyyymmddhhmmss")
    ExportedFile = ThisWorkbook.Path & "\" & NAME_REPORT
    ExportedFile = Replace(ExportedFile, "@OldLeaseNo", OldLeaseNo)
    ExportedFile = Replace(ExportedFile, "@SalesPerson", SalesPerson)
    ExportedFile = Replace(ExportedFile, "@Guid", guid)
    ExportedFile = Replace(ExportedFile, "  ", " ")
    
    Set tempWB = Application.Workbooks.Add(1)
    Set tempWKS = tempWB.Sheets(1)
    
    ThisWorkbook.Sheets(Array("Deal Details", "Suppliers", "Delivery Addresses", "Assets", "Lists")).Copy Before:=tempWB.Sheets(1)
    Application.CutCopyMode = False
    tempWKS.Delete
    
    For Each tempWKS In tempWB.Worksheets
        Call MSecurity.Unprotect_Worksheet(tempWKS)
        Call Clear_Shapes_Worksheet(tempWKS, True)
        tempWKS.Cells.Copy
        tempWKS.Cells.PasteSpecial xlPasteValues
    Next tempWKS
    
    tempWB.SaveAs fileName:=ExportedFile, FileFormat:=xlOpenXMLWorkbookMacroEnabled, CreateBackup:=False
    tempWB.Close (False)
    Call MsgBox("Exported to:" & vbNewLine & ExportedFile, vbInformation)

End Sub


