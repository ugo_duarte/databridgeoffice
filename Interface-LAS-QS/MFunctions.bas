Attribute VB_Name = "MFunctions"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MFunctions"

Function Captalize(X As String)
    Const SUB_NAME As String = "Captalize"
    Dim Temp$, c$, OldC$, i As Integer

    If IsNull(X) Then
        Exit Function
    Else
              Let Temp$ = CStr(LCase(X))

              '  Inicialize OldC$ com um espa�o simples porque a primeira
              '  letra precisa ser aumentada, mas n�o as precedidas por letra.

              Let OldC$ = " "

              For i = 1 To Len(Temp$)
                     Let c$ = Mid$(Temp$, i, 1)

                     If c$ >= "a" And c$ <= "z" And (OldC$ < "a" Or OldC$ > "z") Then
                Mid$(Temp$, i, 1) = UCase$(c$)
            End If

                     Let OldC$ = c$
              Next i

              Let Captalize = Temp$
       End If
End Function
Function Column_Number_To_Letter(ByVal lngCol As Integer) As String
    Dim arrayLetter As Variant
    arrayLetter = Split(Cells(1, lngCol).Address(True, False), "$")
    Column_Number_To_Letter = arrayLetter(0)
    Erase arrayLetter
End Function
Function Convert_Data_to_String_With_Quotes(ByVal LctoStr As String)
        Dim sData As Date
        LctoStr = Trim(LctoStr)
        If LctoStr = "NULL" Or LctoStr = "" Then
            'Convert_Data_to_String_With_Quotes = "NULL"
            Convert_Data_to_String_With_Quotes = "''"
            Exit Function
        End If

        If IsDate(LctoStr) Then
            sData = LctoStr
            LctoStr = Format(sData, "yyyy-MM-dd")
            Convert_Data_to_String_With_Quotes = "'" & LctoStr & "'"
        Else
            'Convert_Data_to_String_With_Quotes = "NULL"
            Convert_Data_to_String_With_Quotes = "''"
        End If

End Function
Function Convert_String_Format_and_Lenght(ByVal LctoStr As String, Optional ByVal LctoLen As Integer = -99, Optional ByVal LctoScale As Integer = -99)
    LctoStr = Regx_Replace(Trim(LctoStr), "'", "�")
    Dim i As Integer
    Dim sFormat As String
    If LctoStr = "NULL" Or LctoStr = "" Then
        'Convert_String_Format_and_Lenght = "NULL"
        Convert_String_Format_and_Lenght = "''"
        Exit Function
    End If

    If Not LctoLen = -99 Then
        Convert_String_Format_and_Lenght = "'" & Left(LctoStr, LctoLen) & "'"
    Else
        Convert_String_Format_and_Lenght = "'" & LctoStr & "'"
    End If

    If Not LctoScale = -99 Then
        sFormat = "0"
        For i = 1 To LctoScale
            If i = 1 Then
                sFormat = sFormat & ".0"
            Else
                sFormat = sFormat & "0"
            End If

        Next

    Else

    End If

End Function
Function Convert_String_to_Decimal(ByVal LctoStr As String, Optional ByVal LctoLen As Integer = -99, Optional ByVal LctoScale As Integer = -99)
    LctoStr = Trim(LctoStr)
    If LctoStr = "NULL" Or LctoStr = "" Then
        'Convert_String_to_Decimal = "NULL"
        Convert_String_to_Decimal = "''"
        Exit Function
    End If

    If Not LctoLen = -99 And IsNumeric(LctoStr) Then
        Convert_String_to_Decimal = Left(LctoStr, LctoLen)
    ElseIf IsNumeric(LctoStr) Then
        Convert_String_to_Decimal = CDec(LctoStr)
    Else
        'Convert_String_to_Decimal = "NULL"
        Convert_String_to_Decimal = "''"
    End If



End Function
Function Get_TimeSerial()
    Get_TimeSerial = Format(Now, "yyyymmddhhmss")
End Function
Function Last_Day_Of_Month(ByVal dtmDate As Date, modo As Boolean) As Date
    ' Return the last day in the specified month.
    If dtmDate = 0 Then
        ' Did the caller pass in a date? If not, use
        ' the current date.
        dtmDate = Date
    End If
    If modo Then
        Last_Day_Of_Month = DateSerial(Year(dtmDate), Month(dtmDate), 0)
    Else
        Last_Day_Of_Month = DateSerial(Year(dtmDate), Month(dtmDate) + 1, 0)
    End If
End Function
Function MaiorData(pRange)
    
    Dim i As Long, dMaior As Date, pTemp As Variant
    
    For i = LBound(pRange) To UBound(pRange)
    
        If Regx_Match(pRange(i, 7), "^\d+\/\d+\/\d+$") Then
        
            If dMaior = "00:00:00" Or CDate(pRange(i, 7)) > dMaior Then
                dMaior = CDate(pRange(i, 7))
            End If
        
        End If
    
    Next i
    
    MaiorData = dMaior
End Function
Function MenorData(pRange)
    
    Dim i As Long, dMenor As Date, pTemp As Variant
    
    For i = LBound(pRange) To UBound(pRange)
    
        If Regx_Match(pRange(i, 7), "^\d+\/\d+\/\d+$") Then
        
            If dMenor = "00:00:00" Or CDate(pRange(i, 7)) < dMenor Then
                dMenor = CDate(pRange(i, 7))
            End If
        
        End If
    
    Next i
    
    MenorData = dMenor
End Function
Function Min(a, b)
    Const SUB_NAME As String = "Min"
    Min = a
    If b < a Then Min = b
End Function
Function PadLeft(Text As Variant, totalLength As Integer, padCharacter As String) As String
    Const SUB_NAME As String = "PadLeft"
    PadLeft = String(totalLength - Len(CStr(Text)), padCharacter) & CStr(Text)
End Function
Function PadRight(Text As Variant, totalLength As Integer, padCharacter As String) As String
    Const SUB_NAME As String = "PadRight"
    PadRight = CStr(Text) & String(totalLength - Len(CStr(Text)), padCharacter)
End Function
Function Remove_Special_Chars(Text As String) As String
    Remove_Special_Chars = Trim(Regx_Replace(Text, "\W", ""))
End Function
Function SplitSplit(ByRef Delimited As String) As Variant
    Dim Rows() As String
    Dim AryOfArys As Variant
    Dim i As Long

    Rows = Split(Delimited, vbNewLine)
    ReDim AryOfArys(UBound(Rows))
    For i = 0 To UBound(Rows)
        AryOfArys(i) = Split(Rows(i), "[~]")
    Next
    SplitSplit = AryOfArys
End Function
Function String_Remove_Accentuation(ByVal sTexto As String)
    Dim a As String * 1
    Dim b As String * 1
    Dim i As Integer
    Const AccChars = "������������������������������������������������������������"
    Const RegChars = "SZszYAAAAAACEEEEIIIIDNOOOOOUUUUYaaaaaaceeeeiiiidnooooouuuuyy"

    For i = 1 To Len(AccChars)
        a = Mid(AccChars, i, 1)
        b = Mid(RegChars, i, 1)
        sTexto = Replace(sTexto, a, b)
    Next

    String_Remove_Accentuation = sTexto

End Function
Function Worksheet_Order(WS As Worksheet, ByVal ColumnsRangesKeys As String)
    'ByVal ColumnsRanges As String
    Dim CounterX As Long
    Dim sRangeKeyString As String
    Dim uCWS As Long
    Dim sLastColumnLetter As String
    Dim CounterRangesKeys As Long
    
    uCWS = Get_Last_Column(WS, 1)
    sLastColumnLetter = Column_Number_To_Letter(uCWS)
    CounterRangesKeys = 0
    
    With WS
        .Sort.SortFields.Clear
        
        For CounterX = 1 To Len(ColumnsRangesKeys)
            sRangeKeyString = Mid(ColumnsRangesKeys, CounterX, 1)
            If sRangeKeyString = "\" Then
                CounterRangesKeys = CounterRangesKeys + 1
            End If
        Next CounterX
        
        For CounterX = 1 To CounterRangesKeys
                If CounterX = CounterRangesKeys Then
                    sRangeKeyString = Mid(ColumnsRangesKeys, 2)
                Else
                    sRangeKeyString = Mid(ColumnsRangesKeys, 2, InStr(2, ColumnsRangesKeys, "\") - 2)
                End If
                If InStr(2, ColumnsRangesKeys, "\") > 0 Then
                    ColumnsRangesKeys = Mid(ColumnsRangesKeys, InStr(2, ColumnsRangesKeys, "\"))
                End If
                
                .Sort.SortFields.Add Key:=Range(sRangeKeyString & ":" & sRangeKeyString), SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
                
        Next CounterX
        
        
        With .Sort
            .SetRange Range("A:" & sLastColumnLetter)
            .Header = xlYes
            .MatchCase = False
            .Orientation = xlTopToBottom
            .SortMethod = xlPinYin
            .Apply
        End With
        
    End With

End Function
