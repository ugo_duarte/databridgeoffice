Attribute VB_Name = "MInterface"
Option Explicit
Option Private Module
Const MODULE_NAME                   As String = "MInterface"
'----------------------------------------------------------
Sub Activate_Main_Menu()
    Const SUB_NAME As String = "Activate Main Manu"
    Call Log_Step(SUB_NAME)

    Call Activate_Worksheet_By_Codename("wksMenu")
End Sub
Sub Activate_Worksheet_By_Codename(Codename As String)
    Const SUB_NAME As String = "Activate Worksheet By Codename"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    
    Set wks = Get_Worksheet_By_Codename(Codename)
    wks.Activate

End Sub
Function Message_Confirm(Message As String, Optional Header As String = "Message: Confirm") As VbMsgBoxResult
    Message_Confirm = MsgBox(Message, vbYesNo + vbExclamation, Header)
End Function
Function Message_Error(Message As String, Optional Header As String = "Message: Error") As VbMsgBoxResult
    Message_Error = MsgBox(Message, vbCritical, Header)
End Function
Function Message_Information(Message As String, Optional Header As String = "Message: Info") As VbMsgBoxResult
    Message_Information = MsgBox(Message, vbInformation, Header)
End Function
Function Message_Confirm_With_Cancel(Message As String, Optional Header As String = "Message: Confirm") As VbMsgBoxResult
    Message_Confirm_With_Cancel = MsgBox(Message, vbYesNoCancel, Header)
End Function
Sub Log_Step(SubName As String)
    Debug.Print Now & " - " & SubName
End Sub
