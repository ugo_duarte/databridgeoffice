Attribute VB_Name = "MCryptoAES"
' AES-256-CBC with HMAC-SHA-256 in VBScript
Option Private Module
Const MODULE_NAME                   As String = "MCryptoAES"

Dim utf8            As Object
Dim b64Enc          As Object
Dim b64Dec          As Object
Dim Mac             As Object
Dim AES             As Object
Dim mem             As Object

Global Const AES_KEY = "CKkPfmeHzhuGf2WYY2CIo5C6aGCyM5JR8gTaaI0IRJg="
Global Const MAC_KEY = "wDF4W9XQ6wy2DmI/7+ONF+mwCEr9tVgWGLGHUYnguh4="

Function B64Decode(b64Str)
    Const SUB_NAME As String = "B64Decode"
    bytes = utf8.GetBytes_4(b64Str)
    B64Decode = b64Dec.TransformFinalBlock((bytes), 0, LenB(bytes))
End Function
Function B64Encode(bytes)
    Const SUB_NAME As String = "B64Encode"
    
    BlockSize = b64Enc.InputBlockSize
    For Offset = 0 To LenB(bytes) - 1 Step BlockSize
        Length = Min(BlockSize, LenB(bytes) - Offset)
        b64Block = b64Enc.TransformFinalBlock((bytes), Offset, Length)
        result = result & utf8.GetString((b64Block))
    Next
    B64Encode = result
End Function
Function Break_Encrypted_Data(ByVal DataEncrypt As String) As Variant
    Const SUB_NAME As String = "Break Encrypted Data"
    
    Dim aData() As Variant
    Const sizeData = 5000
    Dim lenText         As Long
    Dim sizeArray       As Long
    Dim i               As Long
    Dim currentData     As String
    currentData = DataEncrypt
    
    lenText = Len(DataEncrypt)
    
    sizeArray = (lenText / sizeData) + 1
    
    
    ReDim aData(1 To sizeArray + 1, 1 To 1)
    
    For i = 1 To sizeArray
        aData(i, 1) = Left(currentData, sizeData)
        currentData = Mid(currentData, sizeData + 1)
        
    Next i
    aData(UBound(aData), 1) = ENCRYPT_SEPARATOR
    
    'With wksEncrypt
    '    .Cells.Clear
    '    .Cells.NumberFormat = "@"
    '    .Cells(1, 1).Resize(UBound(aData)) = aData
    'End With
    Break_Encrypted_Data = aData
End Function
Function ComputeMAC(msgBytes, keyBytes)
    Const SUB_NAME As String = "ComputeMAC"
    Mac.key = keyBytes
    ComputeMAC = Mac.ComputeHash_2((msgBytes))
End Function
Function ConcatBytes(a, b)
    Const SUB_NAME As String = "ConcatBytes"
    mem.SetLength (0)
    mem.Write (a), 0, LenB(a)
    mem.Write (b), 0, LenB(b)
    ConcatBytes = mem.ToArray()
End Function
Private Function ConvToBase64String(vIn As Variant) As Variant
    Const SUB_NAME As String = "ConvToBase64String"
    
    Dim oD           As Object
      
    Set oD = CreateObject("MSXML2.DOMDocument")
      With oD
        .LoadXML "<root />"
        .DocumentElement.DataType = "bin.base64"
        .DocumentElement.nodeTypedValue = vIn
      End With
    ConvToBase64String = Replace(oD.DocumentElement.Text, vbLf, "")
    
    Set oD = Nothing

End Function
Private Function ConvToHexString(vIn As Variant) As Variant
    Const SUB_NAME As String = "ConvToHexString"
    
    Dim oD As Object
      
    Set oD = CreateObject("MSXML2.DOMDocument")
    With oD
      .LoadXML "<root />"
      .DocumentElement.DataType = "bin.Hex"
      .DocumentElement.nodeTypedValue = vIn
    End With
    ConvToHexString = Replace(oD.DocumentElement.Text, vbLf, "")
    
    Set oD = Nothing

End Function
Function CryptoInfo()
    Const SUB_NAME As String = "CryptoInfo"
    
    Set enc = AES.CreateEncryptor_2(AES.key, AES.IV)
    Set dec = AES.CreateDecryptor_2(AES.key, AES.IV)

    CryptoInfo = "aes.BlockSize: " & AES.BlockSize & vbCrLf & _
                 "aes.FeedbackSize: " & AES.FeedbackSize & vbCrLf & _
                 "aes.KeySize: " & AES.KeySize & vbCrLf & _
                 "aes.Mode: " & AES.Mode & vbCrLf & _
                 "aes.Padding: " & AES.Padding & vbCrLf & _
                 "mac.HashName: " & Mac.HashName & vbCrLf & _
                 "mac.HashSize: " & Mac.HashSize & vbCrLf & _
                 "aesEnc.InputBlockSize: " & enc.InputBlockSize & vbCrLf & _
                 "aesEnc.OutputBlockSize: " & enc.OutputBlockSize & vbCrLf & _
                 "aesDec.InputBlockSize: " & enc.InputBlockSize & vbCrLf & _
                 "aesDec.OutputBlockSize: " & enc.OutputBlockSize & vbCrLf & _
                 "b64Enc.InputBlockSize: " & b64Enc.InputBlockSize & vbCrLf & _
                 "b64Enc.OutputBlockSize: " & b64Enc.OutputBlockSize & vbCrLf & _
                 "b64Dec.InputBlockSize: " & b64Dec.InputBlockSize & vbCrLf & _
                 "b64Dec.OutputBlockSize: " & b64Dec.OutputBlockSize & vbCrLf
End Function
Function DecodeBase64(b64$)
    Const SUB_NAME As String = "DecodeBase64"
    Dim b
    wksTemporary.Cells(2, 1) = b64
    With CreateObject("Microsoft.XMLDOM").createElement("b64")
        .DataType = "bin.base64": .Text = b64
        b = .nodeTypedValue
        With CreateObject("ADODB.Stream")
            .Open: .Type = 1: .Write b: .Position = 0: .Type = 2: .Charset = "utf-8"
            DecodeBase64 = .ReadText
            .Close
        End With
    End With
End Function
Function Decrypt(macIVCiphertext, Optional AESKey As String = AES_KEY, Optional MACKey As String = MAC_KEY)
    Const SUB_NAME As String = "Decrypt"
    
    Call New_Crypto
    
    aesKeyBytes = B64Decode(AESKey)
    macKeyBytes = B64Decode(MACKey)
    tokens = Split(macIVCiphertext, ":")
    macBytes = B64Decode(tokens(0))
    ivBytes = B64Decode(tokens(1))
    cipherBytes = B64Decode(tokens(2))
    macActual = ComputeMAC(ConcatBytes(ivBytes, cipherBytes), macKeyBytes)
    If Not EqualBytes(macBytes, macActual) Then
        err.Raise vbObjectError + 1000, "Decrypt()", "Bad MAC"
    End If
    Set aesDec = AES.CreateDecryptor_2((aesKeyBytes), (ivBytes))
    plainBytes = aesDec.TransformFinalBlock((cipherBytes), 0, LenB(cipherBytes))
    Decrypt = utf8.GetString((plainBytes))
End Function
Function EncodeBase64(Text$)
    Const SUB_NAME As String = "EncodeBase64"
    Dim b
    With CreateObject("ADODB.Stream")
        .Open: .Type = 2: .Charset = "utf-8"
        .WriteText Text: .Position = 0: .Type = 1: b = .Read
        With CreateObject("Microsoft.XMLDOM").createElement("b64")
            .DataType = "bin.base64": .nodeTypedValue = b
            EncodeBase64 = Replace(Mid(.Text, 5), vbLf, "")
        End With
        .Close
    End With
End Function
Function Encrypt(plaintext, Optional AESKey As String = AES_KEY, Optional MACKey As String = MAC_KEY)
    Const SUB_NAME As String = "Encrypt"
    
    Call New_Crypto
    Call AES.GenerateIV
    aesKeyBytes = B64Decode(AESKey)
    macKeyBytes = B64Decode(MACKey)
    Set aesEnc = AES.CreateEncryptor_2((aesKeyBytes), AES.IV)
    plainBytes = utf8.GetBytes_4(plaintext)
    cipherBytes = aesEnc.TransformFinalBlock((plainBytes), 0, LenB(plainBytes))
    macBytes = ComputeMAC(ConcatBytes(AES.IV, cipherBytes), macKeyBytes)
    Encrypt = B64Encode(macBytes) & ":" & B64Encode(AES.IV) & ":" & _
              B64Encode(cipherBytes)
End Function
Function EqualBytes(a, b)
    Const SUB_NAME As String = "EqualBytes"

    EqualBytes = False
    If LenB(a) <> LenB(b) Then Exit Function
    diff = 0
    For i = 1 To LenB(a)
        diff = diff Or (AscB(MidB(a, i, 1)) Xor AscB(MidB(b, i, 1)))
    Next
    EqualBytes = Not diff
End Function
Sub New_Crypto()
    Const SUB_NAME As String = "New Encrypt"
    
    Set utf8 = CreateObject("System.Text.UTF8Encoding")
    Set b64Enc = CreateObject("System.Security.Cryptography.ToBase64Transform")
    Set b64Dec = CreateObject("System.Security.Cryptography.FromBase64Transform")
    Set Mac = CreateObject("System.Security.Cryptography.HMACSHA256")
    Set AES = CreateObject("System.Security.Cryptography.RijndaelManaged")
    Set mem = CreateObject("System.IO.MemoryStream")

End Sub
