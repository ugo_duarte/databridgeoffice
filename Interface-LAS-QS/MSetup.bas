Attribute VB_Name = "MSetup"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MSetup"

Const POSITION_MENU_HEADER_TOP As Long = 14
Const POSITION_MENU_HEADER_LEFT As Long = 60
Const POSITION_MENU_BACKGROUND_TOP As Long = 60
Const POSITION_MENU_BACKGROUND_LEFT As Long = 60
Const POSITION_INTERVAL_I As Long = 5
Const POSITION_INTERVAL_II As Long = 10
Const POSITION_INTERVAL_III As Long = 20
Const POSITION_INTERVAL_IV As Long = 50
Const POSITION_INTERVAL_V As Long = 100
Const POSITION_MARGIN As Long = 5
Const POSITION_PADDING As Long = 5

Public Type ButtonModel
    Shp_Group     As Shape
    Shp_Image     As Shape
    Shp_Label     As Shape
    Shp_Macro     As Shape

End Type

Public Type Layout
    Column_ID               As Long
    Column_Name             As Long
    Column_NameBrazil       As Long
    Column_Description      As Long
    Column_Type             As Long
    Column_DataOrigin       As Long
    Column_DataOriginType   As Long
    Column_DataName         As Long
    Column_DefaultValue     As Long
    Column_Treatment        As Long
    Column_Obs              As Long
    Column_MaxLenght        As Long
    Column_Format           As Long
    Column_DataField        As Long
    Column_Operation        As Long
    Column_DataDestination  As Long
    Column_PrimaryKey       As Long
    
    CurrentValue            As Variant
    LastValue               As Variant

    ID                      As Variant
    Name                    As String
    NameBrazil              As String
    Description             As String
    Type                    As String
    DataOrigin              As Variant
    DataOriginType          As Variant
    DataName                As Variant
    DefaultValue            As Variant
    Treatment               As Variant
    Obs                     As String
    MaxLenght               As Variant
    Format                  As String
    DataField               As String
    Operation               As Variant

End Type

Public Type NamedRangesControl
    Column_NamedRange       As Long
    Column_Order            As Long
    Column_Locked           As Long
    Column_Address          As Long
End Type

Public Type XMLFieldsExtra
    Column_SN               As Long
    Column_FilePath         As Long
    Column_FileID           As Long
End Type

Sub Clean_Lists()
    Const SUB_NAME As String = "Clean Lists"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    
    Set wks = Get_Worksheet_By_Codename("wksList_Manufacturers")
    Call Generate_Worksheet_From_Model(wks, False, "Model_Manufacturers_Header")
    
    Set wks = Get_Worksheet_By_Codename("wksList_AssetsClass")
    Call Generate_Worksheet_From_Model(wks, False, "Model_AssetsClass_Header")
End Sub
Sub Clean_Model_Interfaces()
    Const SUB_NAME As String = "Clean Model Interfaces"
    Call Log_Step(SUB_NAME)

    Application.StatusBar = "Modeling Sheets Interface": DoEvents
    Dim wks As Worksheet
    
    
    Select Case UCase(gProjectName)
        Case UCase(PROJECT_LAS_QS_ASSETS)
            Set wks = Get_Worksheet_By_Codename("wksReport_Interface_QS_Assets")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Interface_Qualisoft_Header")
            
        Case UCase(PROJECT_XML_LAS_ASSETS)
            Set wks = Get_Worksheet_By_Codename("wksReport_Interface_XMLLAS")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Interface_LAS_Header")
            
    End Select
    
Finish:
    On Error GoTo 0
    Exit Sub
    
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish
    
End Sub
Sub Clean_Model_Supplier_Manufacturer()
    Const SUB_NAME As String = "Clean Model Supplier Manufacturer"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Application.StatusBar = "Modeling Supplier Manufacturer": DoEvents
        
    Set wks = Get_Worksheet_By_Codename("wksList_Supplier_Manufacturer")
    Call Generate_Worksheet_From_Model(wks, True, "Model_Purchase_Invoice_Header")

End Sub
Sub Clean_Model_Purchases_Invoices()
    Const SUB_NAME As String = "Clean Model Purchases Invoices"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Application.StatusBar = "Modeling Purchases Invoices": DoEvents
        
    Set wks = Get_Worksheet_By_Codename("wksList_PurchasesInvoices")
    Call Generate_Worksheet_From_Model(wks, True, "Model_Purchase_Invoice_Header")

End Sub

Sub Clean_Model_SerialNumbers()
    Const SUB_NAME As String = "Clean Model Serial Numbers"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet

    Application.StatusBar = "Modeling Sheets Serial Numbers": DoEvents
    
    Set wks = Get_Worksheet_By_Codename("wksData_SerialNumbers")
    Call Generate_Worksheet_From_Model(wks, True, "Model_Data_SerialNumbers_Header")
    
    Set wks = Get_Worksheet_By_Codename("wksReport_SerialNumbers")
    Call Generate_Worksheet_From_Model(wks, True, "Model_Report_SerialNumbers_Header")
End Sub

Sub Clean_Model_XML(Optional CanClearReports As Boolean = True)
    Const SUB_NAME As String = "Clean Model XML"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet

    Application.StatusBar = "Modeling Sheets XML": DoEvents
    
    Set wks = Get_Worksheet_By_Codename("wksData_XML")
    Call MStructure.Clean_Worksheet(wks)

    If CanClearReports Then
        Set wks = Get_Worksheet_By_Codename("wksReport_XML_NFE")
        Call Generate_Worksheet_From_Model(wks, True, "Model_NFE_Header")
        
        Set wks = Get_Worksheet_By_Codename("wksReport_XML_NFE_Desmembered")
        Call Generate_Worksheet_From_Model(wks, True, "Model_NFE_Header")
    End If
End Sub
Sub Clean_Pipeline()
    Const SUB_NAME As String = "Clean Pipeline"
    Call Log_Step(SUB_NAME)

    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksData_Database"), True, "Model_Data_Database_Header")
    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksData_Database_NF_Payable"), True, "Model_Data_Database_NF_Payable_Header")

    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Confirmed_Customers"), True, "Model_Pipeline_Confirmed_Customers")
    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Invoices_Payable"), True, "Model_Pipeline_Invoices_Payable")
    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Pipeline_Adjusted"), True, "Model_Pipeline_Adjusted")
    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Pipeline_Booked"), True, "Model_Pipeline_Booked")
    Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Pipeline_Confirmed"), True, "Model_Pipeline_Confirmed")

End Sub
Sub Clean_Workbook(Optional CleanMode As String = vbNullString)
    Const SUB_NAME As String = "Clean Workbook"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Select Case UCase(gProjectName)
        Case UCase(PROJECT_RPA_INVOICES_USA)


            'Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksData_Invoices"), True, "Model_Input_Data")

            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_ManualInvoices"), True, "Model_LAS_Manual_Invoices_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_PrintLayout"), True, "Model_LAS_Print_Layout_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_InvoiceBlocks"), True, "Model_LAS_Invoice_Blocks_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_BlockPositions"), True, "Model_LAS_Block_Positions_Header")

        Case UCase(PROJECT_XML_LAS_PUCHASES)
            Call Clean_Worksheet(Get_Worksheet_By_Codename("wksData_XML"))

            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_XML_NFE"), True, "Model_NFE_Cover_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksData_Purchases"), True, "Models_RPA_Purchases_Data_Header")

        Case UCase(PROJECT_LAS_QS_SCHEDULES)
                        
            Set wks = Get_Worksheet_By_Codename("wksData_QS_Framing")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Data_Framing")
            
            Set wks = Get_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Data_LAS")
                
            Set wks = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Data_DMS")
                
            Set wks = Get_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Data_LAS_Installation_Certificate")
                
            Set wks = Get_Worksheet_By_Codename("wksReport_QS_Schedules")
            Call Generate_Worksheet_From_Model(wks, True, "Model_Report_QS_Schedules")
                
            Set wks = Get_Worksheet_By_Codename("wksTemporary")
            Call MStructure.Clean_Worksheet(wks)
                
            Set wks = Get_Worksheet_By_Codename("wksTemporary1")
            Call MStructure.Clean_Worksheet(wks)

        Case UCase(PROJECT_DMS_DEALS_FOLLOW_UP)

            If CleanMode <> "ImportDealDetail" Then
                Set wks = Get_Worksheet_By_Codename("wksData_DMSDeals")
                Call Generate_Worksheet_From_Model(wks, True, "Model_Data_DMS_Deals")

            End If

            Call MModels.Model_DealDetails
            Call MModels.Model_DeliveryAddresses
            Call MModels.Model_Assets
            Call MModels.Model_Suppliers

        Case UCase(PROJECT_DEAL_DETAILS)

            Call MModels.Model_DealDetails
            Call MModels.Model_DeliveryAddresses
            Call MModels.Model_Assets
            Call MModels.Model_Suppliers

        Case UCase(PROJECT_LAS_QS_ASSETS)

            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksList_GenerateContracts"), True, "Model_Gerar_Interface_LAS_QS_Assets_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Interface_QS_Assets"), True, "Model_Interface_Qualisoft_Header")

        Case UCase(PROJECT_LAS_CASHFLOW)
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Interface_Beneficiary"), True, "Model_Interface_Cashflow_Beneficiaries")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Interface_Invoices"), True, "Model_Interface_Cashflow_Invoices")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Interface_Schedules"), True, "Model_Interface_Cashflow_Schedules")

        Case UCase(PROJECT_XML_LAS_ASSETS)
            'SERIAL NUMBERS
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksData_SerialNumbers"), True, "Model_Data_SerialNumbers_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_SerialNumbers"), True, "Model_Report_SerialNumbers_Header")
            'XML
            Call MStructure.Clean_Worksheet(Get_Worksheet_By_Codename("wksData_XML"))
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_XML_NFE"), True, "Model_NFE_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_XML_NFE_Desmembered"), True, "Model_NFE_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksReport_Interface_XMLLAS"), True, "Model_Interface_LAS_Header")
            Call Generate_Worksheet_From_Model(Get_Worksheet_By_Codename("wksList_PurchasesInvoices"), True, "Model_Purchase_Invoice_Header")

    End Select

End Sub
Function New_Type_Layout(aLayout() As Variant) As Layout
    Const SUB_NAME As String = "New Type Layout"
    Call Log_Step(SUB_NAME)

    Dim L As Layout

    L.Column_ID = Get_Column_Number(aLayout, "ID")
    L.Column_Name = Get_Column_Number(aLayout, "Name")
    L.Column_NameBrazil = Get_Column_Number(aLayout, "NameBrazil")
    L.Column_Description = Get_Column_Number(aLayout, "Description")
    L.Column_Type = Get_Column_Number(aLayout, "Type")
    L.Column_Format = Get_Column_Number(aLayout, "Format")
    L.Column_DataOrigin = Get_Column_Number(aLayout, "DataOrigin")
    L.Column_DataOriginType = Get_Column_Number(aLayout, "DataOriginType")
    L.Column_DataName = Get_Column_Number(aLayout, "DataName")
    L.Column_DataField = Get_Column_Number(aLayout, "DataField")
    L.Column_DefaultValue = Get_Column_Number(aLayout, "DefaultValue")
    L.Column_Treatment = Get_Column_Number(aLayout, "Treatment")
    L.Column_Obs = Get_Column_Number(aLayout, "Obs.")
    L.Column_MaxLenght = Get_Column_Number(aLayout, "MaxLenght")
    L.Column_Operation = Get_Column_Number(aLayout, "Operation")
    L.Column_DataDestination = Get_Column_Number(aLayout, "DataDestination")
    L.Column_PrimaryKey = Get_Column_Number(aLayout, "PrimaryKey")
    New_Type_Layout = L

End Function
Function New_Type_ModelsSerialNumbers() As ModelsSerialNumbers
    Const SUB_NAME As String = "New Type ModelsSerialNumbers"
    Call Log_Step(SUB_NAME)

    Dim MSN As ModelsSerialNumbers
    Dim aHeader() As Variant
    aHeader = Get_Model_Range("Model_Layout_SerialNumbers_Header")

    'SN  File    File ID

    'MSN.Column_Key = Get_Column_Number(aHeader, "Key")
    'MSN.Column_Model = Get_Column_Number(aHeader, "Model")
    MSN.Column_Description = Get_Column_Number(aHeader, "Description")
    MSN.Column_Type = Get_Column_Number(aHeader, "Type")
    MSN.Column_ModelLAS = Get_Column_Number(aHeader, "Model LAS")
    MSN.Column_Class = Get_Column_Number(aHeader, "Class")
    MSN.Column_DescriptionLAS = Get_Column_Number(aHeader, "Description LAS")
    MSN.Column_Valid = Get_Column_Number(aHeader, "Valid")

    New_Type_ModelsSerialNumbers = MSN
End Function
Function New_Type_NamedRangesControl() As NamedRangesControl
    Const SUB_NAME As String = "New Type NamedRangesControl"
    Call Log_Step(SUB_NAME)

    Dim NRC As NamedRangesControl
    Dim aHeader() As Variant
    aHeader = Get_Model_Range("NamedRangesControl_Header")
    'NamedRange Order   Locked  Address
    NRC.Column_NamedRange = Get_Column_Number(aHeader, "NamedRange")
    NRC.Column_Order = Get_Column_Number(aHeader, "Order")
    NRC.Column_Locked = Get_Column_Number(aHeader, "Locked")
    NRC.Column_Address = Get_Column_Number(aHeader, "Address")
    New_Type_NamedRangesControl = NRC
End Function
Function New_Type_XMLFieldsExtra() As XMLFieldsExtra
    Const SUB_NAME As String = "New Type XMLFieldsExtra"
    Call Log_Step(SUB_NAME)

    Dim XFE As XMLFieldsExtra
    Dim aHeader() As Variant
    aHeader = Get_Model_Range("Model_XML_Fields_Extra")

    'SN  File    File ID
    XFE.Column_SN = Get_Column_Number(aHeader, "SN")
    XFE.Column_FilePath = Get_Column_Number(aHeader, "File")
    XFE.Column_FileID = Get_Column_Number(aHeader, "File ID")

    New_Type_XMLFieldsExtra = XFE
End Function
Sub Setup_Button(shp As Shape)
    Const SUB_NAME As String = "Setup Button"
    Call Log_Step(SUB_NAME)

    Static shpModel As ButtonModel
    Dim shp2 As Shape
    Dim difTop As Double
    Dim difLeft As Double
    Dim wksModel As Worksheet
    Set wksModel = Get_Worksheet_By_Codename("wksModel_Shapes")

    If shpModel.Shp_Group Is Nothing Then
        Set shpModel.Shp_Group = wksModel.Shapes("GroupModelButton")
        Set shpModel.Shp_Image = shpModel.Shp_Group.GroupItems("imgModelButton")
        Set shpModel.Shp_Label = shpModel.Shp_Group.GroupItems("lbModelButton")
        Set shpModel.Shp_Macro = shpModel.Shp_Group.GroupItems("macroModelButton")
    End If



    For Each shp2 In shp.GroupItems

        shp2.LockAspectRatio = msoFalse
        If Regx_Match(shp2.Name, "^lb") Then

            shp2.Height = shpModel.Shp_Label.Height
            shp2.Width = shpModel.Shp_Label.Width

            difTop = (shpModel.Shp_Label.Top - shpModel.Shp_Group.Top)
            difLeft = (shpModel.Shp_Label.Left - shpModel.Shp_Group.Left)

            shp2.Top = shp.Top + difTop
            shp2.Left = shp.Top + difLeft


        ElseIf Regx_Match(shp2.Name, "^img") Then

            shp2.Height = shpModel.Shp_Image.Height
            shp2.Width = shpModel.Shp_Image.Width

            difTop = (shpModel.Shp_Image.Top - shpModel.Shp_Group.Top)
            difLeft = (shpModel.Shp_Image.Left - shpModel.Shp_Group.Left)

            shp2.Top = shp.Top + difTop
            shp2.Left = shp.Top + difLeft

        ElseIf Regx_Match(shp2.Name, "^macro") Then
            shp2.Height = shpModel.Shp_Macro.Height
            shp2.Width = shpModel.Shp_Macro.Width

            difTop = (shpModel.Shp_Macro.Top - shpModel.Shp_Group.Top)
            difLeft = (shpModel.Shp_Macro.Left - shpModel.Shp_Group.Left)

            shp2.Top = shp.Top + difTop
            shp2.Left = shp.Top + difLeft
        End If

    Next shp2


End Sub
Sub Setup_DMS_Description()
    Const SUB_NAME As String = "Setup DMS Description"
    Call Log_Step(SUB_NAME)

    Dim shp As Shape
    Dim LastTop As Long
    Dim ColumnWidth As Double
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksReportDMSDescription")
    
    Set shp = wks.Shapes("Generate_DMS_Description")
    shp.Top = POSITION_MENU_HEADER_TOP
    shp.Left = POSITION_INTERVAL_II
    LastTop = shp.Top + shp.Height + POSITION_INTERVAL_I
     
    Set shp = wks.Shapes("Copy_DMS_Description")
    shp.Top = LastTop
    shp.Left = POSITION_INTERVAL_II

    ColumnWidth = shp.Left * 2 + shp.Width + 5

End Sub

Sub Setup_Lock_All_Worksheets()
    Const SUB_NAME As String = "Setup Lock All Worksheets"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    For Each wks In ThisWorkbook.Worksheets
        If Not Regx_Match(wks.Codename, "wksModel") Then
            wks.Cells.Locked = True
        End If
    Next wks
End Sub
Sub Setup_Locks_Data()
    Const SUB_NAME As String = "Setup Locks Data"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    'Lock ALL Cells
    For Each wks In ThisWorkbook.Worksheets
        If Regx_Match(wks.Codename, "wksData") Then
            wks.Cells.Locked = False
            wks.Rows(1).Locked = True
        End If

    Next wks
End Sub
Sub Setup_Locks_NamedRanges()
    Const SUB_NAME As String = "Setup Locks NamedRanges"
    Call Log_Step(SUB_NAME)

    Dim aNamedRangesControl() As Variant
    Dim i As Long
    Dim NRC As NamedRangesControl
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksConfig_NamedRangesControls")
    Dim rngModel As Range
    NRC = New_Type_NamedRangesControl()


    'Lock ALL Cells
    Call Setup_Lock_All_Worksheets
    
    aNamedRangesControl = MStructure.Get_Current_Region(wks.Range("A1"))
    For i = LBound(aNamedRangesControl) + 1 To UBound(aNamedRangesControl)
        If UCase(aNamedRangesControl(i, NRC.Column_Locked)) = "FALSE" Then
            Set rngModel = Get_Model_Range(aNamedRangesControl(i, NRC.Column_NamedRange))
            rngModel.Locked = False
        End If
    Next i

End Sub
Sub Setup_Menu()
    Const SUB_NAME As String = "Setup Menu"
    Call Log_Step(SUB_NAME)

    Dim shp As Shape
    Dim Background_Bottom As Long
    Dim Background_Right As Long
    Dim LastRight As Long
    Dim Position_Row_1 As Long
    Dim Position_Row_2 As Long
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksMenu")
    
    wks.Cells.Columns.AutoFit

    Dim ProjectName As String

    ProjectName = MSecurity.Get_Project_Name
    
    Select Case UCase(ProjectName)
        Case UCase(PROJECT_XML_LAS_ASSETS), UCase(PROJECT_XML_LAS_PUCHASES)
            Exit Sub
    End Select
    
    LastRight = 0
    Position_Row_1 = POSITION_MENU_BACKGROUND_TOP + POSITION_INTERVAL_IV
    Position_Row_2 = Position_Row_1 + POSITION_INTERVAL_V + POSITION_MARGIN + POSITION_PADDING

    Set shp = wks.Shapes("Menu")
    Call Move_Shape(shp:=shp, Top:=POSITION_MENU_HEADER_TOP, Left:=POSITION_MENU_BACKGROUND_LEFT)
    Background_Bottom = shp.Top + shp.Height
    Background_Right = shp.Left + shp.Width
    
    Set shp = wks.Shapes("lb_WorkbookVersion")
    Call Move_Shape(shp:=shp, Top:=POSITION_MENU_BACKGROUND_TOP + POSITION_MARGIN + POSITION_PADDING, Left:=POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING)
    
    Set shp = wks.Shapes("img_CHG_Logo")
    Call Move_Shape(shp:=shp, Top:=Background_Bottom - (shp.Height + POSITION_MARGIN + POSITION_PADDING), Left:=POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING)
                
    Set shp = wks.Shapes("Navigation_Help")
    Call Move_Shape(shp:=shp, Top:=Background_Bottom - (shp.Height + POSITION_MARGIN + POSITION_PADDING), Left:=Background_Right - (shp.Width + POSITION_MARGIN + POSITION_PADDING))
      
    
    Set shp = wks.Shapes("Clean")
    Call Setup_Button(shp)
    Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
    LastRight = shp.Left + shp.Width

    'Unique shapes from projects
    Select Case UCase(ProjectName)

        Case UCase(PROJECT_DMS_DEALS_FOLLOW_UP)
            
            'Row 2
            Set shp = wks.Shapes("Navigation_Menu_Reports")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Report_DMS_Deals_Data")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Report_DMS_Deals_Emitted")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Report_DMS_Deals_Approved")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Deal_Details")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width

        Case UCase(PROJECT_XML_LAS_PUCHASES)
            Set shp = wks.Shapes("Navigation_GlobalParameters")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Import_Data_Purchases")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Generate_LAS_All_Purchases")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            'Row 2
            Set shp = wks.Shapes("Navigation_Menu_Reports")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Data_Purchases")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Positions")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width

        Case UCase(PROJECT_LAS_QS_SCHEDULES)
            Set shp = wks.Shapes("Navigation_GlobalParameters")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            
            'Row 2
            Set shp = wks.Shapes("Navigation_Menu_Reports")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width



        Case UCase(PROJECT_DEAL_DETAILS)
            Set shp = wks.Shapes("Navigation_Deal")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Suppliers")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_DeliveryAddresses")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width

            Set shp = wks.Shapes("Navigation_Assets")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_DMS_Description")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width


        Case UCase(PROJECT_LAS_QS_ASSETS)
            Set shp = wks.Shapes("Navigation_GlobalParameters")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            
            'ROW 2
            Set shp = wks.Shapes("Navigation_Menu_Reports")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width


        Case UCase(PROJECT_XML_LAS_ASSETS)
            Set shp = wks.Shapes("Navigation_GlobalParameters")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
                              
            'Row 2
            Set shp = wks.Shapes("Navigation_Menu_Reports")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width


        Case UCase(PROJECT_PIPELINE)
            Set shp = wks.Shapes("Navigation_GlobalParameters")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Import_Database")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Import_Database_NF_Payable")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Generate_Pipeline")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Navigation_Menu_Reports")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_2), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
        Case UCase(PROJECT_RH_BIRTHDAYS)
            Set shp = wks.Shapes("Navigation_GlobalParameters")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width
            
            Set shp = wks.Shapes("Generate_Birthdays_Emails")
            Call Setup_Button(shp)
            Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
            LastRight = shp.Left + shp.Width


    End Select

End Sub
Sub Setup_Menu_Reports()
    Const SUB_NAME As String = "Setup Menu Reports"
    Call Log_Step(SUB_NAME)

    Dim shp As Shape

    Dim Background_Bottom As Long
    Dim Background_Right As Long
    Dim LastRight As Long
    Dim Position_Row_1 As Long
    Dim Position_Row_2 As Long
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksMenu_Reports")
    
    
    Dim ProjectName As String

    ProjectName = MSecurity.Get_Project_Name

    Select Case UCase(ProjectName)
        Case UCase(PROJECT_XML_LAS_ASSETS), UCase(PROJECT_XML_LAS_PUCHASES)
            Exit Sub
    End Select

    If Not wks Is Nothing Then

        Position_Row_1 = POSITION_MENU_BACKGROUND_TOP + POSITION_INTERVAL_IV
        Position_Row_2 = Position_Row_1 + POSITION_INTERVAL_V + POSITION_MARGIN + POSITION_PADDING
    
        Set shp = wks.Shapes("Menu")
        Call Move_Shape(shp:=shp, Top:=POSITION_MENU_HEADER_TOP, Left:=POSITION_MENU_BACKGROUND_LEFT)
        Background_Bottom = shp.Top + shp.Height
        Background_Right = shp.Left + shp.Width
       

        Set shp = wks.Shapes("img_CHG_Logo")
        Call Move_Shape(shp:=shp, Top:=Background_Bottom - (shp.Height + POSITION_MARGIN + POSITION_PADDING), Left:=POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING)
        LastRight = 0
        Select Case UCase(ProjectName)

            Case UCase(PROJECT_LAS_QS_SCHEDULES)


            Case UCase(PROJECT_DEAL_DETAILS)
            Case UCase(PROJECT_DMS_DEALS_FOLLOW_UP)
                
                Set shp = wks.Shapes("Export_DMS_Deals_Emitted")
                Call Setup_Button(shp)
                Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING))
                LastRight = shp.Left + shp.Width
                
                Set shp = wks.Shapes("Export_Deal_Details")
                Call Setup_Button(shp)
                Call Move_Shape(shp:=shp, Top:=(Position_Row_1), Left:=(LastRight + POSITION_MARGIN + POSITION_PADDING))
                LastRight = shp.Left + shp.Width


            Case UCase(PROJECT_LAS_QS_ASSETS)
                
                Set shp = wks.Shapes("Export_Interface_Qualisoft")
                Call Setup_Button(shp)
                Call Move_Shape(shp:=shp, Top:=Position_Row_1, Left:=POSITION_MENU_BACKGROUND_LEFT + POSITION_MARGIN + POSITION_PADDING)
                LastRight = shp.Left + shp.Width

        End Select


    End If

End Sub
Sub Setup_Project()
    Const SUB_NAME As String = "Setup_Project"
    Call Log_Step(SUB_NAME)
    
    Dim rngProject As Range
    Dim rngVersion As Range
    Dim wks As Worksheet
    
    
    Set rngProject = MStructure.Get_Model_Range("Project_Name")
    Set rngVersion = MStructure.Get_Model_Range("Project_Version")
    
    Set wks = MStructure.Get_Worksheet_By_Codename("wksMenu")
    
    Call Unprotect_Worksheet(wks)
    
    Call Change_Shape_text(wks, "img_Header", rngProject.Text)
    Call Change_Shape_text(wks, "lb_WorkbookVersion", rngVersion.Text)
    
    Call Protect_Worksheet(wks)
End Sub
Sub Setup_Show_All_Worksheets()
    Const SUB_NAME As String = "Setup_Show_All_Worksheets"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    For Each wks In ThisWorkbook.Worksheets
        wks.Visible = xlSheetVisible
    Next wks
End Sub
Sub Setup_Workbook()
    Const SUB_NAME As String = "Setup Workbook"
    Call Log_Step(SUB_NAME)

    Call Setup_Worksheets_Visibility
    
    'Call Setup_Menu
    'Call Setup_Menu_Reports
    'Call Apply_Locks_NamedRanges
    
End Sub
Sub Setup_Worksheets_Locks()
    Const SUB_NAME As String = "Setup_Worksheets_Locks"
    Call Log_Step(SUB_NAME)
    
    Call Unprotect_All_Worksheet
    
    Call MSecurity.Protect_Worksheet(Get_Worksheet_By_Codename("wksMenu"))
    Call MSecurity.Protect_Worksheet(Get_Worksheet_By_Codename("wksMenu_Reports"))
    
End Sub
Sub Setup_Worksheets_Visibility()
    Const SUB_NAME As String = "Setup_Worksheets_Visibility"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    
    For Each wks In ThisWorkbook.Worksheets
        
        If Regx_Match(wks.Codename, "CHGBrazil") Then
            wks.Visible = xlSheetVeryHidden
                       
        ElseIf Regx_Match(wks.Codename, "wks((?:Menu)|(?:Report)|(?:List)|(?:Data)|(?:FromTo))") Then
            wks.Visible = xlSheetVisible
                
        Else
            wks.Visible = xlSheetVeryHidden
            
        End If

    Next wks

End Sub
