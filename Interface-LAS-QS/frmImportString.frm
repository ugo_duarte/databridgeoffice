VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmImportString 
   Caption         =   "Import"
   ClientHeight    =   6440
   ClientLeft      =   -675
   ClientTop       =   -2835
   ClientWidth     =   6795
   OleObjectBlob   =   "frmImportString.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmImportString"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Option Explicit
Private Sub lbImportString_Click()
    Dim ImportString As String
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_DealDetails")
    ImportString = tbImportString.Text
    
    If Trim(ImportString) = vbNullString Then
        Call MInterface.Message_Error("No data!", "Import")
    Else
        Call MDealDetails.Import_Data_From_Encrypted_String(ImportString)
        Call MModels.Modeling_Data(MODEL_DATA_DEALDETAILS, wks, "Deal Details and Asset Forecast", True)
    End If
    
    Call MInterface.Message_Information(MSG_SUCCESS, "Import")
    Unload Me
End Sub


Private Sub UserForm_Initialize()
    Me.Width = 600
    Me.Height = 350
    Me.StartUpPosition = 2
    tbImportString.Top = 5
    tbImportString.Left = 5
    
    tbImportString.Width = Me.Width - 20
    lbImportString.Width = Me.Width - 20
    
    lbImportString.Top = tbImportString.Top + tbImportString.Height + 10
    lbImportString.Left = 5
    
End Sub
