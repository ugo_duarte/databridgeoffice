Attribute VB_Name = "MEncrypt"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MEncrypt"

Function DecodeBase64_XMLDOM(b64$)
    Dim b
    With CreateObject("Microsoft.XMLDOM").createElement("b64")
        .DataType = "bin.base64": .Text = b64
        b = .nodeTypedValue
        With CreateObject("ADODB.Stream")
            .Open: .Type = 1: .Write b: .Position = 0: .Type = 2: .Charset = "utf-8"
            DecodeBase64_XMLDOM = .ReadText
            .Close
        End With
    End With
End Function
Function EncodeBase64_XMLDOM(Text$)
    Dim b
    With CreateObject("ADODB.Stream")
        .Open: .Type = 2: .Charset = "utf-8"
        .WriteText Text: .Position = 0: .Type = 1: b = .Read
        With CreateObject("Microsoft.XMLDOM").createElement("b64")
            .DataType = "bin.base64": .nodeTypedValue = b
            EncodeBase64_XMLDOM = Replace(Mid(.Text, 5), vbLf, "")
        End With
        .Close
    End With
End Function


