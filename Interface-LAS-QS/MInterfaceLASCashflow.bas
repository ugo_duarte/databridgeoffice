Attribute VB_Name = "MInterfaceLASCashflow"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MInterfaceLASCashflow"
Sub Generate_Interface_LAS_Cashflow_Beneficiaries()
    Const SUB_NAME As String = "Generate Interface LAS Cashflow Beneficiaries"
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim rngData As Range
    Dim rngLayout As Range
    Dim rngReport As Range
    
    Set wksData = Get_Worksheet_By_Codename("wksData_DMSAddresses")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_Interface_Beneficiary")
    
    Set rngData = wksData.Range("A1")
    Set rngLayout = Get_Model_Range("Layout_Interface_Cashflow_Beneficiaries")
    Set rngReport = wksReport.Range("A1")
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Interface_Cashflow_Beneficiaries")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_Interface_Cashflow_Beneficiaries")
    
    wksReport.Cells.WrapText = False
    wksReport.Columns.AutoFit
    
End Sub
Sub Generate_Interface_LAS_Cashflow_Invoices()
    Const SUB_NAME As String = "Generate Interface LAS Cashflow Invoices"
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim rngData As Range
    Dim rngLayout As Range
    Dim rngReport As Range
    
    Set wksData = Get_Worksheet_By_Codename("wksData_Purchases")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_Interface_Invoices")
    
    Set rngData = wksData.Range("A1")
    Set rngLayout = Get_Model_Range("Layout_Interface_Cashflow_Invoices")
    Set rngReport = wksReport.Range("A1")
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Interface_Cashflow_Invoices")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_Interface_Cashflow_Invoices")
    
    wksReport.Cells.WrapText = False
    wksReport.Columns.AutoFit
    
End Sub
Sub Generate_Interface_LAS_Cashflow_Schedules()
    Const SUB_NAME As String = "Generate Interface LAS Cashflow Schedules"
    Call Log_Step(SUB_NAME)

    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim rngData As Range
    Dim rngLayout As Range
    Dim rngReport As Range
    
    Set wksData = Get_Worksheet_By_Codename("wksData_LeaseSchedules")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_Interface_Schedules")
    
    Set rngData = wksData.Range("A1")
    Set rngLayout = Get_Model_Range("Layout_Interface_Cashflow_Schedules")
    Set rngReport = wksReport.Range("A1")
    
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Interface_Cashflow_Schedules")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_Interface_Cashflow_Schedules")
    
    wksReport.Cells.WrapText = False
    wksReport.Columns.AutoFit
    
End Sub
