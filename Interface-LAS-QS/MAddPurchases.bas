Attribute VB_Name = "MAddPurchases"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MAddPurchases"

Global Const AppNameXL As String = "Interface XML-LAS Purchases - Excel"
Global Const CITRIX_LAS_PATH As String = "\\v246\iclass\Citrix\Start.exe ""LAS.NET Live"""
Global Const KEY_ALT_TAB As String = "%{TAB}"
Global Const KEY_TAB As String = "{TAB}"
Global Const SLEEP_CURRENT_MS = 200
Global Const LAS_APP As String = "LAS.NET"
Global Const QLAS_APP As String = "QA MAJOR - LAS.NET"


Sub Add_Individual_Purchase()
    Const SUB_NAME As String = "Add Individual Purchase"
    On Error GoTo ErrorHandler
    
    Dim rngSelection As Range
    Dim wksActive As Worksheet
    Dim wksData As Worksheet
    Dim iData As Long
    Dim aModelSections() As Variant
    Dim aModelFields() As Variant
    
    Dim aDataPurchases() As Variant
    Dim aHeader() As Variant
    
    Dim NumberOfPurchases As Long
    Dim dicMousePositions As Dictionary
    Dim Item As Variant
    Dim InvoiceColumn As Long
    Dim UserInput As VbMsgBoxResult
    
    Dim j As Long
    Dim jApproved As Long
    Dim jEmitted As Long
    Dim aDataApproved() As Variant
    Dim aModelEmitted() As Variant
    Dim aDataEmitted() As Variant
    Dim DealNo As String
    Dim ColumnDealNo As Long
    Dim ColumnDealName As Long
    
    Dim SupplierInvoiceNo As String
    
    Dim rngActive As Range
    
    Dim rowsToDelete As Variant

'Pre-Initialize
    Set rngSelection = Selection
    iData = rngSelection.Row
    Set wksActive = rngSelection.Worksheet
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Purchases")
    
    If wksActive.Codename <> wksData.Codename Then
        Call err.Raise(901, , "Cant confirm outside the '" & wksData.Name & "' Sheet")
    End If
   
'Initialize
    Call MGlobal.Global_Initialize
    
    Set dicMousePositions = MStructure.Load_Dictionary("List_Mouse_Positions_CR", , 0)
    
    'Models
    aModelSections = MStructure.Get_Model_Range("Models_RPA_Purchases_Sections")
    aModelFields = MStructure.Get_Model_Range("Models_RPA_Purchases_Fields")
    
    'Data
    aDataPurchases = Get_Current_Region(wksData.Range("A1"))
    
    'Checks
    If iData > UBound(aDataPurchases) Or iData = 1 Then
        Call err.Raise(901, , "Invoice invalid!")
    End If
    
    'Invoice
    InvoiceColumn = Get_Column_Number(aDataPurchases, "Supplier invoice no")
    SupplierInvoiceNo = aDataPurchases(iData, InvoiceColumn)
    
    If SupplierInvoiceNo = vbNullString Then
        Call err.Raise(901, , "Invoice invalid!")
    End If
    
    'After the check confirms with the user if he wants to add the selected purchase
    If MInterface.Message_Confirm("Create the Purchase for Invoice '" & SupplierInvoiceNo & "'?", SUB_NAME) <> vbYes Then
        
        Call err.Raise(901, , "Canceled!")
        
    Else
        
        Call MRPA.LAS_Activate
        
        Call MRPA.LAS_Add_Purchase
        
        If MAddPurchases.LAS_Create_Purchase_All_Fields(aModelSections, aModelFields, aDataPurchases, iData, dicMousePositions) Then
            wksData.Cells(iData, InvoiceColumn).Interior.Color = enmColors_Verde
        Else
            wksData.Cells(iData, InvoiceColumn).Interior.Color = enmColors_Vermelho
        End If
        
        'Sucess
        Call Excel_Activate
        Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
    End If
    
Finish:
    
    Call MGlobal.Global_Finalize

    Exit Sub
    
ErrorHandler:
    Call Excel_Activate
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Create_All_Purchases()
    Const SUB_NAME As String = "Create ALL Purchases"
    On Error GoTo errHandler
    
    Dim wksData As Worksheet
    Dim aDataPurchases() As Variant
    Dim aHeader() As Variant
    Dim aModel() As Variant
    Dim i As Long
    Dim NumberOfPurchases As Long
    Dim dicMousePositions As Dictionary
    Dim Item As Variant
    Dim InvoiceColumn As Long
    Dim UserInput As VbMsgBoxResult
    
    If MInterface.Message_Confirm("Create ALL Purchases?", SUB_NAME) <> vbYes Then End
    
    Call MGlobal.Global_Initialize
    aModel = Get_Model_Range("Model_Data_Purchases")
    Set dicMousePositions = MStructure.Load_Dictionary("List_Mouse_Positions_CR", , 0)
    Set wksData = Get_Worksheet_By_Codename("wksData_Purchases")
    NumberOfPurchases = Get_Last_Row(wksData, 1)
    wksData.Cells.Interior.Pattern = xlNone
    Call Generate_Worksheet_From_Model(wksData, False, "Model_Data_Purchases")
    aDataPurchases = MStructure.Get_Current_Region(wksData.Range("A1"))
    InvoiceColumn = Get_Column_Number(aDataPurchases, "SUPPLIER INVOICE NO")

    Call LAS_Activate
    Sleep 2000
    'Call RPA_Send_Keys(KEY_ALT_TAB)
    If MInterface.Message_Confirm("Is LAS loaded?") = vbYes Then
        For i = 2 To NumberOfPurchases
            ThisWorkbook.Activate
            Call Excel_Activate
            UserInput = MInterface.Message_Confirm_With_Cancel("Press 'Yes' to create the next Purchase: " & i - 1 & "/" & NumberOfPurchases - 1, SUB_NAME)


            'Call RPA_Send_Keys(KEY_ALT_TAB)
            Call LAS_Activate
            Sleep 500

            Call LAS_Add_Purchase
            Sleep 500

            Select Case UserInput
                Case vbYes
                    If LAS_Create_Purchase(aModel, aDataPurchases, i, dicMousePositions) Then
                        wksData.Cells(i, InvoiceColumn).Interior.Color = RGB(0, 255, 0)
                    Else
                        wksData.Cells(i, InvoiceColumn).Interior.Color = RGB(255, 0, 0)
                    End If
                Case vbCancel
                    Call err.Raise(666, , "Operation canceled!")
            End Select

        Next i

    End If

    'Call RPA_Send_Keys(KEY_ALT_TAB)
    Call Excel_Activate

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Fim:
    Call MGlobal.Global_Finalize
    Exit Sub

errHandler:
    Call Excel_Activate
    Call MInterface.Message_Error(err.Description, err.Number)
    GoTo Fim

End Sub
Sub Create_All_Purchases_All_Fields()
    Const SUB_NAME As String = "Create ALL Purchases All Fields"
    On Error GoTo errHandler


    Dim aModelSections() As Variant
    Dim aModelFields() As Variant

    Dim wksData As Worksheet

    Dim aDataPurchases() As Variant
    Dim aHeader() As Variant


    Dim i As Long
    Dim NumberOfPurchases As Long
    Dim dicMousePositions As Dictionary
    Dim Item As Variant
    Dim InvoiceColumn As Long
    Dim UserInput As VbMsgBoxResult

    If MInterface.Message_Confirm("Create the Purchases with All Fields?", SUB_NAME) <> vbYes Then End



    Call MGlobal.Global_Initialize

    Call Excel_Activate(True)

    aModelSections = MStructure.Get_Model_Range("Models_RPA_Purchases_Sections")
    aModelFields = MStructure.Get_Model_Range("Models_RPA_Purchases_Fields")
    
    Set dicMousePositions = MStructure.Load_Dictionary("List_Mouse_Positions_CR", , 0)
    
    Set wksData = Get_Worksheet_By_Codename("wksData_Purchases")
    NumberOfPurchases = Get_Last_Row(wksData, 1)

    'wksData.Cells.Interior.Pattern = xlNone
    If NumberOfPurchases > 1 Then
        'Call Generate_Worksheet_From_Model(wksData, False, "Models_RPA_Purchases_Fields")
        aDataPurchases = MStructure.Get_Current_Region(wksData.Range("A1"))
        InvoiceColumn = Get_Column_Number(aDataPurchases, "Supplier invoice no")
        
        Call LAS_Activate(True)
        Sleep 2000
        
        If MInterface.Message_Confirm("Is LAS loaded?") = vbYes Then
            
            For i = 2 To NumberOfPurchases
                
                Call Excel_Activate
                
                UserInput = MInterface.Message_Confirm_With_Cancel("Press 'Yes' to create the next Purchase: " & i - 1 & "/" & NumberOfPurchases - 1, SUB_NAME)
                
                'Call RPA_Send_Keys(KEY_ALT_TAB)
                Call LAS_Activate
                Sleep 500
                
                Call LAS_Add_Purchase
                Sleep 500
                
                Select Case UserInput
                    Case vbYes
                        If LAS_Create_Purchase_All_Fields(aModelSections, aModelFields, aDataPurchases, i, dicMousePositions) Then
                            wksData.Cells(i, InvoiceColumn).Interior.Color = RGB(0, 255, 0)
                        Else
                            wksData.Cells(i, InvoiceColumn).Interior.Color = RGB(255, 0, 0)
                        End If
                    Case vbCancel
                        Call err.Raise(666, , "Operation canceled!")
                End Select
                
            Next i
            
            
        End If 'If MInterface.Message_Confirm("Is LAS loaded?") = vbYes Then
        
        'Call RPA_Send_Keys(KEY_ALT_TAB)
        Call Excel_Activate
        
    Else
        Call err.Raise(999, , "No DATA!")
    End If
        
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Fim:
    Call MGlobal.Global_Finalize
    Exit Sub
    
errHandler:
    Call Excel_Activate
    Call MInterface.Message_Error(err.Description, err.Number)
    GoTo Fim
    
End Sub

Sub Generate_Purchases()
    Const SUB_NAME As String = "Generate Purchases"
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim rngData As Range
    Dim rngLayout As Range
    Dim rngReport As Range
    
    Set wksData = Get_Worksheet_By_Codename("wksReport_XML_NFE")
    Set wksReport = Get_Worksheet_By_Codename("wksData_Purchases")
    
    Set rngData = wksData.Range("A1")
    Set rngLayout = Get_Model_Range("Layout_GeneratePurchases")
    Set rngReport = wksReport.Range("A1")
    
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Models_RPA_Purchases_Data_Header")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Models_RPA_Purchases_Data_Header")
    
    wksReport.Cells.WrapText = False
    wksReport.Columns.AutoFit
    
End Sub



Function LAS_Create_Purchase_All_Fields(aModelSections() As Variant, aModelFields() As Variant, aDataPurchases() As Variant, iDataPurchase As Long, dicMousePositions As Dictionary) As Boolean
    Const SUB_NAME As String = "Create Purchase All Fields"
    On Error GoTo errHandler
    
    Dim CurrentSection As String
    Dim CurrentColumn As Long
    Dim CurrentField As String
    Dim CurrentValue As String
    Dim dicColumns As Dictionary
    Dim j As Long
    
    LAS_Create_Purchase_All_Fields = False
    
    Set dicColumns = Load_Dictionary_Columns(aDataPurchases)
    CurrentSection = aModelSections(1, 1)
    For j = LBound(aModelFields, 2) To UBound(aModelFields, 2)
        
        If aModelSections(1, j) <> CurrentSection Then
            CurrentSection = aModelSections(1, j) 'Get the current section
            Call LAS_Click_Section(dicMousePositions, CurrentSection)
        End If
        
        CurrentField = aModelFields(1, j)
        CurrentColumn = dicColumns(aModelFields(1, j))
        
        If CurrentColumn > 0 Then 'Only adds if column exists in the model
        
            CurrentValue = aDataPurchases(iDataPurchase, CurrentColumn) 'Get the current value
            
            If CurrentValue <> vbNullString Then
                Call MRPA.RPA_Send_Value_And_NextField(CurrentValue)
            Else
                Call MRPA.RPA_TAB_To_Next_Field
            End If
            
            
        Else
            Call MRPA.RPA_TAB_To_Next_Field
        End If
        Call Sleep(SLEEP_CURRENT_MS)
    Next j
    
    LAS_Create_Purchase_All_Fields = True
    
Fim:
    Exit Function

errHandler:

    LAS_Create_Purchase_All_Fields = False
    
    Call Excel_Activate
    Call MInterface.Message_Error("Falha!" & err.Description, SUB_NAME)
    GoTo Fim
    
End Function
Function LAS_Create_Purchase(aModel() As Variant, aDataPurchases() As Variant, iDataPurchase As Long, dicMousePositions As Dictionary) As Boolean
    Const SUB_NAME As String = "Create Purchase"
    On Error GoTo errHandler
    
    Dim NumberTry As Long
    Dim i As Long
    Dim X As Long
    Dim Y As Long
    Dim key As Long
    Dim MaxTrys As Long

    LAS_Create_Purchase = False
    MaxTrys = gdicGlobalParameters("NUMBER OF ATTEMPTS")
    
    'MAIN DATA PURCHASE
    Call Send_Value_From_Model("INVOICE TYPE", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("INVOICE SOURCE", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("VOUCHER", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("INVOICE DATE", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("DATE OF RECEIPT", aModel, aDataPurchases, iDataPurchase, NextFieldCount:=7)
    Call Send_Value_From_Model("DISCOUNT GRANTED", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("DISCOUNT KEY", aModel, aDataPurchases, iDataPurchase, NextFieldCount:=3)
    Call Send_Value_From_Model("SUPPLIER ADDRESS", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("SUPPLIER INVOICE NO", aModel, aDataPurchases, iDataPurchase, NextFieldCount:=3)
    Call Send_Value_From_Model("CUSTOMER REF NO", aModel, aDataPurchases, iDataPurchase)
    
    'Destination TAB
    Call RPA_Mouse_Position_And_Click(dicMousePositions, "DESTINATION")
    Call RPA_TAB_To_Next_Field(3)
    
    Call Send_Value_From_Model("CUSTOMER NO", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("DISCOUNT FOR CUSTOMER", aModel, aDataPurchases, iDataPurchase)
    
    'SAVE
    Call RPA_Mouse_Position_And_Click(dicMousePositions, "SAVE PURCHASE")
    Sleep 2000
    
    'Confirma Mensagem
    Call RPA_Send_Keys("{ENTER}", 500)

ConfirmaSalvar:
    Call RPA_Send_Keys(KEY_ALT_TAB, 200)
    Call Excel_Activate

    If MInterface.Message_Confirm("Confirme que foi salvo", "Save") <> vbYes Then
        NumberTry = NumberTry + 1
        If NumberTry > MaxTrys Then
            GoTo Fim
        End If
        GoTo ConfirmaSalvar
    End If

    Call RPA_Send_Keys(KEY_ALT_TAB, 200)
    Call LAS_Activate

    'AM & VAT
    Call Send_Value_From_Model("AM NO", aModel, aDataPurchases, iDataPurchase)
    Call Send_Value_From_Model("VAT KEY", aModel, aDataPurchases, iDataPurchase)

    'ADD
    Call RPA_Mouse_Position_And_Click(dicMousePositions, "ADD PURCHASE")
    'SAVE
    Call RPA_Mouse_Position_And_Click(dicMousePositions, "SAVE PURCHASE")
    Sleep 2000

    Call RPA_Send_Keys(KEY_ALT_TAB, 200)
    Call Excel_Activate

    If MInterface.Message_Confirm("Criada com sucesso?") Then
        LAS_Create_Purchase = True
    End If

Fim:
    Exit Function

errHandler:
    LAS_Create_Purchase = False
    Call Excel_Activate
    Call MInterface.Message_Error("Falha!" & err.Description, SUB_NAME)
    GoTo Fim
End Function
Sub LAS_Open()
    Const SUB_NAME As String = "Open LAS"
    Call Shell(CITRIX_LAS_PATH, vbMaximizedFocus)
End Sub

Sub Send_Value_From_Model(FieldName As String, aModel() As Variant, aData() As Variant, iData As Long, Optional NextFieldCount As Long = 1)
    Dim CurrentValue As String
    Dim CurrentColumn As String
    
    CurrentColumn = Get_Column_Number(aModel, FieldName)
    If CurrentColumn > 0 Then
        CurrentValue = aData(iData, CurrentColumn)
    
        Call RPA_Send_Keys(CurrentValue)
        Call RPA_TAB_To_Next_Field(NextFieldCount)
    End If
    
End Sub
