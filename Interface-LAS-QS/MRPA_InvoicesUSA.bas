Attribute VB_Name = "MRPA_InvoicesUSA"
Option Explicit
Const MODEL_RPA_SECTION As Long = 1
Const MODEL_RPA_FIELD As Long = 2
Const MODEL_RPA_SEQUENCE As Long = 3
Const MODEL_RPA_ENABLED As Long = 4
Const MODEL_RPA_WARNING As Long = 5
Const MODEL_RPA_DATATYPE As Long = 6

Global gMouseDelayMS As Integer
Global gKeyDelayMS As Integer
    

Dim aList_Warnings() As Variant

Public Type WarningResponse
    Window As String
    Action As String
    Status As String
    Message As String
    Pattern As String
    Key As String
End Type


Public Enum enmRPASingleExecution
    enmRPASingleExecution_ManualInvoice = 1
    enmRPASingleExecution_InvoiceBlock
    enmRPASingleExecution_BlockPosition
End Enum
    

Public Enum enmListWarning
    enmListWarning_Window = 1
    enmListWarning_Action = 2
    enmListWarning_Status = 3
    enmListWarning_Message = 4
    enmListWarning_Pattern = 5
    enmListWarning_Key = 6
End Enum
'Manual Invoice
'Print Layout
'Block Text
Sub Add_All_Manual_Invoices()
    Const SUB_NAME As String = "Add_All_Manual_Invoices"
    On Error GoTo ErrorHandler
    
    If MInterface.Message_Confirm("Do you wanto to add manual invoices to LAS?") <> vbYes Then End
    
    Dim dicMousePositions As Dictionary
    Dim i As Long
    Dim aModelBlock() As Variant
    
    'Initialize
    Call MGlobal.Global_Initialize
        
    'Models
    Set dicMousePositions = MStructure.Load_Dictionary("List_Mouse_Positions_CR", , 0)
    aList_Warnings = Get_Current_Region(Get_Model_Range("List_Warnings"))
    
    
    gMouseDelayMS = gdicGlobalParameters("MOUSE DELAY MS")
    gKeyDelayMS = gdicGlobalParameters("KEY DELAY MS")
    
    Call LAS_Activate
    Call RPA_Execution("RPA_Execution_Manual_Invoices", vbNullString, "Manual Invoice ID", dicMousePositions, LogDataOnly:=False)
    
Finish:
    Call Message_Information(MSG_SUCCESS)
    Call MGlobal.Global_Finalize

    Exit Sub
    
ErrorHandler:
    Call Excel_Activate
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Add_Single(eRPA As enmRPASingleExecution)
    Const SUB_NAME As String = "RPA - Add Single"
    On Error GoTo ErrorHandler
    Dim wks As Worksheet
    'Initialize
    Call MGlobal.Global_Initialize
    
    If ValidateCurrentExecution(eRPA) Then
        If Message_Confirm("Confirm Single Execution") <> vbYes Then
            Call err.Raise(999, , "Execution Canceled")
        End If
        
        Set wks = ActiveSheet
        
        Dim i As Long
        Dim aModelBlock() As Variant
        Dim ColID As Long
        Dim PrimaryKeyChild As String
        
        'Models
        Dim dicMousePositions As Dictionary
        Set dicMousePositions = MStructure.Load_Dictionary("List_Mouse_Positions_CR", , 0)
        aList_Warnings = Get_Current_Region(Get_Model_Range("List_Warnings"))
        
        aModelBlock = Get_Current_Region(wks.Range("A1"))
        
        gMouseDelayMS = gdicGlobalParameters("MOUSE DELAY MS")
        gKeyDelayMS = gdicGlobalParameters("KEY DELAY MS")
        
        
        
        
        Call LAS_Activate
        
        Select Case eRPA
            Case enmRPASingleExecution_ManualInvoice
                ColID = Get_Column_Number(aModelBlock, "Manual Invoice ID")
                If ColID > 0 Then
                    PrimaryKeyChild = aModelBlock(ActiveCell.Row, ColID)
                    Call RPA_Execution("RPA_Execution_Manual_Invoices", PrimaryKeyChild, "Manual Invoice ID", dicMousePositions, LogDataOnly:=False)
                End If
                
                
            Case enmRPASingleExecution_InvoiceBlock
                ColID = Get_Column_Number(aModelBlock, "Manual Invoice ID")
                If ColID > 0 Then
                    PrimaryKeyChild = aModelBlock(ActiveCell.Row, ColID)
                    Call RPA_Execution("RPA_Execution_Invoice_Blocks", PrimaryKeyChild, "Manual Invoice ID", dicMousePositions, LogDataOnly:=False)
                
                End If
                
            Case enmRPASingleExecution_BlockPosition
                ColID = Get_Column_Number(aModelBlock, "Invoice Block ID")
                If ColID > 0 Then
                    PrimaryKeyChild = aModelBlock(ActiveCell.Row, ColID)
                    Call RPA_Execution("RPA_Execution_Block_Positions", PrimaryKeyChild, "Invoice Block ID", dicMousePositions, LogDataOnly:=False)
                
                End If
                
        End Select
        
    End If
    
    Call Message_Information(MSG_SUCCESS)
    
Finish:
    Call MGlobal.Global_Finalize

    Exit Sub
    
ErrorHandler:
    Call Excel_Activate
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish

End Sub
Function ValidateCurrentExecution(eRPA As enmRPASingleExecution) As Boolean
    Dim wksA As Worksheet
    Dim wksExpected As Worksheet
    Dim Valid As Boolean
    Dim errorMessage As String
    Valid = True
    
    Set wksA = ActiveSheet
    Select Case eRPA
        Case enmRPASingleExecution_ManualInvoice
            
            Set wksExpected = Get_Worksheet_By_Codename("wksReport_ManualInvoices")
            
            
        Case enmRPASingleExecution_InvoiceBlock
            Set wksExpected = Get_Worksheet_By_Codename("wksReport_InvoiceBlocks")
            
            
        Case enmRPASingleExecution_BlockPosition
            Set wksExpected = Get_Worksheet_By_Codename("wksReport_BlockPositions")
            
            
    End Select
    
    'VALIDATE
    If Not wksA.Codename = wksExpected.Codename Then
        Valid = False
        errorMessage = "Active Worksheet differs from the expected!"
    ElseIf Not ActiveCell.Row > 1 Then
        Valid = False
        errorMessage = "Row selected should be greater then (1)"
        
    End If
    
    'RESULT
    
    
    ValidateCurrentExecution = Valid
    If Not Valid Then
        Call err.Raise(999, , errorMessage)
    End If
    
    
End Function
Sub Generate_Invoices_Data()
    Const SUB_NAME As String = "Generate_Invoices_Data"
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim rngData As Range
    Dim rngLayout As Range
    Dim rngReport As Range
    
    Set wksData = Get_Worksheet_By_Codename("wksData_Invoices")
    
    
    Set rngData = wksData.Range("A1")
    Set rngLayout = Get_Model_Range("Layout_Generate")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_ManualInvoices")
    Set rngReport = wksReport.Range("A1")
    
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_LAS_Manual_Invoices_Header")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_LAS_Manual_Invoices_Header")
    
    
    Set rngLayout = Get_Model_Range("Layout_InvoiceBlocks")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_InvoiceBlocks")
    Set rngReport = wksReport.Range("A1")
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_LAS_Invoice_Blocks_Header")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_LAS_Invoice_Blocks_Header")
    
    Set rngLayout = Get_Model_Range("Layout_BlockPositions")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_BlockPositions")
    Set rngReport = wksReport.Range("A1")
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_LAS_Block_Positions_Header")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_LAS_Block_Positions_Header")
    
    Set rngLayout = Get_Model_Range("Layout_PrintLayout")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_PrintLayout")
    Set rngReport = wksReport.Range("A1")
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_LAS_Print_Layout_Header")
    
    Call MGenerator.Layout_to_Report(rngData, rngLayout, rngReport, "Model_LAS_Print_Layout_Header")
    
    wksReport.Cells.WrapText = False
    wksReport.Columns.AutoFit
    
End Sub
Function Test_Check_TopWindow_Handle()
    Debug.Print "------ LAS Window"
    gHandle_LAS_APP = FindWindow(vbNullString, LAS_Get_Window_Title(QA:=True))
    Call Print_Window_Detail(gHandle_LAS_APP)
    
    LAS_Activate
    Call MRPA.RPA_Send_Keys(KEY_TAB)
    Dim iMessage As LongLong
    'Call RPA_Move_Mouse(1030, 54)
    'Sleep 1000
    'Call RPA_Mouse_Left_Click
    'Sleep 1000
    'Call RPA_Mouse_Left_Click
    
    iMessage = GetForegroundWindow 'GetActiveWindow
    Call Print_Window_Detail(iMessage)
    
  
    
    'Debug.Print CStr(Check_TopWindow_Handle())
    
    'Debug.Print "------ Desktop Window"
    'gHandle_Desktop = GetDesktopWindow
    'Call Print_Window_Detail(gHandle_Desktop)
End Function

Function Check_TopWindow_Handle(Optional sequence) As Boolean
    
    gHandle_Current_Top_Window = GetForegroundWindow
    
    Check_TopWindow_Handle = False
    Debug.Print "-------Current Top Window"
    Print_Window_Detail (gHandle_Current_Top_Window)
    
    If gHandle_Current_Top_Window = gHandle_LAS_APP Then
        Check_TopWindow_Handle = True
    End If
    
End Function
Sub RPA_Execution(ExecutionName As String, ByVal PrimaryKeyTopExecutionValue As String, PrimaryKeyColumnName As String, dicMousePositions As Dictionary, Optional LogDataOnly As Boolean = False)
    
    Dim aRPAExecutionList() As Variant
    Dim dicRPAExecutionList As Dictionary
    
    aRPAExecutionList = Get_Current_Region(Get_Model_Range("RPA_Execution_List"))
    Set dicRPAExecutionList = Load_Dictionary_From_Array(aRPAExecutionList)
    
    Dim wksT As Worksheet
    Set wksT = Get_Worksheet_By_Codename("wksTemporary")
    
    Dim aLog() As Variant
    
    Dim wksData As Worksheet
    Dim aData() As Variant
    Dim dicColumnsData As Dictionary
    Dim iData As Long
    
    Dim wksRPA As Worksheet
    Dim aRPAExecution() As Variant
    Dim dicColumnsRPA As Dictionary
    Dim iSequence As Long
    
    Dim ColumnPrimaryKey As Long
    Dim PrimaryKeyValue As Variant
        
    Dim Warnings()      As String
    Dim cRPA            As clsRPA
    Dim w               As Variant
    Dim PrimaryKeyChild As String
    
    Dim wResponse As WarningResponse
    
    Set cRPA = New clsRPA
     
    ExecutionName = UCase(ExecutionName)
    
    'Recursive
    'Check the LAS handle
    If gHandle_LAS_APP = 0 Then
        Call err.Raise(999, , "LAS is not open!")
    End If
    
    If dicRPAExecutionList.Exists(ExecutionName) Then
        
        'Data execution setup
        Set wksRPA = Get_Model_Range(ExecutionName).Worksheet
        Set wksData = Get_Worksheet_By_Codename(dicRPAExecutionList(ExecutionName))
        
        aRPAExecution = MStructure.Get_Current_Region(wksRPA.Range("A1"))
        aData = MStructure.Get_Current_Region(wksData.Range("A1"))
        
        'Colunas
        Set dicColumnsRPA = Load_Dictionary_Columns(aRPAExecution)
        Set dicColumnsData = Load_Dictionary_Columns(aData)
        
        'Primary key column @
        ColumnPrimaryKey = dicColumnsData(PrimaryKeyColumnName)
        
        For iData = LBound(aData) + 1 To UBound(aData)
            
            If ColumnPrimaryKey > 0 Then
                PrimaryKeyValue = aData(iData, ColumnPrimaryKey)
            End If
            
            If PrimaryKeyTopExecutionValue = PrimaryKeyValue Or PrimaryKeyTopExecutionValue = vbNullString Then
                
                For iSequence = LBound(aRPAExecution) + 1 To UBound(aRPAExecution)
                    
                    'Params
                    cRPA.sequence = aRPAExecution(iSequence, dicColumnsRPA("Sequence"))
                    cRPA.Command = aRPAExecution(iSequence, dicColumnsRPA("Command"))
                    cRPA.Section = aRPAExecution(iSequence, dicColumnsRPA("Section"))
                    cRPA.Field = aRPAExecution(iSequence, dicColumnsRPA("Field"))
                    cRPA.DataType = aRPAExecution(iSequence, dicColumnsRPA("DataType"))
                    cRPA.Warning = aRPAExecution(iSequence, dicColumnsRPA("Warning"))
                    cRPA.CurrentValue = vbNullString
                    cRPA.Print_Info (ExecutionName & " - Executing: " & iData - 1 & " / " & UBound(aData) - 1 & " | ")
                    
                    
                    
                    Select Case UCase(cRPA.Field)
                        Case "[Skip]", "[SKIP]"
                            cRPA.CurrentValue = cRPA.Field
                        Case Else
                            If dicColumnsData.Exists(cRPA.Field) Then
                                cRPA.CurrentValue = aData(iData, dicColumnsData(cRPA.Field))
                            End If
                    End Select
                    
                    If LogDataOnly Then
                        'Log array
                        ReDim aLog(1 To 1, 1 To 9)
                        
                        aLog(1, 1) = ExecutionName
                        aLog(1, 2) = PrimaryKeyTopExecutionValue
                        
                        aLog(1, 3) = cRPA.sequence
                        aLog(1, 4) = cRPA.Command
                        aLog(1, 5) = cRPA.Section
                        aLog(1, 6) = cRPA.Field
                        aLog(1, 7) = cRPA.DataType
                        aLog(1, 8) = cRPA.Warning
                        aLog(1, 9) = cRPA.CurrentValue
                        
                        wksT.Range("A1").Offset(Get_Last_Row(wksT, 1)).Resize(, UBound(aLog, 2)) = aLog
                        
                        If UCase(cRPA.Command) = "EXECUTION" Then
                            PrimaryKeyChild = cRPA.CurrentValue
                            Call RPA_Execution(cRPA.Section, PrimaryKeyChild, cRPA.Field, dicMousePositions, LogDataOnly)
                        End If
                        
                    Else
                    
                        Select Case UCase(cRPA.Command)
                            Case "STOP"
                                Stop
                                
                            Case "LAS ADD"
                                Call LAS_Add(cRPA.Section)
                            
                            Case "FIELD"
                                If UCase(cRPA.Field) = "[SKIP]" Then
                                    Call MRPA.RPA_TAB_To_Next_Field
                                Else
                                    
                                    If cRPA.DataType = "DATE" Then
                                        'cRPA.CurrentValue = CStr(Format(cRPA.CurrentValue, "ddMMyyyy"))
                                        cRPA.CurrentValue = CStr(Format(cRPA.CurrentValue, "MMddyyyy"))
                                    End If
                                
                                    Call MRPA.RPA_Send_Value_And_NextField(cRPA.CurrentValue)
                                End If
                                
                            Case "DCLICK", "DOUBLECLICK"
                                Call MRPA.LAS_Click_Section(dicMousePositions, cRPA.Section, dclick:=True)
                                
                            Case "CLICK"
                                Call MRPA.LAS_Click_Section(dicMousePositions, cRPA.Section)
                                Call MRPA.RPA_TAB_To_Next_Field
                                Sleep gMouseDelayMS
                                
                            Case "CLICK-N"
                                Call MRPA.LAS_Click_Section(dicMousePositions, cRPA.Section)
                                
                                If cRPA.CurrentValue <> vbNullString Then
                                    Call MRPA.RPA_Send_Value_And_NextField(cRPA.CurrentValue)
                                End If
                                
                                Sleep gMouseDelayMS
                                
                            Case "CLICK-N-SELECTALL"
                                Call MRPA.LAS_Click_Section(dicMousePositions, cRPA.Section)
                                
                                Call MRPA.RPA_Send_Keys(RPA_KEYS_CTRL_A)
                                
                                
                                If cRPA.CurrentValue <> vbNullString Then
                                    Call MRPA.RPA_Send_Value_And_NextField(cRPA.CurrentValue)
                                End If
                                
                                Sleep gMouseDelayMS
                                
                            Case "CLICK-BOOL"
                                Select Case UCase(cRPA.CurrentValue)
                                    Case "1", "YES", "Y", "S", "SIM"
                                        Call MRPA.LAS_Click_Section(dicMousePositions, cRPA.Section)
                                End Select

                                Call MRPA.RPA_TAB_To_Next_Field
                                
                                Sleep gMouseDelayMS
                                
                            Case "FLAG"
                                Select Case UCase(cRPA.CurrentValue)
                                    Case "1", "YES", "Y", "S", "SIM"
                                        Call MRPA.RPA_Send_Keys(RPA_KEYS_SPACE)
                                End Select
                                
                                Call MRPA.RPA_TAB_To_Next_Field
                                
                            Case "COMBOLIST"
                                Call MRPA.RPA_Combolist(cRPA.DataType, cRPA.CurrentValue)
                                
                            Case "EXECUTION"
                                PrimaryKeyChild = aData(iData, dicColumnsData(cRPA.Field))
                                Call RPA_Execution(cRPA.Section, PrimaryKeyChild, cRPA.Field, dicMousePositions)
                            
                            Case "CHECK"
                                If Not RPA_CheckFieldValue Then
                                    Call MInterface.Message_Error("Failed to save!")
                                    GoTo nextiData
                                End If
                                
                            Case "KEY"
                                Call RPA_Key(cRPA.Field)
                                
                        End Select
                    End If 'If LogDataOnly Then
                    
                    If Not Check_TopWindow_Handle() Then
                        'Check if LAS is the top window
                        Call RPA_Warning(gHandle_Current_Top_Window, wksData, iData, ColumnPrimaryKey, dicColumnsData("STATUS"))
                    End If
                    
                Next iSequence
            End If
            wksData.Cells(iData, dicColumnsData("STATUS")) = "OK"
nextiData:
        Next iData
    End If
End Sub
Sub RPA_Key(Key As String)
    Select Case UCase(Key)
        Case "ESC"
            Call MRPA.RPA_Send_Keys(RPA_KEYS_ESC)
        Case "Y", "YES", "S", "SIM"
            Call MRPA.RPA_Send_Keys("Y")
    End Select
End Sub
Sub RPA_Warning(hndl As LongLong, wksData As Worksheet, iData As Long, ColumnPrimaryKey As Long, ColumnStatus As Long)
    Dim wResponse As WarningResponse
    
    wResponse = Check_Warning_Window(gHandle_Current_Top_Window)
    Call Print_Warning(wResponse)
    
    Select Case UCase(wResponse.Action)
        
        Case "MISSING"
            
            wksData.Cells(iData, ColumnPrimaryKey).Interior.Color = RGB(255, 0, 0)
            wksData.Cells(iData, ColumnStatus) = wResponse.Message
            
            Call err.Raise(999, , "Warning Action not found!")
            
        Case "ERROR"
            wksData.Cells(iData, ColumnPrimaryKey).Interior.Color = RGB(255, 0, 0)
            wksData.Cells(iData, ColumnStatus) = wResponse.Message
            
            Call MRPA.RPA_Send_Keys(RPA_KEYS_ESC)
            Call MRPA.RPA_Send_Keys("Y")
            
            Call err.Raise(999, , wResponse.Message)
            
        Case "KEY"
            Call RPA_Key(wResponse.Key)
    End Select
    
    
End Sub
Function RPA_Warnings(w As String) As WarningResponse
    Dim Warnings() As String
    Dim Item As Variant
    Dim Warning As Variant
    Dim wResponse As WarningResponse
    Dim currentResponse As WarningResponse
    
    Warnings = Split(w, ";")
    
    wResponse = New_Warning_Response
    
    For Each Item In Warnings
        If Item <> vbNullString Then
            
            Select Case UCase(Item)
                Case "LIST_WARNINGS"
                    '--warning list
                    currentResponse = Check_Warning_Window(0) ' TODO fix handle should not be 0
                    Log_Step ("Warning response")
                    
                    Select Case UCase(currentResponse.Action)
                        Case "WARNING"
                            Call MRPA.RPA_Send_Keys(RPA_KEYS_ESC)
                        Case "ERROR"
                            Log_Step (currentResponse.Window)
                            Log_Step (currentResponse.Action)
                            Log_Step (currentResponse.Message)
                            
                            wResponse.Status = currentResponse.Status
                            wResponse.Action = currentResponse.Action
                            
                            wResponse.Message = wResponse.Message & currentResponse.Message
                            
                            Call MRPA.RPA_Send_Keys(RPA_KEYS_ESC)
                            
                            
                    End Select
                    Sleep SLEEP_CURRENT_MS
                    
                    
                Case ""
                Case "STOP"
                    Stop
                Case "ESC"
                    Call MRPA.RPA_Send_Keys(RPA_KEYS_ESC)
                Case "ENTER"
                    Call MRPA.RPA_Send_Keys(RPA_KEYS_ENTER)
                Case Else
                    Call MRPA.RPA_Send_Keys(CStr(w))
            End Select
        End If
    Next Item
    
    RPA_Warnings = wResponse
End Function
Function New_Warning_Response() As WarningResponse
    New_Warning_Response.Window = vbNullString
    New_Warning_Response.Status = vbNullString
    New_Warning_Response.Message = vbNullString
    New_Warning_Response.Key = vbNullString
End Function
Sub Print_Warning(wResponse As WarningResponse)
    Debug.Print "---------------"
    Debug.Print "Window: " & wResponse.Window
    Debug.Print "Action: " & wResponse.Action
    Debug.Print "Status: " & wResponse.Status
    Debug.Print "Message: " & wResponse.Message
    Debug.Print "Pattern: " & wResponse.Pattern
    Debug.Print "Key: " & wResponse.Key
    Debug.Print "---------------"
End Sub
Function Check_Warning_Window(hndlWarning As LongLong, Optional resetList As Boolean = False) As WarningResponse
    Const SUB_NAME As String = "Check_Warning_Window"
    Dim iList As Long
    Dim hndlMessage As LongLong
    
    Dim Window As String
    Dim Message As String
    
    Dim r As WarningResponse
    
    If resetList Then
        aList_Warnings = Get_Current_Region(Get_Model_Range("List_Warnings"))
    End If
    
    Check_Warning_Window = New_Warning_Response
    If hndlWarning <> 0 Then
        Window = RPA_Get_Handle_Text(hndlWarning)
        Message = Get_Dialog_Message(hndlWarning)
        Call Log_Step(SUB_NAME)
        Call Print_Window_Detail(hndlWarning)
        
        For iList = LBound(aList_Warnings) + 1 To UBound(aList_Warnings)
            
            r.Window = aList_Warnings(iList, enmListWarning_Window)
            r.Action = aList_Warnings(iList, enmListWarning_Action)
            r.Status = aList_Warnings(iList, enmListWarning_Status)
            r.Message = aList_Warnings(iList, enmListWarning_Message)
            r.Pattern = aList_Warnings(iList, enmListWarning_Pattern)
            r.Key = aList_Warnings(iList, enmListWarning_Key)
            If Message <> vbNullString Then
                If Message = r.Message Then
                    Check_Warning_Window = r
                    Exit Function
                End If
                If r.Pattern <> vbNullString Then
                    If MRegex.Regx_Match(Message, r.Pattern) Then
                        Check_Warning_Window = r
                        Exit Function
                    End If
                End If
            End If
            If Window <> vbNullString And r.Pattern <> vbNullString Then
                If MRegex.Regx_Match(Window, r.Pattern) Then
                    Check_Warning_Window = r
                    Exit Function
                End If
            End If
            
            
        Next iList
        
        Check_Warning_Window.Action = "MISSING"
        Check_Warning_Window.Message = "Missing Handler! Window: " & Window & " | Message: '" & Message & "'"
        
    End If
    
    
    
End Function
Function Get_Dialog_Message(hndlWarning As LongLong) As String
    Dim hndlFirstChild As LongLong
    Dim dicHandles As Dictionary
    

    Dim h As LongLong
    Dim CurrentHandle As LongLong
    Dim LastHandle As LongLong
    'h = 4196808
    
    Set dicHandles = New Dictionary
    Get_Dialog_Message = vbNullString
    'hndlWarning = h
    
    Call dicHandles.Add(CStr(hndlWarning), CStr(RPA_Get_Handle_Text(hndlWarning)))
    
    If hndlWarning <> 0 Then
    
        hndlFirstChild = GetNextWindow(hndlWarning, GetWndConsts.GW_CHILD)
        Call dicHandles.Add(CStr(hndlFirstChild), RPA_Get_Handle_Text(hndlFirstChild))
        CurrentHandle = hndlFirstChild
        Do While CurrentHandle <> 0
            LastHandle = CurrentHandle
            CurrentHandle = GetNextWindow(CurrentHandle, GetWndConsts.GW_HWNDNEXT)
            If Not dicHandles.Exists(CStr(CurrentHandle)) Then
                Call dicHandles.Add(CStr(CurrentHandle), RPA_Get_Handle_Text(CurrentHandle))
            End If
        Loop
        
        Get_Dialog_Message = (dicHandles.Item(CStr(LastHandle)))
    End If
    
End Function
Function RPA_Get_Handle_Text(ByVal hndl As LongLong) As String
    Dim textLenght As Long
    Dim hndlText As String
    Dim iText As LongLong
    
    hndlText = String(255, vbNullChar)
    textLenght = GetWindowTextLength(hndl)
    iText = GetWindowText(hndl, hndlText, 255)
    RPA_Get_Handle_Text = Trim(Left(hndlText, InStr(1, hndlText, vbNullChar) - 1))

End Function
Function RPA_CheckFieldValue() As Boolean
    
    Dim MyData As DataObject
    Dim s As String
    Set MyData = New DataObject
    
    Sleep SLEEP_CURRENT_MS
    MRPA.RPA_Send_Keys (RPA_KEYS_CTRL_C)
    
    Call MyData.GetFromClipboard
    
    s = Trim(MyData.GetText)
    If Len(s) > 0 Then
        RPA_CheckFieldValue = True
    Else
        RPA_CheckFieldValue = False
    End If
End Function

Sub LAS_Add(ByVal f As String)
    LAS_Activate
    Select Case UCase(f)
        Case "MANUAL INVOICE"
            Call LAS_Department_Invoices_ManualInvoices_Add
    End Select
    
    Sleep 2000
End Sub


