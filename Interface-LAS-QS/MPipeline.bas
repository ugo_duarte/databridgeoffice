Attribute VB_Name = "MPipeline"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MPipeline"
Const TYPE_BOOKED As String = "1 - Booked"
Const TYPE_ADJUSTED As String = "2 - Adjusted"
Const TYPE_CONFIRMED As String = "3 - Confirmed"

Sub Generate_Pipeline()
    Const SUB_NAME As String = "Generate Pipeline"
    
    Call MPipeline.Generate_Pipeline_Booked
    Call MPipeline.Generate_Pipeline_Adjusted
    Call MPipeline.Generate_Pipeline_Confirmed
    Call MPipeline.Generate_Pipeline_Confirmed_Customers
    Call MPipeline.Generate_Pipeline_Invoices_Payable
    
    Call ThisWorkbook.RefreshAll
    
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksReport_Pipeline")
    wks.Columns.AutoFit
    
End Sub
Sub Generate_Pipeline_Adjusted()
    Const SUB_NAME As String = "Generate Pipeline Adjusted"
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim aData() As Variant
    Dim aReport() As Variant
    Dim aModel() As Variant
    
    Dim i As Long
    Dim j As Long
    Dim ColumnSchedule As Long
    Dim ColumnMLA As Long
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Database")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksReport_Pipeline_Adjusted")
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Adjusted")
    
    ColumnSchedule = Get_Column_Number(aModel, "Schedule")
    ColumnMLA = Get_Column_Number(aModel, "MLA")
    
    aData = Get_Data(aModel, aModel, aData, False, True)
    
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Adjusted")
    aReport = Filter_Array(aData, Get_Column_Number(aModel, "TRAF Date"), "", True)
    aReport = Remove_Header_From_Array(aReport, 1)
    For i = LBound(aReport) To UBound(aReport)
        aReport(i, ColumnMLA) = Left(aReport(i, ColumnSchedule), 4)
    Next i
    
    Call MStructure.Generate_Worksheet_From_Model(wksReport, True, "Model_Pipeline_Adjusted")
    wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(UBound(aReport), UBound(aReport, 2)) = aReport
    wksReport.Columns.AutoFit
    
End Sub

Sub Generate_Pipeline_Booked()
    Const SUB_NAME As String = "Generate Pipeline Booked"

    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim aData() As Variant
    Dim aReport() As Variant
    Dim aModel() As Variant
    
    Dim i As Long
    Dim j As Long
    Dim ColumnSchedule As Long
    Dim ColumnMLA As Long
    Dim ValidFilter As Variant
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Database")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksReport_Pipeline_Booked")
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Booked")
    
    ColumnSchedule = Get_Column_Number(aModel, "Schedule")
    ColumnMLA = Get_Column_Number(aModel, "MLA")
    
    aData = Get_Data(aModel, aModel, aData, False, True)
    
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Booked")
    ValidFilter = MStructure.Get_Model_Range("Global_Data_Inicio_Pipeline_Booked")
    aReport = Filter_Array(aData, Get_Column_Number(aModel, "TRAF Date"), ValidFilter, True, , ">=")
    aReport = Remove_Header_From_Array(aReport, 1)
    For i = LBound(aReport) To UBound(aReport)
        aReport(i, ColumnMLA) = Left(aReport(i, ColumnSchedule), 4)
    Next i
    
    Call MStructure.Generate_Worksheet_From_Model(wksReport, True, "Model_Pipeline_Adjusted")
    wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(UBound(aReport), UBound(aReport, 2)) = aReport
    wksReport.Columns.AutoFit
End Sub
Sub Generate_Pipeline_Confirmed()
    Const SUB_NAME As String = "Generate Pipeline Confirmed"
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim aData() As Variant
    Dim aReport() As Variant
    Dim aModel() As Variant
    
    Dim i As Long
    Dim j As Long
    Dim ColumnSchedule As Long
    Dim ColumnMLA As Long
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Database")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksReport_Pipeline_Confirmed")
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Confirmed")
    
    ColumnSchedule = Get_Column_Number(aModel, "Schedule")
    ColumnMLA = Get_Column_Number(aModel, "MLA")
    
    aData = Get_Data(aModel, aModel, aData, False, True)
    
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Confirmed")
    aReport = Filter_Array(aData, Get_Column_Number(aModel, "TRAF Date"), "", True)
    aReport = Remove_Header_From_Array(aReport, 1)
    For i = LBound(aReport) To UBound(aReport)
        aReport(i, ColumnMLA) = Left(aReport(i, ColumnSchedule), 4)
    Next i
    
    Call MStructure.Generate_Worksheet_From_Model(wksReport, True, "Model_Pipeline_Confirmed")
    wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(UBound(aReport), UBound(aReport, 2)) = aReport
    wksReport.Columns.AutoFit
End Sub
Sub Generate_Pipeline_Confirmed_Customers()
    Const SUB_NAME As String = "Generate Pipeline Confirmed Customers"
   Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim aData() As Variant
    Dim aReport() As Variant
    Dim aModel() As Variant
    
    Dim i As Long
    Dim j As Long
    Dim ColumnSchedule As Long
    Dim ColumnMLA As Long
    Dim ValidFilter As Variant
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Database")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksReport_Confirmed_Customers")
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Confirmed_Customers")
    
    ColumnSchedule = Get_Column_Number(aModel, "Schedule")
    ColumnMLA = Get_Column_Number(aModel, "MLA")
    
    aData = Get_Data(aModel, aModel, aData, False, True)
    
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Confirmed_Customers")
    ValidFilter = MStructure.Get_Model_Range("Global_Data_Inicio_Pipeline")
    aReport = aData
    aReport = Filter_Array(aReport, Get_Column_Number(aModel, "TRAF Date"), "", True)
    aReport = Filter_Array(aReport, Get_Column_Number(aModel, "DO Date"), ValidFilter, True, , ">=")
    ValidFilter = MStructure.Get_Model_Range("Global_Data_Fim_Pipeline")
    aReport = Filter_Array(aReport, Get_Column_Number(aModel, "DO Date"), ValidFilter, True, , "<=")
    
    aReport = Remove_Header_From_Array(aReport, 1)
    For i = LBound(aReport) To UBound(aReport)
        aReport(i, ColumnMLA) = Left(aReport(i, ColumnSchedule), 4)
    Next i
    
    Call MStructure.Generate_Worksheet_From_Model(wksReport, True, "Model_Pipeline_Confirmed_Customers")
    wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(UBound(aReport), UBound(aReport, 2)) = aReport
    wksReport.Columns.AutoFit
End Sub
Sub Generate_Pipeline_Invoices_Payable()
    Const SUB_NAME As String = "Generate Pipeline Invoices Payable"
   Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim aData() As Variant
    Dim aReport() As Variant
    Dim aModel() As Variant
    
    Dim i As Long
    Dim j As Long
    Dim ColumnSchedule As Long
    Dim ColumnContrato As Long
    Dim ColumnType As Long
    Dim ColumnMLA As Long
    Dim ValidFilter As Variant
    
    Dim dicBooked As Dictionary
    Dim dicAdjusted As Dictionary
    
    Set dicBooked = MStructure.Load_Dictionary("Booked_I", , 0)
    Set dicAdjusted = MStructure.Load_Dictionary("Adjusted_I", , 0)
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Database_NF_Payable")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksReport_Invoices_Payable")
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Invoices_Payable")
    
    ColumnSchedule = Get_Column_Number(aModel, "Contrato")
    ColumnType = Get_Column_Number(aModel, "Type")
    ColumnMLA = Get_Column_Number(aModel, "MLA")
    
    aData = Get_Data(aModel, aModel, aData, False, True)
    
    aModel = MStructure.Get_Model_Range("Model_Pipeline_Invoices_Payable")
    ValidFilter = MStructure.Get_Model_Range("Global_Data_Inicio_Pipeline")
    
    aReport = aData
    aReport = Filter_Array(aReport, Get_Column_Number(aModel, "Recebto NF"), ValidFilter, True, , ">=")
    ValidFilter = MStructure.Get_Model_Range("Global_Data_Fim_Pipeline")
    aReport = Filter_Array(aReport, Get_Column_Number(aModel, "Recebto NF"), ValidFilter, True, , "<=")
    
    aReport = Remove_Header_From_Array(aReport, 1)
    For i = LBound(aReport) To UBound(aReport)
        aReport(i, ColumnSchedule) = Trim(aReport(i, ColumnSchedule))
        aReport(i, ColumnMLA) = Left(Trim(aReport(i, ColumnSchedule)), 4)
        
        If dicBooked.Exists(aReport(i, ColumnSchedule)) Then
            aReport(i, ColumnType) = TYPE_BOOKED
        ElseIf dicAdjusted.Exists(aReport(i, ColumnSchedule)) Then
            aReport(i, ColumnType) = TYPE_ADJUSTED
        Else
            aReport(i, ColumnType) = TYPE_CONFIRMED
        End If
        
        
    Next i
    
    Call MStructure.Generate_Worksheet_From_Model(wksReport, True, "Model_Pipeline_Invoices_Payable")
    wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(UBound(aReport), UBound(aReport, 2)) = aReport
    wksReport.Columns.AutoFit
End Sub
