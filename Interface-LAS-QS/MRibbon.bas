Attribute VB_Name = "MRibbon"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MRibbon"

'==================================================
' Ribbon CHG-Meridian

'MAIN
Sub RibbonMainNavigationHome(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Go Home"
    Call MInterface.Activate_Main_Menu
End Sub
Sub RibbonMainNavigationReports(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksMenu_Reports")
End Sub
Sub RibbonMainNavigationGlobals(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_Globals")
End Sub
Sub RibbonMainCleanWorkbook(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Clean Workbook"
    Call MMain.Clean_Click
End Sub
Sub RibbonMainRefresh(control As IRibbonControl)
    Call Refresh_Click
End Sub
Sub RibbonMainRefreshDatabase(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call Login_Database
    Call MGlobal.Global_Finalize
End Sub
Sub RibbonMainOpenLAS(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Open LAS"
    On Error GoTo errHandler
    Call LAS_Open
    Exit Sub
errHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
End Sub

'==================================================
'Access
Sub RibbonAccessLogin(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Login"
    Call MSecurity.Login_Environ
End Sub
Sub RibbonAccessUnlockAll(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Unlock All"
    Call MMain.Unlock_All_Click
End Sub

'==================================================
'About
Sub RibbonAboutHelp(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksHelp")
End Sub
Sub RibbonAboutCHG(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon About CHG"
    Call Workbook_Login
End Sub

'==============================================================================================================
'--------------------------------------------------------------------------------------------------------------

'==================================================
'Navigation
'        Data
Sub RibbonNavigationDataAddresses(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DMSAddresses")
End Sub
Sub RibbonNavigationDataLeaseSchedules(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_LeaseSchedules")
End Sub
Sub RibbonNavigationDataPurchases(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_Purchases")
End Sub

'        From-To
Sub RibbonNavigationFromToLists(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_From_To_Lists")
End Sub
Sub RibbonNavigationFromToCashflowContracts(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_From_To_Contracts")
End Sub
Sub RibbonNavigationFromToCashflowCustomers(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_From_To_Customers")
End Sub
Sub RibbonNavigationFromToCashflowSuppliers(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_From_To_Suppliers")
End Sub

'        Report
Sub RibbonNavigationInterfaceCashflowBeneficiaries(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Interface_Beneficiary")
End Sub
Sub RibbonNavigationInterfaceCashflowSchedules(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Interface_Schedules")
End Sub
Sub RibbonNavigationInterfaceCashflowInvoices(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Interface_Invoices")
End Sub
Sub RibbonNavigationInterfaceXMLLAS(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Interface_XMLLAS")
End Sub

'       Add / Updates
Sub RibbonAdd(control As IRibbonControl)
    Call Add_Click
End Sub

Sub RibbonUpdateData(control As IRibbonControl)
    Call MMain.Update_Data_Click
End Sub
Sub RibbonUpdateDMSDeals(control As IRibbonControl)
    Call MAccess.DMS_Deals
End Sub
Sub RibbonUpdateReports(control As IRibbonControl)
    Call MModels.Refresh_Models_Access_Named_Range
    Call frmUpdateReport.Show
End Sub


'==================================================
'PROJECT_LAS_QS_SCHEDULES - Interface LAS-QS Schedules

Sub RibbonGenerateFraming(control As IRibbonControl)
    Call MMain.Generate_Interface_Framing_Click
End Sub
Sub RibbonGenerateLASQS(control As IRibbonControl)
    Call MMain.Generate_Interface_Qualisoft_Schedules_Click
End Sub

'==================================================
'PROJECT_LAS_QS_ASSETS - Interface LAS-QS Assets



'==================================================
'PROJECT_REPORT_UPDATE - Report Update

Sub RibbonImportDealPrint(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Import Deal Print"

    Call MMain.Import_Deal_Details_Click

End Sub
Sub RibbonImportString(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Import String"

    Dim formImport As frmImportString
    Set formImport = New frmImportString
    
    Call MGlobal.Global_Initialize
    formImport.Show
    Call MGlobal.Global_Finalize
End Sub


'==================================================
'PROJECT_PIPELINE - "Pipeline"
Sub RibbonNavigationPipelineBooked(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Pipeline_Booked")
End Sub
Sub RibbonNavigationPipelineAdjusted(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Pipeline_Adjusted")
End Sub
Sub RibbonNavigationPipelineConfirmed(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Pipeline_Confirmed")
End Sub
Sub RibbonNavigationPipelineDashboard(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_Pipeline")
End Sub
Sub RibbonNavigationPipeline(control As IRibbonControl)
    Call Activate_Worksheet_By_Codename("wksReport_Pipeline")
End Sub
Sub RibbonGeneratePipeline(control As IRibbonControl)
    Call MMain.Generate_Pipeline_Click
End Sub

'==================================================
'PROJECT_LAS_CASHFLOW - "Interface LAS-Cashflow"
'    Import
Sub RibbonImportLASPurchases(control As IRibbonControl)
    Call MMain.Import_Data_Purchases_Click
End Sub

'    Generate
Sub RibbonGenerateInterfaceCashflowBeneficiaries(control As IRibbonControl)
    Call MMain.Generate_Interface_LAS_Cashflow_Beneficiaries_Click
End Sub
Sub RibbonGenerateInterfaceCashflowInvoices(control As IRibbonControl)
    Call MMain.Generate_Interface_LAS_Cashflow_Invoices_Click
End Sub
Sub RibbonGenerateInterfaceCashflowSchedules(control As IRibbonControl)
    Call MMain.Generate_Interface_LAS_Cashflow_Schedules_Click
End Sub

'    Export
Sub RibbonExportInterfaceCashflowBeneficiaries(control As IRibbonControl)
    Call MMain.Export_Interface_LAS_Cashflow_Beneficiaries_Click
End Sub
Sub RibbonExportInterfaceCashflowSchedules(control As IRibbonControl)
    Call MMain.Export_Interface_LAS_Cashflow_Schedules_Click
End Sub
Sub RibbonExportInterfaceCashflowInvoices(control As IRibbonControl)
    Call MMain.Export_Interface_LAS_Cashflow_Invoices_Click
End Sub


'==================================================
'Deal Details
Sub RibbonNavigationDealDetailsDetails(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DealDetails")
End Sub
Sub RibbonNavigationDealDetailsSuppliers(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_Suppliers")
End Sub
Sub RibbonNavigationDealDetailsDelivery(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DeliveryAddresses")
End Sub
Sub RibbonNavigationDealDetailsAssets(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_Assets")
End Sub
Sub RibbonNavigationDealDetailsDMS(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReportDMSDescription")
End Sub

'==================================================
'DMS Follow-up
Sub RibbonNavigationDMSFollowUpEmitted(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
End Sub
Sub RibbonNavigationDMSFollowUpApproved(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DMSDeals_Approved")
End Sub

Sub RibbonConfirmEmitted(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Confirm Emitted"

    Call MGlobal.Global_Initialize
    'Call MDMSDealFollowUp.ConfirmEmitted
    Call MDMSDealFollowUp.ConfirmEmitted_Access
    Call MGlobal.Global_Finalize

End Sub



'==================================================
'Interface XML-LAS Purchases
Sub RibbonImportNFEXML(control As IRibbonControl)
    Call MMain.Import_XML_NFE_Click
End Sub
Sub RibbonGenerateInterfaceXMLLAS(control As IRibbonControl)
    Call MMain.Generate_Interface_XML_LAS_Click
End Sub
Sub RibbonLASAddPurchasesAllFields(control As IRibbonControl)
    Call MAddPurchases.Add_Individual_Purchase
End Sub
Sub RibbonImportAddPurchaseData(control As IRibbonControl)
    Call MMain.Import_Data_Purchases_Click
End Sub
Sub RibbonGeneratePurchases(control As IRibbonControl)
    Call MMain.Generate_Purchases_Click
End Sub

'==================================================
'Interface XML-LAS Assets
Sub RibbonImportSerialNumbers(control As IRibbonControl)
    Call MMain.Import_SerialNumbers_Click
End Sub

Sub RibbonNavigationModelSerial(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_Model_SerialNumber")
End Sub
Sub RibbonNavigationSupplierManufacturer(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_Supplier_Manufacturer")
End Sub
Sub RibbonNavigationPurchaseInvoice(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksList_PurchasesInvoices")
End Sub
Sub RibbonNavigationSerialNumbers(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_SerialNumbers")
End Sub


Sub RibbonImportPurchasesInvoices(control As IRibbonControl)
    Call MMain.Import_PurchasesInvoices_Click
End Sub

'==================================================
'PROJECT_RH_BIRTHDAYS - RH Birthdays

Sub RibbonSearchBdays(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call MRHBirthdays.Search_Birthdays
    Call MGlobal.Global_Finalize
End Sub
Sub RibbonGenerateEmailBday(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call MRHBirthdays.Generate_Birthday_Email
    Call MGlobal.Global_Finalize
End Sub
Sub RibbonConfirmGenerated(control As IRibbonControl)
    Call MGlobal.Global_Initialize

    Select Case UCase(gProjectName)
        Case UCase(PROJECT_RH_BIRTHDAYS)
            Call MRHBirthdays.Confirm_Send_Email_Birthday
    End Select

    Call MGlobal.Global_Finalize
End Sub


'==================================================
'PROJECT_RH_BIRTHDAYS - RH Birthdays


'Callback for btnNavigationCutDates onAction
Sub RibbonNavigationCutDates(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_QS_Cut_Dates")
End Sub
'Callback for btnNavigationDataFraming onAction
Sub RibbonNavigationDataFraming(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_QS_Framing")
End Sub
'Callback for btnNavigationDataLAS onAction
Sub RibbonNavigationDataLASSchedules(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
End Sub

'Callback for btnNavigationDataLASIC onAction
Sub RibbonNavigationDataLASInstallation(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
End Sub

'Callback for btnNavigationDataDMS onAction
Sub RibbonNavigationDataDMS(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_DMS_DealPrintout")
End Sub

'Callback for btnNavigationDataLASAssets onAction
Sub RibbonNavigationDataLASAssets(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_InterfaceQSAssets")
End Sub

'Callback for btnNavigationDataLASAssetsTotals onAction
Sub RibbonNavigationTotalsLASAssets(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_InterfaceQSTotals")
End Sub
'Callback for btnNavigationInterfaceLASQS onAction
Sub RibbonNavigationInterfaceLASQS(control As IRibbonControl)

    Select Case UCase(Get_Project_Name)
        Case UCase(PROJECT_LAS_QS_SCHEDULES)
            Call MInterface.Activate_Worksheet_By_Codename("wksReport_QS_Schedules")
        Case UCase(PROJECT_LAS_QS_ASSETS)
            Call MInterface.Activate_Worksheet_By_Codename("wksReport_Interface_QS_Assets")
    End Select

End Sub

Sub RibbonImportLASLS(control As IRibbonControl)
    Call MMain.Import_LAS_Schedules_Administration_Click
End Sub
Sub RibbonImportLASIC(control As IRibbonControl)
    Call MMain.Import_LAS_InstallCertificate_Administration_Click
End Sub
Sub RibbonImportInterfaceDealPrint(control As IRibbonControl)
    Call MMain.Import_DMS_TXT_DealPrintout_Click
End Sub


'Callback for btnImportLASAssets onAction
Sub RibbonImportLASAssets(control As IRibbonControl)
    Call MMain.Import_LAS_Assets_Overview_Administration_Click
End Sub

'Callback for btnImportLASReportPurchases onAction
Sub RibbonImportLASReportPurchase(control As IRibbonControl)
    Call MMain.Import_LAS_Purchases_IC_Click
End Sub

'Callback for btnImportLASReportPID onAction
Sub RibbonImportLASReportPID(control As IRibbonControl)
    Call MMain.Import_LAS_Assets_Position_Purchases_Click
End Sub
Sub RibbonGenerateSchedulesInterface(control As IRibbonControl)
    Call MMain.Generate_Schedules_For_Interface_QS_Assets
End Sub
'Callback for btnGenerateAssets onAction
Sub RibbonGenerateLASQSAssets(control As IRibbonControl)
    Call MMain.Generate_Interface_Qualisoft_Assets_Click
End Sub


Sub RibbonImportNFEXMLCover(control As IRibbonControl)
    Call MMain.Import_XML_NFE_Cover_Click

End Sub


Sub RibbonUploadDMSDealPrintout(control As IRibbonControl)
    Call MAccess.Upload_DMS_Dealprintout
End Sub

Sub RibbonExportInterfaceQSAssets(control As IRibbonControl)
    Call MMain.Export_Interface_Qualisoft_Assets_Click
End Sub


'======================================================================================================
'------------------------------------------------------------------------------------------------------
' Ribbon CHG-Invoices
Sub RibbonCHGInvoicesUploadInvoices(control As IRibbonControl)
End Sub
Sub RibbonCHGInvoicesCapturarInvoices(control As IRibbonControl)
    Call MInvoices.Capture_Invoices_From_Folders
End Sub
Sub RibbonCHGInvoicesActivateInvoices(control As IRibbonControl)
    Dim rng As Range
    Set rng = ActiveCell
    Call MInvoices.Invoice_Activate_Deactivate(rng)
End Sub


'======================================================================================================
'------------------------------------------------------------------------------------------------------
' Ribbon Download Reports LAS
Sub RibbonCHGLASReport_ADM_LeaseSchedules(control As IRibbonControl)
    Call MRPA.LAS_Export_ADM_LeaseSchedules
End Sub
Sub RibbonCHGLASReport_ADM_InstalationCertificate(control As IRibbonControl)
    Call MRPA.LAS_Export_ADM_InstallationCertificate
End Sub
Sub RibbonCHGLASReport_ADM_Purchases(control As IRibbonControl)
    Call MRPA.LAS_Export_ADM_Purchases
End Sub
Sub RibbonCHGLASReport_ADM_Assets(control As IRibbonControl)
    Call MRPA.LAS_Export_ADM_Assets
End Sub


'======================================================================================================
'------------------------------------------------------------------------------------------------------
' Ribbon CHG-Configuration
Sub RibbonResizeLASWindow(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call MRPA.LAS_Activate
    Call MGlobal.Global_Finalize
End Sub
Sub RibbonResizeExcelWindow(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call MRPA.Excel_Activate
    Call MGlobal.Global_Finalize
End Sub


'Callback for btnNavigationInputData onAction
Sub RibbonNavigationInputData(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_Invoices")
End Sub

'Callback for btnNavigationReportLASManualInvoice onAction
Sub RibbonNavigationLASManualInvoices(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_ManualInvoices")
End Sub

'Callback for btnNavigationReportLASPrintLayout onAction
Sub RibbonNavigationLASPrintLayout(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_PrintLayout")
End Sub

'Callback for btnNavigationReportLASInvoiceBlocks onAction
Sub RibbonNavigationLASInvoiceBlocks(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_InvoiceBlocks")
End Sub

'Callback for btnNavigationReportLASBlockPositions onAction
Sub RibbonNavigationLASBlockPositions(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_BlockPositions")
End Sub

'Callback for btnImportData onAction
Sub RibbonImportData(control As IRibbonControl)

End Sub

'Callback for btnGenerateReports onAction
Sub RibbonGenerateReports(control As IRibbonControl)
    Call MRPA_InvoicesUSA.Generate_Invoices_Data

End Sub

'Callback for btnNavigationInputData onAction
Sub RibbonNavigationInputData(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksData_Invoices")
End Sub

'Callback for btnNavigationReportLASManualInvoice onAction
Sub RibbonNavigationLASManualInvoices(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_ManualInvoices")
End Sub

'Callback for btnNavigationReportLASPrintLayout onAction
Sub RibbonNavigationLASPrintLayout(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_PrintLayout")
End Sub

'Callback for btnNavigationReportLASInvoiceBlocks onAction
Sub RibbonNavigationLASInvoiceBlocks(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_InvoiceBlocks")
End Sub

'Callback for btnNavigationReportLASBlockPositions onAction
Sub RibbonNavigationLASBlockPositions(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksReport_BlockPositions")
End Sub

'Callback for btnImportData onAction
Sub RibbonImportData(control As IRibbonControl)
    Call MMain.Import_Data_Click
End Sub

'Callback for btnGenerateReports onAction
Sub RibbonGenerateReports(control As IRibbonControl)
    Call MRPA_InvoicesUSA.Generate_Invoices_Data
    ThisWorkbook.RefreshAll
End Sub

'Callback for btnRPAManualInvoices onAction
Sub RibbonRPAAddManualInvoices(control As IRibbonControl)
    Call MRPA_InvoicesUSA.Add_All_Manual_Invoices
End Sub

'Callback for btnRPAManualInvoices onAction
Sub RibbonRPAAddManualInvoices(control As IRibbonControl)
    Call MRPA_InvoicesUSA.Add_All_Manual_Invoices
End Sub
'Callback for btnRPAManualInvoiceSingle onAction
Sub RibbonRPAAddManualInvoiceSingle(control As IRibbonControl)
    Call Add_Single(enmRPASingleExecution_ManualInvoice)
End Sub

'Callback for btnRPAInvoiceBlock onAction
Sub RibbonRPAAddInvoiceBlock(control As IRibbonControl)
    Call Add_Single(enmRPASingleExecution_InvoiceBlock)
End Sub

'Callback for btnRPABlockPosition onAction
Sub RibbonRPAAddBlockPosition(control As IRibbonControl)
    Call Add_Single(enmRPASingleExecution_BlockPosition)
End Sub

'Callback for btnAccessLogin onAction
Sub RibbonAccessLogin(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon Login"
    Call MSecurity.Login_Environ
End Sub

'Callback for btnAboutHelp onAction
Sub RibbonAboutHelp(control As IRibbonControl)
    Call MInterface.Activate_Worksheet_By_Codename("wksHelp")
End Sub

'Callback for btnAboutCHG onAction
Sub RibbonAboutCHG(control As IRibbonControl)
    Const SUB_NAME As String = "Ribbon About CHG"
    Call Message_Information("Developer: Ugo Duarte" & vbCrLf & "ugo.duarte@chg-meridian.com", "Info")
End Sub
'Callback for btnResizeLASWindow onAction
Sub RibbonResizeLASWindow(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call MRPA.LAS_Activate
    Call MGlobal.Global_Finalize
End Sub
Sub RibbonResizeExcelWindow(control As IRibbonControl)
    Call MGlobal.Global_Initialize
    Call MRPA.Excel_Activate
    Call MGlobal.Global_Finalize
End Sub

'Callback for btnConfigureMousePositions onAction
Sub RibbonConfigureMousePositions(control As IRibbonControl)
End Sub
