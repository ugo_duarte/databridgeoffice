Attribute VB_Name = "MExport"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MExport"

Sub Export_Range_To_XLSX(NameReport As String, rng As Range)
    Const SUB_NAME As String = "Export Range To XLSX"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim ExportedFile As String
    Dim guid As String
    Dim tempWB As Workbook
    Dim tempWKS As Worksheet

    Application.DisplayAlerts = False


    guid = Format(Now, "yyyymmddhhmmss")
    ExportedFile = ThisWorkbook.Path & "\" & NameReport & "_" & guid & ".xlsx"
    
    
    Set tempWB = Application.Workbooks.Add(1)
    Set tempWKS = tempWB.Sheets(1)
    
    tempWKS.Cells.NumberFormat = "@"
    rng.Copy
    tempWKS.Range("A1").PasteSpecial xlPasteValues
    Application.CutCopyMode = False
    tempWB.SaveAs fileName:=ExportedFile
    tempWB.Close (False)

Finish:
    On Error GoTo 0
    Call MsgBox("Exported to:" & vbNewLine & ExportedFile, vbInformation)
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish
        
    
End Sub
Sub Export_CSV(rng As Range, fileName As String)
    Const SUB_NAME As String = "Export_CSV"
    On Error GoTo err
    Call Log_Step(SUB_NAME)

    Dim wkb As Workbook
    Dim CSVFileName As String

    CSVFileName = ThisWorkbook.Path & "\" & "CSV-Exported-" & fileName & "-" & VBA.Format(VBA.Now, "yyyyMMddhhmmss") & ".csv"

    rng.Copy
    Set wkb = Application.Workbooks.Add(1)
    With wkb
        '.Sheets(1).Cells.NumberFormat ("@")
        .Sheets(1).Range("A1").PasteSpecial xlPasteValues
        .SaveAs fileName:=CSVFileName, FileFormat:=xlCSV, CreateBackup:=False
        .Close
    End With
    

err:
    Application.DisplayAlerts = True
End Sub
Sub Export_CSV_Semicolon(rng As Range, fileName As String, Optional WithHeader As Boolean = True)
    Const SUB_NAME As String = "Export CSV Semicolon"
    On Error GoTo err
    Call Log_Step(SUB_NAME)

    Dim wkb As Workbook

    Dim aData() As Variant
    Dim i As Long
    Dim j As Long
    Dim CSVFileName As String
    Dim CSVString As String
    Dim CurrentString As String
    CSVString = vbNullString
    aData = rng
    For i = LBound(aData) To UBound(aData)
        CurrentString = vbNullString
        For j = LBound(aData, 2) To UBound(aData, 2)
            If Not WithHeader And i = 1 Then
            Else
                CurrentString = CurrentString & rng.Cells(i, j).Text
                If j <> UBound(aData, 2) Then
                    CurrentString = CurrentString & ";"
                End If
            
            End If
        Next j
        If CurrentString <> vbNullString Then
            CSVString = CSVString & CurrentString & vbCrLf
        End If
    Next i
    
    CSVFileName = ThisWorkbook.Path & "\" & "CSV-Exported-" & fileName & "-" & VBA.Format(VBA.Now, "yyyyMMddhhmmss") & ".csv"
    Call Export_TXT(CSVString, CSVFileName)
    Call MInterface.Message_Information("Exported to: " & CSVFileName, SUB_NAME)
err:
    Application.DisplayAlerts = True
End Sub
Function Export_TXT(textString As String, fileName As String)
    Const SUB_NAME As String = "Export TXT"
    Call Log_Step(SUB_NAME)

    'Dim FileNumber As Integer

    'FileNumber = FreeFile                   ' Get unused file number
    'Open fileName For Output As #FileNumber    ' Connect to the file
    'Print #FileNumber, textString           ' Append our string
    'Close #FileNumber                       ' Close the file
     
    With CreateObject("ADODB.Stream")
        .Type = 2: .Charset = "utf-8": .Open
        .WriteText textString
        .SaveToFile fileName, 2        ' saving in coding that you need
        .Close
    End With
     
End Function

