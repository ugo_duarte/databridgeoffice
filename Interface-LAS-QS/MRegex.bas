Attribute VB_Name = "MRegex"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MRegex"

Global Const REGEX_OPCOES As String = "igm"
Global regExMatches As MatchCollection
Global regEx As RegExp


Sub Global_Regex_Configure()
    Const SUB_NAME As String = "Global_Regex_Configure"
    If regEx Is Nothing Then
        Set regEx = New_Regex
    End If
End Sub
Function New_Regex() As RegExp
    Const SUB_NAME As String = "New Regex"

    Dim RX As RegExp
    Set RX = New RegExp
    
    RX.Global = True
    RX.IgnoreCase = True
    RX.MultiLine = True
    
    Set New_Regex = RX
    
End Function
Function Regex_Match_Dictionary(ByVal haystack As String, ByVal originalPattern As String, Optional ByVal options As String) As Scripting.Dictionary
    Const SUB_NAME As String = "Regex Match Dictionary"

    ' An implementation of regex which includes named groups and caching implemented in VBA.
    ' The 'Microsoft VBScript Regular Expressions 5.5' library must be referenced (in VBA-editor: Tools -> References).
    ' Parameters:
    '  - haystack: the string the regex is applied on.
    '  - originalPattern: the regex pattern with or without named groups.
    '    The group naming has to follow .net regex syntax: '(?<group name>group content)'.
    '    Group names may contain the following characters: a-z, A-Z, _ (underscore).
    '    Group names must not be an empty string.
    '  - options: a string that may contain:
    '     - 'i' (the regex will work case-insensitive)
    '     - 'g' (the regex will work globally)
    '     - 'm' (the regex will work in multi-line mode)
    '    or any combination of these.
    ' Returned value: a Scripting.Dictionary object with the following entries:
    '  - Item 0 or "0", 1 or "1" ... for the groups content/submatches,
    '    following the convention of VBScript_RegExp_55.SubMatches collection, which is 0-based.
    '  - Item Match.Count - 2 or "98" for the whole match, assuming that the number of groups is below.
    '  - Item Match.Count - 1 or "99" for number of groups/submatches.
    ' Changes compared to the original version:
    '  - Handles non-capturing and positive and negative lookahead groups.
    '  - Handles left parenthesis inside a character class.
    '  - Named groups do not count twice.
    '    E.g. in the original version the second named group occupies items 3 and 4 of the returned
    '    dictionary, in this revised version only item 1 (item 0 is the first named group).
    '  - Additional 'm' option.
    '  - Fixed fetching cached regexes.
    '  - Early binding.
    '  - Some code cleaning.
    ' For an example take a look at the 'TestRegexMatch' procedure above.

    Dim GroupsPattern As String
    Dim RealPattern As String
    Dim RealRegExp As VBScript_RegExp_55.RegExp
    Dim RealMatches As VBScript_RegExp_55.MatchCollection
    Dim ReturnData As Scripting.Dictionary
    Dim GroupNames As VBScript_RegExp_55.MatchCollection
    Dim Ctr As Integer

    ' Cache regexes and group names for optimisation.
    Static CachedRegExps As Scripting.Dictionary
    Static CachedGroupNames As Scripting.Dictionary

    ' Group 'meta'-regex used to detect named and unnamed capturing groups.
    Static GroupsRegExp As VBScript_RegExp_55.RegExp

    If CachedRegExps Is Nothing Then Set CachedRegExps = New Scripting.Dictionary
    If CachedGroupNames Is Nothing Then Set CachedGroupNames = New Scripting.Dictionary

    If GroupsRegExp Is Nothing Then
      Set GroupsRegExp = New VBScript_RegExp_55.RegExp
      ' Original version: GroupsRegExp.Pattern = "\((?:\?\<(.*?)\>)?"
      GroupsRegExp.Pattern = "\((?!(?:\?:|\?=|\?!|[^\]\[]*?\]))(?:\?<([a-zA-Z0-9_]+?)>)?"
        GroupsRegExp.Global = True
    End If

    ' If the pattern isn't cached, create it.
    If Not CachedRegExps.Exists("(" & options & ")" & originalPattern) Then

        ' Prepare the pattern for retrieving named and unnamed groups.
        GroupsPattern = Replace(Replace(Replace(Replace(originalPattern, "\\", "X"), "\(", "X"), "\[", "X"), "\]", "X")

        ' Store group names for optimisation.
        CachedGroupNames.Add "(" & options & ")" & originalPattern, GroupsRegExp.Execute(GroupsPattern)

        ' Create new VBScript regex valid pattern and set regex for this pattern.
        RealPattern = GroupsRegExp.Replace(originalPattern, "(")
        Set RealRegExp = New VBScript_RegExp_55.RegExp
        RealRegExp.Pattern = RealPattern

        ' Set regex options.
        For Ctr = 1 To Len(options)
            Select Case Mid(options, Ctr, 1)
                Case "i"
                    RealRegExp.IgnoreCase = True
                Case "g"
                    RealRegExp.Global = True
                Case "m"
                    RealRegExp.MultiLine = True
            End Select
        Next

        ' Store this regex for optimisation.
        CachedRegExps.Add "(" & options & ")" & originalPattern, RealRegExp
    End If
    
    ' Get matches.
    Set RealMatches = CachedRegExps.Item("(" & options & ")" & originalPattern).Execute(haystack)
    
    ' Get group names.
    Set GroupNames = CachedGroupNames.Item("(" & options & ")" & originalPattern)
    
    ' Create dictionary to return.
    Set ReturnData = New Scripting.Dictionary
    
    ' Fill dictionary with names and indexes as descibed in the remarks introducing this procedure.
    If RealMatches.Count > 0 Then
        For Ctr = 1 To GroupNames.Count
            If IsEmpty(GroupNames(Ctr - 1).SubMatches(0)) Then
                ReturnData.Add CStr(Ctr - 1), RealMatches(0).SubMatches(Ctr - 1)
            Else
                ReturnData.Add GroupNames(Ctr - 1).SubMatches(0), RealMatches(0).SubMatches(Ctr - 1)
            End If
        Next
        ReturnData.Add "98", RealMatches.Item(0)
        ReturnData.Add "99", GroupNames.Count
    Else
        Set ReturnData = Nothing
    End If
    
    
    ' Return the result.
    Set Regex_Match_Dictionary = ReturnData

End Function
Public Function Regx_Match(ByVal Text As String, ByVal Pattern As String)
    Const SUB_NAME As String = "Regx Match"

    Dim Match

    If regEx Is Nothing Then
        Call Global_Regex_Configure
    End If

    regEx.Pattern = Pattern

    Regx_Match = regEx.Test(Text)
    If Regx_Match Then
        Set regExMatches = regEx.Execute(Text)
        Exit Function
    End If

End Function
Public Function Regx_Position(Text, Pattern) As String
    Const SUB_NAME As String = "Regx Position"

    Dim Match
    Dim sRetorno As String


    If regEx Is Nothing Then
        Call Global_Regex_Configure
    End If

    regEx.Pattern = Pattern
    sRetorno = 0
    Set regExMatches = regEx.Execute(Text)

    For Each Match In regExMatches
        sRetorno = ";" & sRetorno & Match.FirstIndex
    Next

    sRetorno = Mid(sRetorno, 2) 'Corta o primeiro ';' da string
    Regx_Position = sRetorno

End Function
Public Function Regx_Replace(ByVal Text As String, ByVal Pattern As String, ByVal NewText As String)
    Const SUB_NAME As String = "Regx Replace"

    If regEx Is Nothing Then
        Call Global_Regex_Configure
    End If

    regEx.Pattern = Pattern
    Regx_Replace = regEx.Replace(Text, NewText)
End Function
