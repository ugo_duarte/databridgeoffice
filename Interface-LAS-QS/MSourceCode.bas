Attribute VB_Name = "MSourceCode"
Option Explicit
Option Private Module
Const MODULE_NAME = "MSourceCode"


Sub Get_All_Workbook_Names()
    Dim vName
    
    For Each vName In ThisWorkbook.Names
        vName.Visible = True
    Next vName
    
End Sub
Sub Get_All_Workbook_Shapes()
    Dim wks As Worksheet
    Dim shp As Shape
    Dim rng As Range
    Dim i As Long
    Dim wksTemp As Worksheet
    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
    Call MStructure.Clean_Worksheet(wksTemp)
    Set rng = wksTemp.Range("A1")
    rng.Value = "Get All Workbook Shapes"

    For Each wks In ThisWorkbook.Worksheets
        rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column) + 1).Value = "'---------------------------------"
        'Debug.Print "---------------------------------"
        rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = "''Sheet: " & wks.Name

        For Each shp In wks.Shapes
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & "''Shape: " & shp.Name
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & "''Top: " & shp.Top
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & "''Left: " & shp.Left
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "'Setup: "
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "set shp = " & wks.Codename & ".Shapes(" & DQUOTE & shp.Name & DQUOTE & ")"
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "shp.Top = " & shp.Top
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "shp.Left = " & shp.Left
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = ""
        Next shp

    Next wks

End Sub
Sub List_All_Named_Ranges()
    Const SUB_NAME As String = "List All Named Ranges"
    On Error GoTo ErrorHandler


    If MInterface.Message_Confirm("List ALL named ranges?") <> vbYes Then End
    Call MGlobal.Global_Initialize
    Dim vName As Name
    Dim rng As Range
    Dim aData(1 To 1, 1 To 4)
    Dim i As Long
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksConfig_NamedRangesControls")
    i = 1

    Call Generate_Worksheet_From_Model(wks, True, "NamedRangesControl_Header")

    For Each vName In ThisWorkbook.Names
        If InStr(vName.Name, "FilterDatabase") = 0 Then
            Set rng = wks.Range("A1").Offset(Get_Last_Row(wks, 1))
            aData(1, 1) = vName.Name
            aData(1, 2) = i
            'aData(1, 3) = vName.RefersToRange.Locked
            aData(1, 4) = vName.RefersTo
            rng.Resize(, 4) = aData

            wks.Hyperlinks.Add Anchor:=rng, Address:="", SubAddress:=vName.Name, TextToDisplay:=vName.Name
        End If
        i = i + 1
    Next vName

ErrorHandler:
    Call MGlobal.Global_Finalize
End Sub
Sub List_All_Visible_Named_Ranges()
    Const SUB_NAME As String = "List All Visible Named Ranges"
    On Error GoTo ErrorHandler

    Dim vName As Name
    Dim rng As Range
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksConfig_NamedRangesControls")

    Call Generate_Worksheet_From_Model(wks, True, "NamedRangesControl_Header")

    For Each vName In ThisWorkbook.Names
        If vName.Visible Then
            Set rng = wks.Range("A1").Offset(Get_Last_Row(wks, 1))
            rng.Value = vName.Name
            rng.Offset(, 1) = vName.Value
            wks.Hyperlinks.Add Anchor:=rng, Address:="", SubAddress:=vName.Name, TextToDisplay:=vName.Name
        End If
    Next vName

ErrorHandler:

End Sub
Sub Print_Workbook_All_Shapes_Configuration()
    Const SUB_NAME As String = "Print Workbook All Shapes Configuration"

    Dim wks As Worksheet
    Dim wksTemp As Worksheet
    Dim shp As Shape
    Dim rng As Range
    Dim i As Long

    Call MSecurity.Unprotect_All_Worksheet
    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
    Call MStructure.Clean_Worksheet(wksTemp)
    
    Set rng = wksTemp.Range("A1")
    rng.Value = "Get All Workbook Shapes"

    For Each wks In ThisWorkbook.Worksheets
        rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column) + 1).Value = "''---------------------------------"
        rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = "''Sheet: " & wks.Name

        For Each shp In wks.Shapes
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & "''Shape: " & shp.Name
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & "''Top: " & shp.Top
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & "''Left: " & shp.Left
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "'Setup: "
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "set shp = " & wks.Codename & ".Shapes(" & DQUOTE & shp.Name & DQUOTE & ")"
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "shp.Top = " & shp.Top
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = TAB4 & TAB4 & TAB4 & "shp.Left = " & shp.Left
            rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value = ""
        Next shp

    Next wks

End Sub
Sub Print_Worksheets_Codenames(Optional Text As String = vbNullString)
    Const SUB_NAME As String = "Print Worksheets Codenames"

    Dim wks As Worksheet
    For Each wks In ThisWorkbook.Worksheets
        Debug.Print wks.Codename & Text
    Next wks

End Sub
Sub Set_All_Workbook_Names_Visible()
    Const SUB_NAME As String = "Set All Workbook Names Visible"

    Dim vName As Name

    For Each vName In ThisWorkbook.Names
        vName.Visible = True
    Next vName

End Sub

