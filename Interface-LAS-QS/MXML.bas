Attribute VB_Name = "MXML"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MXML"

Global Const XML_ROW_XPATH As Long = 1
Global Const XML_ROW_TYPENODE As Long = 2
Global Const XML_ROW_CAPTION As Long = 3
Global Const XML_ROW_ATTRIB As Long = 4

Global searchNode               As MSXML2.IXMLDOMNode
Global searchNodeFound          As Boolean


Global lLastRowXMLDados         As Long
Global lLastRowXMLTable         As Long

Global gsImpostoTipo            As String
Global gsImpostoGrupo           As String

Global gdicXMLNodes             As Scripting.Dictionary
Global gdicXMLNodesChild        As Scripting.Dictionary


Global gaXML_Table()            As Variant
Global gaXML_TableN()           As Variant
Global gaXML_TableD()           As Variant

Global gaXML_Nodes()            As Variant

Global glRowDataXML             As Long
Global glRowDataXMLMax          As Long


Global gaXML_Fields_Extra()     As Variant



Public Type XMLXPathReader
    Column_XPath            As Long
    Column_Node0            As Long
    Column_Node1            As Long
    Column_Node2            As Long
    Column_Node3            As Long
    Column_Node4            As Long
    Column_Node5            As Long
    Column_Node6            As Long
    Column_NodeFinal        As Long
    Column_Import           As Long
    Column_Order            As Long
    Column_XPathType        As Long
    Column_Attribute        As Long
    Column_Tag              As Long
    Column_TagTreatment     As Long
End Type

Public Type DesignXML
    Column_Filename         As Long
    Column_XPath            As Long
    Column_BaseName         As Long
    Column_Value            As Long
    Column_AttributeName    As Long
    Column_AttributeValue   As Long
End Type

Private Function FindChildNode(oParentNode, ByVal Basename As String)
    
    Dim oChildNode As MSXML2.IXMLDOMNode
    Basename = UCase(Basename)
    If searchNodeFound Then
        Exit Function
    End If
    
    If UCase(oParentNode.Basename) = Basename Then
        Set searchNode = oParentNode
        searchNodeFound = True
        Exit Function
    Else
        If oParentNode.ChildNodes.Length = 0 Then
            Exit Function
        End If
        
        For Each oChildNode In oParentNode.ChildNodes
            Call FindChildNode(oChildNode, Basename)
        Next
    End If

Finish:
End Function

Public Sub Generate_XML_Fields()
    '-------------------------------------------------------'
    'Fun��o para gerar os campos selecionados XML           '
    '-------------------------------------------------------'
    'On Error GoTo ErrorHandler
    
    Dim conExcel                As ADODB.Connection
    Dim rstExcel                As ADODB.Recordset

    Dim Column As Long

    Dim rngTemp                 As Range
    Dim sCaminho                As String
    
    Dim strSQL                  As String
    Dim strSQLN                 As String
    Dim strSQLD                 As String
    Dim arrRecordSet            As Variant
    
    Dim i                       As Long
    Dim j                       As Long
    Dim jXMLNode                As Long
    
    Dim XXPR                    As XMLXPathReader
    Dim wksLayout               As Worksheet
    Dim wksData                 As Worksheet
    XXPR = New_Type_XMLXPathReader
    
    Set conExcel = New_ADODB_Connection(enmConnectionType_MyWorkbook)
    conExcel.Open
    Set rstExcel = New ADODB.Recordset
    
    Set wksLayout = Get_Worksheet_By_Codename("wksLayout_XML")
    Set wksData = Get_Worksheet_By_Codename("wksData_XML")
    
    strSQL = vbNullString
    strSQL = strSQL & "    SELECT *                           " & vbNewLine
    strSQL = strSQL & "      FROM [" & wksLayout.Name & "$]   " & vbNewLine
    strSQL = strSQL & "     WHERE [IMPORT] = 1                " & vbNewLine
    
    'Dependentes
    strSQLD = strSQL & "      AND [XPath Type] = 'D'           " & vbNewLine
    strSQLD = strSQLD & "   ORDER BY  [ORDER]                  " & vbNewLine
    
    'N Itens
    strSQLN = strSQL & "      AND [XPath Type] = 'N'           " & vbNewLine
    strSQLN = strSQLN & "   ORDER BY  [ORDER]                  " & vbNewLine
    
    '1 Item
    'strSQL = strSQL & "       AND [XPath Type] <> 'N'          " & vbNewLine
    strSQL = strSQL & "     ORDER BY  [ORDER], [XPath Type]    " & vbNewLine

    
    rstExcel.Open strSQL, conExcel
    gaXML_Table = rstExcel.GetRows
    rstExcel.Close
    
    rstExcel.Open strSQLD, conExcel
    gaXML_TableD = rstExcel.GetRows
    rstExcel.Close
    
    rstExcel.Open strSQLN, conExcel
    gaXML_TableN = rstExcel.GetRows
    rstExcel.Close
    
    '+2 Colunas Identificadores
    '+1 pois o array est� base 0
    jXMLNode = 1
    ReDim gaXML_Nodes(1 To 3, 1 To (UBound(gaXML_TableN, 2) + 1) + (UBound(gaXML_Table, 2) + 1))
    'For j = LBound(gaXML_TableN, 2) To UBound(gaXML_TableN, 2)
    '    gaXML_Nodes(1, jXMLNode) = gaXML_TableN(0, i)
    '    gaXML_Nodes(2, jXMLNode) = gaXML_TableN(11, i)
    '    gaXML_Nodes(3, jXMLNode) = gaXML_TableN(13, i)
    '    jXMLNode = jXMLNode + 1
    'Next j
    For j = LBound(gaXML_Table, 2) To UBound(gaXML_Table, 2)
        gaXML_Nodes(XML_ROW_XPATH, jXMLNode) = gaXML_Table(XXPR.Column_XPath - 1, j) ' -1 because array is 0 from Select
        gaXML_Nodes(XML_ROW_TYPENODE, jXMLNode) = gaXML_Table(XXPR.Column_XPathType - 1, j)
        gaXML_Nodes(XML_ROW_CAPTION, jXMLNode) = gaXML_Table(XXPR.Column_Tag - 1, j)
        jXMLNode = jXMLNode + 1
    Next j
    
    With wksData
        .Cells.Clear
        .Cells.NumberFormat = "@"
        .Cells(XML_ROW_XPATH, 1).Resize(UBound(gaXML_Nodes), UBound(gaXML_Nodes, 2)) = gaXML_Nodes
        .Cells(XML_ROW_CAPTION, Get_Last_Column(wksData, 1) + 1).Resize(, UBound(gaXML_Fields_Extra, 2)) = gaXML_Fields_Extra
    End With
    
    glRowDataXML = UBound(gaXML_Nodes) + 1
    glRowDataXMLMax = 0
        
    conExcel.Close
    
Finish:
    On Error GoTo 0
    Exit Sub
    
ErrorHandler:
    Call MsgBox(err.Description, vbCritical, MGlobal.MSG_COMPANY)
    GoTo Finish
    
End Sub

Function Get_Layout_Array(LayoutCodename As String, XPathType As String) As Variant
    Dim aLayout() As Variant
    Dim aData() As Variant
    Dim wksLayout As Worksheet
    Dim XXPR As XMLXPathReader
    
    Dim CountImport As Long
    Dim i As Long
    Dim iData As Long
    Dim j As Long
    
    
    XXPR = New_Type_XMLXPathReader
    Set wksLayout = MStructure.Get_Worksheet_By_Codename(LayoutCodename)
    aLayout = MStructure.Get_Current_Region(wksLayout.Range("A1"))
    CountImport = 0
    
    For i = LBound(aLayout) + 1 To UBound(aLayout)
        If CStr(aLayout(i, XXPR.Column_Import)) = "1" Then
            If aLayout(i, XXPR.Column_XPathType) = XPathType Or XPathType = vbNullString Then
                CountImport = CountImport + 1
            End If
        End If
    Next i
    
    
    
    '0- Coluna / 0 linha
    'ReDim aData(1 To CountImport, 1 To UBound(aLayout, 2))
    If CountImport < 1 Then
        CountImport = 1
    End If
    ReDim aData(1 To (UBound(aLayout, 2)), 1 To CountImport)
    iData = 1
    For i = LBound(aLayout) + 1 To UBound(aLayout)
        If CStr(aLayout(i, XXPR.Column_Import)) = "1" Then
        
            If aLayout(i, XXPR.Column_XPathType) = XPathType Or XPathType = vbNullString Then
                
                For j = LBound(aLayout, 2) To UBound(aLayout, 2)
                    
                    aData(j, iData) = aLayout(i, j)
                    
                Next j
                
                iData = iData + 1
                
            End If 'If aLayout(i, XXPR.Column_XPathType) = XPathType Or XPathType = vbNullString Then
        End If
    Next i
    aData = Sort_Matrix(aData, aLayout, "Order")
    Get_Layout_Array = aData
    
End Function

Sub T_Generate_XML_Fields_Array()
    
    Call Global_Initialize
    Call Generate_XML_Fields_Array
    Call Global_Finalize
    
End Sub

Public Sub Generate_XML_Fields_Array()
    Const SUB_NAME As String = "Generate XML Fields Array"
    '-------------------------------------------------------'
    'Fun��o para gerar os campos selecionados XML via array '
    '-------------------------------------------------------'
    'On Error GoTo ErrorHandler
    
    Dim wksLayout               As Worksheet
    Dim wksData                 As Worksheet
    Dim XXPR                    As XMLXPathReader
    Dim i As Long
    Dim j As Long
    Dim aLayout() As Variant
    Dim CountImport, CountD, CountN As Long
    Dim jXMLNode As Long
    
    
    Set wksLayout = Get_Worksheet_By_Codename("wksLayout_XML")
    Set wksData = Get_Worksheet_By_Codename("wksData_XML")
    XXPR = New_Type_XMLXPathReader
    
    aLayout = MStructure.Get_Current_Region(wksLayout.Range("A1"))
    
    gaXML_Table = Get_Layout_Array("wksLayout_XML", "")
    gaXML_TableN = Get_Layout_Array("wksLayout_XML", "N")
    gaXML_TableD = Get_Layout_Array("wksLayout_XML", "D")
    
    jXMLNode = 1
    
    ReDim gaXML_Nodes(1 To 3, 1 To (UBound(gaXML_TableN, 2) + 1) + (UBound(gaXML_Table, 2) + 1))
    'For j = LBound(gaXML_TableN, 2) To UBound(gaXML_TableN, 2)
    '    gaXML_Nodes(1, jXMLNode) = gaXML_TableN(0, i)
    '    gaXML_Nodes(2, jXMLNode) = gaXML_TableN(11, i)
    '    gaXML_Nodes(3, jXMLNode) = gaXML_TableN(13, i)
    '    jXMLNode = jXMLNode + 1
    'Next j
    For j = LBound(gaXML_Table, 2) To UBound(gaXML_Table, 2)
        gaXML_Nodes(XML_ROW_XPATH, jXMLNode) = gaXML_Table(XXPR.Column_XPath, j)
        gaXML_Nodes(XML_ROW_TYPENODE, jXMLNode) = gaXML_Table(XXPR.Column_XPathType, j)
        gaXML_Nodes(XML_ROW_CAPTION, jXMLNode) = gaXML_Table(XXPR.Column_Tag, j)
        jXMLNode = jXMLNode + 1
    Next j
    
    With wksData
        .Cells.Clear
        .Cells.NumberFormat = "@"
        .Cells(XML_ROW_XPATH, 1).Resize(UBound(gaXML_Nodes), UBound(gaXML_Nodes, 2)) = gaXML_Nodes
        .Cells(XML_ROW_CAPTION, Get_Last_Column(wksData, 1) + 1).Resize(, UBound(gaXML_Fields_Extra, 2)) = gaXML_Fields_Extra
    End With
    
    glRowDataXML = UBound(gaXML_Nodes) + 1
    glRowDataXMLMax = 0
    
    
Finish:
    On Error GoTo 0
    Exit Sub
    
ErrorHandler:
    Call MInterface.Message_Error(err.Description, MGlobal.MSG_COMPANY)
    GoTo Finish
    
    

    
End Sub

Sub Generate_XML_Fields_Extra(Optional CanEraseData As Boolean = True)
    gaXML_Fields_Extra = Get_Model_Range("Model_XML_Fields_Extra")
    If CanEraseData Then
        ReDim gaXML_Fields_Extra(LBound(gaXML_Fields_Extra) To UBound(gaXML_Fields_Extra), LBound(gaXML_Fields_Extra, 2) To UBound(gaXML_Fields_Extra, 2))
    End If
    
End Sub

Public Sub Generate_XML_NFS_Fields(xDoc As MSXML2.DOMDocument, ByVal fileName As String)
    Dim aLayoutXML_NFS()    As Variant
    Dim iRowXPathID         As Long
    
    Dim XPathID             As String
    Dim i                   As Long
    Dim j                   As Long
    Dim XXPR                As XMLXPathReader
    Dim xNodeID             As MSXML2.IXMLDOMNode
    Dim jStartingColumn     As Long
    Dim CountImportXPath    As Long
    Dim jNodes              As Long
    Dim wksData             As Worksheet
    Dim wksReport           As Worksheet
    
    'Process
    XXPR = New_Type_XMLXPathReader("Model_Layout_XML_NFS")
    aLayoutXML_NFS = Get_Current_Region(Get_Model_Range("LAYOUT_XML_NFS_I"))
    
    iRowXPathID = Get_Row_Number(aLayoutXML_NFS, "MODEL ID XPATH", XXPR.Column_Tag)
    jStartingColumn = XXPR.Column_XPath
    XXPR.Column_XPath = 0
    If iRowXPathID <> 0 Then
        
        
        For j = jStartingColumn To UBound(aLayoutXML_NFS, 2) 'XPATHS
            XPathID = Trim(aLayoutXML_NFS(iRowXPathID, j))
            XPathID = Replace(XPathID, "\\", "")
            XPathID = Replace(XPathID, "\", "/")
            If XPathID <> vbNullString Then
                Set xNodeID = xDoc.SelectSingleNode(XPathID)
                If Not xNodeID Is Nothing Then
                    XXPR.Column_XPath = j
                    Exit For
                End If
            End If
        Next j
        
        If XXPR.Column_XPath <> 0 Then
            CountImportXPath = 0
            For i = LBound(aLayoutXML_NFS) To UBound(aLayoutXML_NFS)
                If CStr(aLayoutXML_NFS(i, XXPR.Column_Import) = "1") Then
                    CountImportXPath = CountImportXPath + 1
                End If
            Next i
            ReDim gaXML_Nodes(1 To 3, 1 To CountImportXPath)
            jNodes = 1
            For i = LBound(aLayoutXML_NFS) To UBound(aLayoutXML_NFS)
                If CStr(aLayoutXML_NFS(i, XXPR.Column_Import) = "1") Then
                    gaXML_Nodes(XML_ROW_XPATH, jNodes) = aLayoutXML_NFS(i, XXPR.Column_XPath)
                    gaXML_Nodes(XML_ROW_TYPENODE, jNodes) = aLayoutXML_NFS(i, XXPR.Column_XPathType)
                    gaXML_Nodes(XML_ROW_CAPTION, jNodes) = aLayoutXML_NFS(i, XXPR.Column_Tag)
                    
                    jNodes = jNodes + 1
                End If
            Next i
            
            Call Clean_Model_XML(False)
            Set wksData = Get_Worksheet_By_Codename("wksData_XML")
            wksData.Range("A1").Resize(UBound(gaXML_Nodes), UBound(gaXML_Nodes, 2)) = gaXML_Nodes
            
            With wksData
                .Cells.Clear
                .Cells.NumberFormat = "@"
                .Cells(XML_ROW_XPATH, 1).Resize(UBound(gaXML_Nodes), UBound(gaXML_Nodes, 2)) = gaXML_Nodes
                Call Generate_XML_Fields_Extra(False)
                .Cells(XML_ROW_CAPTION, Get_Last_Column(wksData, 1) + 1).Resize(, UBound(gaXML_Fields_Extra, 2)) = gaXML_Fields_Extra
            End With
            
            Call XML_Import_V2(xDoc, fileName)
            Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE")
            Call XML_Generate_Temp_Data(wksReport, "A1")
            
        End If
    End If
    
End Sub

Function Get_Single_Node(xDoc As Object, ByVal ElementXPath As String) As IXMLDOMNode
    Const SUB_NAME As String = "Get Single Node"

    On Error GoTo ErrorHandler

    Dim xNode As IXMLDOMNode
    If ElementXPath <> vbNullString Then
        ElementXPath = Replace(ElementXPath, "\\", "")
        ElementXPath = Replace(ElementXPath, "\", "/")
        Set xNode = xDoc.SelectSingleNode(ElementXPath)
    End If

Finish:
    Set Get_Single_Node = xNode
    Exit Function

ErrorHandler:
    Set xNode = Nothing
    GoTo Finish
End Function

Function Get_XML_Design(fileName As String) As Variant
    Dim xDoc                As MSXML2.DOMDocument
    Dim xDocNode            As MSXML2.IXMLDOMElement
    Dim aData()             As Variant
    Dim IsMainNode          As Boolean
    Dim Column_XML          As Long
    Dim i                   As Long
    Dim j                   As Long
    Dim key                 As Variant
    Dim dx                  As DesignXML
    
    dx = New_Type_DesignXML
    
    Set xDoc = New MSXML2.DOMDocument
    With xDoc
        .async = False
        .validateOnParse = False
        .Load (fileName)
    End With
    
    Set xDocNode = xDoc.DocumentElement
    If Not xDocNode Is Nothing Then
        
        Set gdicXMLNodes = New Scripting.Dictionary
        Set gdicXMLNodesChild = New Scripting.Dictionary
    
        IsMainNode = True
        Column_XML = 0
        Call List_ChildNodes(xDocNode, lLastRowXMLDados + 1, "\", IsMainNode, Column_XML)
            
        ReDim aData(1 To gdicXMLNodes.Count, 1 To 6)
        i = 1
        For Each key In gdicXMLNodes.Keys
            
            Set xDocNode = gdicXMLNodes(key)
            
            aData(i, dx.Column_Filename) = fileName
            aData(i, dx.Column_XPath) = key
            aData(i, dx.Column_BaseName) = xDocNode.Basename
            
            If xDocNode.ChildNodes.Length = 1 Then
                If xDocNode.ChildNodes(0).Basename = "" Then
                    aData(i, dx.Column_Value) = xDocNode.nodeTypedValue
                End If
                
            End If
            
            If xDocNode.Attributes.Length > 0 Then
                aData(i, dx.Column_AttributeName) = xDocNode.Attributes(0).Basename
                aData(i, dx.Column_AttributeValue) = xDocNode.Attributes(0).nodeTypedValue
            Else
                aData(i, dx.Column_AttributeName) = vbNullString
                aData(i, dx.Column_AttributeValue) = vbNullString
            End If
            
            If aData(i, dx.Column_Value) <> vbNullString Or aData(i, dx.Column_AttributeValue) <> vbNullString Then
                i = i + 1
            End If
            
        Next key
        
    Else
        ReDim aData(1 To 1, 1 To 5)
    End If
    Get_XML_Design = aData
End Function

Private Sub List_ChildNodes(objParentNode As IXMLDOMNode, i, nivelXML As String, ByRef nodePrincipal As Boolean, colunaXML As Long)
    Dim oChildNode      As Object
    Dim nivelXML2       As String
    Dim tempStr         As String
    Dim subnivelXML2    As Long
    Dim subnivel        As Long
    Dim bUltimoNivel    As Boolean
    Dim Basename        As String
    Dim sChildName      As String
    
    
    
    If objParentNode.ChildNodes.Length = 0 Then
        Exit Sub
    End If

    subnivel = 0
    bUltimoNivel = False
    
    Basename = nivelXML & "\" & objParentNode.Basename
    If Not gdicXMLNodes.Exists(Basename) Then
        gdicXMLNodes.Add key:=Basename, Item:=objParentNode
    Else
        'Stop
    End If
    For Each oChildNode In objParentNode.ChildNodes
        
        If oChildNode.ChildNodes.Length > 0 Then
            

            'PREENCHE PLANILHA
            sChildName = Basename & "\" & oChildNode.Basename
            If Not gdicXMLNodes.Exists(sChildName) Then
                gdicXMLNodes.Add key:=sChildName, Item:=oChildNode 'objParentNode
            End If
            
            'WS.Cells(i, colunaXML + 1) = oChildNode.Text
            'WS.Cells(i, colunaXML + 2) = baseName
            If oChildNode.Attributes.Length > 0 Then
                'WS.Cells(i, colunaXML + 3) = oChildNode.Attributes(0).baseName
                'WS.Cells(i, colunaXML + 4) = oChildNode.Attributes(0).NodeValue
            End If
            'WS.Cells(i, colunaXML + 5) = nivelXML & "." & baseName '& "." & subnivel
            
            i = i + 1
            
            Call List_ChildNodes(oChildNode, i, Basename, nodePrincipal, colunaXML)
            
        Else
            
            If oChildNode.Basename <> "" Then
                
                'PREENCHE PLANILHA
                sChildName = Basename & "\" & oChildNode.Basename
                If Not gdicXMLNodesChild.Exists(sChildName) Then
                    gdicXMLNodesChild.Add key:=sChildName, Item:=oChildNode 'objParentNode
                End If
                
                'WS.Cells(i, colunaXML + 1) = oChildNode.Text
                'WS.Cells(i, colunaXML + 2) = baseName
                If oChildNode.Attributes.Length > 0 Then
                    'WS.Cells(i, colunaXML + 3) = oChildNode.Attributes(0).baseName
                    'WS.Cells(i, colunaXML + 4) = oChildNode.Attributes(0).NodeValue
                End If
                'WS.Cells(i, colunaXML + 5) = nivelXML & "." & baseName '& "." & subnivel
                
                i = i + 1
                
                Call List_ChildNodes(oChildNode, i, Basename, nodePrincipal, colunaXML)
            Else
                'PREENCHE PLANILHA
                sChildName = Basename
                If Not gdicXMLNodesChild.Exists(sChildName) Then
                    gdicXMLNodesChild.Add key:=sChildName, Item:=oChildNode 'objParentNode
                End If
            End If
        End If
        
nextchild:
    Next
Finish:
End Sub

Function New_Type_DesignXML(Optional Model As String = "Model_DesignXML_Header") As DesignXML
    Dim dx As DesignXML
    Dim aHeader() As Variant
    aHeader = Get_Model_Range(Model)
        
    dx.Column_Filename = Get_Column_Number(aHeader, "Filename")
    dx.Column_XPath = Get_Column_Number(aHeader, "XPath")
    dx.Column_BaseName = Get_Column_Number(aHeader, "BaseName")
    dx.Column_Value = Get_Column_Number(aHeader, "Value")
    dx.Column_AttributeName = Get_Column_Number(aHeader, "AttributeName")
    dx.Column_AttributeValue = Get_Column_Number(aHeader, "AttributeValue")
    
    New_Type_DesignXML = dx
End Function

Function New_Type_XMLXPathReader(Optional Model As String = "Model_Layout_XML_NFE") As XMLXPathReader
    Dim XXPR As XMLXPathReader
    Dim aHeader() As Variant
    aHeader = Get_Model_Range(Model)

    XXPR.Column_XPath = Get_Column_Number(aHeader, "XPath")
    XXPR.Column_Node0 = Get_Column_Number(aHeader, "Node 0")
    XXPR.Column_Node1 = Get_Column_Number(aHeader, "Node 1")
    XXPR.Column_Node2 = Get_Column_Number(aHeader, "Node 2")
    XXPR.Column_Node3 = Get_Column_Number(aHeader, "Node 3")
    XXPR.Column_Node4 = Get_Column_Number(aHeader, "Node 4")
    XXPR.Column_Node5 = Get_Column_Number(aHeader, "Node 5")
    XXPR.Column_Node6 = Get_Column_Number(aHeader, "Node 6")
    XXPR.Column_NodeFinal = Get_Column_Number(aHeader, "Node Final")
    XXPR.Column_Import = Get_Column_Number(aHeader, "Import")
    XXPR.Column_Order = Get_Column_Number(aHeader, "Order")
    XXPR.Column_XPathType = Get_Column_Number(aHeader, "XPath Type")
    XXPR.Column_Attribute = Get_Column_Number(aHeader, "Attribute")
    XXPR.Column_Tag = Get_Column_Number(aHeader, "Tag")
    XXPR.Column_TagTreatment = Get_Column_Number(aHeader, "Tag Treatment")

    New_Type_XMLXPathReader = XXPR
End Function

Public Function Range_To_XML_String(rng As Range, TableName As String, Optional HeaderRow As Long = 1) As String

    Dim strXML As String
    Dim varTable As Variant
    Dim intRow As Integer
    Dim intCol As Integer
    Dim intFileNum As Integer
    Dim strFilePath As String
    Dim strRowElementName As String
    Dim strTableElementName As String
    Dim varColumnHeaders As Variant

    Const TAG_ELEMENT As String = "@element"
    Const TAG_ATTRIBUTE As String = "@att"
    Const TAG_XML_OPEN As String = "<@element@att>"
    Const TAG_XML_CLOSE As String = "</@element>"
    Dim Element As String
    Dim ElementName As String
    Dim ElementRow As String
    Dim ElementRowName As String
    Dim aData() As Variant
    Dim i As Long
    Dim j As Long
    Dim TagOpen As String
    Dim TagClose As String
    Dim Attributee As String
    aData = rng

    ElementRowName = "DATA"
    strXML = vbNullString
    For i = LBound(aData) + 1 To UBound(aData)

        ElementRow = vbNullString
        For j = LBound(aData, 2) To UBound(aData, 2)
            Attributee = vbNullString
            ElementName = MRegex.Regx_Replace(aData(HeaderRow, j), "\W", "")
            TagOpen = Replace(TAG_XML_OPEN, TAG_ELEMENT, ElementName)
            TagOpen = Replace(TagOpen, TAG_ATTRIBUTE, Attributee)
            TagClose = Replace(TAG_XML_CLOSE, TAG_ELEMENT, ElementName)

            Element = TagOpen & aData(i, j) & TagClose
            ElementRow = ElementRow & Element

        Next j

        Attributee = " ID=""" & (i - 1) & """"
        TagOpen = Replace(TAG_XML_OPEN, TAG_ELEMENT, ElementRowName)
        TagOpen = Replace(TagOpen, TAG_ATTRIBUTE, Attributee)
        TagClose = Replace(TAG_XML_CLOSE, TAG_ELEMENT, ElementRowName)

        ElementRow = TagOpen & ElementRow & TagClose
        strXML = strXML & ElementRow

    Next i

    TableName = MRegex.Regx_Replace(TableName, "\W", "")
    Attributee = vbNullString
    TagOpen = Replace(TAG_XML_OPEN, TAG_ELEMENT, TableName)
    TagOpen = Replace(TagOpen, TAG_ATTRIBUTE, Attributee)
    TagClose = Replace(TAG_XML_CLOSE, TAG_ELEMENT, TableName)
    strXML = TagOpen & strXML & TagClose
    Range_To_XML_String = strXML

End Function

Sub Read_All_XMLs_Nodes()
    Const SUB_NAME As String = "Read_All_XMLs_Nodes"
    On Error GoTo ErrorHandler
    
    Dim wks                         As Worksheet
    Dim Column_XML                  As Long
    
    Dim lCounterNrArquivos          As Long
    Dim lCounterNrArquivosTotal     As Long
    Dim IsMainNode              As Boolean
    
    Dim filePath                    As String
    Dim i                           As Long
    Dim j                           As Long
    Dim key                         As Variant
    
    Dim xDoc                        As MSXML2.DOMDocument
    Dim xDocNode                    As MSXML2.IXMLDOMNode
    Dim xChild                      As MSXML2.IXMLDOMNode
    
    Dim gdicFilePath                As Dictionary
    
    
    Call Global_Initialize
    Set wks = Get_Worksheet_By_Codename("wksTemporary")
    With wks
        .Cells.Clear
        .Cells.NumberFormat = "@"
        .Range("A1").Resize(, 7) = Array("Ordem", "Caminho Node", "Formula", "NULL", "Ordem", "Caminho Node Final", "Retorno Formula")
    End With
    
    Set gdicXMLNodes = New Scripting.Dictionary
    Set gdicXMLNodesChild = New Scripting.Dictionary
    
    Set xDoc = New MSXML2.DOMDocument
    
    
    
    Column_XML = 0
    searchNodeFound = False
        
    Call FilesPaths_To_Collection(gdicFilePath)
        
    '5 Iterate through files
    gFileID = 0
    For Each key In gdicPaths.Keys
        gFileID = gFileID + 1
        
        'XML object
        Set xDoc = New MSXML2.DOMDocument
        With xDoc
            .async = False
            .validateOnParse = False
            .Load (key)
        End With
        
        Set xDocNode = xDoc.DocumentElement
        If Not xDoc Is Nothing Then
            IsMainNode = True
            Column_XML = 0
            Call List_ChildNodes(xDocNode, lLastRowXMLDados + 1, "\", IsMainNode, Column_XML)
        End If
    Next key
    
    j = 2
    Set wks = Get_Worksheet_By_Codename("wksTemporary")
    For Each key In gdicXMLNodes.Keys
        wks.Range("A" & j).Resize(, 2) = Array(j - 1, key)
        j = j + 1
    Next key
    
    
    j = 2
    For Each key In gdicXMLNodesChild.Keys
        wks.Range("E" & j).Resize(, 3) = Array(j - 1, key, 1)
        j = j + 1
    Next key
    
    
    
    
    Call Message_Information(MSG_SUCCESS, SUB_NAME)
Finish:
    Call MGlobal.Global_Finalize
    Exit Sub
ErrorHandler:
    Call Message_Error(MSG_FAIL & vbNewLine & err.Number & vbNewLine & err.Description, SUB_NAME)
    GoTo Finish
End Sub

Sub XML_Generate_Temp_Data(Optional wksDestination As Worksheet = Nothing, Optional RangeDestination As String = "A1")
 
    Dim aData()             As Variant
    Dim aNewData()          As Variant
    Dim iRow                As Long
    Dim wks As Worksheet
    Dim wksTemp As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_XML")
    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
    'iRow = Get_Last_Row(wksTemp, 1)
    'If iRow = 1 Then
    '    wks.Range("A3").Resize(, wks.UsedRange.Columns.Count).Copy
    '    wksTemp.Range("A1").PasteSpecial xlPasteAll
    '   Application.CutCopyMode = False
    'End If
    
    'Gera a aba onde ser�o alocados os dados temporariamente durante a execu��o do processamento
    If wks.Range("A3").Value <> vbNullString Then
        aData = wks.Range("A3").Resize(wks.UsedRange.Rows.Count - 2, wks.UsedRange.Columns.Count)
        aNewData = Unify_Unique_Header_From_Array(aData)
        
        
        If Not wksDestination Is Nothing Then
            Call MStructure.Clean_Worksheet(wksTemp)
            wksTemp.Range("A1").Resize(UBound(aNewData), UBound(aNewData, 2)) = aNewData
            
            iRow = Get_Last_Row(wksDestination, 1)
            If iRow = 1 Then
                aNewData = wksTemp.Range("A1").Resize(wks.UsedRange.Rows.Count, wks.UsedRange.Columns.Count)
                wksDestination.Range(RangeDestination).Resize(UBound(aNewData), UBound(aNewData, 2)) = aNewData
            Else
                aNewData = wksTemp.Range("A2").Resize(wks.UsedRange.Rows.Count, wks.UsedRange.Columns.Count)
                wksDestination.Range(RangeDestination).Offset(iRow).Resize(UBound(aNewData), UBound(aNewData, 2)) = aNewData
            End If
            
        Else
            
            wksTemp.Range(RangeDestination).Resize(UBound(aNewData), UBound(aNewData, 2)) = aNewData
        End If
        
    End If
    
Finish:
    On Error GoTo 0
    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, MSG_COMPANY)
    GoTo Finish
End Sub

Public Sub XML_Import(xDoc As MSXML2.DOMDocument, ByVal fileName As String, Optional ArrayBase As Long = 1, Optional OnlyUnique As Boolean = False)
    On Error GoTo ErrorHandler
    Dim i           As Long
    Dim j           As Long
    Dim iNodeList   As Long
    Dim lNodeList   As Long
    
    Dim xNode       As IXMLDOMNode
    Dim xNodeD      As IXMLDOMNode
    Dim xNodeList   As IXMLDOMNodeList
    
    Dim aNodeList() As Variant
    Dim sElement    As String
    Dim sElementN   As String
    Dim sElementD   As String
    Dim XFE         As XMLFieldsExtra
    Dim wksData     As Worksheet

    'Setup
    XFE = New_Type_XMLFieldsExtra
    
    'Para cada item selecionado com quantidade N
    For i = LBound(gaXML_TableN, 2) To UBound(gaXML_TableN, 2)
        
        'Gets the element node list!
        sElementN = Replace(gaXML_TableN(ArrayBase, i), "//", "")  'ArrayBase should be 1 for array generated
        Set xNodeList = xDoc.getElementsByTagName(sElementN)
        
        If glRowDataXML + xNodeList.Length > glRowDataXMLMax Then
            If OnlyUnique Then
                glRowDataXMLMax = glRowDataXML + 1
            Else
                glRowDataXMLMax = glRowDataXML + xNodeList.Length
            End If
        End If
        
        If xNodeList.Length > 0 Then
            If OnlyUnique Then
                ReDim aNodeList(1 To 1, 1 To UBound(gaXML_Nodes, 2))
            Else
                ReDim aNodeList(1 To xNodeList.Length, 1 To UBound(gaXML_Nodes, 2))
            End If
            
            lNodeList = 1
            
            For Each xNode In xNodeList
                For j = LBound(gaXML_Nodes, 2) To UBound(gaXML_Nodes, 2) 'For each column
                    
                    'SE FOR DEPENDENTE
                    If gaXML_Nodes(XML_ROW_TYPENODE, j) = "D" Then
                        If Regx_Match(gaXML_Nodes(XML_ROW_XPATH, j), "(" & sElementN & ")/(.+)") Then
                            sElementD = regExMatches(0).SubMatches(1)
                            
                            Set xNodeD = xNode.SelectSingleNode(sElementD)
                            If Not xNodeD Is Nothing Then
                                aNodeList(lNodeList, j) = xNodeD.nodeTypedValue
                            End If
                            
                        End If                   'If Regx_match(gaXML_Nodes(1, j), "(" & sElementN & ")/(.+)") Then
                    ElseIf gaXML_Nodes(XML_ROW_TYPENODE, j) = "N" Then
                        aNodeList(lNodeList, j) = xNode.Attributes(0).nodeTypedValue
                    End If                       'If gaXML_Nodes(2, j) = "D" Then
                Next j
                
                lNodeList = lNodeList + 1
                If OnlyUnique Then
                    Exit For
                End If
            Next xNode
        
        Else
        
            ReDim aNodeList(1 To 1, 1 To UBound(gaXML_Nodes, 2))
            
        End If                                   'If xNodeList.Length > 0 Then
        
        For j = LBound(gaXML_Nodes, 2) To UBound(gaXML_Nodes, 2)
            
            'SE FOR INDIVIDUAL(Unique)
            If gaXML_Nodes(XML_ROW_TYPENODE, j) = "U" Then
                
                sElement = gaXML_Nodes(1, j)
                
                Set xNode = xDoc.SelectSingleNode(sElement)
                
                If Not xNode Is Nothing Then
                    For iNodeList = LBound(aNodeList) To UBound(aNodeList)
                        aNodeList(iNodeList, j) = xNode.nodeTypedValue
                    Next iNodeList
                End If
                
            End If                               'If gaXML_Nodes(2, j) = "1" Then
            
        Next j
        
        Set wksData = Get_Worksheet_By_Codename("wksData_XML")
        wksData.Cells(glRowDataXML, 1).Resize(UBound(aNodeList), UBound(aNodeList, 2)) = aNodeList
        
        
        'EXTRAS
        Call Generate_XML_Fields_Extra
        gaXML_Fields_Extra(1, XFE.Column_SN) = vbNullString
        gaXML_Fields_Extra(1, XFE.Column_FilePath) = fileName
        gaXML_Fields_Extra(1, XFE.Column_FileID) = gFileID
        
        
        'Transfer the data from array to sheet
        j = Get_Last_Column(wksData, 1) + 1
        If glRowDataXMLMax - glRowDataXML = 0 Then
            wksData.Cells(glRowDataXML, j).Resize(, UBound(gaXML_Fields_Extra, 2)) = gaXML_Fields_Extra
            glRowDataXML = glRowDataXMLMax + 1
        Else
            wksData.Cells(glRowDataXML, j).Resize(glRowDataXMLMax - glRowDataXML, UBound(gaXML_Fields_Extra, 2)) = gaXML_Fields_Extra
            glRowDataXML = glRowDataXMLMax
        End If
        
    Next i
    Exit Sub
ErrorHandler:
    Stop
    Resume
    Call MInterface.Message_Error(err.Description)
End Sub

Public Sub XML_Import_V2(xDoc As MSXML2.DOMDocument, ByVal fileName As String)
    Const SUB_NAME As String = "XML_Import_V2"
    On Error GoTo ErrorHandler
    
    Dim i           As Long
    Dim j           As Long
    Dim iNodeList   As Long
    Dim lNodeList   As Long
    
    Dim xNode       As IXMLDOMNode
    Dim xNodeD      As IXMLDOMNode
    Dim xNodeList   As IXMLDOMNodeList
    
    Dim aNodeList() As Variant
    Dim sElement    As String
    Dim ItemNo      As String
    
    Dim XFE         As XMLFieldsExtra
    Dim wksData     As Worksheet
    
    
    'Setup
    Set wksData = Get_Worksheet_By_Codename("wksData_XML")
    XFE = New_Type_XMLFieldsExtra
    ItemNo = 1
    ReDim aNodeList(1 To 1, 1 To UBound(gaXML_Nodes, 2))
    For j = LBound(gaXML_Nodes, 2) To UBound(gaXML_Nodes, 2)
        
        sElement = gaXML_Nodes(XML_ROW_XPATH, j)
        If UCase(gaXML_Nodes(XML_ROW_CAPTION, j)) = "ITEMNO" Then
            aNodeList(1, j) = ItemNo
            ItemNo = ItemNo + 1
        End If
        
        Set xNode = Get_Single_Node(xDoc, sElement)
        If Not xNode Is Nothing Then
            aNodeList(1, j) = xNode.nodeTypedValue
        End If
        
    Next j
    glRowDataXML = Get_Last_Row(wksData, 1) + 1
    wksData.Cells(glRowDataXML, 1).Resize(UBound(aNodeList), UBound(aNodeList, 2)) = aNodeList
    
    
    'EXTRAS
    Call Generate_XML_Fields_Extra
    gaXML_Fields_Extra(1, XFE.Column_SN) = vbNullString
    gaXML_Fields_Extra(1, XFE.Column_FilePath) = fileName
    gaXML_Fields_Extra(1, XFE.Column_FileID) = gFileID
    
    
    'Transfer the data from array to sheet
    glRowDataXML = Get_Last_Row(wksData, 1)
    j = Get_Last_Column(wksData, 1) + 1
    wksData.Cells(glRowDataXML, j).Resize(, UBound(gaXML_Fields_Extra, 2)) = gaXML_Fields_Extra
    
    
ErrorHandler:
End Sub


