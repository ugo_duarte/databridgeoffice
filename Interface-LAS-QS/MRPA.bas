Attribute VB_Name = "MRPA"
Option Explicit
Global Const AppNameXL As String = "Interface XML-LAS Purchases - Excel"
Global Const CITRIX_LAS_PATH As String = "\\v246\iclass\Citrix\Start.exe ""LAS.NET Live"""
Global Const KEY_ALT_TAB As String = "%{TAB}"
Global Const KEY_TAB As String = "{TAB}"
Global Const SLEEP_CURRENT_MS = 200

Global Const RPA_KEYS_SPACE As String = "{ }"
Global Const RPA_KEYS_ARROW_DOWN As String = "{DOWN}"
Global Const RPA_KEYS_ALT As String = "%"
Global Const RPA_KEYS_ENTER As String = "{Enter}"
Global Const RPA_KEYS_ESC As String = "{ESC}"
Global Const RPA_KEYS_DEL As String = "{ESC}"

Global Const RPA_KEYS_CTRL_A As String = "^a"
Global Const RPA_KEYS_CTRL_C As String = "^c"
Global Const RPA_KEYS_CTRL_V As String = "^v"
Global Const LAS_APP            As String = "LAS.NET"
Global Const LAS_QA_APP         As String = "QA Major - LAS.NET"

Global gHandle_LAS_APP As LongLong
Global gHandle_Current_Top_Window As LongLong
Global gHandle_Desktop As LongLong
#If Win64 Then
    'Window
    Public Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    Public Declare PtrSafe Function FindWindowChild Lib "user32" Alias "FindWindowExA" (ByVal hwnd As LongLong, ByVal childAfter As Long, Optional ByVal ClassName As String, Optional ByVal windowTitle As String) As LongLong
    Public Declare PtrSafe Function GetActiveWindow Lib "user32.dll" () As LongLong
    Public Declare PtrSafe Function GetForegroundWindow Lib "user32.dll" () As LongLong
    Public Declare PtrSafe Function GetFocus Lib "user32.dll" () As LongLong
    
    Public Declare PtrSafe Function GetAncestor Lib "user32.dll" (ByVal hwnd As Long, ByVal gaFlags As Long) As LongLong
    Public Declare PtrSafe Function GetNextWindow Lib "user32" Alias "GetWindow" (ByVal hwnd As LongLong, ByVal wCmd As Long) As LongLong
    Public Declare PtrSafe Function GetWindowTextLength Lib "user32" Alias "GetWindowTextLengthA" (ByVal hwnd As LongLong) As Long
    Public Declare PtrSafe Function GetTopWindow Lib "user32" (Optional ByVal hwnd As LongLong) As LongLong
    Public Declare PtrSafe Function GetDesktopWindow Lib "user32" () As LongLong
    
    Public Declare PtrSafe Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As LongLong, ByVal lpString As String, ByVal cch As Long) As LongLong
    Public Declare PtrSafe Function GetDlgItem Lib "user32" (ByVal hDlg As Long, nIDDlgItem As Long) As Long
    
    Public Declare PtrSafe Function MoveWindow Lib "user32" (ByVal hwnd As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, Optional ByVal bRepaint As Boolean) As Boolean
    
    Public Declare PtrSafe Function SetActiveWindow Lib "user32" (ByVal hwnd As LongLong) As Long
    Public Declare PtrSafe Function SetWindowPos Lib "user32" (ByVal hwnd As LongLong, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
    
    
    'Mouse
    Public Declare PtrSafe Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)
    Public Declare PtrSafe Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
    Public Declare PtrSafe Function SetCursorPos Lib "user32" (ByVal x As Long, ByVal y As Long) As Long
    
    
    'O.S.
    Public Declare PtrSafe Function GetSystemMetrics32 Lib "user32" Alias "GetSystemMetrics" (ByVal nIndex As Long) As Long
    Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal Milliseconds As LongPtr)
    
    
#Else
    'Window
    Public Declare PtrSafe Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
    Public Declare PtrSafe Function MoveWindow Lib "user32" (ByVal hwnd As Long, ByVal x As Long, ByVal y As Long, ByVal nWidth As Long, ByVal nHeight As Long, Optional ByVal bRepaint As Boolean) As Boolean
    'Mouse
    
    Public Declare PtrSafe Function GetCursorPos Lib "user32" (lpPoint As POINTAPI) As Long
    Public Declare PtrSafe Function SetCursorPos Lib "user32" (ByVal x As Long, ByVal y As Long) As Long
    Public Declare PtrSafe Sub mouse_event Lib "user32" (ByVal dwFlags As Long, ByVal dx As Long, ByVal dy As Long, ByVal cButtons As Long, ByVal dwExtraInfo As Long)
    
    Public Declare PtrSafe Function SetActiveWindow Lib "user32" (ByVal hwnd As Long) As Long
    Public Declare PtrSafe Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
    
    Public Declare PtrSafe Function SetActiveWindow Lib "user32" (ByVal hwnd As Long) As Long
    Public Declare PtrSafe Function GetAncestor Lib "user32" (ByVal hwnd As LongPtr, ByVal wFlags As Long)
    Public Declare PtrSafe Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hwnd As LongPtr, ByVal lpString As String, ByVal cch As LongLong) As LongLong
    
    'O.S.
    Public Declare PtrSafe Function GetSystemMetrics32 Lib "user32" Alias "GetSystemMetrics" (ByVal nIndex As Long) As Long
    Public Declare PtrSafe Sub Sleep Lib "kernel32" (ByVal Milliseconds As Long)
#End If

Public Enum GetWndConsts
       GW_HWNDFIRST = 0
       GW_HWNDLAST = 1
       GW_HWNDNEXT = 2
       GW_HWNDPREV = 3
       GW_OWNER = 4
       GW_CHILD = 5
       GW_ENABLEDPOPUP = 6
       GW_MAX = 6
 End Enum
 Sub LAS_Open()
    Const SUB_NAME As String = "Open LAS"
    Call Shell(CITRIX_LAS_PATH, vbMaximizedFocus)
End Sub
Sub Test_LAS_QA()
    Dim hndlLAS As LongLong
    Const DOCK_BOTTOM As String = "Dock Bottom"
    Const STATUS_BAR As String = "Status bar"
    Const SELECT_COMPANY As String = "menuItemAction:select_company"
    
    hndlLAS = FindWindow(vbNullString, LAS_Get_Window_Title(QA:=True))
    
    Print_Window_Detail (hndlLAS)
    
    Dim hndlDockBottom As LongLong
    hndlDockBottom = FindWindowChild(hndlLAS, 0, , STATUS_BAR)
    Print_Window_Detail (hndlDockBottom)
End Sub
Sub Map_Window_Childs(hndlParent As LongLong)
    Dim hndlChild As LongLong
    Dim hndlLastChild As LongLong
    Dim i As Long
    i = 0
    If hndlParent <> 0 Then
        Print_Window_Detail (hndlParent)
        Do
            If i = 0 Then
                hndlChild = GetNextWindow(hndlParent, GetWndConsts.GW_CHILD)
            Else
                hndlLastChild = hndlChild
                hndlChild = GetNextWindow(hndlChild, GetWndConsts.GW_HWNDNEXT)
            End If
            
            Map_Window_Childs (hndlChild)
            
            i = i + 1
        Loop While hndlChild <> 0
    End If
    
End Sub

Sub Print_Window_Detail(ByVal hndl As LongLong)
        
    Dim textL As Long
    Dim iText As LongLong
    Dim WinText As String
    
    WinText = String(255, vbNullChar)
    iText = GetWindowText(hndl, WinText, 255)
    WinText = Left(WinText, InStr(1, WinText, vbNullChar) - 1)
    textL = GetWindowTextLength(hndl)
    
    Debug.Print hndl & " -Hex:=" & CStr(Hex(hndl))
    Debug.Print "Text Lenght:" & CStr(textL)
    Debug.Print "Text:" & CStr(WinText)
    
End Sub


Sub Test_MODFAKTURA()
    Const MOD_TEXT As String = "MODPURCHASE"
    
    Dim LASHandle As LongLong
    Dim pos
    
    Dim w As Long
    Dim h As Long
    
    If gdicGlobalParameters Is Nothing Then
        Call Global_Dictionary_Load
    End If
    
    w = gdicGlobalParameters("LAS RESOLUTION WIDTH")
    h = gdicGlobalParameters("LAS RESOLUTION HEIGHT")
    
    'LASHandle = FindWindow(vbNullString, LAS_APP) '"New tab")
    LASHandle = FindWindow(vbNullString, LAS_Get_Window_Title(QA:=False))
    Debug.Print CStr(Hex(LASHandle))
    
    Dim hndlMODFAKTURA As Long
    hndlMODFAKTURA = FindWindow(vbNullString, MOD_TEXT)
    Debug.Print "MODFAKTURA:" & CStr(Hex(hndlMODFAKTURA))
'    Stop
    Dim t As String
    
    Dim hndlOKButton As LongLong
    Dim hndlBlankButton As LongLong
    Dim hndlMessageButton As LongLong
    Dim hndlMessageButton1 As LongLong
    Dim i As LongLong
    
    
    hndlOKButton = GetNextWindow(hndlMODFAKTURA, GetWndConsts.GW_CHILD)
    Debug.Print hndlOKButton & " -Hex:=" & CStr(Hex(hndlOKButton))
    hndlBlankButton = GetNextWindow(hndlOKButton, GetWndConsts.GW_HWNDNEXT)
    Debug.Print hndlBlankButton & " -Hex:=" & CStr(Hex(hndlBlankButton))
    hndlMessageButton = GetNextWindow(hndlBlankButton, GetWndConsts.GW_HWNDNEXT)
    Debug.Print hndlMessageButton & " -Hex:=" & CStr(Hex(hndlMessageButton))
    Dim textL As Long
    textL = GetWindowTextLength(hndlMessageButton)
    Debug.Print "TextL:" & CStr(textL)
    
    Dim WinText As String
    
    Dim iText As LongLong
    
    WinText = String(255, vbNullChar)
    iText = GetWindowText(hndlMessageButton, WinText, 255)
    WinText = Left(WinText, InStr(1, WinText, vbNullChar) - 1)
    Debug.Print iText, WinText
    
    
    hndlMessageButton1 = FindWindowChild(hndlMODFAKTURA, 0, , "OK")
    Debug.Print "hndlMessageButton1  " & CStr(hndlMessageButton1) & " -Hex:=" & CStr(Hex(hndlMessageButton1))
    hndlMessageButton1 = FindWindowChild(hndlMODFAKTURA, 0, , "Cannot save. Errors in data, examine the fields !")
    Debug.Print "hndlMessageButton1  " & CStr(hndlMessageButton1) & " -Hex:=" & CStr(Hex(hndlMessageButton1))
    hndlMessageButton1 = FindWindowChild(hndlMODFAKTURA, 0, , "Cannot%")
    Debug.Print "hndlMessageButton1  " & CStr(hndlMessageButton1) & " -Hex:=" & CStr(Hex(hndlMessageButton1))
End Sub
Sub Get_Mouse_Current_Position()
    Dim currentCoordinates As POINTAPI
    GetCursorPos currentCoordinates
    Debug.Print (currentCoordinates.Xcoord & "," & currentCoordinates.Ycoord)
    
End Sub
Sub Configure_Mouse_Positions()
    Const SUB_NAME As String = "Configure Mouse Positions"
    On Error GoTo ErrorHandler
    
    Dim wks As Worksheet
    Dim aData() As Variant
    Dim i As Long
    Dim x As Long
    Dim y As Long
    Dim Field As String
    
    Dim currentCoordinates As POINTAPI
    Dim rngMousePositions As Range
    
    Dim msg As Long
    msg = MsgBox("Configure Individual Mouse Position?", vbYesNoCancel, SUB_NAME)
    
    If msg = vbCancel Then End
    Set wks = ActiveCell.Worksheet
    Dim iRow As Long
    iRow = ActiveCell.Row
    Call MGlobal.Global_Initialize
    
    If msg = vbYes Then
        
        wks.Activate
        Field = Trim(wks.Cells(iRow, 1))
        If Field <> vbNullString Then
            Call MsgBox("Position the mouse and press enter: " & vbNewLine & "Field: " & Field)
            
            GetCursorPos currentCoordinates
            wks.Cells(iRow, 2) = currentCoordinates.Xcoord
            wks.Cells(iRow, 3) = currentCoordinates.Ycoord
            
        End If
    Else
    
        
        Set rngMousePositions = Get_Model_Range("List_Mouse_Positions_CR")
        aData = Get_Current_Region(rngMousePositions)
        
        x = 2: y = 3
        For i = LBound(aData) + 1 To UBound(aData)
            Call MsgBox("Position the mouse and press enter: " & vbNewLine & "Field: " & aData(i, 1))
            
            GetCursorPos currentCoordinates
            aData(i, x) = currentCoordinates.Xcoord
            aData(i, y) = currentCoordinates.Ycoord
        Next i
        
        rngMousePositions.Resize(UBound(aData), UBound(aData, 2)) = aData
        
    End If 'If msg = vbYes Then
    
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Fim:
    Call MGlobal.Global_Finalize
    
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbNewLine & err.Description)
    GoTo Fim
    
End Sub
Sub Excel_Activate(Optional ShouldResize As Boolean = True)
    Dim LASHandle As Long
    Dim pos
    
    Sleep SLEEP_CURRENT_MS
    
    If ShouldResize Then
        Call Excel_Resize
    End If
    
    'LASHandle = FindWindow(vbNullString, Application.Caption)
    'Debug.Print LASHandle
    'AppActivate Application.Caption, True
    'SetActiveWindow LASHandle
    DoEvents
    Sleep SLEEP_CURRENT_MS
End Sub
Sub Excel_Resize()
    Dim total
    Dim Left As Long
    Dim Top As Long
    Dim Width As Long
    Dim Height As Long
    
    Application.WindowState = xlNormal

    If gdicGlobalParameters Is Nothing Then
        Call Global_Dictionary_Load
    End If
    
    Width = gdicGlobalParameters("EXCEL RESOLUTION WIDTH")
    Height = gdicGlobalParameters("EXCEL RESOLUTION HEIGHT")
    Left = gdicGlobalParameters("EXCEL WINDOW LEFT")
    Top = gdicGlobalParameters("EXCEL WINDOW TOP")

    
'    With Application
'     .WindowState = xlNormal
'     .Width = Width
'     .Height = Height
'     .Top = Top 'dllGetHorizontalResolution - 400
'     .Left = Left 'dllGetHorizontalResolution - 600
'    End With

    Dim LASHandle
    Dim pos
    
    LASHandle = FindWindow(vbNullString, Application.Caption)
    pos = SetWindowPos(LASHandle, 0, Left, Top, Width, Height, 64)
    SetActiveWindow LASHandle
    'AppActivate Application.Caption, True

End Sub
Sub LAS_Activate()
    Const SUB_NAME As String = "LAS Activate"
    
    Dim LAS_QA As Boolean
    
    LAS_QA = Get_Model_Range("Globals_LAS_QA_MAJOR")
    
    Sleep SLEEP_CURRENT_MS
    
    Call LAS_Resize(LAS_QA)
    
    DoEvents
    Sleep SLEEP_CURRENT_MS
End Sub
Sub LAS_Add_Purchase()
    Const sleepTimer As Long = 400
    
    Call RPA_Send_Keys("%A", sleepTimer)
    Call RPA_Send_Keys("p", sleepTimer)
    Call RPA_Send_Keys("u", sleepTimer)
    
    Sleep sleepTimer
    
End Sub
Sub LAS_Department_Invoices_ManualInvoices_Add()
    Const sleepTimer As Long = 400
    
    Call RPA_Send_Keys("%d", sleepTimer)
    Call RPA_Send_Keys("i", sleepTimer)
    Call RPA_Send_Keys("m", sleepTimer)
    Call RPA_Send_Keys("a", sleepTimer)
    Sleep sleepTimer
    
End Sub
Sub LAS_Click_Section(dicMousePositions As Dictionary, Section As String, Optional dclick As Boolean = False)
    If UCase(Section) = "CONFIRM SAVE" Then
        Sleep SLEEP_CURRENT_MS
        Call SendKeys("{ENTER}")
    Else
        Call RPA_Mouse_Position_And_Click(dicMousePositions, Section, dclick)
    End If
    
End Sub
Sub LAS_Resize(Optional QA As Boolean = False)
    
    Dim pos
    
    Dim w As Long
    Dim h As Long
    
    If gdicGlobalParameters Is Nothing Then
        Call Global_Dictionary_Load
    End If
    
    w = gdicGlobalParameters("LAS RESOLUTION WIDTH")
    h = gdicGlobalParameters("LAS RESOLUTION HEIGHT")
    
    gHandle_LAS_APP = FindWindow(vbNullString, LAS_Get_Window_Title(QA:=QA))
    
    If gHandle_LAS_APP = 0 Then
        Call err.Raise(999, , "LAS is not open!")
    End If
    pos = SetWindowPos(gHandle_LAS_APP, 0, 0, 0, w, h, 64)
    'pos = MoveWindow(LASHandle, 0, 0, w, h, 1)
    
    SetActiveWindow gHandle_LAS_APP
    
End Sub
Function Get_Latest_Report() As String
    Dim MyPath As String
    Dim MyFile As String
    Dim LMD As Date
    Dim tryNumber As Long
    tryNumber = 0
    MyPath = "\\chg-meridian.com\attachments\udu\iCLASS Export\Live\"

    If Right(MyPath, 1) <> "\" Then MyPath = MyPath & "\"

getFiles:
    MyFile = Dir(MyPath & "*.xlsx", vbNormal)
    
    
    Do While Len(MyFile) > 0

        LMD = FileDateTime(MyPath & MyFile)

        If LMD > gTimer Then
            Get_Latest_Report = MyPath & MyFile
            Exit Function
        End If

        MyFile = Dir

    Loop
    
    If tryNumber < 5 Then
        tryNumber = tryNumber + 1
        Sleep 5000
        GoTo getFiles
    End If
    
    Get_Latest_Report = vbNullString
End Function
Sub Copy_Latest_Report(ReportName As String)
    Dim ReportFile As String
    ReportFile = Get_Latest_Report
    If ReportFile <> vbNullString Then
        Call Copy_File(Get_Latest_Report, ReportName)
    End If
End Sub
Sub LAS_Export_ADM_LeaseSchedules()

    Dim sleepTimer As Long
    
    sleepTimer = 400
    Call LAS_Activate
    Call RPA_Send_Keys("%A", sleepTimer)
    Call RPA_Send_Keys("l", sleepTimer)
    Call RPA_Send_Keys("l", sleepTimer)
    Call RPA_Send_Keys("{F5}", sleepTimer)
    Call RPA_Send_Keys("^e", sleepTimer)
    
    Call RPA_Send_Keys("y", sleepTimer)
    
    Call RPA_Send_Keys("{ESC}", sleepTimer)
    Sleep 5000
    Call Copy_Latest_Report(REPORTS_FOLDER & "LAS_Adm_LeaseSchedules.xlsx")
    
End Sub
Sub LAS_Export_ADM_InstallationCertificate()

    Dim sleepTimer As Long
    sleepTimer = 400
    
    Call LAS_Activate
    
    Call RPA_Send_Keys("%A", sleepTimer)
    Call RPA_Send_Keys("l", sleepTimer)
    Call RPA_Send_Keys("i", sleepTimer)
    Call RPA_Send_Keys("{F5}", sleepTimer)
    Call RPA_Send_Keys("^e", sleepTimer)
    Call RPA_Send_Keys("y", sleepTimer)
    Call RPA_Send_Keys("{ESC}", sleepTimer)
    
    Sleep 5000
    Call Copy_Latest_Report(REPORTS_FOLDER & "LAS_Adm_InstallationCertificates.xlsx")
End Sub
Sub LAS_Export_ADM_Purchases()

    Dim sleepTimer As Long
    sleepTimer = 400
    
    Call LAS_Activate
    
    Call RPA_Send_Keys("%A", sleepTimer)
    Call RPA_Send_Keys("p", sleepTimer)
    Call RPA_Send_Keys("p", sleepTimer)
    Call RPA_Send_Keys("{F5}", sleepTimer)
    Call RPA_Send_Keys("^e", sleepTimer)
    Call RPA_Send_Keys("y", sleepTimer)
    Call RPA_Send_Keys("{ESC}", sleepTimer)
    
    Sleep 5000
    Call Copy_Latest_Report(REPORTS_FOLDER & "LAS_Adm_Purchases.xlsx")
    
End Sub
Sub LAS_Export_ADM_Assets()

    Dim sleepTimer As Long
    sleepTimer = 400
    
    Call LAS_Activate
    
    Call RPA_Send_Keys("%A", sleepTimer)
    Call RPA_Send_Keys("p", sleepTimer)
    Call RPA_Send_Keys("a", sleepTimer)
    Call RPA_Send_Keys("{F5}", sleepTimer)
    Call RPA_Send_Keys("^e", sleepTimer)
    Call RPA_Send_Keys("y", sleepTimer)
    Call RPA_Send_Keys("{ESC}", sleepTimer)
    
    Sleep 5000
    
    Call Copy_Latest_Report(REPORTS_FOLDER & "LAS_Adm_Assets.xlsx")
    
End Sub
Function LAS_Get_Window_Title(QA As Boolean) As String
    If QA Then
        LAS_Get_Window_Title = LAS_QA_APP
    Else
        LAS_Get_Window_Title = LAS_APP
    End If

End Function
Sub Copy_File(filePath, NewFilePath)
If gobjFSO Is Nothing Then
    Set gobjFSO = New FileSystemObject
End If
    Call gobjFSO.CopyFile(filePath, NewFilePath, True)
    
End Sub

Sub Get_Folder_Latest_File()

    Dim MyPath As String

    Dim MyFile As String

    Dim LatestFile As String

    Dim LatestDate As Date

    Dim LMD As Date

    MyPath = "C:\Users\Documents\"

    If Right(MyPath, 1) <> "\" Then MyPath = MyPath & "\"

    MyFile = Dir(MyPath & "*.xls", vbNormal)

    If Len(MyFile) = 0 Then

        MsgBox "No files were found...", vbExclamation

        Exit Sub

    End If

    Do While Len(MyFile) > 0

        LMD = FileDateTime(MyPath & MyFile)

        If LMD > LatestDate Then

            LatestFile = MyFile

            LatestDate = LMD

        End If

        MyFile = Dir

    Loop

    Workbooks.Open MyPath & LatestFile

End Sub
Sub Teste_Activate()
Call MGlobal.Global_Initialize
    Call LAS_Activate
    
    Sleep 2000
    Call Excel_Activate(True)
    If MInterface.Message_Confirm("Funcionou") = vbYes Then
        Call LAS_Activate
    End If
    Call MGlobal.Global_Finalize
End Sub
Sub Teste_Switch_Window()
    
repeat:
    Call LAS_Activate
    Sleep 500
    Call Excel_Activate
    Sleep 500
    
    If MInterface.Message_Confirm("Repetir?") = vbYes Then
        GoTo repeat
    End If
    
End Sub
Sub RPA_Drag_Mouse(x, y)
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    Sleep (SLEEP_CURRENT_MS / 4)
    SetCursorPos x, y
    Sleep (SLEEP_CURRENT_MS / 4)
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
End Sub
Sub RPA_Mouse_DoubleLeft_Click(Optional sleepTime As Long = SLEEP_CURRENT_MS)
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
    Sleep 50
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
    Sleep sleepTime
End Sub
Sub RPA_Mouse_Left_Click(Optional sleepTime As Long = SLEEP_CURRENT_MS)
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
    Sleep sleepTime
End Sub
Sub RPA_Mouse_Position_And_Click(dic As Dictionary, Key As String, Optional dclick As Boolean = False)
    Dim MousePosition As Variant
    Dim x As Long
    Dim y As Long
    x = 2: y = 3
    MousePosition = dic(UCase(Key))
    Call RPA_Move_Mouse(MousePosition(x), MousePosition(y))
    Sleep SLEEP_CURRENT_MS
    If dclick Then
        Call RPA_Mouse_DoubleLeft_Click
    Else
        Call RPA_Mouse_Left_Click
    End If
    
End Sub
Sub tttt()
    Call RPA_Move_Mouse(210, 82)
    Call RPA_Mouse_Left_Click
End Sub
Sub RPA_Move_Mouse(ByVal x As Long, ByVal y As Long, Optional sleepTime As Long = SLEEP_CURRENT_MS)
    SetCursorPos x, y
    Sleep sleepTime
End Sub
Sub RPA_Send_Keys(ByVal Text As String, Optional sleepTime As Long = SLEEP_CURRENT_MS)
    SendKeys Text, True
    DoEvents
    Sleep sleepTime
End Sub
Sub RPA_TAB_To_Next_Field(Optional NumberOfFields As Long = 1)
    Dim i As Long
    For i = 1 To NumberOfFields
        Call RPA_Send_Keys(KEY_TAB)
    Next i
End Sub
Sub t()
    Call MRPA.LAS_Activate
    Call RPA_Combolist("List_TransmissionMode", "Internal Printer;eInvoicing")

End Sub
Sub RPA_Combolist(List As String, Value As String)
    Dim aList() As Variant
    Dim i As Long
    Dim aValues() As String
    Dim iValues As Long
    aValues = Split(Value, ";")
    aList = Get_Model_Range(List)
    
    
    MRPA.RPA_Send_Keys (RPA_KEYS_ALT & RPA_KEYS_ARROW_DOWN)
    
    For i = LBound(aList) To UBound(aList)
            
        For iValues = LBound(aValues) To UBound(aValues)
            If UCase(Trim(aList(i, 1))) = UCase(Trim(aValues(iValues))) Then
                MRPA.RPA_Send_Keys (RPA_KEYS_SPACE)
                Exit For
            End If
        Next iValues
        
        MRPA.RPA_Send_Keys (RPA_KEYS_ARROW_DOWN)
        
    Next i
    
    MRPA.RPA_Send_Keys (RPA_KEYS_ENTER)
    
End Sub
Sub RPA_Send_Value_And_NextField(CurrentValue As String, Optional ForceNull As Boolean = False)
    If CurrentValue <> vbNullString Or ForceNull = True Then
        Call RPA_Send_Keys(CurrentValue)
    End If
    Call RPA_TAB_To_Next_Field
        
End Sub
