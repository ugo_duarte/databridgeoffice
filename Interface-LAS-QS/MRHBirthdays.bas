Attribute VB_Name = "MRHBirthdays"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MRHBirthdays"

Public Function Get_Next_BDay(Bday As Date) As Date
    Get_Next_BDay = CDate(DateSerial(Year(Now), Month(Bday), Day(Bday)))
End Function


Sub Confirm_Send_Email_Birthday()
    Const SUB_NAME As String = "Confirm Send Email Birthday"
    On Error GoTo ErrorHandler
    
    Dim rngActive As Range
    Dim rngSelection As Range
    Dim wksReportBDay As Worksheet
    Dim wksActive As Worksheet
    Dim iApproved As Long
    Dim ColumnID As Long
    Dim aDataApproved() As Variant
    
    Set rngSelection = Selection
    Set wksActive = rngSelection.Worksheet
    Set wksReportBDay = MStructure.Get_Worksheet_By_Codename("wksReport_Birthdays")
    If wksActive.Codename <> wksReportBDay.Codename Then
        Call err.Raise(901, , "Cant confirm outside the 'Dashboard Birthdays' Sheet")
    End If
    
    iApproved = rngSelection.Row
    aDataApproved = MStructure.Get_Current_Region(rngSelection)
    ColumnID = MStructure.Get_Column_Number(aDataApproved, "ID")
    If MInterface.Message_Confirm("Confirm e-mail " & aDataApproved(iApproved, ColumnID) & " is sent") <> vbYes Then
        Call err.Raise(901, , "Not confirmed")
    End If
    
    Call Update_Generated_Date(aDataApproved(iApproved, ColumnID))

    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish
End Sub


Sub Update_Generated_Date(ByVal ID As String)
    Dim wksData As Worksheet
    Dim rngBirthdays As Range
    Dim columnCells As Range
    Dim cell As Range
    Dim aBdays() As Variant
    Dim ColumnGenerated As Long
    
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Birthdays")
    Set rngBirthdays = wksData.Range("A1").CurrentRegion
    
    aBdays = wksData.Range("A1").Resize(, Get_Last_Column(wksData, 1))
    
    ColumnGenerated = Get_Column_Number(aBdays, "Generated")
    
    For Each columnCells In rngBirthdays.Columns(1)
        For Each cell In columnCells.Rows
            If CStr(cell.Value) = ID Then
                cell.Offset(, ColumnGenerated - 1) = Now
                Exit Sub
            End If
        Next cell
    Next columnCells
    
End Sub
Sub Search_Birthdays()
    Dim rngBirthdays As Range
    Dim wksData As Worksheet
    Dim aBdays() As Variant
    
    Dim ColumnDaysToBirthday As Long
    Dim ColumnGenerated As Long
    
    Dim rngReportBirthdayData As Range
    
    'wksReport_Birthdays
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Birthdays")
    Set rngBirthdays = wksData.Range("A1").CurrentRegion
    aBdays = wksData.Range("A1").Resize(, Get_Last_Column(wksData, 1))
    ColumnDaysToBirthday = Get_Column_Number(aBdays, "Days to Birthday")
    ColumnGenerated = Get_Column_Number(aBdays, "Generated")
    
    Set rngReportBirthdayData = MStructure.Get_Model_Range("Report_Birthday_Data")
    
    'Days to Birthday
    rngBirthdays.AutoFilter
    rngBirthdays.AutoFilter Field:=ColumnDaysToBirthday, Criteria1:="<=0"
    rngBirthdays.AutoFilter Field:=ColumnGenerated, Criteria1:=""
    rngReportBirthdayData.CurrentRegion.Clear
    rngBirthdays.SpecialCells(xlCellTypeVisible).Copy
    rngReportBirthdayData.PasteSpecial xlPasteAll
    rngReportBirthdayData.PasteSpecial xlPasteValues
    rngReportBirthdayData.Worksheet.Columns.AutoFit
    rngReportBirthdayData.Worksheet.Activate
    rngBirthdays.AutoFilter
    
    Call MInterface.Message_Information(MSG_SUCCESS)
    
End Sub
Sub Generate_Birthday_Email()
    Const SUB_NAME As String = "Generate Email Birthday"
    On Error GoTo ErrorHandler
    Dim OutApp As Outlook.Application
    Dim OutMail As Outlook.MailItem
    Dim rngTemplate As Range
    Dim oCht As Chart
    Dim obj
    Dim originalText As String
    Dim bdayText As String
    Dim shpTemplate As Shape
    Dim wksTemplate As Worksheet
    
    Dim Name As String
    Dim Birthday As String
    
    Set OutApp = New Outlook.Application
    
    Dim Fname As String
    Dim FnamePath As String
    Dim rngActive As Range
    Dim rngSelection As Range
    Dim wksReportBDay As Worksheet
    Dim wksActive As Worksheet
    Dim iApproved As Long
    
    Dim ColumnID As Long
    Dim ColumnName As Long
    Dim ColumnBirthday As Long
    
    Dim aDataApproved() As Variant
    
    Set rngSelection = Selection
    Set wksActive = rngSelection.Worksheet
    Set wksReportBDay = MStructure.Get_Worksheet_By_Codename("wksReport_Birthdays")
    If wksActive.Codename <> wksReportBDay.Codename Then
        Call err.Raise(901, , "Cant confirm outside the 'Dashboard Birthdays' Sheet")
    End If
    
    iApproved = rngSelection.Row
    aDataApproved = MStructure.Get_Current_Region(rngSelection)
    ColumnID = MStructure.Get_Column_Number(aDataApproved, "ID")
    ColumnName = MStructure.Get_Column_Number(aDataApproved, "Name")
    ColumnBirthday = MStructure.Get_Column_Number(aDataApproved, "Next Birthday")
    
    If MInterface.Message_Confirm("Generate e-mail " & aDataApproved(iApproved, ColumnID) & "?") <> vbYes Then
        Call err.Raise(901, , "Not confirmed")
    End If
    
    Fname = "BirthdayCard_" & Format(Now, "yyyymmddhhmmss") & ".jpg"
    FnamePath = ThisWorkbook.Path & "\" & Fname
    
    Set oCht = MStructure.Get_Worksheet_By_Codename("chtBirthday")
    For Each obj In oCht.Shapes
        obj.Delete
    Next
    Set wksTemplate = MStructure.Get_Worksheet_By_Codename("wksReport_Template")
    Set rngTemplate = wksTemplate.Range("A1:K20")
    
    originalText = Get_Shape_text(wksTemplate, "lb_Birthday_Text")
    bdayText = originalText
    
    Name = aDataApproved(iApproved, ColumnName)
    Birthday = aDataApproved(iApproved, ColumnBirthday)
    
    bdayText = Replace(bdayText, "@Name", Name)
    bdayText = Replace(bdayText, "@Birthday", Birthday)
    
    
    Call Change_Shape_text(wksTemplate, "lb_Birthday_Text", bdayText)
    
    'rngTemplate.CopyPicture xlScreen, xlBitmap
    rngTemplate.CopyPicture xlPrinter, xlPicture
    With oCht
        .Paste
        .Export fileName:=Fname, Filtername:="JPG"
        '.Delete
    End With
    
    Set OutMail = OutApp.CreateItem(0)
    With OutMail
         .To = "Teste"
         .Subject = "Teste 2"
         .Attachments.Add Fname, 1, 0
         .HTMLBody = "<html>" & _
                     "<img src=""cid:" & Fname & """>"
                    '"<img src=""cid:" & Fname & """height=520 width=750>"
         .Display
    End With
    
    Call Change_Shape_text(wksTemplate, "lb_Birthday_Text", originalText)
    
    
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Set OutMail = Nothing
    Set OutApp = Nothing
    
    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish
    
End Sub
Function SendActiveWorksheet(strTo As String, strSubject As String, Optional strCC As String, Optional strBody As String) As Boolean
   On Error GoTo eh
'declare variables to hold the objects required
   Dim wbDestination As Workbook
   Dim strDestName As String
   Dim wbSource As Workbook
   Dim wsSource As Worksheet
   Dim OutApp As Object
   Dim OutMail As Object
   Dim strTempName As String
   Dim strTempPath As String
'first create destination workbook
   Set wbDestination = Workbooks.Add
   strDestName = wbDestination.Name
'set the source workbook and sheet
   Set wbSource = ActiveWorkbook
   Set wsSource = wbSource.ActiveSheet
'copy the activesheet to the new workbook
   wsSource.Copy After:=Workbooks(strDestName).Sheets(1)
'save with a temp name
   strTempPath = Environ$("temp") & "\"
   strTempName = "List obtained from " & wbSource.Name & ".xlsx"
   With wbDestination
      .SaveAs strTempPath & strTempName
'now email the destination workbook
      Set OutApp = CreateObject("Outlook.Application")
      Set OutMail = OutApp.CreateItem(0)
      With OutMail
         .To = strTo
         .Subject = strSubject
         .body = strBody
         .Attachments.Add wbDestination.FullName
'use send to send immediately or display to show on the screen
         .Display 'or .Display
      End With
      .Close False
  End With
'delete temp workbook that you have attached to your mail
   Kill strTempPath & strTempName
'clean up the objects to release the memory
   Set wbDestination = Nothing
   Set wbSource = Nothing
   Set wsSource = Nothing
   Set OutMail = Nothing
   Set OutApp = Nothing
Exit Function
eh:
   MsgBox err.Description
End Function

