Attribute VB_Name = "MMouse"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MMouse"

Global Const MOUSEEVENTF_LEFTDOWN = &H2
Global Const MOUSEEVENTF_LEFTUP = &H4
Global Const MOUSEEVENTF_RIGHTDOWN As Long = &H8
Global Const MOUSEEVENTF_RIGHTUP As Long = &H10
Global Const MOUSEEVENTF_ABSOLUTE = &H8000

Global Const MOUSEEVENTF_MIDDLEDOWN = &H20
Global Const MOUSEEVENTF_MIDDLEUP = &H40
Global Const MOUSEEVENTF_MOVE = &H1

Global Const MOUSEEVENTF_XDOWN = &H80
Global Const MOUSEEVENTF_XUP = &H100
Global Const MOUSEEVENTF_WHEEL = &H800
Global Const MOUSEEVENTF_HWHEEL = &H1000
     
Type POINTAPI
    Xcoord As Long
    Ycoord As Long
End Type

Sub Set_Layout_Query_Parameter()
    Dim qry As Parameter
    ThisWorkbook.Queries("Current_Mouse_Layout").Formula = ASPAS_DUPLAS & Get_Windows_UserName & ASPAS_DUPLAS
End Sub

Sub Print_Cursor_Current_Position()
    Dim llCoord As POINTAPI
    GetCursorPos llCoord
    Debug.Print "X Position: " & llCoord.Xcoord & vbNewLine & "Y Position: " & llCoord.Ycoord
End Sub
Sub Mouse_Click()
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
End Sub
Sub Drag_Mouse(x, y)
    mouse_event MOUSEEVENTF_LEFTDOWN, 0, 0, 0, 0
    Sleep 50
    SetCursorPos x, y
    Sleep 50
    mouse_event MOUSEEVENTF_LEFTUP, 0, 0, 0, 0
End Sub
Public Function dllGetMonitors() As Long
    Const SM_CMONITORS = 80
    dllGetMonitors = GetSystemMetrics32(SM_CMONITORS)
End Function
Public Function dllGetHorizontalResolution() As Long
    Const SM_CXVIRTUALSCREEN = 78
    dllGetHorizontalResolution = GetSystemMetrics32(SM_CXVIRTUALSCREEN)
End Function
Public Function dllGetVerticalResolution() As Long
    Const SM_CYVIRTUALSCREEN = 79
    dllGetVerticalResolution = GetSystemMetrics32(SM_CYVIRTUALSCREEN)
End Function

Public Sub Show_DisplayInfo()
    Dim msg As String
    msg = "Total monitors: " & vbTab & vbTab & dllGetMonitors
    msg = msg & vbCrLf & "Horizontal Resolution: " & vbTab & dllGetHorizontalResolution
    msg = msg & vbCrLf & "Vertical Resolution: " & vbTab & dllGetVerticalResolution
    Call Message_Information(msg, "Display info")
End Sub

