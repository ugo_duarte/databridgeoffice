Attribute VB_Name = "MGlobal"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MGlobal"

'    /$$$$$$  /$$           /$$                 /$$
'   /$$__  $$| $$          | $$                | $$
'  | $$  \__/| $$  /$$$$$$ | $$$$$$$   /$$$$$$ | $$
'  | $$ /$$$$| $$ /$$__  $$| $$__  $$ |____  $$| $$
'  | $$|_  $$| $$| $$  \ $$| $$  \ $$  /$$$$$$$| $$
'  | $$  \ $$| $$| $$  | $$| $$  | $$ /$$__  $$| $$
'  |  $$$$$$/| $$|  $$$$$$/| $$$$$$$/|  $$$$$$$| $$
'   \______/ |__/ \______/ |_______/  \_______/|__/
                                           
' CONSTANTS ---------------------------------------------
Global Const DQUOTE As String = """"
Global Const ENCRYPT_SEPARATOR As String = "|B64|"
Global Const FOLDER_DEALDETAILS As String = "DealDetails"
Global Const MODEL_IMG_CHG_LOGO As String = "img_CHG_Logo"
Global Const MODEL_LABEL_HEADER As String = "label_Header"
Global Const MSG_COMPANY As String = "CHG-Meridian"
Global Const MSG_FAIL As String = "Fail!"
Global Const MSG_SUCCESS As String = "Success!"
Global Const ASPAS_DUPLAS As String = """"


Global Const SEPARADOR As String = "_"
Global Const SPLIT1 As String = "|"
Global Const SPLIT2 As String = "#"
Global Const TAB4 As String = "    "
Global Const TAG_CURRENT_RANGE As String = "_CR"
Global Const TAG_FOOTER As String = "_Footer"
Global Const TAG_HEADER As String = "_Header"
Global Const TAG_RANGE As String = "_Range"

Global Const PATTERN_XML As String = "(\<TAG\>.+?\<\/TAG\>)"
Global Const PATTERN_OLD_LEASE As String = "(\d{4}\-\d{2,4}\-\d{3})"
Global Const PATTERN_CNPJ As String = "(\d{2}\.?\d{3}\.?\d{3}\/?\d{4}\-?\d{2})"

Global Const REPORTS_FOLDER As String = "S:\TI\Projetos\CHGBrazil\Reports\"

' OBJECTS -----------------------------------------------
Global gTimerFinish As String
Global gTimerStart As String
Global gTimer As Date
Global gTimeserial As String
Global gCurrentDeal As String

Global gFileID As Long
Global gcolFolderPaths As Collection
Global gdicPaths As Dictionary
Global gdicGlobalParameters As Dictionary

Global gSequence As Long

'Scripting
Global gobjFSO As Scripting.FileSystemObject
Global gobjFile As Scripting.File
Global gobjFolder As Scripting.Folder
Global gobjSubFolder As Scripting.Folder
Global gActiveWorksheet As Worksheet
Global gDicWorksheets As Dictionary

Enum enmFileExtension
    PDF = 1
    TXT
    XLS
    XML
    SemFiltro
End Enum

Enum enmColors
    enmColors_Vermelho = 255
    enmColors_Amarelo = 65535
    enmColors_Verde = 65280
    enmColors_Azul_Agua = 16776960

End Enum
Sub Global_Finalize(Optional ProtectWorksheet As Boolean = False)
    Const SUB_NAME As String = "Global Finalize"
    Call Log_Step(SUB_NAME)
    
    Set gobjFSO = Nothing
    Set gobjFolder = Nothing
    Set gobjFile = Nothing
    Set gcolFolderPaths = Nothing
    Set gDicWorksheets = Nothing
    gFileID = 0

    If ProtectWorksheet Then
        Call MSetup.Setup_Worksheets_Locks
    End If

    gActiveWorksheet.Activate

    Call MStructure.OptimizeVBA(True)
    
    Call Log_Step(" === End Program === ")

End Sub
Sub Global_Initialize(Optional UnprotectWorksheet As Boolean = False)
    Const SUB_NAME As String = "Global Initialize"
    Call Log_Step(SUB_NAME)
    
    Call MStructure.OptimizeVBA(False)
    
    
    Set gActiveWorksheet = ActiveSheet
    Set gobjFSO = New Scripting.FileSystemObject
    Set gcolFolderPaths = New Collection
    

    gSequence = 0
    gFileID = 0
    gTimer = Now
    gTimerStart = Now
    
    If UnprotectWorksheet Then
        Call MSecurity.Unprotect_All_Worksheet
    End If
    
    Call MSetup.Setup_Worksheets_Locks
    
    Call MRegex.New_Regex
    Call MSecurity.Get_Project_Name
    Call Global_Dictionary_Load
    
    gTimeserial = MStructure.New_Timeserial
    
End Sub

Sub Global_Dictionary_Load()
    Const SUB_NAME As String = "Global Dictionary Load"
    Call Log_Step(SUB_NAME)
    
    Set gdicGlobalParameters = Load_Dictionary("Menu_Parameters")
End Sub

