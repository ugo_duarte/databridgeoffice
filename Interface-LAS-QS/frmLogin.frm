VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmLogin 
   Caption         =   "Login"
   ClientHeight    =   890
   ClientLeft      =   -660
   ClientTop       =   -2625
   ClientWidth     =   6405
   OleObjectBlob   =   "frmLogin.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmLogin"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False























Option Explicit

Private Sub imgCHG_MouseUp(ByVal Button As Integer, ByVal Shift As Integer, ByVal x As Single, ByVal y As Single)
    Const SUB_NAME As String = "Login"
    If tbPassword.Text = WORKBOOK_PASSWORD Then
        Call MsgBox(MSG_SUCCESS, vbInformation, SUB_NAME)
        Call MSecurity.Unprotect_All_Worksheet
        Call MSecurity.Show_All_Worksheets
    Else
        Call MsgBox(MSG_FAIL, vbCritical, SUB_NAME)
        Call MSetup.Setup_Worksheets_Visibility
        Call MSecurity.Protect_All_Worksheet
    End If
    Call UserForm_Terminate
End Sub

Private Sub UserForm_Initialize()
    Me.StartUpPosition = 0
    
    Me.Top = Application.Top + (Application.UsableHeight / 2) - (Me.Height / 2)
    Me.Left = Application.Left + (Application.UsableWidth / 2) - (Me.Width / 2)
    
    Me.Width = 330
    Me.Height = 70
    
    imgCHG.Width = 60
    imgCHG.Height = 20
    imgCHG.Top = 10
    imgCHG.Left = 240
    
    tbPassword.Width = 215
    tbPassword.Height = 20
    tbPassword.Top = 10
    tbPassword.Left = 10
    
End Sub

Private Sub UserForm_Terminate()
    Unload Me
End Sub
