Attribute VB_Name = "MBitbucket"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MBitbucket"

Global Const BITBUCKET_DIRECTORY As String = "C:\Users\udu\Documents\CHG-Meridian\SourceCode\databridgeoffice"
Global Const GIT_PROJECT_NAME As String = "Interface-LAS-QS"
Global Const DEPLOY_PATH As String = "C:\Users\udu\OneDrive\OneDrive - CHG-MERIDIAN AG\Deploy\"
Global Const DEPLOY_PATH_SOURCE As String = "C:\Users\udu\Documents\CHG-Meridian\SourceCode\databridgeoffice\"
Global PathToExport As String
Sub DeleteAndMake(Path As String)
    Const SUB_NAME As String = "Delete And Make"
    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    On Error Resume Next
    fso.DeleteFolder Path
    On Error GoTo 0

    MkDir Path

End Sub
Sub Deploy_To_OneDrive()
    Const SUB_NAME As String = "Deploy To OneDrive"
    Dim ProjectName As String
    Dim folderName As String

    folderName = DEPLOY_PATH & gProjectName & "\"
    Call MStructure.Create_Folder(folderName)
    ThisWorkbook.SaveCopyAs (folderName & ThisWorkbook.Name)

End Sub
Sub Deploy_To_SourceControl()
    Const SUB_NAME As String = "Deploy To Source Control"
    Dim ProjectName As String
    Dim folderName As String

    folderName = DEPLOY_PATH_SOURCE
    Call MStructure.Create_Folder(folderName)
    ThisWorkbook.SaveCopyAs (folderName & ThisWorkbook.Name)

End Sub
Sub ExportModules(Path As String, Optional ModuleName As String = vbNullString)
    Const SUB_NAME As String = "Export Modules"
    
    If Dir(Path) <> "" Then
        Kill Path & "*.*"
    End If

    Dim wkb As Workbook:   Set wkb = Excel.Workbooks(ThisWorkbook.Name)
    
    Dim unitsCount As Long
    Dim filePath As String
    Dim component As VBIDE.VBComponent
    Dim tryExport As Boolean

    For Each component In wkb.VBProject.VBComponents
        tryExport = True
        filePath = component.Name
        If ModuleName = vbNullString Or UCase(ModuleName) = UCase(component.Name) Then
            Select Case component.Type
                Case vbext_ct_ClassModule
                    filePath = filePath & ".cls"
                Case vbext_ct_MSForm
                    filePath = filePath & ".frm"
                Case vbext_ct_StdModule
                    filePath = filePath & ".bas"
                Case vbext_ct_Document
                    tryExport = False
            End Select

            If tryExport Then
                unitsCount = unitsCount + 1
                Debug.Print unitsCount & " exporting " & filePath
                component.Export Path & "\" & filePath
            End If
        End If
    Next

    Debug.Print "Exported at " & Path

End Sub
Sub GitPush()
    Const SUB_NAME As String = "Git Push"
    Debug.Print "git add ."
    Debug.Print "git commit"
    Debug.Print "git push"
End Sub
Sub GitSave()
    Const SUB_NAME As String = "Git Save"
    PathToExport = BITBUCKET_DIRECTORY & "\" & GIT_PROJECT_NAME

    Call MGlobal.Global_Initialize

    Call DeleteAndMake(PathToExport)
    Call ExportModules(PathToExport)

    Call Deploy_To_OneDrive
    Call Deploy_To_SourceControl

    'Call PrintAllCode(PathToExport)
    'Call PrintAllContainers(PathToExport)

    Call MGlobal.Global_Finalize

    Call MInterface.Message_Information(MSG_SUCCESS)

End Sub
Public Sub Import_Source_Files()
    Const SUB_NAME As String = "Import Source Files"
    On Error Resume Next

    Dim File As String
    Dim regx As RegExp
    Dim FolderGitDirectory As String
    
    Set regx = New RegExp
    regx.Global = True
    regx.IgnoreCase = True
    regx.Pattern = "\.(cls|bas|frm)"
    FolderGitDirectory = BITBUCKET_DIRECTORY & "\" & GIT_PROJECT_NAME & "\"
    File = Dir(FolderGitDirectory)

    Call Remove_All_Modules_Except_This

    While (File <> vbNullString)

        If regx.Test(File) And InStr(1, File, MODULE_NAME) = 0 Then
            Application.VBE.ActiveVBProject.VBComponents.Import FolderGitDirectory & File
        End If
        File = Dir
    Wend
        
End Sub
Sub PrintAllCode(Path As String)
    Const SUB_NAME As String = "Print All Code"
    Dim Item As Variant
    Dim textToPrint As String
    Dim lineToPrint As String

    For Each Item In ThisWorkbook.VBProject.VBComponents
        lineToPrint = Item.CodeModule.Lines(1, Item.CodeModule.CountOfLines)
        Debug.Print lineToPrint
        textToPrint = textToPrint & vbCrLf & lineToPrint
    Next Item


    If Dir(Path) <> "" Then Kill Path & "*.*"
    SaveTextToFile textToPrint, Path & "\all_code.vb"

End Sub
Sub PrintAllContainers(Path As String)
    Const SUB_NAME As String = "Print All Containers"

    Dim Item As Variant
    Dim textToPrint As String
    Dim lineToPrint As String

    For Each Item In ThisWorkbook.VBProject.VBComponents
        lineToPrint = Item.Name
        Debug.Print lineToPrint
        textToPrint = textToPrint & vbCrLf & lineToPrint
    Next Item

    SaveTextToFile textToPrint, Path & "\all_modules.vb"

End Sub
Sub Remove_All_Modules_Except_This(Optional ModuleName As String = MODULE_NAME)

    Dim unitsCount As Long
    Dim filePath As String
    Dim vbCom As Object
    Dim component As VBIDE.VBComponent
    Set vbCom = Application.VBE.ActiveVBProject.VBComponents

    
    For Each component In ThisWorkbook.VBProject.VBComponents

        filePath = component.Name
        If filePath <> ModuleName Then
            Select Case component.Type
                Case vbext_ct_ClassModule
                    vbCom.Remove VBComponent:=vbCom.Item(filePath)
            Case vbext_ct_MSForm
                    vbCom.Remove VBComponent:=vbCom.Item(filePath)
            Case vbext_ct_StdModule
                    vbCom.Remove VBComponent:=vbCom.Item(filePath)

        End Select

        End If

    Next

End Sub
Sub SaveTextToFile(dataToPrint As String, PathToExport As String)

    Dim fileSystem As Object
    Dim textObject As Object
    Dim fileName As String
    Dim newFile As String
    Dim shellPath As String

    If Dir(ThisWorkbook.Path & newFile, vbDirectory) = vbNullString Then MkDir ThisWorkbook.Path & newFile
    
    Set fileSystem = CreateObject("Scripting.FileSystemObject")
    Set textObject = fileSystem.CreateTextFile(PathToExport, True)
    
    textObject.WriteLine dataToPrint
    textObject.Close

    On Error GoTo 0
    Exit Sub

CreateLogFile_Error:

    MsgBox "Error " & err.Number & " (" & err.Description & ") in procedure CreateLogFile of Sub mod_TDD_Export"

End Sub


