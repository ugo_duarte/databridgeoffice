Attribute VB_Name = "MContractIntegrity"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MContractIntegrity"

'Desenvolvimento

'Algoritmo
' Iniciar da pasta 'Backoffice' - OK
' Mapear todas as pastas por nivel - OK
' Identificar os nomes individuais da pasta Split '/' - TODO
' Identificar as pastas por nr. contrato / Lease schedule - OK
' Mapear os arquivos presentes na pasta baseado na lista de lease schedules selecionados - OK
' Identificar palavras chaves como SIGNED / CANCELED
' Identificar as chaves das notas e o valor total da mesma


'Parte 2

'Baseado na lista de Lease Schedules
'Installation Certificate - Purchases - Supplier invoice no
' Mover da pasta principal de download para a pasta localizada com o LS baseado na Chave da NF

Sub Map_Folder_Backoffice()
    Const SUB_NAME              As String = "Map Folder Backoffice"
    On Error GoTo ErrorHandler
    
    Dim xDoc                As MSXML2.DOMDocument
    Dim xDocNode            As IXMLDOMNode
        
    Dim key                 As Variant
    Dim Item                As Variant
    
    Dim IsMainNode          As Boolean
    
    Dim wksData             As Worksheet
    
    Call MGlobal.Global_Initialize
    
    Call MStructure.FoldersPaths_To_Collection
    
    If gdicPaths.Count > 0 Then
        
        'CLEAN
        Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Folders_Backoffice")
        Call MStructure.Generate_Worksheet_From_Model(wksData, True, "Model_Data_Map_Folder")
        
        'IMPORT: XML
        gFileID = 0
        For Each key In gdicPaths.Keys
            gFileID = gFileID + 1
            wksData.Range("A1").Offset(Get_Last_Row(wksData, 1)).Resize(, 2) = Array(key, MContractIntegrity.Get_Contract_Number(key))
        Next key
        wksData.Columns.AutoFit
    End If 'If gcolFolderPaths.Count > 0 Then
    
    
Finish:
    Call MGlobal.Global_Finalize
    Exit Sub
ErrorHandler:
    GoTo Finish
    
    
End Sub
Sub Select_From_List_Lease_Schedules()
    Dim wksData As Worksheet
    Dim wksDataFiles As Worksheet
    Dim wksList As Worksheet
    Dim aData() As Variant
    Dim aList() As Variant
    Dim aDataRow() As Variant
    Dim aModelDataFiles() As Variant
    
    Dim iData As Long
    Dim iList As Long
    
    Dim ColumnDataLS As Long
    Dim ColumnDataOriginalPath As Long
    Dim ColumnListLS As Long
    
    Dim ColumnDataFiles_LS As Long
    Dim ColumnDataFiles_FilePath As Long
    Dim ColumnDataFiles_FileName As Long
    Dim ColumnDataFiles_Extension As Long
    Dim ColumnDataFiles_Classification As Long

    
    Dim dicData As Dictionary
    Dim dicList As Dictionary
    
    Dim keyLeaseSchedule As Variant
    Dim FolderPath As String
    
    Dim dicFiles As Dictionary
    Dim keyFile As Variant
    Dim ItemFile As Variant
    
    'Process
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Folders_Backoffice")
    Set wksList = MStructure.Get_Worksheet_By_Codename("wksList_Lease_Schedules")
    Set wksDataFiles = MStructure.Get_Worksheet_By_Codename("wksData_Lease_Schedules_Files")
    
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    aList = MStructure.Get_Current_Region(wksList.Range("A1"))
    
    ColumnDataLS = MStructure.Get_Column_Number(aData, "Lease Schedule")
    ColumnDataOriginalPath = MStructure.Get_Column_Number(aData, "Original Path")
    ColumnListLS = MStructure.Get_Column_Number(aList, "Lease Schedule")
    
    Set dicData = MStructure.Load_Dictionary_From_Array(aData, ColumnDataLS, 0)
    Set dicList = MStructure.Load_Dictionary_From_Array(aList, ColumnListLS, 0)
    
    Call MStructure.Generate_Worksheet_From_Model(wksDataFiles, True, "Model_Data_Lease_Schedules_Files")
    aModelDataFiles = MStructure.Get_Model_Range("Model_Data_Lease_Schedules_Files")
    
    ColumnDataFiles_LS = MStructure.Get_Column_Number(aModelDataFiles, "Lease Schedule")
    ColumnDataFiles_FilePath = MStructure.Get_Column_Number(aModelDataFiles, "File Path")
    ColumnDataFiles_FileName = MStructure.Get_Column_Number(aModelDataFiles, "File Name")
    ColumnDataFiles_Extension = MStructure.Get_Column_Number(aModelDataFiles, "Extension")
    ColumnDataFiles_Classification = MStructure.Get_Column_Number(aModelDataFiles, "Classification")
    
    For Each keyLeaseSchedule In dicList
    
        If dicData.Exists(keyLeaseSchedule) Then
        
            aDataRow = dicData(keyLeaseSchedule)
            
            FolderPath = aDataRow(ColumnDataOriginalPath)
            Set dicFiles = New Dictionary
            Call MStructure.FilesPaths_To_Collection(dicFiles, "", FolderPath)
            
            For Each keyFile In dicFiles.Keys
            
                ReDim aModelDataFiles(LBound(aModelDataFiles) To UBound(aModelDataFiles), LBound(aModelDataFiles, 2) To UBound(aModelDataFiles, 2))
                
                ItemFile = dicFiles(keyFile)
                
                aModelDataFiles(1, ColumnDataFiles_LS) = keyLeaseSchedule
                aModelDataFiles(1, ColumnDataFiles_FilePath) = keyFile
                aModelDataFiles(1, ColumnDataFiles_FileName) = ItemFile
                aModelDataFiles(1, ColumnDataFiles_Extension) = gobjFSO.GetExtensionName(ItemFile)
                aModelDataFiles(1, ColumnDataFiles_Classification) = Get_Classification(keyFile)
                
                wksDataFiles.Range("A1").Offset(Get_Last_Row(wksDataFiles, ColumnDataFiles_LS)).Resize(, UBound(aModelDataFiles, 2)) = aModelDataFiles
                
            Next keyFile
            
        End If 'If dicData.Exists(keyLeaseSchedule) Then
    Next keyLeaseSchedule
    
    
End Sub
Function Get_Classification(ByVal Text As String)
    Dim RX As RegExp
    Set RX = New RegExp
    
    RX.Global = True
    RX.IgnoreCase = True
    RX.MultiLine = True
    
    RX.Pattern = "((?:CANCEL[EDAO]+)|(?:SIGNED))"
    Get_Classification = ""
    If RX.Test(Text) Then
       Set regExMatches = RX.Execute(Text)
        
       Get_Classification = regExMatches(regExMatches.Count - 1).SubMatches(0)
    End If
    
    
End Function

Function Get_Contract_Number(ByVal Text As String)
    Dim RX As RegExp
    Set RX = New RegExp
    
    RX.Global = True
    RX.IgnoreCase = True
    RX.MultiLine = True
    
    RX.Pattern = "\\?((?:\d{3,4}\s*\-\s*\d{2,3}\s*\-\s*\d{2,4}\s*)|(?:\d{6,}))\s*"
    Get_Contract_Number = ""
    If RX.Test(Text) Then
       Set regExMatches = RX.Execute(Text)
        
       Get_Contract_Number = regExMatches(regExMatches.Count - 1).SubMatches(0)
    End If
    
    
End Function
