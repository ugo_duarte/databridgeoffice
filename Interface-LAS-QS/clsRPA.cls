VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsRPA"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sequence As Long
Public Command As String
Public Section As String
Public Field As String
Public Enabled As Boolean
Public Warnings As String
Public Warning As String
Public DataType As String

Public CurrentValue As Variant

Sub Print_Info(Optional msg As String = "")
    Debug.Print "-- RPA"
    Debug.Print "   Sequence: " & sequence
    Debug.Print "   Command: " & Command
    Debug.Print "   Section: " & Section
    Debug.Print "   Field: " & Field
    Debug.Print "   Enabled: " & Enabled
    Debug.Print "   Warnings: " & Warnings
    Debug.Print "   Warning: " & Warning
    Debug.Print "   DataType: " & DataType
    Debug.Print "   CurrentValue: " & CurrentValue
    
    Debug.Print "------"
    
    Dim StatusMsg As String
    If Field <> vbNullString Then
    
        StatusMsg = Trim(msg & Section & " - " & Field)
    Else
        StatusMsg = Trim(msg & Section)
    End If
    Application.StatusBar = StatusMsg
    
End Sub
