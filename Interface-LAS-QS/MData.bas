Attribute VB_Name = "MData"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MData"

Sub Correct_TaxID(ByRef aData() As Variant, Table As String)
    Dim i As Long
    Dim CurrentValue As String
    Dim ColumnTaxID As Long
    ColumnTaxID = 0
    Select Case UCase(Table)
        Case UCase("DMS_Addresses")
            ColumnTaxID = Get_Column_Number(aData, "IDNumber")
        Case UCase("CASHFLOW_Beneficiaries")
            ColumnTaxID = Get_Column_Number(aData, "TaxIDNumber")
        Case UCase("QUALISOFT_TBL_CLIJURIDICA"), UCase("QUALISOFT_TBL_FORNECJUR")
            ColumnTaxID = Get_Column_Number(aData, "NUM_CGC")
        
    End Select
    If ColumnTaxID > 0 Then
        For i = LBound(aData) + 1 To UBound(aData)
            CurrentValue = aData(i, ColumnTaxID)
            CurrentValue = Replace(CurrentValue, ".", "")
            CurrentValue = Replace(CurrentValue, "/", "")
            CurrentValue = Replace(CurrentValue, "-", "")
            
            aData(i, ColumnTaxID) = Right(String(14, "0") & CurrentValue, 14)
        Next i
        
        
    End If
    
End Sub
