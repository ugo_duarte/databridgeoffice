Attribute VB_Name = "MInterfaceLASQS"
Option Explicit
Option Private Module
Sub Query_Interface_LAS_QS_Schedules()
    '------------------------------------------------------'
    'Fun��o para gerar os dados importados na aba "Temp"   '
    '------------------------------------------------------'
    On Error GoTo ErrorHandler
    
    Dim strSQL As String
    Dim wks As Worksheet
    
    Dim wksReport As Worksheet
    Dim wksLASSchedules As Worksheet
    Dim wksLASInstallation As Worksheet
    Dim wksDMSDealPrint As Worksheet
    Dim wksQSFraming As Worksheet
    
    
    Set wks = Get_Worksheet_By_Codename("wksTemporary")
    
    Set wksLASSchedules = Get_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
    Set wksLASInstallation = Get_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
    Set wksDMSDealPrint = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")
    Set wksQSFraming = Get_Worksheet_By_Codename("wksData_QS_Framing")
    
    Set wksReport = Get_Worksheet_By_Codename("wksTemporary")
    
    
    'Query
    Application.StatusBar = "Generating Data via SQL"

    strSQL = vbNullString
    strSQL = strSQL & " SELECT *  " & vbNewLine
        
    strSQL = strSQL & "    FROM ((([@LAS_LS$]  As LASLS " & vbNewLine
    strSQL = strSQL & "   INNER JOIN [@LAS_IC$]  As LASIC " & vbNewLine
    strSQL = strSQL & "           ON LASIC.[LeaseSchedule] = LASLS.[LeaseSchedule] ) " & vbNewLine
    
    strSQL = strSQL & "   INNER JOIN [@DMS_PRINT$]  As DMSP " & vbNewLine
    strSQL = strSQL & "           ON DMSP.[DealNo] = LASIC.[Deal] ) " & vbNewLine
    
    strSQL = strSQL & "   INNER JOIN [@QS_FRAMING$]  As QSF " & vbNewLine
    strSQL = strSQL & "           ON QSF.[Contrato] = LASLS.[LeaseSchedule] ) " & vbNewLine

    'Data Source
    strSQL = Replace(strSQL, "@LAS_LS", wksLASSchedules.Name)
    strSQL = Replace(strSQL, "@LAS_IC", wksLASInstallation.Name)
    strSQL = Replace(strSQL, "@DMS_PRINT", wksDMSDealPrint.Name)
    strSQL = Replace(strSQL, "@QS_FRAMING", wksQSFraming.Name)

    'Execution
    Call Generate_Data_SQL(enmConnectionType_MyWorkbook, strSQL, wksReport, True)

Finish:
    On Error GoTo 0
    Exit Sub

ErrorHandler:
    Call MsgBox(err.Description, vbCritical, MSG_COMPANY)
    GoTo Finish
    

End Sub
Sub Extract_DMS_Data(ByVal DMSDeal As String, ByVal DecriptedString As String)
    Dim i As Long
    Dim iLayout As Long
    Dim j As Long
    
    Dim wksTemp As Worksheet
    Dim wksLayout As Worksheet
    Dim wksData As Worksheet
    
    Dim aDataLayout() As Variant
    Dim aDescription() As Variant
    Dim aLayout() As Variant
    Dim aData() As Variant
    
    Dim ModelName As Name
    Dim dicNames As Dictionary
    
    Dim dicDescription As Dictionary
    Dim key As Variant
    Dim CurrentValue As Variant
    Dim Pattern As String

    Dim rng As Range

    Dim lay As Layout
    
    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
    Set wksLayout = Get_Worksheet_By_Codename("wksLayout_DMSDeal")
    Set wksData = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")
    
    aDataLayout = Get_Current_Region(Get_Model_Range("Model_Data_DMS"))
    Call MStructure.Erase_Data(aDataLayout)
    aDescription = Get_Table_XML_DMS_Description(DecriptedString)
    aLayout = Get_Current_Region(wksLayout.Range("A1"))
    lay = MSetup.New_Type_Layout(aLayout)
    
    Set dicNames = New Dictionary
    Set dicDescription = New Dictionary
    
    DMSDeal = MRegex.Regx_Replace(DMSDeal, " +", " ")
    
    For Each ModelName In ThisWorkbook.Names
        If Regx_Match(ModelName.Name, "Model.DMS.Data.") Then
            'Debug.Print ModelName.Name
            'Debug.Print ModelName.RefersToRange.AddressLocal
            If Not dicNames.Exists(ModelName.Name) Then
                If InStr(1, ModelName.RefersToRange.AddressLocal, ":") = 0 Then
                    ReDim aData(1 To 1, 1 To 1)
                    aData(1, 1) = wksTemp.Range(ModelName.RefersToRange.AddressLocal)
                Else
                    aData = wksTemp.Range(ModelName.RefersToRange.AddressLocal)
                End If
                Call dicNames.Add(ModelName.Name, aData)
            End If
        End If

    Next
    
    For iLayout = LBound(aLayout) + 1 To UBound(aLayout)
        CurrentValue = ""
        Select Case UCase(aLayout(iLayout, lay.Column_DataName))
            
            Case "DEALDESCRIPTION"
                For i = LBound(aDescription) To UBound(aDescription)
                    
                    If aDescription(i, 2) = aLayout(iLayout, lay.Column_DataField) Then
                        CurrentValue = aDescription(i, 5)
                        Exit For
                    End If
                    
                Next i
            
            Case Else
                Pattern = aLayout(iLayout, lay.Column_Treatment)
                Debug.Print "-------------------"
                Debug.Print Pattern
                Debug.Print aLayout(iLayout, lay.Column_Name)
                Debug.Print Now
                If Pattern <> vbNullString Then
                    If Regx_Match(DMSDeal, Pattern) Then
                        CurrentValue = regExMatches(0).SubMatches(CLng(aLayout(iLayout, lay.Column_DefaultValue)))
                        CurrentValue = Trim(MRegex.Regx_Replace(CurrentValue, "\s+", " "))
                        
                    End If
                End If
                Debug.Print Now
                
        End Select
        
        Select Case UCase(aLayout(iLayout, lay.Column_Type))
            Case "NUMBER"
                aDataLayout(1, aLayout(iLayout, lay.Column_ID)) = CDbl(Convert_Number(CurrentValue, Check_Number_Format))
            Case Else
                aDataLayout(1, aLayout(iLayout, lay.Column_ID)) = Trim(CurrentValue)
        End Select
        

    Next iLayout
    
    wksData.Range("A1").Offset(Get_Last_Row(wksData, 1)).Resize(UBound(aDataLayout), UBound(aDataLayout, 2)) = aDataLayout
    wksData.Columns.AutoFit
    
    
End Sub
Sub Interface_Layout_Model(wksLayout As Worksheet, wksReport As Worksheet, Model As String)
    Dim dicSchedules As Dictionary
    Dim dicLASInstalCert As Dictionary
    Dim dicLASInstalCert2 As Dictionary
    Dim dicDMS As Dictionary
    Dim dicFraming As Dictionary

    Dim aDataLAS() As Variant
    Dim aDataLASInstalCert() As Variant
    Dim aDataDMS() As Variant
    Dim aDataFraming() As Variant
    Dim aLayout() As Variant
    Dim aQSSchedule() As Variant

    Dim iLayout As Long
    Dim iDataLAS As Long
    Dim iDataDMS As Long
    Dim iDataCutDate As Long
    Dim lay As Layout
    Dim key As Variant
    Dim Key2 As Variant
    Dim Item As Variant
    Dim ItemValue As Variant
    Dim CutDateMonth As String

    Dim DataOrigin As String
    Dim ColumnSchedule As Long
    Dim ColumnLASICSchedule As Long
    Dim ColumnLASICDeal As Long
    Dim ColumnDMSDeal As Long
    Dim ColumnFramingSchedule As Long

    Dim InstallationCertificateFound As Boolean

    Dim wksData As Worksheet
    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
    aDataLAS = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
    aDataLASInstalCert = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")
    aDataDMS = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_QS_Framing")
    aDataFraming = MStructure.Get_Current_Region(wksData.Range("A1"))



    ColumnSchedule = Get_Column_Number(aDataLAS, "LeaseSchedule")
    ColumnLASICSchedule = Get_Column_Number(aDataLASInstalCert, "LeaseSchedule")
    ColumnLASICDeal = Get_Column_Number(aDataLASInstalCert, "Deal")
    ColumnDMSDeal = Get_Column_Number(aDataDMS, "DealNo")
    ColumnFramingSchedule = Get_Column_Number(aDataFraming, "Contrato")
    
    Set dicDMS = Load_Dictionary("Data_DMS_I", ColumnDMSDeal, 0)
    Set dicLASInstalCert = Load_Dictionary("Data_LAS_InstallationCertificate_I", ColumnLASICSchedule, ColumnLASICDeal)
    Set dicLASInstalCert2 = Load_Dictionary("Data_LAS_InstallationCertificate_I", ColumnLASICSchedule, 0)
    Set dicSchedules = Load_Dictionary("Data_LAS_I", ColumnSchedule, 0)
    Set dicFraming = Load_Dictionary("Data_Framing_I", ColumnFramingSchedule, 0)
    
    
    aLayout = MStructure.Get_Current_Region(wksLayout.Range("A1"))

    aQSSchedule = Get_Model_Range("Model_Report_QS_Schedules")

    lay = New_Type_Layout(aLayout)

    For iLayout = LBound(aLayout) + 1 To UBound(aLayout)
        Select Case UCase(aLayout(iLayout, lay.Column_DataOrigin))
            Case "DMS"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataDMS, aLayout(iLayout, lay.Column_DataName))
            Case "LAS"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataLAS, aLayout(iLayout, lay.Column_DataName))
            Case "FRAMING"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataFraming, aLayout(iLayout, lay.Column_DataName))
            Case "INSTALLATION"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataLASInstalCert, aLayout(iLayout, lay.Column_DataName))
        End Select
    Next iLayout

    For Each key In dicSchedules.Keys

        Call Erase_Data(aQSSchedule)
        InstallationCertificateFound = False
        For iLayout = LBound(aLayout) + 1 To UBound(aLayout)

            'Default Value

            Select Case UCase(aLayout(iLayout, lay.Column_DataOrigin))
                Case "INSTALLATION"
                    Item = dicLASInstalCert2(key)
                    If aLayout(iLayout, lay.Column_DataField) > 0 Then
                        ItemValue = Item(aLayout(iLayout, lay.Column_DataField))

                        If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = vbNullString Then
                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = ItemValue
                        End If

                    End If
                Case "DMS"
                    If dicLASInstalCert.Exists(key) Then
                        InstallationCertificateFound = True
                        Key2 = Trim(dicLASInstalCert.Item(key))
                        If dicDMS.Exists(Key2) Then
                            Item = dicDMS.Item(Key2)
                            If aLayout(iLayout, lay.Column_DataField) > 0 Then

                                If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) <> "" Then

                                    Select Case aLayout(iLayout, lay.Column_Name)
                                        Case "ScheduleTerm"
                                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) + CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))

                                        Case "PMTValue", "Servi�o", "GracePeriod", "Fator Leasing"
                                            Select Case aLayout(iLayout, lay.Column_Operation)
                                                Case "*"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) * CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                                Case "*1-"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) * (1 - CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format)))
                                                Case "+"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) + CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                                Case "-"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) - CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                            End Select

                                    End Select

                                Else
                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = Item(aLayout(iLayout, lay.Column_DataField))

                                End If 'If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) <> "" Then
                            End If 'If aLayout(iLayout, lay.Column_DataField) > 0 Then
                        End If 'If dicDMS.Exists(Key2) Then
                    End If 'If dicLASInstalCert.Exists(key) Then

                Case "LAS"
                    Item = dicSchedules(key)
                    If aLayout(iLayout, lay.Column_DataField) > 0 Then
                        ItemValue = Item(aLayout(iLayout, lay.Column_DataField))

                        If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = vbNullString Then
                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = ItemValue
                        End If

                    End If

                    Select Case aLayout(iLayout, lay.Column_Name)
                        Case "GracePeriod", ""
                            Select Case aLayout(iLayout, lay.Column_Operation)
                                Case "CutDate"

                                    If CDbl(aQSSchedule(1, aLayout(iLayout, lay.Column_ID))) > Day(ItemValue) Then
                                        aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateDiff("d", CDate(ItemValue), DateSerial(Year(ItemValue), Month(ItemValue), CInt(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)))))
                                    Else
                                        aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateDiff("d", CDate(ItemValue), DateAdd("M", 1, DateSerial(Year(ItemValue), Month(ItemValue), CInt(aQSSchedule(1, aLayout(iLayout, lay.Column_ID))))))
                                    End If

'                                    For iDataCutDate = LBound(aDataCutDate) + 1 To UBound(aDataCutDate)
'
'                                        If CDate(ItemValue) >= CDate(aDataCutDate(iDataCutDate, 1)) And CDate(ItemValue) <= CDate(aDataCutDate(iDataCutDate, 2)) Then
'                                            CutDateMonth = aDataCutDate(iDataCutDate, 3)
'                                            CutDateMonth = CDbl(CutDateMonth) + 1
'                                            If CDbl(CutDateMonth) > 12 Then
'                                                CutDateMonth = 1
'                                            End If
'
'                                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateSerial(Year(ItemValue), CutDateMonth, aQSSchedule(1, aLayout(iLayout, lay.Column_ID)))
'
'                                        End If
'                                    Next iDataCutDate

                                Case "DateDiff"
                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateDiff("D", CDate(ItemValue), CDate(aQSSchedule(1, aLayout(iLayout, lay.Column_ID))))
                            End Select

                    End Select


                Case "FRAMING"
                    If dicFraming.Exists(key) Then
                        Item = dicFraming.Item(key)
                        aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = Item(aLayout(iLayout, lay.Column_DataField))
                    End If

            End Select

            If Not aLayout(iLayout, lay.Column_Treatment) = vbNullString Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = Interface_Treatment(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), aLayout(iLayout, lay.Column_Treatment))
            End If

            If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = vbNullString Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = aLayout(iLayout, lay.Column_DefaultValue)
            End If

            If aLayout(iLayout, lay.Column_Type) = "Number" Or aLayout(iLayout, lay.Column_Type) = "Numerico" Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CDbl(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format))
            End If

            If Not aLayout(iLayout, lay.Column_Format) = vbNullString Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = VBA.Format(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), aLayout(iLayout, lay.Column_Format))
            End If

        Next iLayout

        If InstallationCertificateFound Then
            wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(, UBound(aQSSchedule, 2)) = aQSSchedule

        End If

    Next key

End Sub
Sub Interface_Framing()
    Dim wksLayout As Worksheet
    Dim wksReport As Worksheet
    Set wksLayout = Get_Worksheet_By_Codename("wksLayout_InterfaceFraming")
    Set wksReport = Get_Worksheet_By_Codename("wksData_QS_Framing")
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Data_Framing")
    Call Interface_Layout_Model(wksLayout, wksReport, "Layout_Interface_Framing")

    Call ThisWorkbook.RefreshAll
    Sleep 1000

    wksReport.Columns.AutoFit
End Sub
Sub Interface_LAS_QS_Schedules()
    Const SUB_NAME As String = "Interface QS Schedules"
    Dim wksLayout As Worksheet
    Dim wksReport As Worksheet
    Set wksLayout = Get_Worksheet_By_Codename("wksLayout_QS_Schedules")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_QS_Schedules")
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Report_QS_Schedules")
    Call Interface_Layout_Model(wksLayout, wksReport, "Layout_QS_Schedules")

End Sub
Sub Generate_Schedules_Interface_LAS_QS_Assets()
    Dim wks As Worksheet
    Dim aData() As Variant
    Dim aHeader() As Variant
    Dim i As Long
    Dim SQL As String
    Dim Table As String

    Table = "Gerar_Interface_LAS_QS_Assets"
    Set wks = MStructure.Get_Worksheet_By_Codename("wksList_GenerateContracts")
    
    
    
    SQL = "DELETE FROM " & Table & " WHERE UserCHG = '" & MSecurity.Get_Windows_UserName & "'"
    Call Delete_From_Access(SQL)


    aHeader = MStructure.Get_Model_Range("Models_Gerar_Interface_LAS_QS_Assets_Access")
    aData = MStructure.Get_Current_Region(wks.Range("A1"))
    aData = MStructure.Get_Data(aHeader, aHeader, aData, , True)

    If UBound(aData) > 1 Then

        Call Insert_UserCHG_To_Array(aData)
        Call Insert_To_Access_Recordset(Table, aHeader, aData)

    End If


End Sub
Sub Interface_LAS_QS_Assets()
    Const SUB_NAME As String = "Interface LAS QS Assets"

    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Set wksData = Get_Worksheet_By_Codename("wksData_InterfaceQSAssets")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_Interface_QS_Assets")
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Interface_Qualisoft_Header")

    Call MGenerator.Layout_to_Report(wksData.Range("A1"), Get_Model_Range("Layout_Interface_QS_Assets_I"), wksReport.Range("A1"), "Model_Interface_Qualisoft_Header")
    'Call MInterfaceXMLLAS.Interface_Layout(wksData, wksReport, "QS_Assets_CR", 1)
End Sub

Function Interface_Treatment(ByVal OriginalValue, ByVal Treatment As String, Optional ByVal ColumnName As String = vbNullString, Optional ShowErrorMessage As Boolean = True) As String
    Const SUB_NAME As String = "Interface_Treatment"

    Dim dicNames As Dictionary
    Dim aData() As Variant
    Dim i As Long
    Dim j As Long
    Dim Found As Boolean
    Dim rng As Range
    Dim ColumnReturn As Long


    If Not (IsEmpty(OriginalValue) Or OriginalValue = vbNullString) Then

        If dicNames Is Nothing Then
            Set dicNames = New Dictionary
        End If

        If Not dicNames.Exists(Treatment) Then
            Set rng = Get_Model_Range(Treatment)
            aData = rng.CurrentRegion
            Call dicNames.Add(Treatment, aData)
        End If

        aData = dicNames.Item(Treatment)
        If ColumnName <> vbNullString Then
            ColumnReturn = Get_Column_Number(aData, ColumnName)
        Else
            ColumnReturn = 2
        End If

        Found = False
        Interface_Treatment = vbNullString
        For i = LBound(aData) To UBound(aData)

            If UCase(OriginalValue) = UCase(aData(i, 1)) And OriginalValue <> vbNullString Then
                Interface_Treatment = aData(i, ColumnReturn)
                Found = True
                Exit For

            ElseIf aData(i, 1) = "@DEFAULT" Or aData(i, 1) = "@PADRAO" And Interface_Treatment = vbNullString Then
                Interface_Treatment = aData(i, ColumnReturn)
                Found = True
            End If

        Next i

        If Not Found And Interface_Treatment = vbNullString Then
            rng.Cells(Get_Last_Row(rng.Worksheet, 1) + 1, 1) = CStr(OriginalValue)
            If ShowErrorMessage Then
                Call MInterface.Message_Error("Falha ao buscar From-To" & vbCrLf & "Tratamento: " & Treatment & vbCrLf & "Valor: " & OriginalValue)
            End If
        End If
    End If

End Function
