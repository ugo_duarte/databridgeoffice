Attribute VB_Name = "MDMSDealFollowUp"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MDMSDealFollowUp"

' /$$$$$$$  /$$      /$$  /$$$$$$        /$$$$$$$$ /$$$$$$  /$$       /$$        /$$$$$$  /$$      /$$       /$$   /$$ /$$$$$$$
'| $$__  $$| $$$    /$$$ /$$__  $$      | $$_____//$$__  $$| $$      | $$       /$$__  $$| $$  /$ | $$      | $$  | $$| $$__  $$
'| $$  \ $$| $$$$  /$$$$| $$  \__/      | $$     | $$  \ $$| $$      | $$      | $$  \ $$| $$ /$$$| $$      | $$  | $$| $$  \ $$
'| $$  | $$| $$ $$/$$ $$|  $$$$$$       | $$$$$  | $$  | $$| $$      | $$      | $$  | $$| $$/$$ $$ $$      | $$  | $$| $$$$$$$/
'| $$  | $$| $$  $$$| $$ \____  $$      | $$__/  | $$  | $$| $$      | $$      | $$  | $$| $$$$_  $$$$      | $$  | $$| $$____/
'| $$  | $$| $$\  $ | $$ /$$  \ $$      | $$     | $$  | $$| $$      | $$      | $$  | $$| $$$/ \  $$$      | $$  | $$| $$
'| $$$$$$$/| $$ \/  | $$|  $$$$$$/      | $$     |  $$$$$$/| $$$$$$$$| $$$$$$$$|  $$$$$$/| $$/   \  $$      |  $$$$$$/| $$
'|_______/ |__/     |__/ \______/       |__/      \______/ |________/|________/ \______/ |__/     \__/       \______/ |__/
'


Function DMS_FollowUp_Mail_List()

End Function
Function DMS_FollowUp_Mail_HTML_Body_Model() As String
    Dim MailHTMLBody As String
    MailHTMLBody = gdicGlobalParameters(UCase("DMSFollowUpMail"))

    'CHGLogoRGB
    'font-family:Arial

    MailHTMLBody = MailHTMLBody

    '    MailHTMLBody = MailHTMLBody & "<html>"
    '    MailHTMLBody = MailHTMLBody & "<h1 style=""font-family:Arial;color: #5e9ca0;""><strong><span style=""color: #333399;"">CHG-Meridian</span></strong></h1>"
    '    MailHTMLBody = MailHTMLBody & "<h2 style=""font-family:Arial;color: #2e6c80;""><span style=""color: #ff0000;"">DMS Follow Up</span></h2>"
    '    MailHTMLBody = MailHTMLBody & "<p><span style=""font-family:Arial;background-color: #2b2301; color: #fff; display: inline-block; padding: 3px 10px; font-weight: bold; border-radius: 5px;"">DEAL #@DEAL IS APPROVED!</span></p>"
    '    MailHTMLBody = MailHTMLBody & "</html>"

    '"<img src=""cid:" & Fname & """height=520 width=750>"

    DMS_FollowUp_Mail_HTML_Body_Model = MailHTMLBody
End Function
Sub Mail_Approved_Deals()
    Const SUB_NAME As String = "Mail Approved Deals"
    Dim aDataApproved() As Variant
    Dim wksData As Worksheet
    Dim MailToList As String
    Dim MailCCList As String
    Dim MailSubject As String
    Dim MailBody As String

    Dim ColumnDeal As Long
    Dim ColumnSaleEmail As Long
    Dim ColumnInternalSalesEmail As Long
    Dim Deal As String
    Const MASCARA_VALOR As String = """R$ ""#,##0.00"
    Const MASCARA_NUMERO As String = "###.###"
    Dim i As Long

    Call MGlobal.Global_Initialize
    
    Set wksData = Get_Worksheet_By_Codename("wksData_DMSDeals_Approved")
    aDataApproved = MStructure.Get_Current_Region(wksData.Range("A1"))
    ColumnDeal = Get_Column_Number(aDataApproved, "Deal")
    ColumnSaleEmail = Get_Column_Number(aDataApproved, "SalesEmail")
    ColumnInternalSalesEmail = Get_Column_Number(aDataApproved, "InternalSalesEmail")
    For i = LBound(aDataApproved) + 1 To UBound(aDataApproved)

        Deal = aDataApproved(i, ColumnDeal)

        MailToList = aDataApproved(i, ColumnInternalSalesEmail)
        MailCCList = aDataApproved(i, ColumnSaleEmail)
        MailBody = DMS_FollowUp_Mail_HTML_Body_Model

        MailBody = Replace(MailBody, "@DealName", aDataApproved(i, Get_Column_Number(aDataApproved, "DealName")))
        MailBody = Replace(MailBody, "@Deal", Deal)
        MailBody = Replace(MailBody, "@AddressNumber", aDataApproved(i, Get_Column_Number(aDataApproved, "AddressNumber")))
        MailBody = Replace(MailBody, "@AddressName", aDataApproved(i, Get_Column_Number(aDataApproved, "AddressName")))
        MailBody = Replace(MailBody, "@LeaseOrigination", Format(aDataApproved(i, Get_Column_Number(aDataApproved, "LeaseOrigination")), MASCARA_VALOR))
        MailBody = Replace(MailBody, "@UpdateDate", Format(aDataApproved(i, Get_Column_Number(aDataApproved, "UpdateDate")), "dd/mm/yyyy hh:mm:ss"))
        MailBody = Replace(MailBody, "@InternalSalesUser", aDataApproved(i, Get_Column_Number(aDataApproved, "InternalSalesUsername")))

        MailSubject = "[DMS FOLLOW UP] APPROVED DEAL #" & Deal
        Call MEmail.Create_Email(MailToList, MailCCList, MailSubject, MailBody, CanSend:=False)
        Exit For
    Next i

    Call DirectSendAndReceiveCall

    Call MGlobal.Global_Finalize

End Sub
Sub Update_Access_LAS_InstallationCertificate()
    Const SUB_NAME As String = "Update Access LAS Installation Certificate"
    On Error GoTo ErrorHandler

    Dim Table As String
    Dim aHeader() As Variant
    Dim aData() As Variant
    Dim wksData As Worksheet
    Dim i As Long


    Call MGlobal.Global_Initialize

    Table = "LAS_InstallationCertificate"
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_LAS_InstCert")
    aHeader = MStructure.Get_Model_Range("Model_Access_LAS_InstallationCertificate")
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    aData = MStructure.Get_Data(aHeader, aHeader, aData, , True)


    Call Delete_From_Access("DELETE FROM " & Table)
    Call Insert_Timeserial_To_Array(aData)
    Call Insert_To_Access_Recordset(Table, aHeader, aData)

Fim:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim

End Sub


Sub Test_Insert_To_Access_Deal_Details()
    Dim SQL As String
    Dim Table As String
    Dim aHeader() As Variant
    Dim aData() As Variant

    Table = "DMS_FollowUp_EmittedDeals"

    aHeader = MStructure.Get_Model_Range("Model_DMS_FollowUp_EmittedDeals_Access")
    aData = MStructure.Get_Model_Range("Model_DMS_FollowUp_EmittedDeals_Access")

    Call MGlobal.Global_Initialize
    Call Insert_To_Access(Table, aHeader, aData, aData)
    Call MGlobal.Global_Finalize
End Sub
Sub Generate_DMS_Deals_Approved_SQL()
    Const SUB_NAME As String = "Generate DMS Deals Approved SQL"
    Dim SQLQuery As String

    Dim wksData As Worksheet
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals")
    Dim wksDataEmitted As Worksheet
    'Set wksDataEmitted = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    Set wksDataEmitted = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    
    Dim wksDataApproved As Worksheet
    Set wksDataApproved = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Approved")
    Call MStructure.Generate_Worksheet_From_Model(wksDataApproved, True, "Model_Data_DMS_Deals")
    SQLQuery = vbNullString
    SQLQuery = SQLQuery & " SELECT DMSDeals.* " & vbNewLine
    SQLQuery = SQLQuery & "   FROM [" & wksData.Name & "$]  As DMSDeals  " & vbNewLine
    SQLQuery = SQLQuery & "   LEFT JOIN [" & wksDataEmitted.Name & "$]  As ED  " & vbNewLine
    SQLQuery = SQLQuery & "          ON (DMSDeals.[Deal no] = ED.[Deal no] "
    SQLQuery = SQLQuery & "         AND cdate(DMSDeals.[Update date]) = cdate(ED.[Update date]) "
    SQLQuery = SQLQuery & "         AND ccur(DMSDeals.[Lease origination]) = ccur(ED.[Lease origination]) "
    SQLQuery = SQLQuery & "             )  " & vbNewLine
    SQLQuery = SQLQuery & "       WHERE ED.[Deal no] IS NULL  " & vbNewLine
    SQLQuery = SQLQuery & "         AND DMSDeals.[Deal status] = 'Approved'  " & vbNewLine
    SQLQuery = SQLQuery & "         AND DMSDeals.[Success probability] = 'Won (100%)'  " & vbNewLine

    Call MSQL.Generate_Data_SQL(enmConnectionType_MyWorkbook, SQLQuery, wksDataApproved, True)

End Sub
Sub Export_DMS_Deals_Emitted()
    Dim rng As Range
    Dim wks As Worksheet
    
    'Set wks = Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    Set wks = Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    
    Set rng = wks.Range("A1").CurrentRegion
    
    If rng.Rows.Count > 1 Then
        Call MExport.Export_Range_To_XLSX("DMS_Deals_Emitted", rng)
    End If

End Sub
Sub Import_Deal_Details()
    Dim wks As Worksheet

    Dim ImportString As String
    Dim TextCollection As Collection
    Dim TextCollectionNames As Collection
    
    Set TextCollection = New Collection
    Set TextCollectionNames = New Collection
    
    Call MStructure.Import_TXT(TextCollection, TextCollectionNames)

    ImportString = TextCollection(1)
    If MRegex.Regx_Match(ImportString, "Deal\s*\#(\d+)") Then
        gCurrentDeal = regExMatches(0).SubMatches(0)
    End If


    Call MDealDetails.Import_Data_From_Encrypted_String(ImportString)
    Set wks = MStructure.Get_Worksheet_By_Codename("wksData_DealDetails")
    Call MModels.Modeling_Data(MODEL_DATA_DEALDETAILS, wks, "Deal Details and Asset Forecast", True)
    Get_Model_Range("DealDetails_Overview_Deal").Value = gCurrentDeal

End Sub
Sub ConfirmEmitted()
    Const SUB_NAME As String = "Confirm Emitted"
    On Error GoTo ErrorHandler

    Dim wksReportApproved As Worksheet
    Dim wksReportEmitted As Worksheet
    Dim wksActive As Worksheet
    Dim iApproved As Long
    Dim j As Long
    Dim jApproved As Long
    Dim jEmitted As Long
    Dim aDataApproved() As Variant
    Dim aModelEmitted() As Variant
    Dim aDataEmitted() As Variant
    Dim DealNo As String
    Dim ColumnDealNo As Long
    Dim ColumnDealName As Long

    Dim rngActive As Range
    Dim rngSelection As Range
    Dim rowsToDelete As Variant

    Call MGlobal.Global_Initialize
    
    Set rngSelection = Selection
    Set rngSelection = rngSelection.SpecialCells(xlCellTypeVisible)
    
    
    Set wksActive = rngSelection.Worksheet
    Set wksReportApproved = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Approved")
    Set wksReportEmitted = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    
    If wksActive.Codename <> wksReportApproved.Codename Then
        Call err.Raise(901, , "Cant confirm outside the 'DMS Deals Approved' Sheet")
    End If

    For Each rngActive In rngSelection.Rows

        iApproved = rngActive.Row
        aDataApproved = MStructure.Get_Current_Region(wksReportApproved.Range("A1"))
        ColumnDealNo = MStructure.Get_Column_Number(aDataApproved, "Deal")
        ColumnDealName = MStructure.Get_Column_Number(aDataApproved, "Dealname")


        If MInterface.Message_Confirm("Confirm deal " & aDataApproved(iApproved, ColumnDealNo) & " is emitted") <> vbYes Then
            Call err.Raise(901, , "Not confirmed")
        End If

        rowsToDelete = rowsToDelete & iApproved & ";"


        aModelEmitted = MStructure.Get_Model_Range("Model_Data_DMS_Deals_Emitted")
        ReDim aDataEmitted(1 To 1, LBound(aModelEmitted, 2) To UBound(aModelEmitted, 2))

        For jEmitted = LBound(aModelEmitted, 2) To UBound(aModelEmitted, 2)
            Select Case UCase(aModelEmitted(1, jEmitted))
                Case "EMISSION DATE"
                    aDataEmitted(1, jEmitted) = CStr(Format(Now, "dd/mm/yyyy hh:mm:ss"))
                Case "USER CHG"
                    aDataEmitted(1, jEmitted) = Get_Windows_UserName & " - " & Get_Office_UserName
                Case Else
                    For jApproved = LBound(aDataApproved, 2) To UBound(aDataApproved, 2)
                        If aModelEmitted(1, jEmitted) = aDataApproved(1, jApproved) Then
                            aDataEmitted(1, jEmitted) = aDataApproved(iApproved, jApproved)
                            Exit For
                        End If
                    Next jApproved
            End Select

        Next jEmitted


        wksReportEmitted.Range("A1").Offset(Get_Last_Row(wksReportEmitted, 1)).Resize(, UBound(aDataEmitted, 2)) = aDataEmitted

    Next rngActive

    wksReportEmitted.Columns.AutoFit
    rngSelection.Rows.Delete Shift:=xlUp


    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish

End Sub

Sub ConfirmEmitted_Access()
    Const SUB_NAME As String = "Confirm Emitted Access"
    On Error GoTo ErrorHandler

    Dim Table As String
    Dim aHeader() As Variant


    Dim wksReportApproved As Worksheet
    Dim wksReportEmitted As Worksheet
    Dim wksActive As Worksheet
    Dim iApproved As Long
    Dim j As Long
    Dim jApproved As Long
    Dim aDataApproved() As Variant
    Dim DealNo As String
    Dim ColumnDealNo As Long
    Dim ColumnDealName As Long

    Dim rngActive As Range
    Dim rngSelection As Range

    Call MGlobal.Global_Initialize
    
    Set rngSelection = Selection
    Set rngSelection = rngSelection.SpecialCells(xlCellTypeVisible)
    
    
    Set wksActive = rngSelection.Worksheet
    Set wksReportApproved = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Approved")
    
    If wksActive.Codename <> wksReportApproved.Codename Then
        Call err.Raise(901, , "Cant confirm outside the 'DMS Deals Approved' Sheet")
    End If

    Table = "DMS_FollowUp_EmittedDeals"
    aHeader = MStructure.Get_Model_Range("Model_DMS_FollowUp_EmittedDeals_Access")
    aDataApproved = MStructure.Get_Current_Region(wksReportApproved.Range("A1"))
    aDataApproved = MStructure.Get_Data(aHeader, aHeader, aDataApproved)
    ColumnDealNo = MStructure.Get_Column_Number(aDataApproved, "Deal")

    Call Insert_Field_To_Array(aDataApproved, "EmissionDate", Now)
    Call Insert_Field_To_Array(aDataApproved, "UserCHG", Get_Windows_UserName)

    For Each rngActive In rngSelection.Rows
        iApproved = rngActive.Row
        If MInterface.Message_Confirm("Confirm deal " & aDataApproved(iApproved, ColumnDealNo) & " is emitted") <> vbYes Then
            Call err.Raise(901, , "Not confirmed")
        End If
        Call Insert_To_Access_Recordset(Table, aHeader, aDataApproved, iApproved)

    Next rngActive


    
Finish:
    Call ThisWorkbook.RefreshAll
    Call MGlobal.Global_Finalize

    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish

End Sub


Sub Generate_DMS_Deals_Approved()
    Const SUB_NAME As String = "Generate DMS Deals Approved"

    Dim i As Long
    Dim j As Long
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    Dim wksReportEmitted As Worksheet

    Dim aData() As Variant
    Dim aReport() As Variant
    Dim aEmitted() As Variant

    Dim dicEmitted As Dictionary
    Dim Item As Variant

    Dim ColumnFilter As Long
    Dim ColumnFilter_DealNo As Long
    Dim ColumnFilter_UpdateDate As Long
    Dim ColumnFilter_LeaseOrigination As Long

    Dim ColumnEmitted As Long
    Dim ColumnEmitted_DealNo As Long
    Dim ColumnEmitted_UpdateDate As Long
    Dim ColumnEmitted_LeaseOrigination As Long

    Dim iEmitted As Long
    Dim jEmitted As Long

    Dim iReport As Long
    Dim IsEmitted As Boolean
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals")
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Approved")
    Set wksReportEmitted = MStructure.Get_Worksheet_By_Codename("wksData_DMSDeals_Emitted")
    
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    aEmitted = MStructure.Get_Current_Region(wksReportEmitted.Range("A1"))

    ColumnFilter = MStructure.Get_Column_Number(aData, "Deal Status")
    aData = Filter_Array(aData, ColumnFilter, "Approved")
    ColumnFilter = MStructure.Get_Column_Number(aData, "Success probability")
    aData = Filter_Array(aData, ColumnFilter, "Won (100%)")

    ColumnFilter_DealNo = MStructure.Get_Column_Number(aData, "Deal no")
    ColumnFilter_UpdateDate = MStructure.Get_Column_Number(aData, "Update date")
    ColumnFilter_LeaseOrigination = MStructure.Get_Column_Number(aData, "Lease origination")

    ColumnEmitted_DealNo = MStructure.Get_Column_Number(aEmitted, "Deal no")
    ColumnEmitted_UpdateDate = MStructure.Get_Column_Number(aEmitted, "Update date")
    ColumnEmitted_LeaseOrigination = MStructure.Get_Column_Number(aEmitted, "Lease origination")

    ReDim aReport(LBound(aData) To UBound(aData), LBound(aData, 2) To UBound(aData, 2))

    'For j = LBound(aData, 2) To UBound(aData, 2)
    '    aReport(1, j) = aData(1, j)
    'Next j
    iReport = 1
    DoEvents
    For i = LBound(aData) + 1 To UBound(aData)
        Application.StatusBar = "Searching Emitted! " & i & " / " & UBound(aData)
        If i Mod 10 = 0 Then
            DoEvents
        End If
        IsEmitted = False
        'Busca Emitted
        For iEmitted = LBound(aEmitted) + 1 To UBound(aEmitted)
            If CStr(aEmitted(iEmitted, ColumnEmitted_DealNo)) = CStr(aData(i, ColumnFilter_DealNo)) Then
                If CStr(aEmitted(iEmitted, ColumnEmitted_UpdateDate)) = CStr(aData(i, ColumnFilter_UpdateDate)) Then
                    If CStr(aEmitted(iEmitted, ColumnEmitted_LeaseOrigination)) = CStr(aData(i, ColumnFilter_LeaseOrigination)) Then
                        IsEmitted = True
                        Exit For
                    End If
                End If
            End If
        Next iEmitted
        If Not IsEmitted Then
            For j = LBound(aData, 2) To UBound(aData, 2)
                aReport(iReport, j) = aData(i, j)
            Next j
            iReport = iReport + 1
        End If
nextiData:
    Next i

    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Data_DMS_Deals", , True)

    wksReport.Range("A1").Offset(MStructure.Get_Last_Row(wksReport, 1)).Resize(UBound(aReport), UBound(aReport, 2)) = aReport
    With wksReport.Cells
        .WrapText = False
        .Columns.AutoFit
    End With

End Sub
Sub Upload_DMS_Deals_Emitted_to_Access()
End Sub


