Attribute VB_Name = "MGenerator"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MGenerator"

Sub Layout_to_Report(rngData As Range, rngLayout As Range, rngReport As Range, ModelNameReport As String, Optional HeaderRow As Long = 1, Optional IsPositionalLayout As Boolean = False)
    Const SUB_NAME As String = "Layout to Report"
    
    Dim aModel() As Variant
    Dim aLayout() As Variant
    Dim aData() As Variant
    
    Dim iData As Long
    Dim iLayout As Long
    Dim jLayout As Long
    
    Dim lay As Layout
    
    Dim CurrentValue
    Dim CurrentColumn
    Dim MaxLenght As Long
    Dim StringFormat As String
    Dim CurrentColumnModel As Long
    Dim dicDataColumns As Dictionary
    
    Dim dicPrimaryKey As Dictionary
    Set dicPrimaryKey = New Dictionary
    
    'Read the layout
    aLayout = MStructure.Get_Current_Region(rngLayout)
    aData = MStructure.Get_Current_Region(rngData)
    
    aModel = MStructure.Get_Model_Range(ModelNameReport)
    
    lay = MSetup.New_Type_Layout(aLayout)
    
    Set dicDataColumns = MStructure.Load_Dictionary_Columns(aData)
    
    For iData = LBound(aData) + HeaderRow To UBound(aData)
        
        Application.StatusBar = "Generating: " & iData & " / " & UBound(aData)
        If iData Mod 100 = 0 Then
            DoEvents
        End If
        
        ReDim aModel(LBound(aModel) To UBound(aModel), LBound(aModel, 2) To UBound(aModel, 2))
        
        For iLayout = LBound(aLayout) + 1 To UBound(aLayout)
            
            CurrentColumn = dicDataColumns(aLayout(iLayout, lay.Column_DataName))
            CurrentColumnModel = CInt(aLayout(iLayout, lay.Column_ID))
            MaxLenght = aLayout(iLayout, lay.Column_MaxLenght)
            StringFormat = aLayout(iLayout, lay.Column_Format)
            
            If aModel(1, CurrentColumnModel) <> vbNullString Then
            
                CurrentValue = aModel(1, CurrentColumnModel)
                
            ElseIf IsPositionalLayout Then
                
                CurrentValue = Space(MaxLenght)
            End If
            
            'Default Value if no value
            If Trim(CurrentValue) = vbNullString And Len(aLayout(iLayout, lay.Column_DefaultValue)) > 0 Then
                CurrentValue = aLayout(iLayout, lay.Column_DefaultValue)
            End If
            
            'Se encontrou a coluna correspondente
            If Not IsEmpty(CurrentColumn) Then
                If Len(CurrentValue) > 0 Then
                    CurrentValue = CurrentValue & aData(iData, CurrentColumn)
                Else
                    CurrentValue = aData(iData, CurrentColumn)
                End If
            End If
            
            If Not aLayout(iLayout, lay.Column_Operation) = vbNullString Then
                CurrentValue = Interface_Operation(CurrentValue, aLayout(iLayout, lay.Column_Operation), Header:=aLayout(iLayout, lay.Column_Name))
            End If
            
            If Not aLayout(iLayout, lay.Column_Treatment) = vbNullString Then
                CurrentValue = Interface_Treatment(CurrentValue, aLayout(iLayout, lay.Column_Treatment))
            End If
            
            If Len(StringFormat) > 0 Then
                CurrentValue = VBA.Format(CurrentValue, StringFormat)
            End If
            If MaxLenght > 0 Then
                CurrentValue = Left(CurrentValue, MaxLenght)
            End If
            
            Select Case UCase(aLayout(iLayout, lay.Column_PrimaryKey))
                Case "Y", "YES", "SIM", "S"
                    If Not dicPrimaryKey.Exists(CurrentValue) Then
                        Call dicPrimaryKey.Add(CurrentValue, iData)
                    Else
                        GoTo nextiData
                    End If
            End Select
            
            aModel(1, CurrentColumnModel) = CurrentValue
            CurrentValue = vbNullString
            
        Next iLayout
        
        If rngReport.Text = vbNullString Then
            rngReport.Resize(, UBound(aModel, 2)) = aModel
        Else
            rngReport.Offset(Get_Last_Row(rngReport.Worksheet, 1)).Resize(, UBound(aModel, 2)) = aModel
        End If
        
nextiData:
        CurrentValue = vbNullString
    Next iData

    rngReport.Worksheet.Columns.AutoFit

End Sub

Sub Interface_Layout_QS_Schedules(wksLayout As Worksheet, wksReport As Worksheet, Model As String)
    Dim dicSchedules As Dictionary
    Dim dicLASInstalCert As Dictionary
    Dim dicDMS As Dictionary
    Dim dicFraming As Dictionary

    Dim aDataLAS() As Variant
    Dim aDataLASInstalCert() As Variant
    Dim aDataDMS() As Variant
    Dim aDataFraming() As Variant
    Dim aLayout() As Variant
    Dim aQSSchedule() As Variant
    Dim aDataCutDate() As Variant

    Dim iLayout As Long
    Dim iDataLAS As Long
    Dim iDataDMS As Long
    Dim iDataCutDate As Long
    Dim lay As Layout
    Dim Key As Variant
    Dim Key2 As Variant
    Dim Item As Variant
    Dim ItemValue As Variant
    Dim CutDateMonth As String

    Dim DataOrigin As String
    Dim ColumnSchedule As Long
    Dim ColumnLASICSchedule As Long
    Dim ColumnLASICDeal As Long
    Dim ColumnDMSDeal As Long
    Dim ColumnFramingSchedule As Long
    
    Dim InstallationCertificateFound As Boolean
    
    Dim wksData As Worksheet
    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Schedules")
    aDataLAS = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_LAS_Adm_Installation")
    aDataLASInstalCert = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_DMS_DealPrintout")
    aDataDMS = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_QS_Framing")
    aDataFraming = MStructure.Get_Current_Region(wksData.Range("A1"))
    Set wksData = Get_Worksheet_By_Codename("wksData_QS_Cut_Dates")
    aDataCutDate = MStructure.Get_Current_Region(wksData.Range("A1"))
    
    ColumnSchedule = Get_Column_Number(aDataLAS, "Lease schedule no")
    ColumnLASICSchedule = Get_Column_Number(aDataLASInstalCert, "Lease schedule no")
    ColumnLASICDeal = Get_Column_Number(aDataLASInstalCert, "Deal")
    ColumnDMSDeal = Get_Column_Number(aDataDMS, "DealNo")
    ColumnFramingSchedule = Get_Column_Number(aDataFraming, "Contrato")
    
    Set dicDMS = Load_Dictionary("Data_DMS_I", ColumnDMSDeal, 0)
    Set dicLASInstalCert = Load_Dictionary("Data_LAS_InstallationCertificate_I", ColumnLASICSchedule, ColumnLASICDeal)
    Set dicSchedules = Load_Dictionary("Data_LAS_I", ColumnSchedule, 0)
    Set dicFraming = Load_Dictionary("Data_Framing_I", ColumnFramingSchedule, 0)
    
    
    aLayout = MStructure.Get_Current_Region(wksLayout.Range("A1"))

    aQSSchedule = Get_Model_Range("Model_Report_QS_Schedules")

    lay = New_Type_Layout(aLayout)
    
    For iLayout = LBound(aLayout) + 1 To UBound(aLayout)
        Select Case UCase(aLayout(iLayout, lay.Column_DataOrigin))
            Case "DMS"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataDMS, aLayout(iLayout, lay.Column_DataName))
            Case "LAS"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataLAS, aLayout(iLayout, lay.Column_DataName))
            Case "FRAMING"
                aLayout(iLayout, lay.Column_DataField) = Get_Column_Number(aDataFraming, aLayout(iLayout, lay.Column_DataName))
        End Select
    Next iLayout

    For Each Key In dicSchedules.Keys

        Call Erase_Data(aQSSchedule)
        InstallationCertificateFound = False
        For iLayout = LBound(aLayout) + 1 To UBound(aLayout)

            'Default Value
            
            Select Case UCase(aLayout(iLayout, lay.Column_DataOrigin))
                Case "DMS"
                    If dicLASInstalCert.Exists(Key) Then
                        InstallationCertificateFound = True
                        Key2 = Trim(dicLASInstalCert.Item(Key))
                        If dicDMS.Exists(Key2) Then
                            Item = dicDMS.Item(Key2)
                            If aLayout(iLayout, lay.Column_DataField) > 0 Then
                                
                                If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) <> "" Then
                                    
                                    Select Case aLayout(iLayout, lay.Column_Name)
                                        Case "ScheduleTerm"
                                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) + CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                            
                                        Case "PMTValue", "Servi�o", "GracePeriod", "Fator Leasing"
                                            Select Case aLayout(iLayout, lay.Column_Operation)
                                                Case "*"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) * CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                                Case "*1-"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) * (1 - CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format)))
                                                Case "+"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) + CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                                Case "-"
                                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CCur(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format)) - CCur(Convert_Number(Item(aLayout(iLayout, lay.Column_DataField)), Check_Number_Format))
                                            End Select
                                                    
                                    End Select
                                
                                Else
                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = Item(aLayout(iLayout, lay.Column_DataField))
                                
                                End If 'If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) <> "" Then
                            End If 'If aLayout(iLayout, lay.Column_DataField) > 0 Then
                        End If 'If dicDMS.Exists(Key2) Then
                    End If 'If dicLASInstalCert.Exists(key) Then

                Case "LAS"
                    Item = dicSchedules(Key)
                    If aLayout(iLayout, lay.Column_DataField) > 0 Then
                        ItemValue = Item(aLayout(iLayout, lay.Column_DataField))
                        
                        If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = vbNullString Then
                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = ItemValue
                        End If
                        
                    End If
                    
                    Select Case aLayout(iLayout, lay.Column_Name)
                        Case "GracePeriod", ""
                            Select Case aLayout(iLayout, lay.Column_Operation)
                                Case "CutDate"
                                    
                                    If CDbl(aQSSchedule(1, aLayout(iLayout, lay.Column_ID))) > Day(ItemValue) Then
                                        aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateDiff("d", CDate(ItemValue), DateSerial(Year(ItemValue), Month(ItemValue), CInt(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)))))
                                    Else
                                        aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateDiff("d", CDate(ItemValue), DateAdd("M", 1, DateSerial(Year(ItemValue), Month(ItemValue), CInt(aQSSchedule(1, aLayout(iLayout, lay.Column_ID))))))
                                    End If
                                    
'                                    For iDataCutDate = LBound(aDataCutDate) + 1 To UBound(aDataCutDate)
'
'                                        If CDate(ItemValue) >= CDate(aDataCutDate(iDataCutDate, 1)) And CDate(ItemValue) <= CDate(aDataCutDate(iDataCutDate, 2)) Then
'                                            CutDateMonth = aDataCutDate(iDataCutDate, 3)
'                                            CutDateMonth = CDbl(CutDateMonth) + 1
'                                            If CDbl(CutDateMonth) > 12 Then
'                                                CutDateMonth = 1
'                                            End If
'
'                                            aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateSerial(Year(ItemValue), CutDateMonth, aQSSchedule(1, aLayout(iLayout, lay.Column_ID)))
'
'                                        End If
'                                    Next iDataCutDate
                                    
                                Case "DateDiff"
                                    aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = DateDiff("D", CDate(ItemValue), CDate(aQSSchedule(1, aLayout(iLayout, lay.Column_ID))))
                            End Select
                                    
                    End Select
                    
                
                Case "FRAMING"
                    If dicFraming.Exists(Key) Then
                        Item = dicFraming.Item(Key)
                        aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = Item(aLayout(iLayout, lay.Column_DataField))
                    End If
                    
            End Select
            
            
            If Not aLayout(iLayout, lay.Column_Treatment) = vbNullString Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = Interface_Treatment(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), aLayout(iLayout, lay.Column_Treatment))
            End If
            
            If aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = vbNullString Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = aLayout(iLayout, lay.Column_DefaultValue)
            End If
            
            If aLayout(iLayout, lay.Column_Type) = "Number" Or aLayout(iLayout, lay.Column_Type) = "Numerico" Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = CDbl(Convert_Number(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), Check_Number_Format))
            End If
            
            If Not aLayout(iLayout, lay.Column_Format) = vbNullString Then
                aQSSchedule(1, aLayout(iLayout, lay.Column_ID)) = VBA.Format(aQSSchedule(1, aLayout(iLayout, lay.Column_ID)), aLayout(iLayout, lay.Column_Format))
            End If

        Next iLayout
        
        If InstallationCertificateFound Then
            wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(, UBound(aQSSchedule, 2)) = aQSSchedule
            
        End If

    Next Key

End Sub
Function Interface_Operation(ByVal OriginalValue, ByVal Operation As String, Optional ByVal Header As String = vbNullString, Optional ByVal LastValue) As Variant
    Const SUB_NAME As String = "Interface_Treatment"

    Dim CurrentValue As Variant

    Dim i As Long
    Dim j As Long
    Dim Found As Boolean
    
    CurrentValue = OriginalValue
    Select Case UCase(Operation)
        Case UCase("Extract-OldLease")
            If Regx_Match(CurrentValue, PATTERN_OLD_LEASE) Then
                CurrentValue = regExMatches(0).SubMatches(0)
            End If
        Case UCase("CLEAN-CNPJ")
            If Regx_Match(CurrentValue, PATTERN_CNPJ) Then
                CurrentValue = CStr(CurrentValue)
                CurrentValue = Replace(CurrentValue, ".", "")
                CurrentValue = Replace(CurrentValue, "-", "")
                CurrentValue = Replace(CurrentValue, "/", "")
                CurrentValue = Right(String(14, "0") & CurrentValue, 14)
            Else
                CurrentValue = ""
            End If
            
        Case UCase("Extract-CNPJ")
            If Regx_Match(CurrentValue, PATTERN_CNPJ) Then
                CurrentValue = regExMatches(0).SubMatches(0)
                CurrentValue = MRegex.Regx_Replace(CurrentValue, "\D", "")
                CurrentValue = Right(String(14, "0") & CurrentValue, 14)
            Else
                CurrentValue = ""
            End If
            
        Case UCase("Multiply_Percent_100")
            CurrentValue = Round(LastValue * (1 + (OriginalValue / 100)), 2)
            
        Case UCase("NUMBER_US")
            CurrentValue = Convert_Number(OriginalValue, "")
            
        Case UCase("NUMBER_BRL")
            CurrentValue = Convert_Number(OriginalValue, "BRL")
        Case UCase("HEADERMATCH")
            CurrentValue = HeaderMatch(OriginalValue, Header)
    End Select
    
    Interface_Operation = CurrentValue
End Function
Function HeaderMatch(ByVal Value As String, Header As String) As Boolean
    Dim PatternYes As String
    Dim PatternNo As String
    PatternYes = Header & "(\s*\=\s*(?:yes|y|1|Sim|s|True))?"
    PatternNo = Header & "(\s*\=\s*(?:No|N|N[�a]O|0|False))"
    
    'Checa o negativo
    If Regx_Match(Value, PatternNo) Then
        HeaderMatch = False
        
    Else
    
        If Regx_Match(Value, PatternYes) Then
            HeaderMatch = True
        Else
            HeaderMatch = False
        End If
        
    End If
       
       
End Function
Function Interface_Treatment(ByVal OriginalValue, ByVal Treatment As String, Optional ShowErrorMessage As Boolean = False) As String
    Const SUB_NAME As String = "Interface_Treatment"

    Dim dicNames As Dictionary
    Dim aData() As Variant
    Dim i As Long
    Dim j As Long
    Dim Found As Boolean


    If UCase(Treatment) = "SEQUENCE" Then

        gSequence = gSequence + 1
        Interface_Treatment = gSequence
    ElseIf UCase(Treatment) = "SEQUENCE+" Then
        gSequence = gSequence + 1
        Interface_Treatment = gSequence + OriginalValue
    ElseIf UCase(Treatment) = "CONVERT-DATE-T" Then
        Interface_Treatment = Convert_Date_T(OriginalValue)

    ElseIf UCase(Treatment) = "CONVERT-NUMBER-US" Then
        Interface_Treatment = Convert_Number(OriginalValue, "BRL")

    ElseIf UCase(Treatment) = "CONVERT-NUMBER-BR" Then
        Interface_Treatment = Convert_Number(OriginalValue, "BRL")

    ElseIf UCase(Treatment) = "YN" Then
        If OriginalValue Then
            Interface_Treatment = "Y"
        Else
            Interface_Treatment = "N"
        End If

    ElseIf Not (IsEmpty(OriginalValue) Or OriginalValue = vbNullString) Then

        If dicNames Is Nothing Then
            Set dicNames = New Dictionary
        End If

        If Not dicNames.Exists(Treatment) Then
            aData = Get_Current_Region(Get_Model_Range(Treatment))
            Call dicNames.Add(Treatment, aData)
        End If

        aData = dicNames(Treatment)
        Found = False
        Interface_Treatment = vbNullString
        For i = LBound(aData) To UBound(aData)

            If UCase(OriginalValue) = UCase(aData(i, 1)) And OriginalValue <> vbNullString Then
                Interface_Treatment = aData(i, 2)
                Found = True
                Exit For
            ElseIf aData(i, 1) = "@DEFAULT" Or aData(i, 1) = "@PADRAO" And Interface_Treatment = vbNullString Then
                Interface_Treatment = aData(i, 2)
                Found = True
            End If

        Next i

        If Not Found And Interface_Treatment = vbNullString Then
            If ShowErrorMessage Then
                Call MInterface.Message_Error("Falha ao buscar From-To" & vbCrLf & "Tratamento: " & Treatment & vbCrLf & "Valor: " & OriginalValue)
            End If

            Interface_Treatment = "Falha Tratamento: " & Treatment & " | Valor: " & OriginalValue
        End If
    End If

End Function
Sub Interface_Layout(wksData As Worksheet, wksReport As Worksheet, LayoutName As String, ColumnUL As Long, Optional ParametersName As String = "Menu_Parameters")

    Dim aData() As Variant
    Dim aLayout() As Variant
    Dim aHeaderPurchasesInstallation() As Variant
    Dim rngPurchasesInstallation As Range

    Dim dicAssetsPositionPurchase As Dictionary
    Dim dicPurchasesInstallation As Dictionary
    Dim dicModelsSerial As Dictionary
    Dim dicPurnoInvoice As Dictionary
    Dim dicSupplierManufacturer As Dictionary
    Dim dicNewSupplierManufacturer As Dictionary
    Dim dicNewPurnoInvoice As Dictionary

    Dim aRecord() As Variant

    Dim i As Long
    Dim iRecord As Long
    Dim iLayout As Long

    Dim lay As Layout
    Dim MSN As ModelsSerialNumbers

    Dim Column_ID As Long
    Dim Column_DataName As Long
    Dim Column_DefaultValue As Long
    Dim Column_KeyModel As Long
    Dim Column_PID As Long
    Dim Column_Purchase As Long

    Dim Column_MSN As Long 'Model Serial Number

    Dim key As Variant
    Dim Item As Variant
    Dim PurchaseNo As Variant
    Dim NumberFormat As String
    Dim LASNumberFormat As String

    Dim wksList As Worksheet

    'PRE
    NumberFormat = Check_Number_Format

    aData = wksData.Range("A1").CurrentRegion
    aLayout = Get_Model_Range(LayoutName)
    lay = MSetup.New_Type_Layout(aLayout)
    LASNumberFormat = gdicGlobalParameters.Item("FORMATO VALOR")

    Select Case UCase(gProjectName)

        Case UCase(PROJECT_LAS_QS_ASSETS)
            Set dicAssetsPositionPurchase = Load_Dictionary("Assets_Position_Purchases_I", 1)
            Column_PID = Get_Column_Number(aData, "PID")
            
            Set dicPurchasesInstallation = Load_Dictionary("Purchases_IC_I", 1, 0)
            Set rngPurchasesInstallation = Get_Model_Range("Purchases_IC_I")
            aHeaderPurchasesInstallation = rngPurchasesInstallation.Resize(, Get_Last_Column(rngPurchasesInstallation.Worksheet, 1))


        Case Else
            Set dicModelsSerial = Load_Dictionary("ModelsSerialNumbers_I", 1, 0)
            Set dicPurnoInvoice = Load_Dictionary("Purchases_Invoices_I", 1, 2)
            Set dicSupplierManufacturer = Load_Dictionary("Supplier_Manufacturer_I", 1, 2)
            
            Set dicNewSupplierManufacturer = New Dictionary
            Set dicNewPurnoInvoice = New Dictionary
            
            MSN = New_Type_ModelsSerialNumbers
            Column_KeyModel = Get_Column_Number(aData, "AssetName")

    End Select


    'Identifica as colunas do layout
    For iLayout = LBound(aLayout) + 1 To UBound(aLayout)
        If InStr(1, UCase(aLayout(iLayout, lay.Column_DataOriginType)), "DATA") > 0 Then
            aLayout(iLayout, lay.Column_DataName) = Get_Column_Number(aData, aLayout(iLayout, lay.Column_DataName))
        End If
    Next iLayout

    ReDim aRecord(1 To 1, 1 To UBound(aLayout) - 1) '-1 for there is a header

    For i = LBound(aData) + 1 To UBound(aData) 'For each record in Data

        For iLayout = LBound(aLayout) + 1 To UBound(aLayout) 'For each field in layout

            If aLayout(iLayout, lay.Column_DefaultValue) <> vbNullString Then
                aRecord(1, aLayout(iLayout, lay.Column_ID)) = aLayout(iLayout, lay.Column_DefaultValue)
            End If

            Select Case UCase(aLayout(iLayout, lay.Column_DataOriginType))
                Case "PARAMETERS"
                    key = UCase(aLayout(iLayout, lay.Column_DataName))
                    If gdicGlobalParameters.Exists(key) Then
                        Item = gdicGlobalParameters.Item(key)
                        If Regx_Match(Item, "PREENCHER") Then
                            Call Message_Error("Verificar !" & vbCrLf & "Parametro: " & key)
                            Exit Sub
                        End If
                        aRecord(1, aLayout(iLayout, lay.Column_ID)) = Item 'Get_Parameter_Value(aParameter, aData(i, aLayout(iLayout, lay.Column_DataName)))
                    End If

                Case "MODELSN"
                    key = UCase(aData(i, Column_KeyModel))
                    If dicModelsSerial.Exists(key) Then
                        Item = dicModelsSerial.Item(key)

                        Column_MSN = 0
                        Select Case UCase(aLayout(iLayout, lay.Column_DataName))
                            Case "TYPE"
                                Column_MSN = MSN.Column_Type
                            Case "MODEL"
                                Column_MSN = MSN.Column_ModelLAS
                            Case "CLASS"
                                Column_MSN = MSN.Column_Class
                            Case "ASSETNAME", "DESCRIPTION LAS", "DESCRIPTION_LAS"
                                Column_MSN = MSN.Column_DescriptionLAS
                        End Select

                        If Not Column_MSN = 0 Then
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = Item(Column_MSN)
                        End If

                    End If
                Case "DATA"
                    If IsNumeric(aLayout(iLayout, lay.Column_DataName)) Then
                        If aLayout(iLayout, lay.Column_DataName) > 0 Then
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = aData(i, aLayout(iLayout, lay.Column_DataName))
                        End If
                    End If

                Case "PID"
                    key = UCase(aData(i, Column_PID))
                    PurchaseNo = Trim(dicAssetsPositionPurchase.Item(key)) 'Purchase

                    Item = dicPurchasesInstallation(PurchaseNo)


                    If Not IsEmpty(Item) Then
                        Column_Purchase = MStructure.Get_Column_Number(aHeaderPurchasesInstallation, aLayout(iLayout, lay.Column_DataName))

                        If Not Column_Purchase = 0 Then
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = Item(Column_Purchase)
                        End If
                    End If

            End Select

            If aLayout(iLayout, lay.Column_Treatment) <> vbNullString Then

                Select Case UCase(aLayout(iLayout, lay.Column_Treatment))
                    Case "CONVERT-DATE"
                        aRecord(1, aLayout(iLayout, lay.Column_ID)) = MStructure.Convert_Date(aRecord(1, aLayout(iLayout, lay.Column_ID)))
                    Case "DEC2"
                        If IsNumeric(aRecord(1, aLayout(iLayout, lay.Column_ID))) Then
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = Convert_Number(aRecord(1, aLayout(iLayout, lay.Column_ID)), NumberFormat)
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = Round(CDbl(aRecord(1, aLayout(iLayout, lay.Column_ID))), 2)
                        End If
                        aRecord(1, aLayout(iLayout, lay.Column_ID)) = Format_Number_US(aRecord(1, aLayout(iLayout, lay.Column_ID)), LASNumberFormat)

                    Case "SUPPLIER-MANUFACTURER"
                        If dicSupplierManufacturer.Exists(aRecord(1, aLayout(iLayout, lay.Column_ID))) Then
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = dicSupplierManufacturer.Item(aRecord(1, aLayout(iLayout, lay.Column_ID)))

                        Else
                            If Not dicNewSupplierManufacturer.Exists(aRecord(1, aLayout(iLayout, lay.Column_ID))) Then
                                Call dicNewSupplierManufacturer.Add(aRecord(1, aLayout(iLayout, lay.Column_ID)), "")
                            End If

                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = "MANUFACTURER NOT FOUND: " & aRecord(1, aLayout(iLayout, lay.Column_ID))
                        End If

                    Case "PURNO-INVOICE"
                        If dicPurnoInvoice.Exists(aRecord(1, aLayout(iLayout, lay.Column_ID))) Then
                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = dicPurnoInvoice.Item(aRecord(1, aLayout(iLayout, lay.Column_ID)))

                        Else
                            If Not dicNewPurnoInvoice.Exists(aRecord(1, aLayout(iLayout, lay.Column_ID))) Then
                                Call dicNewPurnoInvoice.Add(aRecord(1, aLayout(iLayout, lay.Column_ID)), "")
                            End If

                            aRecord(1, aLayout(iLayout, lay.Column_ID)) = "INVOICE NOT FOUND: " & aRecord(1, aLayout(iLayout, lay.Column_ID))
                        End If

                End Select
            End If

            If aLayout(iLayout, lay.Column_MaxLenght) <> vbNullString Then
                aRecord(1, aLayout(iLayout, lay.Column_ID)) = Left(CStr(aRecord(1, aLayout(iLayout, lay.Column_ID))), CDbl(aLayout(iLayout, lay.Column_MaxLenght)))
            End If


        Next iLayout

        wksReport.Range("A1").Offset(Get_Last_Row(wksReport, ColumnUL)).Resize(, UBound(aRecord, 2)) = aRecord

    Next i

    wksReport.Columns.AutoFit

    Select Case UCase(gProjectName)
        Case UCase(PROJECT_LAS_QS_ASSETS)

        Case Else

            If dicNewPurnoInvoice.Count > 0 Then
                Set wksList = Get_Worksheet_By_Codename("wksList_PurchasesInvoices")
                Call Dictionary_To_Range(dicNewPurnoInvoice, wksList.Range("A1"))
                Call MInterface.Message_Error("NEW Purchase-Invoice!", "Purchase-Invoice")
            End If

            If dicNewSupplierManufacturer.Count > 0 Then
                Set wksList = Get_Worksheet_By_Codename("wksList_Supplier_Manufacturer")
                Call Dictionary_To_Range(dicNewSupplierManufacturer, wksList.Range("A1"))
                Call MInterface.Message_Error("NEW Supplier-Manufacturer!", "Supplier-Manufacturer")
            End If

    End Select


End Sub

