Attribute VB_Name = "MStructure"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MStructure"

Public Type ButtonModel
    Shp_Group     As Shape
    Shp_Image     As Shape
    Shp_Label     As Shape
    Shp_Macro     As Shape

End Type

'    /$$$$$$   /$$                                     /$$
'   /$$__  $$ | $$                                    | $$
'  | $$  \__//$$$$$$    /$$$$$$  /$$   /$$  /$$$$$$$ /$$$$$$   /$$   /$$  /$$$$$$   /$$$$$$
'  |  $$$$$$|_  $$_/   /$$__  $$| $$  | $$ /$$_____/|_  $$_/  | $$  | $$ /$$__  $$ /$$__  $$
'   \____  $$ | $$    | $$  \__/| $$  | $$| $$        | $$    | $$  | $$| $$  \__/| $$$$$$$$
'   /$$  \ $$ | $$ /$$| $$      | $$  | $$| $$        | $$ /$$| $$  | $$| $$      | $$_____/
'  |  $$$$$$/ |  $$$$/| $$      |  $$$$$$/|  $$$$$$$  |  $$$$/|  $$$$$$/| $$      |  $$$$$$$
'   \______/   \___/  |__/       \______/  \_______/   \___/   \______/ |__/       \_______/
'---------------------------------------------------------------------------------------------
Function Add_Percentage_To_String(ByVal Caption As String, ByVal Value As String)
    Const SUB_NAME As String = "Add Percentage To String"
    Call Log_Step(SUB_NAME)

    If InStr(1, Caption, "%") > 0 Then
        If IsNumeric(Value) Then
            Value = CStr(Round(CDbl(Value) * 100, 2)) & " %"
        End If
    End If

    Add_Percentage_To_String = Value

End Function

Function Check_Date_Format() As String
    Const SUB_NAME As String = "Check_Date_Format"
    Call Log_Step(SUB_NAME)

    If Month(CDate("01/12/2020")) = 12 Then
        Check_Date_Format = "BRL"
    Else
        Check_Date_Format = "USD"
    End If
End Function
Function Check_Number_Format() As String
    Const SUB_NAME As String = "Check_Number_Format"
    Call Log_Step(SUB_NAME)

    If CDbl("1,000") = 1 Then
        Check_Number_Format = "BRL"
    Else
        Check_Number_Format = "USD"
    End If
End Function
Function Choose_File(Optional ExtensaoArquivo As enmFileExtension) As String
    Const SUB_NAME As String = "Choose_File"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)


    'Arrange
    Const CONVERSION_TYPE As String = "TABULADO"
    Dim vFileName As Variant
    Dim sTitulo As String
    Dim sFileFilter As String

    'Act
    'Identifica o tipo de extens�o
    Select Case ExtensaoArquivo
        Case enmFileExtension.PDF
            sTitulo = "Selecione um arquivo PDF para convers�o"
            sFileFilter = "PDF Files(*.pdf),*.pdf"

        Case enmFileExtension.TXT
            sTitulo = "Selecione um arquivo TXT para convers�o"
            sFileFilter = "TXT Files(*.txt),*.txt"

        Case enmFileExtension.XLS
            sTitulo = "Selecione um arquivo EXCEL para convers�o"
            sFileFilter = "EXCEL Files(*.xls;*.xlsx;*.xlsm;*.csv;*.xlsb),*.xls;*.xlsx;*.xlsm;*.csv;*.xlsb"

        Case enmFileExtension.XML
            sTitulo = "Selecione um arquivo XML para convers�o"
            sFileFilter = "XML Files(*.xml),*.xml"

        Case enmFileExtension.SemFiltro
            sTitulo = "Selecione um arquivo para convers�o"
            sFileFilter = ""
    End Select

    vFileName = CStr(Application.GetOpenFilename(Title:=sTitulo, MultiSelect:=False, FileFilter:=sFileFilter))

    'Valida se o arquivo foi selecionado
    If vFileName = "Falso" Or vFileName = "False" Then
        Call err.Raise(999, SUB_NAME, "O CAMINHO DOS ARQUIVOS NAO FOI ESPECIFICADO!")
    Else
        Choose_File = vFileName
    End If

Fim:
    Exit Function

ErrorHandler:
    Call err.Raise(err.Number, SUB_NAME, err.Description)
    GoTo Fim
End Function
Sub Clean_Formats(wks As Worksheet)
    Const SUB_NAME As String = "Clean_Formats"
    Call Log_Step(SUB_NAME)

    wks.Cells.Interior.Pattern = xlNone
End Sub
Sub Clean_Worksheet(wksClean As Worksheet, Optional Format As String = "@")
    Const SUB_NAME As String = "Clean Worksheet"
    Call Log_Step(SUB_NAME)

    With wksClean.Cells
        .Clear
        .WrapText = False
        .NumberFormat = Format
    End With

End Sub
Sub Clear_Shapes_Worksheet(wksClear As Worksheet, Optional IgnorePattern As Boolean = False)
    Const SUB_NAME As String = "Clear_Shapes_Worksheet"
    Call Log_Step(SUB_NAME)

    Dim shp As Shape

    Dim RX As RegExp
    Set RX = New RegExp
    RX.Pattern = "\_(?:ADD|CLEAN|REFRESH)"
    RX.IgnoreCase = True

    For Each shp In wksClear.Shapes

        If Not RX.Test(shp.Name) Or IgnorePattern Then
            shp.Delete
        End If
    Next shp

End Sub
Sub Create_Folder(FolderPath, Optional shouldReplaceDirectory As Boolean = False)
    Const SUB_NAME As String = "Create_Folder"
    Call Log_Step(SUB_NAME)

    Dim canCreateFolder As Boolean
    If gobjFSO Is Nothing Then
        Set gobjFSO = New Scripting.FileSystemObject
    End If
    canCreateFolder = True
    If gobjFSO.FolderExists(FolderPath) Then
        If shouldReplaceDirectory Then
            gobjFSO.DeleteFolder FolderPath
        Else
            canCreateFolder = False
        End If
    End If

    If canCreateFolder Then
        MkDir FolderPath
    End If

End Sub
Function Convert_Date(ByVal MyDate As String) As String
    Const SUB_NAME As String = "Convert_Date"
    Call Log_Step(SUB_NAME)

    '2021-01-15T10:26:26-03:00
    Convert_Date = MyDate
    If Regx_Match(MyDate, "(\d{4})\-(\d{2})\-(\d{2})") Then
        Convert_Date = regExMatches(0).SubMatches(2) & "/" & regExMatches(0).SubMatches(1) & "/" & regExMatches(0).SubMatches(0)
    End If

End Function
Function Convert_Date_T(ByVal MyDate As String) As String
    Const SUB_NAME As String = "Convert_Date_T"
    Call Log_Step(SUB_NAME)

    '2021-01-15T10:26:26-03:00
    Convert_Date_T = MyDate
    If Regx_Match(MyDate, "^(\d{4})\-(\d{2})\-(\d{2})") Then
        Convert_Date_T = regExMatches(0).SubMatches(2) & "/" & regExMatches(0).SubMatches(1) & "/" & regExMatches(0).SubMatches(0)
    End If

End Function
Function Convert_Number(ByVal Number As String, ByVal NumberFormat As String) As String
    Const SUB_NAME As String = "Convert_Number"
    Call Log_Step(SUB_NAME)

    Dim ConvertedNumber As String

    If Number = vbNullString Then
        Number = "0"
    End If

    If UCase(NumberFormat) = "BRL" Then
        'Number = Replace(Number, ",", "")
        'Number = Replace(Number, ".", ",")
        Convert_Number = Format_Number_US(Number, "")
    Else
        Convert_Number = Format_Number_US(Number, "PONTO")
    End If

End Function
Function Convert_Boolean(ByVal Value) As Boolean
    Const SUB_NAME As String = "Convert_Boolean"
    Call Log_Step(SUB_NAME)

    Convert_Boolean = False
    If MRegex.Regx_Match(CStr(Value), "1|YES|SIM") Then
        Convert_Boolean = True
    End If
End Function
Public Sub Delete_NamedRange(Name As String)
    Const SUB_NAME As String = "Delete_NamedRange"
    Call Log_Step(SUB_NAME)

    'Declare
    Dim NameBusca As Name

    'Process
    For Each NameBusca In ThisWorkbook.Names
        If NameBusca.Name = Name Then
            NameBusca.Delete
            Exit For
        End If
    Next NameBusca
End Sub
Public Sub Dictionary_To_Range(dic As Dictionary, rng As Range)
    Const SUB_NAME As String = "Dictionary_To_Range"
    Call Log_Step(SUB_NAME)

    Dim Key As Variant
    For Each Key In dic.Keys
        rng.Offset(Get_Last_Row(rng.Worksheet, rng.Column)).Value2 = Key
    Next Key
    rng.Worksheet.Columns.AutoFit
End Sub
Sub Erase_Data(aData() As Variant)
    Const SUB_NAME As String = "Erase_Data"
    Call Log_Step(SUB_NAME)

    ReDim aData(LBound(aData) To UBound(aData), LBound(aData, 2) To UBound(aData, 2))
End Sub
Function Filter_Array(aData() As Variant, ColumnFilter As Long, ValidFilter As Variant, _
                      Optional ShouldExist As Boolean = True, Optional HeaderRow As Long = 1, _
                      Optional Filter_Mode As String = "equals") As Variant
    Const SUB_NAME As String = "Filter_Array"
    Call Log_Step(SUB_NAME)

    Dim aRetorno() As Variant
    Dim i As Long
    Dim j As Long

    Dim Key As Variant

    Dim dicValid As Dictionary
    Set dicValid = New Dictionary
    aRetorno = aData
    For i = LBound(aData) + HeaderRow To UBound(aData)
        If ShouldExist Then
            Select Case Filter_Mode
                Case "", "equals"
                    If CStr(aData(i, ColumnFilter)) = CStr(ValidFilter) Then
                        Call dicValid.Add(i, i)
                    End If
                Case ">"
                    If aData(i, ColumnFilter) > ValidFilter And aData(i, ColumnFilter) <> "" Then
                        Call dicValid.Add(i, i)
                    End If
                Case ">="
                    If aData(i, ColumnFilter) >= ValidFilter And aData(i, ColumnFilter) <> "" Then
                        Call dicValid.Add(i, i)
                    End If
                Case "<"
                    If aData(i, ColumnFilter) < ValidFilter And aData(i, ColumnFilter) <> "" Then
                        Call dicValid.Add(i, i)
                    End If
                Case "<="
                    If aData(i, ColumnFilter) <= ValidFilter And aData(i, ColumnFilter) <> "" Then
                        Call dicValid.Add(i, i)
                    End If
            End Select


        Else
            If aData(i, ColumnFilter) <> ValidFilter Then
                Call dicValid.Add(i, i)
            End If
        End If
    Next i
    Application.StatusBar = "Filtering Array"
    If dicValid.Count > 0 Then

        ReDim aRetorno(1 To dicValid.Count + 1, LBound(aData, 2) To UBound(aData, 2))
        Dim iRetorno As Long

        iRetorno = 1
        For j = LBound(aData, 2) To UBound(aData, 2)
            aRetorno(iRetorno, j) = aData(1, j)
        Next j
        iRetorno = iRetorno + 1
        For Each Key In dicValid.Keys
            For j = LBound(aData, 2) To UBound(aData, 2)
                aRetorno(iRetorno, j) = aData(Key, j)
            Next j
            iRetorno = iRetorno + 1
        Next Key
    Else
        ReDim aRetorno(1 To 1, LBound(aRetorno, 2) To UBound(aRetorno, 2))
        For j = LBound(aRetorno, 2) To UBound(aRetorno, 2)
            aRetorno(1, j) = aData(1, j)
        Next j

    End If

    Filter_Array = aRetorno

End Function
Function Filter_Array_From_Dictionary(aData() As Variant, ColumnFilter As Long, dicFilter As Dictionary, ShouldExist As Boolean) As Variant
    Const SUB_NAME As String = "Filter_Array_From_Dictionary"
    Call Log_Step(SUB_NAME)

    Dim aRetorno() As Variant
    Dim i As Long
    Dim j As Long

    Dim Key As Variant
    Dim isValid As Boolean

    Dim dicValid As Dictionary
    Set dicValid = New Dictionary
    
    For i = LBound(aData) + 1 To UBound(aData)
        Key = CStr(aData(i, ColumnFilter))
        isValid = False
        If ShouldExist Then
            If dicFilter.Exists(Key) Then
                isValid = True
            End If
        Else
            If Not dicFilter.Exists(Key) Then
                isValid = True
            End If
        End If

        If isValid Then
            Call dicValid.Add(i, i)
        End If
    Next i

    If dicValid.Count > 0 Then

        ReDim aRetorno(1 To dicValid.Count, LBound(aData, 2) To UBound(aData, 2))
        Dim iRetorno As Long

        iRetorno = 1
        For j = LBound(aData, 2) To UBound(aData, 2)
            aRetorno(iRetorno, j) = aData(1, j)
        Next j

        For Each Key In dicValid.Keys
            For j = LBound(aData, 2) To UBound(aData, 2)
                aRetorno(iRetorno, j) = aData(Key, j)
            Next j
            iRetorno = iRetorno + 1
        Next Key

    End If

    Filter_Array_From_Dictionary = aRetorno
End Function
Function Format_Number_US(ByVal Value As String, Format As String) As String
    Const SUB_NAME As String = "Format_Number_US"
    Call Log_Step(SUB_NAME)

    Value = Trim(Value)
    Const NUMBER_REGEX_BRL As String = "^\-?\s*[\d\.]+\,\d+$"
    Const NUMBER_REGEX_US As String = "^\-?\s*[\d\,]+\.\d+$"
    Const NUMBER_REGEX_BRL_PERCENT As String = "^\-?\s*[\d\.]+\,\d+\s*\%\s*$"
    Const NUMBER_REGEX_US_PERCENT As String = "^\-?\s*[\d\,]+\.\d+\s*\%\s*$"
    Dim CurrentFormat As String

    If Regx_Match(Value, NUMBER_REGEX_US) Then
        CurrentFormat = "US"
    ElseIf Regx_Match(Value, NUMBER_REGEX_BRL) Then
        CurrentFormat = "BRL"
    ElseIf Regx_Match(Value, NUMBER_REGEX_US_PERCENT) Then
        CurrentFormat = "USP"
    ElseIf Regx_Match(Value, NUMBER_REGEX_BRL_PERCENT) Then
        CurrentFormat = "BRLP"
    End If

    If (UCase(Format) = "PONTO" Or UCase(Format) = "US") And (CurrentFormat = "BRL" Or CurrentFormat = "BRLP") Then
        Value = Replace(Value, ".", "")
        Value = Replace(Value, ",", ".")



    ElseIf (UCase(Format) = "" Or UCase(Format) = "BRL") And (CurrentFormat = "US" Or CurrentFormat = "USP") Then
        Value = Replace(Value, ",", "")
        Value = Replace(Value, ".", ",")

    End If

    If CurrentFormat = "BRLP" Or CurrentFormat = "USP" Then
        Value = Replace(Value, "%", "")
        Value = CDbl(Value) / 100
    End If

    Format_Number_US = Value

End Function
Public Sub Format_Words(Model As String, rng As Range)
    Const SUB_NAME As String = "Format_Words"
    Call Log_Step(SUB_NAME)

    Dim rngModel As Range
    Dim rngItem As Range
    Set rngModel = Get_Model_Range(Model)
    
    Dim RX As RegExp:    Set RX = New RegExp
    
    Dim MyFontStyle As String
    Dim MyColor As Long
    Dim Sentence As String

    rng.ClearFormats
    rng.WrapText = True
    RX.Global = True
    RX.IgnoreCase = True
    RX.MultiLine = True
    RX.Pattern = "^(\r\n|\r|\n)"
    'rng.Value = rx.Replace(rng.Value, Chr(13))
    rng.Value = RX.Replace(rng.Value, "")


    For Each rngItem In rngModel
        MyFontStyle = rngItem.Font.FontStyle
        MyColor = rngItem.Font.Color
        Sentence = rngItem.Value

        Call Model_Sentence(Sentence, rng, MyFontStyle, MyColor)
    Next rngItem

    'rx.Pattern = "\r1\;"
    'rng.Value2 = rx.Replace(rng.Value, vbNewLine)
    'rx.Pattern = "\@n\;"
    'rng.Value2 = rx.Replace(rng.Value, vbNewLine)

End Sub
Sub Formula_To_Range(Model As String, NameRangeDestination As String)
    Const SUB_NAME As String = "Formula To Range"
    Call Log_Step(SUB_NAME)

    Dim rngModel As Range
    Dim rngNamedRange As Range
    Set rngModel = Get_Model_Range(Model)
    Set rngNamedRange = Get_Model_Range(NameRangeDestination)
    rngNamedRange = rngModel.Formula
End Sub
Sub Generate_Worksheet_From_Model(wksGenerate As Worksheet, CanClean As Boolean, Model As String, _
                                  Optional ModelRange As String = "A1", Optional CanFormat As Boolean = True)
    Const SUB_NAME As String = "Generate Worksheet From Model"
    Call Log_Step(SUB_NAME)

    Dim rngCell As Range
    Dim rngGenerate As Range
    Dim rngModel As Range
    Dim ColumnsCount As Long

    If CanClean Then
        Call MStructure.Clean_Worksheet(wksClean:=wksGenerate)
    End If
    
    Set rngGenerate = wksGenerate.Range(ModelRange)
    Set rngModel = Get_Model_Range(Model)
    
    
    'Cabe�alho de acordo com modelo selecionado
    Call Model_To_Range(Model:=Model, rng:=rngGenerate)

    If CanFormat Then
        'Conta quantas colunas existem neste modelo de cabe�alho
        ColumnsCount = rngModel.Columns.Count 'Model_Columns_Count(Model:=Model)

        'Formata��o cada coluna de acordo com a formata��o do modelo de cabe�alho
        For Each rngCell In rngGenerate.Resize(, ColumnsCount)
            wksGenerate.Columns(rngCell.Column).NumberFormat = rngCell.NumberFormat
            wksGenerate.Columns(rngCell.Column).Locked = rngCell.Locked
        Next rngCell

        rngGenerate.Resize(, ColumnsCount).Locked = True
        rngGenerate.Worksheet.Columns.AutoFit
    End If
End Sub
Sub GerarDadosFormatoQuery(wksOrigemDados As Worksheet, wksDestinationDados As Worksheet, sRangeHeaderDados As String)
    Const SUB_NAME As String = "Generate Worksheet From Model"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Const RANGE_DESTINO_DADOS As String = "A1"

    'Dados

Finish:
ErrorHandler:

End Sub
Public Function Get_Column_Number(aHeader() As Variant, ByVal Column_Name As String, Optional iRow As Long = 1)
    Const SUB_NAME As String = "Get Column Number"
    Call Log_Step(SUB_NAME)

    Dim j As Long
    Get_Column_Number = 0
    For j = LBound(aHeader, 2) To UBound(aHeader, 2)
        If UCase(Trim(Column_Name)) = UCase(Trim(aHeader(iRow, j))) Then
            Get_Column_Number = j
            Exit Function
        End If
    Next j
End Function
Public Function Get_Column_Number_Variant(aHeader As Variant, ByVal Column_Name As String)
    Const SUB_NAME As String = "Get Column Number"
    Call Log_Step(SUB_NAME)

    Dim j As Long
    Get_Column_Number_Variant = 0
    For j = LBound(aHeader) To UBound(aHeader)
        If UCase(Trim(Column_Name)) = UCase(Trim(aHeader(j))) Then
            Get_Column_Number_Variant = j
            Exit Function
        End If
    Next j

End Function
Function Get_Current_Region(rng As Range)
    Const SUB_NAME As String = "Get Current Region"
    Call Log_Step(SUB_NAME)

    Dim aData() As Variant
    If Regx_Match(rng.CurrentRegion.Address, "^\$\w+\$\d+$") Then
        ReDim aData(1 To 1, 1 To 1)
    Else
        aData = rng.CurrentRegion
    End If
    Get_Current_Region = aData

End Function
Function Get_Data(aModel() As Variant, aModelRX() As Variant, aValidate() As Variant, _
                  Optional RX As Boolean = False, Optional IncludeHeader As Boolean = True) As Variant
    Const SUB_NAME As String = "Get Data"
    Call Log_Step(SUB_NAME)

    Dim i As Long
    Dim j As Long
    Dim isFound As Boolean
    Dim HeadersNotFound As String
    Dim jValidate As Long
    Dim aData() As Variant
    Dim aModelHeader() As Variant
    Dim aValidateData() As Variant

    'Process
    ReDim aData(LBound(aValidate) To UBound(aValidate), LBound(aModel, 2) To UBound(aModel, 2))
    aValidateData = aValidate

    If IncludeHeader Then
        For j = LBound(aModel, 2) To UBound(aModel, 2)
            aData(1, j) = aModel(1, j)
        Next j
    End If

    If Not RX Then
        aModelHeader = aModel
        For j = LBound(aModelHeader, 2) To UBound(aModelHeader, 2)
            aModelHeader(1, j) = Treat_Valid_Model(aModelHeader(1, j))
        Next j
    End If
    For j = LBound(aValidateData, 2) To UBound(aValidateData, 2)
        aValidateData(1, j) = Treat_Valid_Model(aValidateData(1, j))
    Next j

    For j = LBound(aModelHeader, 2) To UBound(aModelHeader, 2)

        For jValidate = LBound(aValidateData, 2) To UBound(aValidateData, 2)

            If RX Then
                If MRegex.Regx_Match(aValidate(1, jValidate), aModelRX(1, j)) Then
                    For i = LBound(aValidate) + 1 To UBound(aValidate)
                        aData(i - 1, j) = aValidate(i, jValidate)
                    Next i
                End If
            Else
                If aValidateData(1, jValidate) = aModelHeader(1, j) Then
                    If IncludeHeader Then
                        aData(1, j) = aModel(1, j)
                    End If
                    For i = LBound(aValidate) + 1 To UBound(aValidate)
                        If IncludeHeader Then
                            aData(i, j) = aValidate(i, jValidate)
                        Else
                            aData(i - 1, j) = aValidate(i, jValidate)
                        End If

                    Next i
                End If
            End If
        Next jValidate
    Next j

    Get_Data = aData

End Function
Function Get_EncriptedString_Data(ByVal Text As String) As String
    Const SUB_NAME As String = "Get EncriptedString Data"
    Call Log_Step(SUB_NAME)

    Dim b64String As String
    Dim b64Split As Variant
    Dim i As Long

    b64Split = Split(Text, ENCRYPT_SEPARATOR)
    If UBound(b64Split) = 0 Then
        Call err.Raise(666, , "Encripted Separator not found in text!")
    End If
    b64String = b64Split(1)
    'b64Split = Split(b64String, Chr(13))
    'b64String = vbNullString
    'For i = LBound(b64Split) To UBound(b64Split)
    'b64String = b64String & MRegex.Regx_Replace((b64Split(i)), "\s", "")
    'Next i
    'b64String = Join(b64Split)
    b64String = Replace(b64String, Chr(13), "")
    b64String = MRegex.Regx_Replace(b64String, "\s", "")
    Get_EncriptedString_Data = b64String
End Function
Function Get_File_Extension(CaminhoArquivo As String)
    Const SUB_NAME As String = "Get_File_Extension"
    Call Log_Step(SUB_NAME)

    Dim gobjFSO As Scripting.FileSystemObject
    If Len(CaminhoArquivo) > 0 Then
        Set gobjFSO = New Scripting.FileSystemObject
        Get_File_Extension = gobjFSO.GetExtensionName(CaminhoArquivo)
    Else
        Get_File_Extension = vbNullString
    End If
    Set gobjFSO = Nothing
    
End Function
Function Get_Last_Column(ByVal wksData As Worksheet, ByVal Linha As Long) As Long
    'Objetivo: Fun��o que retorna a ultima Column da Planilha
    Const SUB_NAME As String = "Get_Last_Column"
    Call Log_Step(SUB_NAME)

    Get_Last_Column = 0
    Get_Last_Column = wksData.Cells(Linha, wksData.Columns.Count).End(XlDirection.xlToLeft).Column
End Function
Function Get_Last_Row(ByVal wksData As Worksheet, ByVal Column As Long) As Long
    'Objetivo: Fun��o que retorna a ultima linha da Planilha
    Const SUB_NAME As String = "Get_Last_Row"
    Call Log_Step(SUB_NAME)

    Get_Last_Row = 0
    Get_Last_Row = wksData.Cells(wksData.Rows.Count, CInt(Column)).End(XlDirection.xlUp).Row
End Function
Function Get_Model_Range(ByVal Model As String) As Range
    Const SUB_NAME As String = "Get Model Range"
    Call Log_Step(SUB_NAME)

    Set Get_Model_Range = ThisWorkbook.Names(Model).RefersToRange
End Function
Function Get_Range_To_String(RangeName As String, HasHeader As Boolean)
    Const SUB_NAME As String = "Get_Range_To_String"
    Call Log_Step(SUB_NAME)

    Dim vName As Name
    Dim aData() As Variant
    Dim i As Long
    Dim returnString As String
    Dim iHeader As Long

    aData = Get_Model_Range(RangeName)

    iHeader = 0
    If HasHeader Then
        iHeader = 1
    End If

    For i = LBound(aData) + iHeader To UBound(aData)
        If aData(i, 1) <> vbNullString Then
            returnString = returnString & SPLIT1 & Join(Application.WorksheetFunction.Index(aData, i), SPLIT2)
        End If
    Next i

    Get_Range_To_String = returnString

End Function
Public Function Get_Row_Number(aData() As Variant, ByVal RowName As String, ByVal Column As Long)
    Const SUB_NAME As String = "Get Row Number"
    Call Log_Step(SUB_NAME)

    Dim i As Long
    Get_Row_Number = 0

    For i = LBound(aData) To UBound(aData)
        If UCase(Trim(RowName)) = UCase(Trim(aData(i, Column))) Then
            Get_Row_Number = i
            Exit Function
        End If
    Next i
End Function
Function Get_Selected_Files(Optional AllowMulti As Boolean = True, Optional fileName As String = vbNullString) As Variant
    Const SUB_NAME As String = "Get_Selected_Files"
    Call Log_Step(SUB_NAME)

    Dim SelectedFiles() As Variant
    Dim i As Long


    If fileName <> vbNullString Then
        If gobjFSO.FileExists(fileName) Then
            ReDim SelectedFiles(1 To 1)
            SelectedFiles(1) = fileName
        End If
    Else

        With Application.FileDialog(msoFileDialogFilePicker)
            .AllowMultiSelect = AllowMulti
            .InitialFileName = ThisWorkbook.Path
            If .Show = -1 Then

                ReDim SelectedFiles(1 To .SelectedItems.Count)
                For i = 1 To .SelectedItems.Count
                    SelectedFiles(i) = .SelectedItems(i)
                Next i

            Else
                ReDim SelectedFiles(1 To 1)
            End If
        End With
    End If
    Get_Selected_Files = SelectedFiles
End Function
Function Get_Table_To_String(wksTable As Worksheet)
    Const SUB_NAME As String = "Get_Table_To_String"
    Call Log_Step(SUB_NAME)

    Dim aData() As Variant
    Dim i As Long
    Const SPLIT1 As String = "|"
    Const SPLIT2 As String = "#"

    Dim returnString As String

    aData = wksTable.Range("A1").CurrentRegion

    For i = LBound(aData) + 1 To UBound(aData)
        'returnString = returnString & split1 & aData(i, 1) & split2 & aData(i, 2)
        returnString = returnString & SPLIT1 & Join(Application.WorksheetFunction.Index(aData, i), SPLIT2)
    Next i

    Get_Table_To_String = returnString
End Function
Function Get_Total(aData() As Variant, ByVal ColumnSum As Long) As Long
    Const SUB_NAME As String = "Get_Total"
    Call Log_Step(SUB_NAME)

    Dim i As Long
    Dim NumberFormat As String

    NumberFormat = Check_Number_Format
    'Measure the total quantity of items
    For i = LBound(aData) To UBound(aData)

        aData(i, ColumnSum) = MStructure.Convert_Number(aData(i, ColumnSum), NumberFormat)

        If aData(i, ColumnSum) = vbNullString Then
            aData(i, ColumnSum) = 1
        End If

        If IsNumeric(aData(i, ColumnSum)) Then
            aData(i, ColumnSum) = CDbl(aData(i, ColumnSum))
        End If

    Next i

    Get_Total = Application.WorksheetFunction.Sum(Application.WorksheetFunction.Index(aData, 0, ColumnSum))
End Function
Sub Check_Worksheets_in_Global_Dictionary()
    Const SUB_NAME As String = "Check_Worksheets_in_Global_Dictionary"
    Call Log_Step(SUB_NAME)

    If gDicWorksheets Is Nothing Then
        Call Load_Worksheets_To_Global_Dictionary
    ElseIf gDicWorksheets.Count = 0 Then
        Call Load_Worksheets_To_Global_Dictionary
    End If
End Sub
Sub Load_Worksheets_To_Global_Dictionary()
    Const SUB_NAME As String = "Load_Worksheets_To_Global_Dictionary"
    Call Log_Step(SUB_NAME)

    Set gDicWorksheets = New Dictionary
    Dim wks As Worksheet
    For Each wks In ThisWorkbook.Worksheets
        If Not gDicWorksheets.Exists(wks.Codename) Then
            Call gDicWorksheets.Add(wks.Codename, wks)
        End If
    Next wks

End Sub
Function Get_Worksheet_By_Codename(Codename As String) As Worksheet
    Const SUB_NAME As String = "Get_Worksheet_By_Codename"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet

    Call Check_Worksheets_in_Global_Dictionary

    If gDicWorksheets.Exists(Codename) Then
        Set Get_Worksheet_By_Codename = gDicWorksheets.Item(Codename)
    Else
        Set Get_Worksheet_By_Codename = Nothing
    End If

End Function
Function Get_Worksheet_Range(Codename As String) As Range
    Const SUB_NAME As String = "Get_Worksheet_Range"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Set Get_Worksheet_Range = Nothing
    Set wks = Get_Worksheet_By_Codename(Codename)
    
    If Not wks Is Nothing Then
        Set Get_Worksheet_Range = wks.Range("A1")
    End If

End Function

Sub GoalSeek(rngGoal As Range, rngChange)
    Const SUB_NAME As String = "GoalSeek"
    Call Log_Step(SUB_NAME)

    rngGoal.GoalSeek Goal:=0, ChangingCell:=rngChange
End Sub
Sub Import_TXT(TextCollection As Collection, TextCollectionNames As Collection)
    Const SUB_NAME As String = "Import_TXT"
    Call Log_Step(SUB_NAME)

    Dim fileName As String, textData As String, FileNo As Integer
    Dim SelectedFiles() As Variant
    Dim wkbFile As Workbook
    Dim wksFile As Worksheet
    Dim SelectedFile As Variant
    SelectedFiles = Get_Selected_Files(AllowMulti:=True)
    FileNo = 1
    For Each SelectedFile In SelectedFiles
        If Not IsEmpty(SelectedFile) Then
            Open SelectedFile For Input As #FileNo
            textData = Input$(LOF(FileNo), FileNo)
            Call TextCollection.Add(textData, CStr(FileNo))
            Call TextCollectionNames.Add(SelectedFile, CStr(FileNo))

            Close #FileNo
            FileNo = FileNo + 1

        End If
    Next SelectedFile


End Sub
Sub Import_XLSX(Optional WorksheetImportIndex As Long = 1)
    Const SUB_NAME As String = "Import_XLSX"
    Call Log_Step(SUB_NAME)

    Dim SelectedFiles() As Variant
    Dim wkbFile As Workbook
    Dim wksFile As Worksheet
    Dim wks As Worksheet
    Dim SelectedFile As Variant
    SelectedFiles = Get_Selected_Files(AllowMulti:=False)
    Set wks = Get_Worksheet_By_Codename("wksTemporary")
    For Each SelectedFile In SelectedFiles
        If Not IsEmpty(SelectedFile) Then
            Set wkbFile = Workbooks.Open(SelectedFile)
            Set wksFile = wkbFile.Worksheets(WorksheetImportIndex)
            wksFile.Cells.Copy
            wks.Cells.PasteSpecial xlPasteAll
            wkbFile.Close (False)
            Set wkbFile = Nothing
        End If
    Next SelectedFile

End Sub
Sub Import_XLSX_Model(wksData As Worksheet, DataModel As String, Optional WorksheetImportIndex As Long = 1, _
                Optional WorksheetHeaderRow As Long = 1, Optional WorksheetHeaderColumn As Long = 1, _
                Optional HeaderAddress As String = "A1", Optional DataAddress As String = "A2", _
                Optional CanEraseData As Boolean = False, Optional fileName As String = vbNullString)
    Const SUB_NAME As String = "Import_XLSX_Model"
    Call Log_Step(SUB_NAME)

    Dim wkbFile As Workbook
    Dim wksFile As Worksheet
    Dim wks As Worksheet

    Dim rngModel As Range
    Dim aModel() As Variant

    Dim aFileHeader() As Variant
    Dim aFileData() As Variant
    Dim aTemp() As Variant
    Dim aData() As Variant
    Dim SelectedFiles() As Variant
    Dim SelectedFile As Variant
    Dim i As Long
    Dim j As Long
    Dim jData As Long
    Dim jModel As Long
    
    
    Set rngModel = Get_Model_Range(DataModel)
    
    'Process
    SelectedFiles = Get_Selected_Files(, fileName)

    If SelectedFiles(LBound(SelectedFiles)) = vbNullString Then
        GoTo Finish
    End If

    Call MStructure.Generate_Worksheet_From_Model(wksData, CanEraseData, DataModel)

    Set wks = Get_Worksheet_By_Codename("wksTemporary")
    
   For Each SelectedFile In SelectedFiles

        Select Case UCase(gProjectName)
            Case UCase(PROJECT_PIPELINE)
                If MRegex.Regx_Match(SelectedFile, "rental") Then
                    Set rngModel = Get_Model_Range(DataModel & "_Rental")
                Else
                    Set rngModel = Get_Model_Range(DataModel)
                End If
        End Select

        aModel = rngModel
        
        Set wkbFile = Workbooks.Open(SelectedFile)
        Set wksFile = wkbFile.Worksheets(WorksheetImportIndex)
        
        aFileHeader = wksFile.Range(HeaderAddress).Resize(, Get_Last_Column(wksFile, WorksheetHeaderRow))
        aFileData = wksFile.Range("A1").Resize(Get_Last_Row(wksFile, 1), Get_Last_Column(wksFile, 1))

        If Get_Data_From_Valid_Model(aModel, aModel, aFileData) Then
            wksData.Range(HeaderAddress).Offset(MStructure.Get_Last_Row(wksData, WorksheetHeaderColumn)).Resize(UBound(aFileData), UBound(aFileData, 2)) = aFileData
        Else
            Call MInterface.Message_Error("Layout diverge do padr�o! Verificar cabe�alho!" & vbNewLine & "File: " & wkbFile.Name & vbNewLine, SUB_NAME)
        End If

        Call wkbFile.Close(False)
        Set wkbFile = Nothing
        
    Next SelectedFile

    Select Case UCase(gProjectName)
        Case UCase(PROJECT_PIPELINE)
            Call MStructure.Generate_Worksheet_From_Model(wksData, False, DataModel & "_Header")
    End Select

    wksData.Columns.AutoFit

Finish:
    If Not wkbFile Is Nothing Then
        wkbFile.Close (False)
    End If

End Sub
Sub Insert_Timeserial_To_Array(aData() As Variant)
    Const SUB_NAME As String = "Insert_Timeserial_To_Array"
    Call Log_Step(SUB_NAME)

    Call Insert_Field_To_Array(aData, "Timeserial", gTimeserial)
End Sub
Sub Insert_Field_To_Array(aData() As Variant, ColumnName As String, FieldValue As Variant)
    Const SUB_NAME As String = "Insert_Field_To_Array"
    Call Log_Step(SUB_NAME)

    Dim i As Long
    Dim j As Long
    j = Get_Column_Number(aData, ColumnName)
    If j > 0 Then
        For i = LBound(aData) + 1 To UBound(aData)
            aData(i, j) = FieldValue
        Next i
    End If
End Sub
Sub Treat_Valid_Model_Array(ByRef aData() As Variant, Optional HeaderRow As Long = 1)
    Const SUB_NAME As String = "Treat_Valid_Model_Array"
    Call Log_Step(SUB_NAME)

    Dim j As Long
    For j = LBound(aData, 2) To UBound(aData, 2)
        aData(HeaderRow, j) = Treat_Valid_Model(aData(HeaderRow, j))
    Next j
End Sub
Function Get_Data_From_Valid_Model(aModel() As Variant, aModelRX() As Variant, aValidate() As Variant, Optional RX As Boolean = False) As Boolean
    Const SUB_NAME As String = "Is Valid Model"
    Call Log_Step(SUB_NAME)

    Dim i As Long
    Dim j As Long
    Dim isFound As Boolean
    Dim ValidHeader As Boolean
    Dim HeadersNotFound As String
    Dim jValidate As Long
    Dim aData() As Variant
    Dim aCompareModel() As Variant

    'Process

    'Redim with ROWS from DATA and COLUMNS from MODEL
    ReDim aData(LBound(aValidate) To UBound(aValidate), LBound(aModel, 2) To UBound(aModel, 2))


    'Treat the headers
    aCompareModel = aModel
    If Not RX Then
        Call Treat_Valid_Model_Array(aCompareModel)
    End If

    Call Treat_Valid_Model_Array(aValidate)


    Get_Data_From_Valid_Model = True
    'Loop the MODEL COLUMNS x the DATA COLUMNS
    For j = LBound(aModel, 2) To UBound(aModel, 2)
        ValidHeader = False

        For jValidate = LBound(aValidate, 2) To UBound(aValidate, 2)

            'COMPARE MODE
            If RX Then
                If MRegex.Regx_Match(aValidate(1, jValidate), aModelRX(1, j)) Then
                    ValidHeader = True
                    Exit For
                End If
            Else
                If aValidate(1, jValidate) = aCompareModel(1, j) Then
                    ValidHeader = True
                    Exit For
                End If
            End If



        Next jValidate

        If ValidHeader Then
            'IF THE HEADER IS FOUND THEN LOOP THE DATA
            For i = LBound(aValidate) + 1 To UBound(aValidate)
                aData(i - 1, j) = aValidate(i, jValidate)
            Next i

        Else

            If HeadersNotFound = "" Then
                HeadersNotFound = aModel(1, j)
            Else
                HeadersNotFound = HeadersNotFound & vbCrLf & aModel(1, j)
            End If

            Get_Data_From_Valid_Model = False

        End If

nextj:
    Next j

    If Not Get_Data_From_Valid_Model Then
        Call MInterface.Message_Error("Fields not found: " & vbCrLf & HeadersNotFound)
    Else
        aValidate = aData
    End If

End Function
Function Treat_Valid_Model(ByVal Text As String) As String
    Const SUB_NAME As String = "Treat_Valid_Model"
    Call Log_Step(SUB_NAME)

    Treat_Valid_Model = Remove_Special_Chars(UCase(Trim(Text)))
End Function
Function Get_Data_From_Valid_Model_FixedPosition(aModel() As Variant, aValidate() As Variant)
    Const SUB_NAME As String = "Get_Data_From_Valid_Model_FixedPosition"
    Call Log_Step(SUB_NAME)

    Dim j As Long

    Get_Data_From_Valid_Model_FixedPosition = True
    For j = LBound(aModel, 2) To UBound(aModel, 2)
        If UCase(Trim(aValidate(1, j))) <> UCase(Trim(aModel(1, j))) Then
            Get_Data_From_Valid_Model_FixedPosition = False
            Exit Function
        End If
    Next j

End Function
Function Load_Dictionary(NomeModeloRange As String, Optional colunaChave As Long = 1, Optional colunaRetorno As Long = 2, Optional ColumnValid As Long = 0, Optional ByRef dicRetorno As Dictionary = Nothing) As Dictionary
    Const SUB_NAME As String = "Load Dictionary"
    Call Log_Step(SUB_NAME)


    Dim LayoutDados() As Variant
    Dim i As Long
    Dim j As Long
    Dim Chave As String
    Dim aDataRow() As Variant
    If dicRetorno Is Nothing Then
        Set dicRetorno = New Dictionary
    End If

    ThisWorkbook.Activate
    LayoutDados = Get_Current_Region(Get_Model_Range(NomeModeloRange))

    For i = LBound(LayoutDados) + 1 To UBound(LayoutDados)

        Chave = UCase(LayoutDados(i, colunaChave))

        If ColumnValid > 0 Then
            If LayoutDados(i, ColumnValid) = "" Or CStr(LayoutDados(i, ColumnValid)) = "0" Then
                GoTo nextI
            End If
        End If
        If Chave = vbNullString Then GoTo nextI


        If Not dicRetorno.Exists(Chave) Then
            If colunaRetorno = 0 Then
                ReDim aDataRow(LBound(LayoutDados, 2) To UBound(LayoutDados, 2))
                For j = LBound(LayoutDados, 2) To UBound(LayoutDados, 2)
                    aDataRow(j) = LayoutDados(i, j)
                Next j
                Call dicRetorno.Add(Chave, aDataRow)
            Else
                Call dicRetorno.Add(Chave, LayoutDados(i, colunaRetorno))
            End If

        End If
nextI:
    Next i
    
    Set Load_Dictionary = dicRetorno
    
End Function
Function Load_Dictionary_From_Array(LayoutDados() As Variant, Optional colunaChave As Long = 1, Optional colunaRetorno As Long = 2, Optional ColumnValid As Long = 0, Optional ValidValue As String = vbNullString, Optional ByRef dicRetorno As Dictionary = Nothing) As Dictionary
    Const SUB_NAME As String = "Load Dictionary"
    Call Log_Step(SUB_NAME)

    Dim i As Long
    Dim Chave As String

    If dicRetorno Is Nothing Then
        Set dicRetorno = New Dictionary
    End If

    For i = LBound(LayoutDados) + 1 To UBound(LayoutDados)
        Application.StatusBar = "Loading Dictionary: " & i & " / " & UBound(LayoutDados)
        Chave = UCase(LayoutDados(i, colunaChave))

        If ColumnValid > 0 Then
            If LayoutDados(i, ColumnValid) <> ValidValue Or CStr(LayoutDados(i, ColumnValid)) = "0" Then
                GoTo nextI
            End If
        End If
        If Chave = vbNullString Then GoTo nextI


        If Not dicRetorno.Exists(Chave) Then
            If colunaRetorno = 0 Then
                Call dicRetorno.Add(Chave, Application.WorksheetFunction.Index(LayoutDados, i))
            Else
                Call dicRetorno.Add(Chave, LayoutDados(i, colunaRetorno))
            End If

        End If
nextI:
    Next i
    
    Set Load_Dictionary_From_Array = dicRetorno
    
End Function
Function Load_Dictionary_Columns(aModel() As Variant, Optional HeaderRow As Long = 1) As Dictionary
    Const SUB_NAME As String = "Load_Dictionary_Columns"
    Call Log_Step(SUB_NAME)

    Dim dicRetorno As Dictionary
    Dim jModel As Long
    Dim Key As Variant
    
    Set dicRetorno = New Dictionary
    
    For jModel = LBound(aModel, 2) To UBound(aModel, 2)
        Key = aModel(HeaderRow, jModel)

        If Not dicRetorno.Exists(Key) Then
            Call dicRetorno.Add(Key, jModel)
        End If

    Next jModel
    
    Set Load_Dictionary_Columns = dicRetorno
    
End Function
Function Model_Columns_Count(Model As String)
    Const SUB_NAME As String = "Model Columns Count"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksModels")
    Model_Columns_Count = wks.Range(Model).Columns.Count
End Function
Public Sub Model_Sentence(Sentence As String, rng As Range, Optional MyFontStyle As String = "Bold", Optional MyColor As Long = -16776961)
    Const SUB_NAME As String = "Model_Sentence"
    Call Log_Step(SUB_NAME)

    Dim RX As RegExp :     Set RX = New RegExp
    Dim matchesCol As MatchCollection
    Dim matchItem As Match
    RX.Global = True
    RX.IgnoreCase = True
    RX.MultiLine = True

    RX.Pattern = "[\\\/\-\*\+\|\&\%\$\#\@\!\�\(\)\:]"

    Sentence = RX.Replace(Sentence, ".")



    RX.Pattern = Sentence
    'rx.Pattern = "[\r\n]*" & Sentence
    Set matchesCol = RX.Execute(rng.Value)
    
    For Each matchItem In matchesCol

        With rng.Characters(Start:=matchItem.FirstIndex + 1, Length:=matchItem.Length).Font
            '.Name = "Calibri"
            .FontStyle = MyFontStyle
            .Color = MyColor
            '.Size = 11
            '.Strikethrough = False
            '.Superscript = False
            '.Subscript = False
            '.OutlineFont = False
            '.Shadow = False
            '.Underline = xlUnderlineStyleNone
            '.ThemeColor = xlThemeColorLight1
            '.TintAndShade = 0
            '.ThemeFont = xlThemeFontMinor
        End With
    Next matchItem

End Sub
Sub Model_to_NamedRange(Model As String, NamedRange As String, Optional PasteType As XlPasteType)
    Const SUB_NAME As String = "Model_to_NamedRange"
    Call Log_Step(SUB_NAME)

    Dim rngOrigin As Range
    Dim rngDestination As Range
    
    Set rngOrigin = Get_Model_Range(Model)
    Set rngDestination = Get_Model_Range(NamedRange)
    
    rngOrigin.Copy
    rngDestination.PasteSpecial PasteType
    Application.CutCopyMode = False

End Sub
Sub Model_To_Range(Model As String, rng As Range)
    Const SUB_NAME As String = "Model To Range"
    Call Log_Step(SUB_NAME)

    Dim rngModel As Range
    Set rngModel = Get_Model_Range(Model)
    
    rngModel.Copy
    rng.PasteSpecial xlPasteAll
    Application.CutCopyMode = False
End Sub
Public Sub Move_Shape(shp As Shape, ByVal Top As Long, ByVal Left As Long)
    Const SUB_NAME As String = "Move_Shape"
    Call Log_Step(SUB_NAME)

    shp.Top = Top
    shp.Left = Left
End Sub
Public Sub New_NamedRange(Name As String, rng As Range, Optional wksNamedRange As Worksheet = Nothing)
    Const SUB_NAME As String = "New_NamedRange"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)


    Call Delete_NamedRange(Name)

    If wksNamedRange Is Nothing Then
        ThisWorkbook.Names.Add Name:=Name, RefersTo:=rng
    Else
        wksNamedRange.Names.Add Name:=Name, RefersTo:=rng
    End If

Finish:
    Exit Sub

ErrorHandler:
    Call err.Raise(err.Number, SUB_NAME, err.Description)
End Sub
Function New_Timeserial() As String
    Const SUB_NAME As String = "New_Timeserial"
    Call Log_Step(SUB_NAME)

    New_Timeserial = Format(Now, "yyyymmddhhmmss")
End Function

Function OptimizeVBA(TurnOn As Boolean)
    Const SUB_NAME As String = "Optimize VBA"
    Call Log_Step(SUB_NAME)

    If TurnOn = True Then
        Application.ScreenUpdating = True
        Application.Calculation = xlAutomatic
        Application.StatusBar = False

    Else

        Application.ScreenUpdating = False
        Application.Calculation = xlManual

    End If
End Function
Sub Paste_From_Clipboard_To_Range(rng As Range)
    Const SUB_NAME As String = "Paste From Clipboard To Range"
    Call Log_Step(SUB_NAME)

    Dim ClipboardText As String
    Dim DataObj As MSForms.DataObject
    On Error GoTo ErrorHandler
    
    Set DataObj = New MSForms.DataObject
    DataObj.GetFromClipboard

    ClipboardText = DataObj.GetText(1)

    rng.Value = ClipboardText
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error("Fail to paste!" & vbCrLf & "Try copying from a text file!")
End Sub
Function File_Exists(Caminho As String) As Boolean
    Const SUB_NAME As String = "File Exists"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    If Dir(Caminho, vbArchive) = vbNullString Then
        File_Exists = False
    Else
        File_Exists = True
    End If

Fim:
    Exit Function

ErrorHandler:

    Call MInterface.Message_Error("Erro ao verificar a existencia do Arquivo", SUB_NAME)
    On Error GoTo 0
    GoTo Fim

End Function
Sub FilesPaths_To_Collection(Optional dicFiles As Dictionary = Nothing, Optional FileExtension As String = "XML", Optional FolderPath As String = vbNullString)
    Const SUB_NAME As String = "Files Paths To Collection"
    Call Log_Step(SUB_NAME)

    Dim filePath As String
    Dim Files As Long
    Dim ValidFiles As Long

    Files = 0
    ValidFiles = 0

    If gobjFSO Is Nothing Then
        Set gobjFSO = New FileSystemObject
    End If


    If dicFiles Is Nothing Then
        Set gdicPaths = New Dictionary
    End If
    
    Set gcolFolderPaths = New Collection
    
    If FolderPath = vbNullString Then

        With Application.FileDialog(msoFileDialogFolderPicker)
            .InitialFileName = ThisWorkbook.Path

            If .Show = -1 Then

                filePath = .SelectedItems(1)

            End If 'If .Show = -1 Then
        End With 'With Application.FileDialog(msoFileDialogFolderPicker)
    Else
        filePath = FolderPath
    End If 'If FolderPath = vbNullString Then

    If filePath <> vbNullString Then
        gcolFolderPaths.Add gobjFSO.GetFolder(filePath)

        Do While gcolFolderPaths.Count > 0
            
            'Gets the first folder & remove from collection
            Set gobjFolder = gcolFolderPaths(1)
            gcolFolderPaths.Remove 1

            'Add all subfolders in the current folder
            For Each gobjSubFolder In gobjFolder.SubFolders
                gcolFolderPaths.Add gobjSubFolder
            Next gobjSubFolder


            For Each gobjFile In gobjFolder.Files
                Files = Files + 1
                If Files Mod 100 = 0 Then DoEvents : Files = Files + 1

                If UCase(gobjFSO.GetExtensionName(gobjFile.Path)) = FileExtension Or FileExtension = vbNullString Then

                    ValidFiles = ValidFiles + 1

                    If dicFiles Is Nothing Then
                        If Not gdicPaths.Exists(gobjFile.Path) Then
                            Call gdicPaths.Add(gobjFile.Path, gobjFile.Name)
                        End If

                    Else
                        If Not dicFiles.Exists(gobjFile.Path) Then
                            Call dicFiles.Add(gobjFile.Path, gobjFile.Name)
                        End If
                    End If


                End If
                Application.StatusBar = "Valid Files: " & ValidFiles & " / " & Files
            Next

        Loop 'Do While gcolFolderPaths.Count > 0

    End If 'If filePath <> vbNullString Then

End Sub
Sub FoldersPaths_To_Collection(Optional dicFiles As Dictionary = Nothing, Optional FileExtension As String = "XML")
    Const SUB_NAME As String = "Folders Paths To Collection"
    Call Log_Step(SUB_NAME)

    Dim filePath As String
    Dim Files As Long
    Dim ValidFiles As Long


    Files = 0
    ValidFiles = 0

    If gobjFSO Is Nothing Then
        Set gobjFSO = New FileSystemObject
    End If


    If dicFiles Is Nothing Then
        Set gdicPaths = New Dictionary
    End If
    
    Set gcolFolderPaths = New Collection
    
    With Application.FileDialog(msoFileDialogFolderPicker)
        .InitialFileName = ThisWorkbook.Path

        If .Show = -1 Then

            filePath = .SelectedItems(1)
            gcolFolderPaths.Add gobjFSO.GetFolder(filePath)

            Do While gcolFolderPaths.Count > 0
                
                
                'Gets the first folder & remove from collection
                Set gobjFolder = gcolFolderPaths(1)
                gcolFolderPaths.Remove 1

                If Not gdicPaths.Exists(gobjFolder) Then
                    ValidFiles = ValidFiles + 1
                    Application.StatusBar = "Valid Folders: " & ValidFiles & " | " & gobjFolder
                    Call gdicPaths.Add(gobjFolder, gobjFolder)
                End If

                'Add all subfolders in the current folder
                For Each gobjSubFolder In gobjFolder.SubFolders
                    gcolFolderPaths.Add gobjSubFolder
                Next gobjSubFolder

            Loop 'Do While gcolFolderPaths.Count > 0
        End If 'If .Show = -1 Then
    End With 'With Application.FileDialog(msoFileDialogFolderPicker)
End Sub
Function RawData_To_Array(RawData As String)
    Const SUB_NAME As String = "RawData_To_Array"
    Call Log_Step(SUB_NAME)

    Dim RawX As Variant
    Dim RawY As Variant

    Dim aData() As Variant

    RawX = Split(RawData, SPLIT1)
    RawY = Split(RawX(1), SPLIT2)
    ReDim aData(1 To UBound(RawX), 1 To UBound(RawY) + 1)
    Dim i As Long
    Dim j As Long
    Dim iData As Long
    iData = 1
    For i = LBound(RawX) + 1 To UBound(RawX)
        RawY = Split(RawX(i), SPLIT2)
        For j = LBound(RawY) To UBound(RawY)
            aData(iData, j + 1) = RawY(j)

        Next j
        iData = iData + 1
    Next i
    RawData_To_Array = aData
End Function
Public Sub Range_To_Image(rngImage As Range)
    Const SUB_NAME As String = "Range_To_Image"
    Call Log_Step(SUB_NAME)

    Dim wksTemp As Worksheet
    Set wksTemp = MStructure.Get_Worksheet_By_Codename("wksTemporary")
    rngImage.Copy
    wksTemp.Pictures.Paste.Select
End Sub
Sub SelectedRangeToImage()
    Const SUB_NAME As String = "SelectedRangeToImage"
    Call Log_Step(SUB_NAME)

    Dim tmpChart As Chart, n As Long, shCount As Long, sht As Worksheet, sh As Shape
    Dim fileSaveName As Variant, pic As Variant
    'Create temporary chart as canvas
    Set sht = Selection.Worksheet
    Selection.Copy
    sht.Pictures.Paste.Select
    Set sh = sht.Shapes(sht.Shapes.Count)
    Set tmpChart = Charts.Add
    tmpChart.ChartArea.Clear
    tmpChart.Name = "PicChart" & (Rnd() * 10000)
    Set tmpChart = tmpChart.Location(Where:=xlLocationAsObject, Name:=sht.Name)
    tmpChart.ChartArea.Width = sh.Width
    tmpChart.ChartArea.Height = sh.Height
    tmpChart.Parent.Border.LineStyle = 0
    'Paste range as image to chart
    sh.Copy
    tmpChart.ChartArea.Select
    tmpChart.Paste
    'Save chart image to file
    fileSaveName = Application.GetSaveAsFilename(FileFilter:="Image (*.jpg), *.jpg")
    If fileSaveName <> False Then
        tmpChart.Export fileName:=fileSaveName, Filtername:="jpg"
    End If
    'Clean up
    sht.Cells(1, 1).Activate
    sht.ChartObjects(sht.ChartObjects.Count).Delete
    sh.Delete
End Sub
Public Sub Refresh_NamedRanges_Between_NamedRanges(wksRefresh As Worksheet, ColumnName As String, LineHeader As Long, LineData As Long, rngI As Range, rngO As Range)
    Const SUB_NAME As String = "Refresh_NamedRanges_Between_NamedRanges"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim rngName As Range
    Dim Name As String
    Dim i As Long
    Dim j As Long
    Dim aHeader() As Variant

    aHeader = wksRefresh.Range("A" & LineHeader).Resize(, Get_Last_Column(wksRefresh, LineHeader))
    For j = LBound(aHeader, 2) To UBound(aHeader, 2)
        Name = aHeader(1, j)

        If Name <> vbNullString Then
            Set rngName = wksRefresh.Range(wksRefresh.Cells(rngI.Offset(1).Row, j), wksRefresh.Cells(rngO.Offset(-1).Row, j))
            Call New_NamedRange(Name, rngName)
        End If
    Next j


ErrorHandler:

End Sub
Public Sub Refresh_NamedRanges_Lists(Optional wksRefresh As Worksheet = Nothing, Optional ColumnName As String = "A", Optional LineHeader As Long = 1, Optional LineData As Long = 4)
    Const SUB_NAME As String = "Refresh_NamedRanges_Lists"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim rngName As Range
    Dim Name As String
    Dim i As Long
    Dim j As Long
    Dim aHeader() As Variant


    If wksRefresh Is Nothing Then
        Set wksRefresh = Get_Worksheet_By_Codename("wksList_From_To_Lists")
    End If

    aHeader = wksRefresh.Range(ColumnName & LineHeader).Resize(, Get_Last_Column(wksRefresh, LineHeader))
    For j = LBound(aHeader, 2) To UBound(aHeader, 2)
        Name = aHeader(1, j)

        If Name <> vbNullString Then
            i = Get_Last_Row(wksRefresh, j)
            If (i > (LineData - 1)) Then
                Set rngName = wksRefresh.Cells(LineData, j).Resize(i - (LineData - 1))
                Call New_NamedRange(Name, rngName)
            End If
        End If
    Next j

    Call Message_Information(MSG_SUCCESS, SUB_NAME)
ErrorHandler:

End Sub
Sub Remove_All_Filters_Workbook()
    Const SUB_NAME As String = "Remove_All_Filters_Workbook"
    Call Log_Step(SUB_NAME)

    Dim wksTemp As Worksheet

    For Each wksTemp In ThisWorkbook.Worksheets
        wksTemp.AutoFilterMode = False
    Next wksTemp

End Sub
Function Remove_Header_From_Array(aData() As Variant, HeaderRow As Long) As Variant
    Const SUB_NAME As String = "Remove_Header_From_Array"
    Call Log_Step(SUB_NAME)

    Dim aReturn() As Variant
    Dim i As Long
    Dim j As Long

    If UBound(aData) - HeaderRow <= 0 Then
        ReDim aReturn(LBound(aData) To UBound(aData), LBound(aData, 2) To UBound(aData, 2))
    Else
        ReDim aReturn(LBound(aData) To UBound(aData) - HeaderRow, LBound(aData, 2) To UBound(aData, 2))
        For i = LBound(aReturn) To UBound(aReturn)
            For j = LBound(aReturn, 2) To UBound(aReturn, 2)
                aReturn(i, j) = aData(i + HeaderRow, j)
            Next j
        Next i
    End If
    Remove_Header_From_Array = aReturn
End Function
Sub Shape_to_NamedRange(Model As String, NamedRange As String, Optional WorksheetModelCodename As String = "wksModels")
    Const SUB_NAME As String = "Shape_to_NamedRange"
    Call Log_Step(SUB_NAME)

    Dim shp As Shape
    Dim rngOrigin As Range
    Dim rngDestination As Range
    Dim wks As Worksheet
    
    Set wks = Get_Worksheet_By_Codename(WorksheetModelCodename)
    Set shp = wks.Shapes.Range(Array(Model))
    
    Set rngOrigin = Get_Model_Range(Model)
    Set rngDestination = Get_Model_Range(NamedRange)
    
    rngOrigin.Copy
    rngDestination.PasteSpecial xlPasteAll
    Application.CutCopyMode = False

End Sub
Sub Shape_to_Range(Model As String, wksRange As Worksheet, CellRange As String, Optional Text As String = vbNullString, Optional Top As Long = 0, Optional Left As Long = 0)
    Const SUB_NAME As String = "Shape_to_Range"
    Call Log_Step(SUB_NAME)

    Dim shp As Object
    Dim shpDestination As Object
    Dim rngDestination As Range
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksModels")
    Set shp = wks.Shapes(Model)
    Set rngDestination = wksRange.Range(CellRange)
    
    'shp.Copy
    Set shpDestination = shp.Duplicate
    shpDestination.Cut
    wksRange.Paste
    Application.CutCopyMode = False

    If Top = 0 Then Top = wksRange.Range(CellRange).Top
    If Left = 0 Then Left = wksRange.Range(CellRange).Left
    
    Set shpDestination = wksRange.Shapes(wksRange.Shapes.Count)
    With shpDestination
        .Name = shp.Name
        .Top = Top
        .Left = Left
        If Text <> vbNullString Then
            .TextFrame2.TextRange.Characters.Text = Text
        End If
    End With
End Sub
Function Sort_Matrix(ByRef InputArray() As Variant, Header() As Variant, HeaderToSortBy As String) As Variant
    Const SUB_NAME As String = "Sort_Matrix"
    Call Log_Step(SUB_NAME)

    Dim SortedArray() As Variant
    Dim Positions() As Variant
    Dim i As Long
    Dim iInput As Long
    Dim j As Long
    Dim HeaderColumn As Long
    HeaderColumn = MStructure.Get_Column_Number(Header, HeaderToSortBy)

    ReDim SortedArray(LBound(InputArray) To UBound(InputArray), LBound(InputArray, 2) To UBound(InputArray, 2))
    ReDim Positions(LBound(InputArray, 2) To UBound(InputArray, 2))
    For i = LBound(InputArray) To UBound(InputArray, 2)
        Positions(i) = InputArray(HeaderColumn, i)
    Next i

    Call QSortInPlace(Positions)

    For i = LBound(Positions) To UBound(Positions)
        For iInput = LBound(InputArray, 2) To UBound(InputArray, 2)
            If Positions(i) = InputArray(HeaderColumn, iInput) Then
                For j = LBound(InputArray) To UBound(InputArray)
                    SortedArray(j, i) = InputArray(j, iInput)
                Next j
            End If
        Next iInput

    Next i

    Sort_Matrix = SortedArray
End Function
Function Unify_Unique_Header_From_Array(aData() As Variant, Optional RowHeader As Long = 1) As Variant
    Const SUB_NAME As String = "Unify_Unique_Header_From_Array"
    Call Log_Step(SUB_NAME)


    Dim aNewData() As Variant
    Dim dicHeader As Dictionary
    Dim i As Long
    Dim j As Long
    Dim jNewData As Long
    Dim Header As String

    Dim dicTagFormats As Dictionary
    Dim TagFormat As String
    Set dicTagFormats = Load_Dictionary("LAYOUT_XML_NFE_I", 14, 15)
    Dim Key As Variant
    
    Set dicHeader = New Dictionary
    jNewData = 1
    For j = LBound(aData, 2) To UBound(aData, 2)

        Header = aData(RowHeader, j)
        If Header <> vbNullString Then
            If Not dicHeader.Exists(Header) Then
                Call dicHeader.Add(Header, jNewData)
                jNewData = jNewData + 1
            End If
        End If
    Next j

    ReDim aNewData(LBound(aData) To UBound(aData), LBound(aData, 2) To dicHeader.Count)

    'Columns x Rows
    For j = LBound(aData, 2) To UBound(aData, 2)
        Header = aData(RowHeader, j)
        If dicHeader.Exists(Header) And Header <> vbNullString Then
            jNewData = dicHeader.Item(Header)
            aNewData(1, jNewData) = Header
            Key = UCase(Header)
            For i = LBound(aData) + 1 To UBound(aData)
                TagFormat = dicTagFormats.Item(Key)

                If aNewData(i, jNewData) = vbNullString Then
                    aNewData(i, jNewData) = CStr(aData(i, j))
                Else
                    Select Case UCase(TagFormat)
                        Case "SUM_CURRENCY"
                            aNewData(i, jNewData) = CCur(aNewData(i, jNewData)) + CCur(aData(i, j))
                        Case "CONCAT"
                            aNewData(i, jNewData) = CStr(aNewData(i, jNewData)) & CStr(aData(i, j))
                        Case "CONCAT_"
                            aNewData(i, jNewData) = CStr(aNewData(i, jNewData)) & "_" & CStr(aData(i, j))
                        Case Else
                            If aNewData(i, jNewData) = vbNullString Then
                                aNewData(i, jNewData) = CStr(aData(i, j))
                            End If

                    End Select
                End If
            Next i
        End If
    Next j

    Unify_Unique_Header_From_Array = aNewData

End Function
Sub Worksheet_Columns_Autofit(Codename As String)
    Const SUB_NAME As String = "Worksheet_Columns_Autofit"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename(Codename)
    wks.Columns.AutoFit
End Sub
Sub Worksheets_Remove_All_Filters()
    Const SUB_NAME As String = "Worksheet Remove All Filters"
    Call Log_Step(SUB_NAME)

    Dim wksTemp As Worksheet

    For Each wksTemp In ThisWorkbook.Worksheets
        wksTemp.AutoFilterMode = False
    Next wksTemp

End Sub