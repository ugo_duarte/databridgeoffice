Attribute VB_Name = "MPDF"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MPDF"

Global Const ARQUIVO_PDF_TO_TEXT_PATH       As String = "C:\Users\udu\Documents\CHG-Meridian\SourceCode\databridgeoffice\"
Global Const ARQUIVO_PDF_TO_TEXT_PATH_CLOUD As String = "S:\TI\Projetos\CHGBrazil\"
Global Const ARQUIVO_PDF_TO_TEXT As String = "pdftotext.chg"
Global Const ASPAS_DUPLAS   As String = """"

Function Get_PDF_To_Text_Exec_Path() As String
    If MSecurity.Is_Cloud_Enviroment Then
        Get_PDF_To_Text_Exec_Path = ARQUIVO_PDF_TO_TEXT_PATH_CLOUD & ARQUIVO_PDF_TO_TEXT
    Else
        Get_PDF_To_Text_Exec_Path = ARQUIVO_PDF_TO_TEXT_PATH & ARQUIVO_PDF_TO_TEXT
    End If
End Function
Sub Create_File_PDF_To_Text()
    Const SUB_NAME As String = "Create File PDF To Text"
    
    Const UseBinaryStreamType = 1
    Const SaveWillCreateOrOverwrite = 2
    
    Dim vBase64() As Variant
    Dim sBase64 As String
    Dim i As Long
    Dim streamOutput: Set streamOutput = CreateObject("ADODB.Stream")
    Dim xmlDoc: Set xmlDoc = CreateObject("Microsoft.XMLDOM")
    Dim xmlElem: Set xmlElem = xmlDoc.createElement("tmp")
    Dim wksPDF As Worksheet
    Set wksPDF = MStructure.Get_Worksheet_By_Codename("wksPdfToText")
    vBase64 = Get_Model_Range("rngPdfToText")
    sBase64 = vbNullString
    
    For i = LBound(vBase64) To UBound(vBase64)
        sBase64 = sBase64 & vBase64(i, 1)
    Next i
    
    xmlElem.DataType = "bin.base64"
    xmlElem.Text = sBase64
    
    streamOutput.Open
    streamOutput.Type = UseBinaryStreamType
    streamOutput.Write = xmlElem.nodeTypedValue
    
    streamOutput.SaveToFile Get_PDF_To_Text_Exec_Path, SaveWillCreateOrOverwrite
    
    Set streamOutput = Nothing
    Set xmlDoc = Nothing
    Set xmlElem = Nothing
    
End Sub
Sub Import_File_PDF(wksDestination As Worksheet, CanPileUp As Boolean, Optional vFileName As Variant = vbNullString)
    Const SUB_NAME             As String = "Import_File_PDF"
    Const CONVERSION_TYPE       As String = "TABULADO"

    
    If vFileName = vbNullString Then
        vFileName = Choose_File(PDF)
    End If
    
    If Not Process_File_PDF(vFileName, CONVERSION_TYPE, wksDestination, 1, CanPileUp) Then
        Call MInterface.Message_Error("Falha ao tentar importar arquivo: " & vbNewLine & vFileName, SUB_NAME)
        Call err.Raise(err.Number, err.Source, err.Description)
    End If
    
    
    If wksDestination.UsedRange.Rows.Count <= 1 Then
        Call MInterface.Message_Error("N�o existem dados a serem importados!" & vbNewLine & vFileName & vbNewLine & _
                      "Copiar dados a serem importados na ABA " & wksDestination.Name & "!", SUB_NAME)
        Exit Sub
    End If
    
ExitHere:
    On Error Resume Next
    Exit Sub
    
errHandler:
    Select Case err.Number
        ' Add cases for each
        ' error number you want to trap.
        Case Else
            ' Add "last-ditch" error handler.
            Stop
            Resume
            MsgBox "Error: " & err.Description
    End Select
    
    Resume ExitHere
End Sub
Function Get_Conversion_Format(ConversionType As String) As String
    ConversionType = UCase(ConversionType)
    Get_Conversion_Format = "-layout"
    If ConversionType = "SIMPLES" Then
        Get_Conversion_Format = "-raw"
    ElseIf ConversionType = "TABULADO" Then
        Get_Conversion_Format = "-table"
    ElseIf ConversionType = "LAYOUT" Then
        Get_Conversion_Format = "-layout"
    End If
End Function
     
Public Function Process_File_PDF(ByVal fileName As String, _
                                      ByVal ConversionType As String, _
                                      ByRef wksData As Worksheet, _
                                      Optional ByVal Column As Integer = 1, _
                                      Optional ByVal CanPileUp As Boolean = False)
    
    Const SUB_NAME As String = "Process File PDF"
    
    Const ForReading = 1
    Const SYSTEM = &H25&
    Const SYSTEMx86 = &H29&
    
    Dim objFile          As Scripting.File
    Dim FileNamePDF      As String
    Dim FileNameTXT      As String
    Dim ConversionFormat    As String
    Dim objShell            As Object
    
    
    Dim RodaPDF     As Object
    
    Dim objPath     As Object

    Dim ULWS      As Long
    
    Dim arquivotxt  As String
    Dim formato     As String
    Dim caminhocompleto
    Dim Linha       As Long
    Dim linhaatual  As String
    Dim sSYSTEM     As String
    Dim sSYSTEMx86  As String
    Dim bExcel64    As Boolean
    Dim bWin64      As Boolean
    Dim bTemPDFToText  As Boolean
    Dim FilePathConversorPDF As String
    
    Dim iContadorEspera As Integer
    Dim iMaxWait As Integer
    
    'Process
    
    FilePathConversorPDF = Get_PDF_To_Text_Exec_Path 'Define caminho do executavel de convers�o
    iMaxWait = 10 'Tempo maximo de espera para nova tentativa
    ConversionFormat = Get_Conversion_Format(UCase(ConversionType))
    
    If Not File_Exists(FilePathConversorPDF) Then
        Call Create_File_PDF_To_Text
    End If
    
    If gobjFSO Is Nothing Then
        Set gobjFSO = New Scripting.FileSystemObject
    End If
    
    Set objFile = gobjFSO.GetFile(fileName)
    FileNamePDF = UCase(objFile.Path)
    FileNameTXT = Replace(FileNamePDF, ".PDF", ".TXT")

    Set objShell = CreateObject("WScript.Shell") 'Set objShell = New Shell32.Shell
    
    If UCase(Get_File_Extension(FileNamePDF)) = "PDF" Then

CreateFileTXT:
        'caminhocompleto = ThisWorkbook.Path & "\pdftotext.exe " & formato & " """ & UCase(objFile.Path) & """ """ & arquivotxt & """"
        caminhocompleto = "CMD /c " & ASPAS_DUPLAS & ASPAS_DUPLAS & FilePathConversorPDF & ASPAS_DUPLAS & " " & ConversionFormat & " """ & FileNamePDF & ASPAS_DUPLAS & " " & ASPAS_DUPLAS & FileNameTXT & ASPAS_DUPLAS & ASPAS_DUPLAS
        
        'Executa convers�o
        Call objShell.Run(caminhocompleto, 0, False)
        
        iContadorEspera = 0
        Do While (gobjFSO.FileExists(FileNameTXT) = False And iContadorEspera <= iMaxWait)
            
            Application.StatusBar = "Convertendo arquivo Pdf, aguardando... " & iContadorEspera
            Application.Wait (Now + TimeValue("0:00:01"))
            iContadorEspera = iContadorEspera + 1
            
        Loop
        
        Application.StatusBar = False
        
        If gobjFSO.FileExists(FileNameTXT) = False Then
            If MInterface.Message_Confirm("Atingido o tempo maximo de espera! " & iMaxWait & " Segundo(s)" & vbNewLine & "O arquivo: '" & arquivotxt & "'." & vbNewLine & "N�o foi localizado!" & vbNewLine & "Deseja tentar novamente?", "Converter Pdf em TXT") = vbYes Then
                iMaxWait = iMaxWait + 10
                GoTo CreateFileTXT
            Else
                
                Call MInterface.Message_Error("Falha ao converter arquivo PDF!" & vbNewLine & "Tente novamente mais tarde!" & vbNewLine & "Se o problema persistir, entre em contato com o suporte!", SUB_NAME)
                Call err.Raise(999, , "Falha ao converter arquivo PDF!")
                
            End If
        End If
        
    End If
    
    If CanPileUp Then
        Call MStructure.Generate_Worksheet_From_Model(wksData, False, "Model_Import_PDF")
        Call Importar_Arquivo_TXT(wksData.Range("A" & Get_Last_Row(wksData, 1) + 1), FileNameTXT, False)
        
    Else
        Call MStructure.Generate_Worksheet_From_Model(wksData, True, "Model_Import_PDF")
        Call Importar_Arquivo_TXT(wksData.Range("A1").Offset(1), FileNameTXT, False)
    End If
    Process_File_PDF = True
    
    Set gobjFSO = Nothing
    Set objShell = Nothing
    Set RodaPDF = Nothing
    

End Function

Sub Importar_Arquivo_TXT(rngDestino As Range, CaminhoArquivo As String, Optional Maiuscula As Boolean = True)
    
    Dim objTextFile     As Object
    Dim ULWS            As Long
    Dim Registro        As String
    
    
    If gobjFSO Is Nothing Then
        Set gobjFSO = New Scripting.FileSystemObject
    End If
    
    '#### COPIA A INFORMACOES PARA A PLANILHA
    Set objTextFile = gobjFSO.OpenTextFile(CaminhoArquivo, ForReading)
    
    Dim i   As Long
    i = 0
    Do Until objTextFile.AtEndOfStream
        
        Registro = objTextFile.ReadLine
        
        If Maiuscula Then
            Registro = UCase(Registro)
        End If
        
        Registro = Replace(Registro, Chr(15), "")
        
        If Trim(Registro) <> vbNullString Then
            'rngDestino.Offset(i).Value = CStr(IIf(Left(Registro, 1) = "=", "'", "") & CStr(Registro))
            rngDestino.Offset(i).Resize(, 2).Value = Array(CStr(IIf(Left(Registro, 1) = "=", "'", "") & CStr(Registro)), CaminhoArquivo)
            i = i + 1
        End If
        
    Loop
    
    Set objTextFile = Nothing
    
End Sub



