Attribute VB_Name = "MSupplierControl"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MSupplierControl"

Sub Consultar_Parcelas()
    Dim rngParcelas As Range
    Dim OldLeaseNo As String
    Dim wksData As Worksheet
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Controle")
    Set rngParcelas = wksData.Range("A1").CurrentRegion
    Dim rngConsultaEquipamento As Range
    Dim rngConsultaServico As Range
    
    Debug.Print rngParcelas.Address
    OldLeaseNo = MStructure.Get_Model_Range("Consulta_Contratos_Old_Lease_No").Text
    Set rngConsultaEquipamento = MStructure.Get_Model_Range("Consulta_Contratos_Parcelas_Equipamentos")
    Set rngConsultaServico = MStructure.Get_Model_Range("Consulta_Contratos_Parcelas_Servicos")
    
    rngParcelas.AutoFilter
    rngParcelas.AutoFilter Field:=1, Criteria1:=OldLeaseNo
    rngParcelas.AutoFilter Field:=3, Criteria1:="Equipamento"
    rngConsultaEquipamento.CurrentRegion.Clear
    rngParcelas.SpecialCells(xlCellTypeVisible).Copy
    rngConsultaEquipamento.PasteSpecial xlPasteAll
    rngConsultaEquipamento.PasteSpecial xlPasteValues
    
    
    rngParcelas.AutoFilter Field:=3, Criteria1:="Servi�os"
    rngConsultaServico.CurrentRegion.Clear
    rngParcelas.SpecialCells(xlCellTypeVisible).Copy
    rngConsultaServico.PasteSpecial xlPasteAll
    rngConsultaServico.PasteSpecial xlPasteValues
    
    rngParcelas.AutoFilter
    
End Sub
Sub Verificar_Pagamentos_Click()
    Dim rngParcelas As Range
    Dim OldLeaseNo As String
    Dim DiasPagar As Variant
    ThisWorkbook.RefreshAll
    Dim wksData As Worksheet
    Dim wksParcelas As Worksheet
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Controle")
    Set rngParcelas = wksData.Range("A1").CurrentRegion
    Dim rngParcelasParaPagar As Range
    
    OldLeaseNo = MStructure.Get_Model_Range("Consulta_Contratos_Old_Lease_No").Text
    DiasPagar = MStructure.Get_Model_Range("Globals_Time_To_Pay").Text
    
    rngParcelas.AutoFilter
    rngParcelas.AutoFilter Field:=1, Criteria1:=OldLeaseNo
    rngParcelas.AutoFilter Field:=10, Criteria1:="<=" & DiasPagar
    
    Set wksParcelas = MStructure.Get_Worksheet_By_Codename("wksData_Parcelas_Pagar")
    wksParcelas.Cells.Clear
    rngParcelas.SpecialCells(xlCellTypeVisible).Copy
    wksParcelas.Range("A1").PasteSpecial xlPasteFormats
    wksParcelas.Range("A1").PasteSpecial xlPasteValues
    wksParcelas.Cells.Columns.AutoFit
    wksParcelas.Activate
    rngParcelas.AutoFilter
'Dim range1 as Range, range2 as Range, multipleRangeAs Range
'Set range1 = Sheets("Sheet1").Range("A1:B4000")
'Set range2 = Sheets("Sheet1").Range("F1:F4000")
'Set multipleRange = Union(range1, range2)
End Sub
Sub Verificar_Pagamentos_Enxuto_Click()
    Dim rngParcelas As Range
    Dim OldLeaseNo As String
    Dim DiasPagar As Variant
    Dim Rows As Long
    Dim rngParcelasParaPagar As Range
    Dim wksData As Worksheet
    Dim wksParcelas As Worksheet
    
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Controle")
    ThisWorkbook.RefreshAll
    Set rngParcelas = wksData.Range("A1").CurrentRegion
    
    
    OldLeaseNo = MStructure.Get_Model_Range("Consulta_Contratos_Old_Lease_No").Text
    DiasPagar = MStructure.Get_Model_Range("Globals_Time_To_Pay").Text
    
    rngParcelas.AutoFilter
    rngParcelas.AutoFilter Field:=1, Criteria1:=OldLeaseNo
    rngParcelas.AutoFilter Field:=10, Criteria1:="<=" & DiasPagar
    
    Rows = rngParcelas.CurrentRegion.Rows.Count
    Set rngParcelasParaPagar = Union(rngParcelas.Range("A1").Offset(, 0).Resize(Rows), rngParcelas.Range("A1").Offset(, 3).Resize(Rows))
    Set rngParcelasParaPagar = Union(rngParcelasParaPagar, rngParcelas.Range("A1").Offset(, 4).Resize(Rows))
    
    Set wksParcelas = MStructure.Get_Worksheet_By_Codename("wksData_Parcelas_Pagar")
    wksParcelas.Cells.Clear
    'rngParcelas.SpecialCells(xlCellTypeVisible).Copy
    rngParcelasParaPagar.SpecialCells(xlCellTypeVisible).Copy
    wksParcelas.Range("A1").PasteSpecial xlPasteFormats
    wksParcelas.Range("A1").PasteSpecial xlPasteValues
    wksParcelas.Cells.Columns.AutoFit
    wksParcelas.Activate
    rngParcelas.AutoFilter
    
End Sub
Sub Buscar_Contrato()
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Dim rngConsulta_OldLeaseNo As Range
    Dim rngConsulta_Equipamento As Range
    Dim rngConsulta_Servico As Range
    
    Dim OldLeaseNo As String
    Dim LeaseScheduleNo As String
    Dim Tipo As String
    Dim aData() As Variant
    Dim aData_Equipamento() As Variant
    Dim aData_Servico() As Variant
    Dim ColumnFilter_OldLeaseNo As Long
    
    Set wksData = MStructure.Get_Worksheet_By_Codename("wksData_Controle")
    Set wksReport = MStructure.Get_Worksheet_By_Codename("wksReport_Consulta_Contrato")
    
    Set rngConsulta_OldLeaseNo = MStructure.Get_Model_Range("Consulta_Contratos_Old_Lease_No")
    Set rngConsulta_Equipamento = MStructure.Get_Model_Range("Consulta_Contratos_Parcelas_Equipamentos")
    Set rngConsulta_Servico = MStructure.Get_Model_Range("Consulta_Contratos_Parcelas_Servicos")
    
    aData = MStructure.Get_Current_Region(wksData.Range("A1"))
    ColumnFilter_OldLeaseNo = MStructure.Get_Column_Number(aData, "Old Lease No")
    
    OldLeaseNo = rngConsulta_OldLeaseNo.Text
    aData = Filter_Array(aData, ColumnFilter_OldLeaseNo, OldLeaseNo)
    
    
End Sub
Sub Limpar_Contrato()

End Sub
Sub Salvar_Contrato()

End Sub
Sub Atualizar_Parcelas_Servico()

End Sub
