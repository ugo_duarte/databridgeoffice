Attribute VB_Name = "TestModule"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MTestModule"

'@TestModule
'@Folder("Tests")

Private Assert As Object
Private Fakes As Object

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
    Set Fakes = CreateObject("Rubberduck.FakesProvider")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
    Set Fakes = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
End Sub

'@TestMethod("Structure")
Private Sub Test_Add_Percentage_To_String()                        'TODO Rename test
    On Error GoTo TestFail
    
    'Arrange:
    
    'Act:

    'Assert:
    Assert.Succeed

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & err.Number & " - " & err.Description
    Resume TestExit
End Sub

Sub T_DataLink()
    Dim wks As Worksheet
    Dim key As Variant
    Set wks = MStructure.Get_Worksheet_By_Codename("wksData_CHGBrazil_Projects")
    
    For Each key In wks.ListObjects
        Debug.Print key
    Next key
    
    
End Sub
