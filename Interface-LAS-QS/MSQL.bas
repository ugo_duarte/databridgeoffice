Attribute VB_Name = "MSQL"
Option Explicit
Option Private Module
Const MODULE_NAME = "MSQL"

Enum enmConnectionType
    enmConnectionType_MyWorkbook
    enmConnectionType_Access
End Enum

Global Const ACCESS_DATABASE_PATH       As String = "C:\Users\udu\Documents\CHG-Meridian\SourceCode\DBCHGBrazil.accdb"
Global Const ACCESS_DATABASE_PATH_CLOUD As String = "\\chg-meridian.com\Data\brazil\TI\Projetos\CHGBrazil\DBCHGBrazil.accdb"
Global Const ACCESS_INSERT_STATEMENT    As String = " INSERT INTO @TABLE (@HEADERS) VALUES(@VALUES) "

Sub CreateAndRunQuery()

    'Declaring the necessary variables.
    Dim con As ADODB.Connection
    Dim rs As Object
    Dim AccessFile As String
    Dim strTable As String
    Dim SQL As String
    Dim i As Integer
    Dim wksTemp As Worksheet

    Call MGlobal.Global_Initialize
    
    Set wksTemp = MStructure.Get_Worksheet_By_Codename("wksTemporary")
    
    'Set the name of the table you want to retrieve the data.
    strTable = "TableTeste"

    On Error Resume Next
    'Create the ADODB connection object.
    Set con = CreateObject("ADODB.connection")
    'Check if the object was created.
    If err.Number <> 0 Then
        MsgBox "Connection was not created!", vbCritical, "Connection Error"
        Exit Sub
    End If
    On Error GoTo 0

    'Open the connection.
    con.Open "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ACCESS_DATABASE_PATH

    'Create the SQL statement to retrieve the data from the table.
    'Get the necessary information (first name etc.) for all the Canadian customers.
    'SQL = "SELECT Description, LastName, Address, City, Phone FROM " & strTable & " WHERE COUNTRY='Canada'"
    SQL = "SELECT Description FROM " & strTable

    On Error Resume Next
    'Create the ADODB recordset object.
    Set rs = CreateObject("ADODB.Recordset")
    'Check if the object was created.
    If err.Number <> 0 Then
        'Error! Release the objects and exit.
        Set rs = Nothing
        Set con = Nothing
        'Display an error message to the user.
        MsgBox "Recordset was not created!", vbCritical, "Recordset Error"
        Exit Sub
    End If
    On Error GoTo 0

    'Set thee cursor location.
    rs.CursorLocation = 3 'adUseClient on early  binding
    rs.CursorType = 1 'adOpenKeyset on early  binding

    'Open the recordset.
    rs.Open SQL, con

    'Check if the recordset is empty.
    If rs.EOF And rs.BOF Then
        'Close the recordset and the connection.
        rs.Close
        con.Close
        'Release the objects.
        Set rs = Nothing
        Set con = Nothing
        'Enable the screen.
        Application.ScreenUpdating = True
        'In case of an empty recordset display an error.
        MsgBox "There are no records in the recordset!", vbCritical, "No Records"
        Exit Sub
    End If

    'Copy the recordset headers.
    For i = 0 To rs.Fields.Count - 1
        wksTemp.Cells(1, i + 1) = rs.Fields(i).Name
    Next i

    'Write the query values in the sheet.
    wksTemp.Range("A2").CopyFromRecordset rs

    'Close the recordset and the connection.
    rs.Close
    con.Close
    
    'Release the objects.
    Set rs = Nothing
    Set con = Nothing
    
    'Adjust the columns' width.
    wksTemp.Columns.AutoFit

    'Enable the screen.
    Application.ScreenUpdating = True

    'Inform the user that the macro was executed successfully.
    Call MInterface.Message_Information(MSG_SUCCESS)

End Sub
Sub Test_Insert_To_Access_Deal_Details()
    Dim SQL         As String
    Dim strTable    As String
    Dim aHeader() As Variant
    Dim aData() As Variant

    strTable = "DealDetails"

    aHeader = MStructure.Get_Model_Range("Model_DMS_FollowUp_EmittedDeals_Access")
    aData = MStructure.Get_Model_Range("Model_DMS_FollowUp_EmittedDeals_Access")

    Call MGlobal.Global_Initialize
    Call Insert_To_Access(strTable, aHeader, aData, aData)
    Call MGlobal.Global_Finalize
End Sub
Function Get_String_Headers(aHeader() As Variant, Optional HeaderRow As Long = 1) As String
    Dim i As Long
    Dim j As Long
    Dim Headers As String
    Get_String_Headers = vbNullString
    
    For j = LBound(aHeader, 2) To UBound(aHeader, 2)
        If Get_String_Headers <> vbNullString Then
            Get_String_Headers = Get_String_Headers & ", [" & CStr(aHeader(HeaderRow, j)) & "]"
        Else
            Get_String_Headers = "[" & CStr(aHeader(HeaderRow, j)) & "]"
        End If
        
    Next j
    
End Function
Function Get_String_Values(aHeader() As Variant, aValues() As Variant, Optional ValueRow As Long = 1) As String
    Dim i As Long
    Dim j As Long
    Dim jHeader As Long
    Dim Headers As String
    Dim CurrentValue As Variant
    
    Get_String_Values = vbNullString
    
    
    For jHeader = LBound(aHeader, 2) To UBound(aHeader, 2)
        CurrentValue = aHeader(1, jHeader)

        For j = LBound(aValues, 2) To UBound(aValues, 2)
            If aHeader(1, jHeader) = aValues(1, j) Then
                CurrentValue = aValues(ValueRow, j)
                Exit For
            End If
        Next j
        
        If Get_String_Values <> vbNullString Then
            Get_String_Values = Get_String_Values & ", '" & CStr(CurrentValue) & "'"
        Else
            Get_String_Values = "'" & CStr(CurrentValue) & "'"
        End If
        
    Next jHeader
    
End Function
Function Get_String_Values_DataType(aHeader() As Variant, aValues() As Variant, aDataType() As Variant, Optional ValueRow As Long = 1) As String
    Dim i As Long
    Dim j As Long
    Dim jHeader As Long
    Dim Headers As String
    Dim CurrentValue As Variant
    Dim CurrentHeader As Variant
    
    Get_String_Values_DataType = vbNullString
    
    
    For jHeader = LBound(aHeader, 2) To UBound(aHeader, 2)
        
        CurrentHeader = aHeader(1, jHeader)

        For j = LBound(aValues, 2) To UBound(aValues, 2)
            If CurrentHeader = aValues(1, j) Then
                Select Case UCase(aDataType(1, jHeader))
                    Case "ID"
                        
                    Case "NUMBER"
                        CurrentValue = MStructure.Convert_Number(aValues(ValueRow, j), "US")
                    Case "TEXT"
                        If UCase(aHeader(1, jHeader)) = "USERCHG" Then
                            aValues(ValueRow, j) = Replace(aValues(ValueRow, j), ",", "")
                        End If
                        aValues(ValueRow, j) = Replace(aValues(ValueRow, j), "'", "''")
                        CurrentValue = "'" & Trim(CStr(aValues(ValueRow, j))) & "'"
                    Case "BOOLEAN"
                        If MRegex.Regx_Match(aValues(ValueRow, j), "YES|1|SIM") Then
                            CurrentValue = 1
                        Else
                            CurrentValue = 0
                        End If
                End Select
                Exit For
            End If
        Next j
        
        If Get_String_Values_DataType <> vbNullString Then
            Get_String_Values_DataType = Get_String_Values_DataType & ", " & CStr(CurrentValue)
        Else
            Get_String_Values_DataType = CStr(CurrentValue)
        End If
        
    Next jHeader
    
End Function

Sub Insert_To_Access(TableName As String, aHeader() As Variant, aData() As Variant, aDataType() As Variant, Optional rowNumber As Long = 1)
    Const SUB_NAME As String = "Insert To Access"
    On Error GoTo ErrorHandler
    'Declaring the necessary variables.
    Dim con As ADODB.Connection
    Dim SQL As String
    Dim i As Long
    Dim Headers As String
    Dim Values As String

    Headers = Get_String_Headers(aHeader)
    Values = Get_String_Values_DataType(aHeader, aData, aDataType, rowNumber)

    Values = Replace(Values, "Emission date", Now())
    Values = Replace(Values, "User CHG", MSecurity.Get_Windows_UserName)
    Values = Replace(Values, "UserCHG", MSecurity.Get_Windows_UserName)


    SQL = ACCESS_INSERT_STATEMENT
    SQL = Replace(SQL, "@TABLE", TableName)
    SQL = Replace(SQL, "@HEADERS", Headers)
    SQL = Replace(SQL, "@VALUES", Values)
    
    
    'Create the ADODB connection object.
    Set con = New_ADODB_Connection(enmConnectionType_Access)
    con.Execute SQL
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)

Finish:
    If Not con Is Nothing Then
        con.Close
        Set con = Nothing
    End If

    Exit Sub

ErrorHandler:
    Set con = Nothing
    If InStr(1, err.Description, "create duplicate values in the index") > 0 Then
        Call MInterface.Message_Error("Deal j� foi confirmado! Atualize a base!")
    Else
        Call MInterface.Message_Error(err.Number & vbNewLine & err.Description)

    End If

    GoTo Finish

End Sub
Sub Delete_From_Access(SQL As String)
    Dim con As ADODB.Connection
     'Create the ADODB connection object.
    Set con = New_ADODB_Connection(enmConnectionType_Access)
    
    con.Execute (SQL)
    Set con = Nothing
End Sub

Sub DMS_Dealprintout_Insert_To_Access(aHeader() As Variant, aData() As Variant, Optional iRow As Long = 0, Optional ShowMessage As Boolean = False)
    Const SUB_NAME As String = "DMS Dealprintout Insert To Access via Recordset"
    On Error GoTo ErrorHandler

    Dim con As ADODB.Connection
    Dim rs As ADODB.Recordset
    Dim SQL As String
    Dim i As Long
    Dim j As Long
    Dim jData As Long
    Dim Header As String
    Dim Value As String
    Dim ColumnDeal As Long
    
    'Create the ADODB connection object.
    Set con = New_ADODB_Connection(enmConnectionType_Access)
    SQL = "Select * from [DMS_DealPrintout]"
    
    Set rs = New ADODB.Recordset
    rs.CursorType = adOpenDynamic
    rs.LockType = adLockOptimistic

    Call rs.Open(SQL, con)
    ColumnDeal = Get_Column_Number(aHeader, "Deal")
    For i = LBound(aData) + 1 To UBound(aData)
        rs.Find ("[Deal] = " & aData(i, ColumnDeal))

        If rs.EOF Then
            rs.AddNew
        End If

        For j = LBound(aHeader, 2) To UBound(aHeader, 2)
            Header = aHeader(1, j)
            jData = MStructure.Get_Column_Number(aData, Header)
            If jData > 0 Then
                Debug.Print rs(aHeader(1, j)).Type
                Select Case rs(aHeader(1, j)).Type
                    Case DataTypeEnum.adCurrency
                        aData(i, jData) = CCur(aData(i, jData))
                    Case DataTypeEnum.adDecimal
                        aData(i, jData) = CDbl(aData(i, jData))
                    Case DataTypeEnum.adBoolean
                        aData(i, jData) = Convert_Boolean(aData(i, jData))

                End Select
                rs(aHeader(1, j)) = aData(i, jData)
            End If
        Next j
        rs.Update
nextI:
    Next i

    If ShowMessage Then
        Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    End If

Fim:
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not con Is Nothing Then
        con.Close
        Set con = Nothing
    End If

    Exit Sub

ErrorHandler:
    If InStr(1, err.Description, "create duplicate values in the index") > 0 Then
        Resume Next
    Else
        Call MInterface.Message_Error(err.Number & vbNewLine & err.Description)
        Call err.Raise(err.Number, err.Source, err.Description)
    End If
    
    Set con = Nothing
    Set rs = Nothing
    
    GoTo Fim
End Sub
Sub Insert_To_Access_Recordset(TableName As String, aHeader() As Variant, aData() As Variant, Optional iRow As Long = 0, Optional ShowMessage As Boolean = False)
    Const SUB_NAME As String = "Insert To Access via Recordset"
    On Error GoTo ErrorHandler

    Dim con As ADODB.Connection
    Dim rs As ADODB.Recordset
    Dim SQL As String
    Dim i As Long
    Dim j As Long
    Dim jData As Long
    Dim Header As String
    Dim Value As String
    
    'Create the ADODB connection object.
    Set con = New_ADODB_Connection(enmConnectionType_Access)
    SQL = "Select * from " & TableName
    
    Set rs = New ADODB.Recordset
    rs.CursorType = adOpenDynamic
    rs.LockType = adLockOptimistic

    Call rs.Open(SQL, con)

    If iRow > 0 Then
        rs.AddNew
        For j = LBound(aHeader, 2) To UBound(aHeader, 2)
            Header = aHeader(1, j)
            jData = MStructure.Get_Column_Number(aData, Header)
            If jData > 0 Then
                rs(aHeader(1, j)) = aData(iRow, jData)
            End If
        Next j
        rs.Update
    Else

        For i = LBound(aData) + 1 To UBound(aData)
            rs.AddNew
            For j = LBound(aHeader, 2) To UBound(aHeader, 2)
                Header = aHeader(1, j)
                jData = MStructure.Get_Column_Number(aData, Header)
                If jData > 0 Then
                    Select Case rs(aHeader(1, j)).Type
                        Case DataTypeEnum.adCurrency
                            aData(i, jData) = CCur(aData(i, jData))
                        Case DataTypeEnum.adDecimal
                            aData(i, jData) = CDbl(aData(i, jData))
                        Case DataTypeEnum.adBoolean
                            aData(i, jData) = Convert_Boolean(aData(i, jData))

                    End Select
                    rs(aHeader(1, j)) = aData(i, jData)
                End If
            Next j
            rs.Update
nextI:
        Next i
    End If

    If ShowMessage Then
        Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    End If

Fim:
    If Not rs Is Nothing Then
        rs.Close
        Set rs = Nothing
    End If
    If Not con Is Nothing Then
        con.Close
        Set con = Nothing
    End If

    Exit Sub

ErrorHandler:
    If InStr(1, err.Description, "create duplicate values in the index") > 0 Then
        Resume Next
    Else
        Call MInterface.Message_Error(err.Number & vbNewLine & err.Description)
        Call err.Raise(err.Number, err.Source, err.Description)
    End If
    
    Set con = Nothing
    Set rs = Nothing
    
    GoTo Fim
End Sub
Sub Generate_Data_SQL(enmConnection As enmConnectionType, QueryString As String, wksDestination As Worksheet, _
                      Optional GenerateHeader As Boolean = False, Optional RangeDestination As String = "A2", Optional RangeDestinationHeader As String = "A1")
    Const SUB_NAME As String = "Generate_Data_SQL"
    On Error GoTo ErrorHandler

    '------------------------------------------------------'
    'Fun��o gen�rica para gerar dados atrav�s de uma query '
    '------------------------------------------------------'


    Dim conExcel As New ADODB.Connection
    Dim rstExcel As New ADODB.Recordset
    Dim ConnectionString As String
    Dim i As Long
    Dim aHeader() As Variant

    'Connection
    ConnectionString = Get_SQL_ConnectionString(enmConnection)
    If ConnectionString = vbNullString Then GoTo Finish
    conExcel.ConnectionString = ConnectionString
    conExcel.Open

    'Execute
    rstExcel.Open QueryString, conExcel

    If Not rstExcel.EOF Then
        rstExcel.MoveFirst

        'Header
        If GenerateHeader Then
            
            ReDim aHeader(1 To 1, 1 To rstExcel.Fields.Count)
            For i = 0 To rstExcel.Fields.Count - 1
                aHeader(1, i + 1) = rstExcel.Fields.Item(i).Name
            Next i
            wksDestination.Range(RangeDestinationHeader).Resize(, UBound(aHeader, 2)) = aHeader
        End If

        'Data
        wksDestination.Range(RangeDestination).CopyFromRecordset rstExcel
        wksDestination.Columns.AutoFit
    End If


    rstExcel.Close
    conExcel.Close

Finish:
    On Error GoTo 0
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & " - " & err.Description, SUB_NAME)
    GoTo Finish

End Sub
Function Get_Header_From_Recordset(rstExcel As ADODB.Recordset) As Variant
    Dim aHeader() As Variant
    Dim i As Long
    ReDim aHeader(1 To 1, 1 To rstExcel.Fields.Count)
    For i = 0 To rstExcel.Fields.Count - 1
        aHeader(1, i + 1) = rstExcel.Fields.Item(i).Name
    Next i
End Function
Sub Generate_Data_XML_NFE()
    '------------------------------------------------------'
    'Fun��o para gerar os dados importados na aba "XML"    '
    '------------------------------------------------------'
    'On Error GoTo ErrorHandler
    Dim strSQL As String
    Dim wks As Worksheet
    Dim wksReport As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksTemporary")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE")
    
    
    'Query
    Application.StatusBar = "Generating Data via SQL"

    strSQL = vbNullString
    strSQL = strSQL & " SELECT  DISTINCT *                      " & vbNewLine
    strSQL = strSQL & "   FROM [@SHEET_NAME$]  As DX          " & vbNewLine
    strSQL = strSQL & "  WHERE (DX.[NFE CHAVE NOTA] IS NOT NULL AND DX.[NFE CHAVE NOTA] <> '')  " & vbNewLine
    strSQL = strSQL & "  ORDER BY DX.[NFE CHAVE NOTA]           " & vbNewLine

    'Data Source
    strSQL = Replace(strSQL, "@SHEET_NAME", wks.Name)

    'Execution
    Call Generate_Data_SQL(enmConnectionType_MyWorkbook, strSQL, wksReport, True)

Finish:
    On Error GoTo 0
    Exit Sub

ErrorHandler:
    Call MsgBox(err.Description, vbCritical, MSG_COMPANY)
    GoTo Finish
    
End Sub
Sub Select_From_Worksheet(originCodename As String, destinationCodename As String)
    '------------------------------------------------------'
    'Fun��o para gerar os dados importados na aba "XML"    '
    '------------------------------------------------------'
    'On Error GoTo ErrorHandler
    Dim strSQL As String
    Dim wksOrigin As Worksheet
    Dim wksDestination As Worksheet
    
    Set wksOrigin = Get_Worksheet_By_Codename(originCodename)
    Set wksDestination = Get_Worksheet_By_Codename(destinationCodename)
    
    
    'Query
    Application.StatusBar = "Generating Data via SQL"

    strSQL = vbNullString
    strSQL = strSQL & " SELECT  DISTINCT *            " & vbNewLine
    strSQL = strSQL & "   FROM [@SHEET_NAME$]  As DX  " & vbNewLine
    strSQL = strSQL & "  WHERE (DX.[NFE CHAVE NOTA] IS NOT NULL AND DX.[NFE CHAVE NOTA] <> '')  " & vbNewLine
    strSQL = strSQL & "  ORDER BY DX.[NFE CHAVE NOTA]           " & vbNewLine

    'Data Source
    strSQL = Replace(strSQL, "@SHEET_NAME", wksOrigin.Name)

    'Execution
    Call Generate_Data_SQL(enmConnectionType_MyWorkbook, strSQL, wksDestination, True)

Finish:
    On Error GoTo 0
    Exit Sub

ErrorHandler:
    Call MsgBox(err.Description, vbCritical, MSG_COMPANY)
    GoTo Finish
    
End Sub
Function Get_SQL_ConnectionString(enmConnection As enmConnectionType) As String
    
    Select Case enmConnection
        Case enmConnectionType_MyWorkbook
            Get_SQL_ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ThisWorkbook.FullName & ";Extended Properties=""Excel 12.0 Macro;HDR=YES"";"
            
        Case enmConnectionType_Access
            
            If Check_Master_User_Computer_Name Then
                Call MAccess.Change_Access_Database(ACCESS_DATABASE_PATH_CLOUD, ACCESS_DATABASE_PATH)
                Get_SQL_ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ACCESS_DATABASE_PATH
            Else
                Call MAccess.Change_Access_Database(ACCESS_DATABASE_PATH, ACCESS_DATABASE_PATH_CLOUD)
                Get_SQL_ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" & ACCESS_DATABASE_PATH_CLOUD
            End If
            
    End Select
    
End Function
Function New_ADODB_Connection(ConnType As enmConnectionType) As ADODB.Connection
    Dim conExcel            As ADODB.Connection
    Dim ConnectionString    As String
    
    Set conExcel = New ADODB.Connection
    ConnectionString = Get_SQL_ConnectionString(ConnType)
    conExcel.ConnectionString = ConnectionString
    conExcel.Open
    
    Set New_ADODB_Connection = conExcel
    
End Function
