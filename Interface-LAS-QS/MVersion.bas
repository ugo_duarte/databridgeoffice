Attribute VB_Name = "MVersion"
Option Private Module
Const MODULE_NAME As String = "MVersion"

Global Const PROJECT_CONTRACT_INTEGRITY As String = "Contract Integrity"
Global Const PROJECT_DEAL_DETAILS As String = "Deal Details and Asset Forecast"
Global Const PROJECT_DMS_DEALS_FOLLOW_UP As String = "DMS Deal Follow Up"
Global Const PROJECT_LAS_QS_ASSETS As String = "Interface LAS-QS Assets"
Global Const PROJECT_LAS_QS_SCHEDULES As String = "Interface LAS-QS Schedules"
Global Const PROJECT_PIPELINE As String = "Pipeline"
Global Const PROJECT_RH_BIRTHDAYS As String = "RH Birthdays"
Global Const PROJECT_EMAIL_LIST As String = "EMail-List"
Global Const PROJECT_LAS_CASHFLOW As String = "Interface LAS-Cashflow"
Global Const PROJECT_XML_LAS_ASSETS As String = "Interface XML-LAS Assets"
Global Const PROJECT_XML_LAS_PUCHASES As String = "LAS Add Purchases"
Global Const PROJECT_REPORT_UPDATE As String = "Report Update"
Global Const PROJECT_RPA_INVOICES_USA As String = "RPA Invoices USA"

'   /$$    /$$                              /$$
'  | $$   | $$                             |__/
'  | $$   | $$ /$$$$$$   /$$$$$$   /$$$$$$$ /$$  /$$$$$$  /$$$$$$$
'  |  $$ / $$//$$__  $$ /$$__  $$ /$$_____/| $$ /$$__  $$| $$__  $$
'   \  $$ $$/| $$$$$$$$| $$  \__/|  $$$$$$ | $$| $$  \ $$| $$  \ $$
'    \  $$$/ | $$_____/| $$       \____  $$| $$| $$  | $$| $$  | $$
'     \  $/  |  $$$$$$$| $$       /$$$$$$$/| $$|  $$$$$$/| $$  | $$
'      \_/    \_______/|__/      |_______/ |__/ \______/ |__/  |__/

'   - Last Update: 29-September-2022 09:01:51

'
'   - CHG Template '   - Last Update: 31-August-2021 09:31:10
'       v1.01
'
'   - Contract Integrity
'       v1.03 '   - Last Update: 27-October-2021 10:37:25
'
'   - Controle Nestle
'       v1.03 '   - Last Update: 30-August-2021 12:05:57
'
'   - DMS Deal Details & Asset Forecast
'       v1.41 '   - Last Update: 16-September-2021 12:32:56
'
'   - DMS Deals Follow Up
'       v1.12 '   - Last Update: 22-September-2021 11:48:22
'
'   - EMail-List
'       v1.01 '   - Last Update: 21-October-2021 10:18:50
'
'   - Interface LAS-QS Assets
'       v1.07 '   - Last Update: 20-January-2022 10:39:31
'
'   - Interface LAS-QS Schedules
'       v1.17 '   - Last Update: 30-September-2021 14:30:20
'
'   - Interface LAS-Cashflow
'       v1.04 '   - Last Update: 02-February-2022 09:53:26
'
'   - Interface XML-LAS Assets
'       v1.30 '   - Last Update: 22-December-2021 10:45:33
'
'   - Interface XML-LAS Purchases
'       v1.15 '   - Last Update: 17-December-2021 11:23:06
'
'   - Report Update
'       v1.03 '   - Last Update: 20-January-2022 11:38:27
'
'   - Pipeline
'       v1.05 '   - Last Update: 27-September-2021 09:17:57
'
'   - RPA Invoices USA
'       v1.16 '   - Last Update: 29-September-2022 09:01:51
Sub Print_Version_Control()
    Debug.Print "'   - Last Update: " & Format(Now, "dd-mmmm-yyyy hh:mm:ss")
End Sub

