VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmUpdateReport 
   Caption         =   "Update Report"
   ClientHeight    =   5800
   ClientLeft      =   110
   ClientTop       =   450
   ClientWidth     =   11140
   OleObjectBlob   =   "frmUpdateReport.frx":0000
End
Attribute VB_Name = "frmUpdateReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub lbUpdateReport_Click()
    Dim i As Long
    Dim Table As String
    For i = 0 To listboxReports.ListCount - 1
        If listboxReports.Selected(i) = True Then
            Table = listboxReports.List(i)
        End If
    Next i
    
    If Table <> vbNullString Then
        Call MGlobal.Global_Initialize
        Call Access_Delete_And_Upload_Data(Table)
        Call MGlobal.Global_Finalize
    End If
    
End Sub

Private Sub UserForm_Initialize()
    Me.Height = 250
    Me.Width = 600
    
    Me.StartUpPosition = 0
    
    Me.Top = Application.Top + (Application.UsableHeight / 2) - (Me.Height / 2)
    Me.Left = Application.Left + (Application.UsableWidth / 2) - (Me.Width / 2)
    
    
    Dim wks As Worksheet
    Set wks = MStructure.Get_Worksheet_By_Codename("wksData_CHGBrazil_Tables")
    
    Me.listboxReports.Top = 30
    Call LoadListbox(wks, "CHGBrazil_Tables")
    Me.lbUpdateReport.Top = Me.listboxReportsHeader.Top
    Me.lbUpdateReport.Left = (Me.listboxReports.Width + Me.listboxReports.Left) + 20
    
    
End Sub
Private Sub LoadListbox(wks As Worksheet, ListObjectName As String)
    Dim objList As ListObject
    
    Me.listboxReports.Clear 'First clear existing list
    Set objList = wks.ListObjects(ListObjectName)
    Me.listboxReports.List = objList.ListColumns("Table").DataBodyRange.Value
    Call CreateListBoxHeader(listboxReports, listboxReportsHeader, Array("Tables"))
'    With Me.listboxReports
'        While rs.EOF = False
'            .AddItem rs.Fields(0).Value
'            rs.MoveNext
'        Wend
'    End With
    
End Sub

Public Sub CreateListBoxHeader(body As MSForms.ListBox, header As MSForms.ListBox, arrHeaders)
        ' make column count match
        header.ColumnCount = body.ColumnCount
        header.ColumnWidths = body.ColumnWidths

        ' add header elements
        header.Clear
        header.AddItem
        Dim i As Integer
        For i = 0 To UBound(arrHeaders)
            header.List(0, i) = arrHeaders(i)
        Next i

        ' make it pretty
        body.ZOrder (1)
        header.ZOrder (0)
        header.SpecialEffect = fmSpecialEffectFlat
        header.BackColor = RGB(200, 200, 200)
        header.Height = 15

        ' align header to body (should be done last!)
        header.Width = body.Width
        header.Left = body.Left
        header.Top = body.Top - (header.Height - 1)
End Sub
