Attribute VB_Name = "MSecurity"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MSecurity"

Global Const WORKBOOK_PASSWORD As String = "CHGMeridian"
Global Const WORKBOOK_MASTER_USER As String = "udu"

Global WindowsUsername As String
Global OfficeUsername As String
Global DesktopPath As String
Global DocumentsPath As String
Global gProjectName As String
Global gProjectVersion As String

Public Type NamedRangesControl
    Column_NamedRange       As Long
    Column_Order            As Long
    Column_Locked           As Long
    Column_Address          As Long
End Type
Sub Apply_Locks_NamedRanges()
    Const SUB_NAME As String = "Apply Locks NamedRanges"
    Call Log_Step(SUB_NAME)

    Dim aNamedRangesControl() As Variant
    Dim i As Long
    Dim NRC As NamedRangesControl
    Dim wks As Worksheet
    Dim rngModel As Range
    
    NRC = New_Type_NamedRangesControl


    'Lock ALL Cells
    Call Lock_All_Worksheets
            
    Set wks = Get_Worksheet_By_Codename("wksConfig_NamedRangesControls")

    wks.Cells.Locked = False
    aNamedRangesControl = Get_Current_Region(wks.Range("A1"))
    wks.Cells.Locked = True
    
    For i = LBound(aNamedRangesControl) + 1 To UBound(aNamedRangesControl)
        If UCase(aNamedRangesControl(i, NRC.Column_Locked)) = "FALSE" Then
            Set rngModel = Get_Model_Range(aNamedRangesControl(i, NRC.Column_NamedRange))
            rngModel.Locked = False
        End If
    Next i

End Sub
Sub Check_Cloud_Enviroment()
    Const SUB_NAME As String = "Check Cloud Enviroment"
    Call Log_Step(SUB_NAME)
    
    If Get_Windows_UserName <> WORKBOOK_MASTER_USER Then
        If Not Is_Cloud_Enviroment Then
            Call err.Raise(777, , "Not in the Cloud enviroment!" & vbNewLine & "Please login to the Cloud!")
        End If
    End If
End Sub
Function Check_Master_User_Computer_Name() As Boolean
    Const SUB_NAME As String = "Check_Master_User_Computer_Name"
    Call Log_Step(SUB_NAME)
    
    Check_Master_User_Computer_Name = False
    If Environ("ComputerName") = "CHG-046590" Then
        Check_Master_User_Computer_Name = True
    End If
End Function
Function Is_Cloud_Enviroment() As Boolean
    Const SUB_NAME As String = "Is Cloud Enviroment"
    Call Log_Step(SUB_NAME)
    
    Is_Cloud_Enviroment = False
    If gobjFSO.FolderExists(Get_CHG_Cloud_TI_Projects_Deal_Details_Path) Then
       Is_Cloud_Enviroment = True
    End If
End Function
Function Is_Master_User() As Boolean
    Const SUB_NAME As String = "Is Master User"
    Call Log_Step(SUB_NAME)

    Is_Master_User = False
    If Get_Windows_UserName = WORKBOOK_MASTER_USER Then
        Is_Master_User = True
    End If
End Function
Function Check_Excel_Template()
    Const SUB_NAME As String = "Check Excel Template"
    Call Log_Step(SUB_NAME)

    Check_Excel_Template = False
    If MRegex.Regx_Match(ThisWorkbook.Name, "\.xltm") Then
        If Get_Windows_UserName <> WORKBOOK_MASTER_USER Then
            Check_Excel_Template = True
        End If
    End If
End Function
Function Get_Office_UserName()
    Const SUB_NAME As String = "Get Office Username"
    Call Log_Step(SUB_NAME)

    OfficeUsername = Application.UserName
    Get_Office_UserName = OfficeUsername
End Function
Function Get_Project_Name() As String
    Const SUB_NAME As String = "Get Project Name"
    Call Log_Step(SUB_NAME)

    If gProjectName = "" Then
        Dim aProject() As Variant
        Dim i As Long
        aProject = MStructure.Get_Current_Region(Get_Model_Range("Project_CR"))
        For i = LBound(aProject) To UBound(aProject)
            If UCase(aProject(i, 1)) = "NAME" Then
                gProjectName = aProject(i, 2)
            ElseIf UCase(aProject(i, 1)) = "VERSION" Then
                gProjectVersion = aProject(i, 2)
            End If
        Next i
    End If
    Get_Project_Name = gProjectName
End Function
Sub Check_Project_Version_Access()
    Const SUB_NAME As String = "Check Project Version Access"
    Call Log_Step(SUB_NAME)

    'Declaring the necessary variables.

    Dim con As ADODB.Connection
    Dim rs As ADODB.Recordset
    Dim SQL As String
    Dim Item As Variant

    Set con = MSQL.New_ADODB_Connection(enmConnectionType_Access)

    SQL = "SELECT Version FROM CHGBrazil_Projects WHERE Project = '@Project'"
    SQL = Replace(SQL, "@Project", gProjectName)
    
    On Error Resume Next
    'Create the ADODB recordset object.
    Set rs = New ADODB.Recordset
    
    'Set thee cursor location.
    rs.CursorLocation = 3 'adUseClient on early  binding
    rs.CursorType = 1 'adOpenKeyset on early  binding

    'Open the recordset.
    rs.Open SQL, con
    
    If rs.EOF Then
        Call MInterface.Message_Error("Project version not found! Contact the IT Admin", SUB_NAME)
    Else
        
        Item = rs("Version")
        If Not Item = gProjectVersion Then
            Call MInterface.Message_Error("Project version is not the latest!" & vbNewLine & "This Version: " & gProjectVersion & vbNewLine & "Current Version: " & Item, SUB_NAME)
        End If
        
    End If
    
    rs.Close
    con.Close

    'Release the objects.
    Set rs = Nothing
    Set con = Nothing

End Sub
Function Get_CHG_Cloud_Desktop_Path()
    Const SUB_NAME As String = "Get CHG Cloud Desktop Path"
    Call Log_Step(SUB_NAME)

    Get_CHG_Cloud_Desktop_Path = "\\chg-meridian.com\UserProfile\" & Environ("username") & "\Desktop"
End Function
Function Get_CHG_Cloud_TI_Projects_Deal_Details_Path()
    Const SUB_NAME As String = "Get CHG Cloud TI Projects Deal Details Path"
    Call Log_Step(SUB_NAME)

    Get_CHG_Cloud_TI_Projects_Deal_Details_Path = "\\chg-meridian.com\Data\brazil\TI\Projetos\Interface LAS QS\DealDetails"
End Function
Function Get_Windows_Desktop_Path()
    Const SUB_NAME As String = "Get Windows Desktop Path"
    Call Log_Step(SUB_NAME)

    DesktopPath = Environ("USERPROFILE") & "\Desktop"
    Get_Windows_Desktop_Path = DesktopPath
End Function
Function Get_Windows_Documents_Path()
    Const SUB_NAME As String = "Get Windows Documents Path"
    Call Log_Step(SUB_NAME)

    DocumentsPath = Environ("USERPROFILE") & "\Documents"
    Get_Windows_Documents_Path = DocumentsPath
End Function
Function Get_Windows_UserName()
    Const SUB_NAME As String = "Get Windows Username"
    Call Log_Step(SUB_NAME)

    WindowsUsername = Environ("username")
    Get_Windows_UserName = WindowsUsername
End Function
Sub Hide_All_Worksheets(visibleWorksheet As Worksheet)
    Const SUB_NAME As String = "Hide All Worksheets"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    visibleWorksheet.Visible = xlSheetVisible

    For Each wks In ThisWorkbook.Worksheets

        If wks.Codename <> visibleWorksheet.Codename Then
            wks.Visible = xlSheetVeryHidden
        End If

    Next wks
End Sub
Sub Lock_All_Worksheets()
    Const SUB_NAME As String = "Lock All Worksheets"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    On Error Resume Next
    For Each wks In ThisWorkbook.Worksheets
        If Not Regx_Match(wks.Codename, "wksModel") Then
            wks.Cells.Locked = True
        End If
    Next wks
End Sub
Sub Login_Admin()
    Const SUB_NAME As String = "Admin Login"
    Call Log_Step(SUB_NAME)

    Call Show_All_Worksheets
    
    'Call Window_Controls(True)
    
    Call MsgBox("Admin!", vbInformation, SUB_NAME)
    
End Sub
Sub Login_Database()
    Const SUB_NAME As String = "Login Database"
    Call Log_Step(SUB_NAME)

    'TODO Change when open
    If Check_Master_User_Computer_Name Then
        Call MAccess.Change_Access_Database(ACCESS_DATABASE_PATH_CLOUD, ACCESS_DATABASE_PATH)
    Else
        If MSecurity.Is_Cloud_Enviroment Then
            Call MAccess.Change_Access_Database(ACCESS_DATABASE_PATH, ACCESS_DATABASE_PATH_CLOUD)
        End If
    End If
    
End Sub
Sub Login_Environ()
    Const SUB_NAME As String = "Login Environ"
    On Error GoTo ErrorHandler
    Call Log_Step(SUB_NAME)

    Dim msg As String

    Call MGlobal.Global_Initialize

    Call MSecurity.Workbook_Login_Username
    Call MSecurity.Login_Query_Parameters
    Call MSecurity.Check_Project_Version_Access

    Call MSetup.Setup_Project

Finish:
    Call MInterface.Activate_Main_Menu
    Call MGlobal.Global_Finalize
    Exit Sub

ErrorHandler:
    Call MInterface.Message_Error(err.Number & vbNewLine & err.Description, SUB_NAME)
    GoTo Finish

End Sub
Sub Login_User()
    Const SUB_NAME As String = "User Login"
    Call Log_Step(SUB_NAME)

    Call Setup_Worksheets_Visibility
    'Call Window_Controls(False)
End Sub
Sub Login_Query_Parameters()
    Const SUB_NAME As String = "Login_Query_Parameters"
    On Error GoTo CreateUser
    Call Log_Step(SUB_NAME)

    Dim qry As Parameter

    ThisWorkbook.Queries("Current_User").Formula = ASPAS_DUPLAS & Get_Windows_UserName & ASPAS_DUPLAS
    Exit Sub
    
CreateUser:
    Call Create_PowerBI_Current_User
End Sub
Sub Create_PowerBI_Current_User()
    Const SUB_NAME As String = "Create_PowerBI_Current_User"
    Call Log_Step(SUB_NAME)

    On Error Resume Next
    ActiveWorkbook.Queries.Add Name:="Current_User", Formula:="""udu"" meta [IsParameterQuery=true, Type=""Text"", IsParameterQueryRequired=false]", Description:="The current user"
End Sub
Sub Protect_All_Worksheet()
    Const SUB_NAME As String = "Protect All Worksheet"
    Call Log_Step(SUB_NAME)

    Dim wksT As Worksheet
    
    For Each wksT In ThisWorkbook.Worksheets
        Call Protect_Worksheet(wksT)
    Next wksT
End Sub
Sub Protect_Worksheet(wksp As Worksheet)
    Const SUB_NAME As String = "Protect Worksheet"
    Call Log_Step(SUB_NAME)

    Call wksp.Protect(WORKBOOK_PASSWORD)
End Sub
Sub Show_All_Worksheets()
    Const SUB_NAME As String = "Show All Worksheets"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet

    For Each wks In ThisWorkbook.Worksheets
        wks.Activate
        'Call Window_Controls(True)
        wks.Visible = xlSheetVisible

    Next wks

    Call MInterface.Activate_Main_Menu
End Sub
Sub Unlock_All_Data_Worksheets()
    Const SUB_NAME As String = "Unlock All Data Worksheets"
    Call Log_Step(SUB_NAME)

    Dim wks As Worksheet
    For Each wks In ThisWorkbook.Worksheets
        If Regx_Match(wks.Codename, "wksData_") Then
            wks.Cells.Locked = False
            wks.Rows(1).Locked = True
        End If

    Next wks
End Sub
Sub Unprotect_All_Worksheet()
    Const SUB_NAME As String = "Unprotect All Worksheet"
    Call Log_Step(SUB_NAME)

    Dim wksT As Worksheet

    For Each wksT In ThisWorkbook.Worksheets
        Call Unprotect_Worksheet(wksT)
    Next wksT
End Sub
Sub Unprotect_Worksheet(wksp As Worksheet)
    Const SUB_NAME As String = "Unprotect Worksheet"
    Call Log_Step(SUB_NAME)

    Call wksp.Unprotect(WORKBOOK_PASSWORD)
End Sub
Sub Unprotect_Worksheet_by_Codename(Codename As String)
    Const SUB_NAME As String = "Unprotect Worksheet by Codename"
    On Error Resume Next
    Call Log_Step(SUB_NAME)

    Dim wksp As Worksheet
    Set wksp = MStructure.Get_Worksheet_By_Codename(Codename)
    
    If Not wksp Is Nothing Then
        Call wksp.Unprotect(WORKBOOK_PASSWORD)
    End If
    
End Sub
Sub Window_Controls(IsOn As Boolean)
    Const SUB_NAME As String = "Window Controls"
    On Error Resume Next
    Call Log_Step(SUB_NAME)

    Dim wksActive As Worksheet
    Dim wks As Worksheet
    Set wksActive = ActiveSheet
    
    Application.DisplayFormulaBar = IsOn
    For Each wks In ThisWorkbook.Worksheets
        If wks.Visible = xlSheetVisible Then

            wks.Activate
            With ActiveWindow
                .DisplayGridlines = IsOn
                .DisplayHeadings = IsOn
                '.DisplayWorkbookTabs = IsOn
                .AutoFilterDateGrouping = IsOn
                
            End With
        End If
    Next wks
    wksActive.Activate
End Sub
Sub Workbook_Login()
    Const SUB_NAME As String = "Workbook Login"
    Call Log_Step(SUB_NAME)

    Dim frm As frmLogin
    
    Set frm = New frmLogin
    
    Call MGlobal.Global_Initialize
    Call frm.Show(1)
    Call MGlobal.Global_Finalize
    Call MInterface.Activate_Main_Menu
End Sub
Sub Workbook_Login_Username()
    Const SUB_NAME As String = "Workbook Login Username"
    Call Log_Step(SUB_NAME)

    Dim msg As String

    If WindowsUsername = vbNullString Then
        Call MSecurity.Get_Windows_UserName
    End If

    Call MSecurity.Hide_All_Worksheets(visibleWorksheet:=Get_Worksheet_By_Codename("wksMenu"))
    
    'Mensagem Boas-vindas ao usuario
    msg = "Welcome! " & Get_Windows_UserName & vbCrLf & Get_Office_UserName
    Call MInterface.Message_Information(msg, "Login")
    
    If UCase(WindowsUsername) = UCase(WORKBOOK_MASTER_USER) Then
        Call MSecurity.Login_Admin
    Else
        
        Call MSecurity.Login_User
    End If

    Call MInterface.Activate_Main_Menu

End Sub



