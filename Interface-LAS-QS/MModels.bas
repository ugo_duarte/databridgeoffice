Attribute VB_Name = "MModels"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MModels"

Global Const MODEL_DATA                                 As String = "ModelData"
Global Const MODEL_DATA_ASSETS                          As String = "Assets"
Global Const MODEL_DATA_DEALDETAILS                     As String = "DealDetails"
Global Const MODEL_DATA_DELIVERYADDRESSES               As String = "DeliveryAddresses"
Global Const MODEL_DATA_SUPPLIERS                       As String = "Suppliers"

Public Type ModelsSerialNumbers
    Column_Description      As Long
    Column_Type             As Long
    Column_ModelLAS         As Long
    Column_Class            As Long
    Column_DescriptionLAS   As Long
    Column_Valid            As Long
End Type

Public Sub Add_Data(wksModeling As Worksheet, Model As String, Optional Protect As Boolean = True)
    Const SUB_NAME As String = "Add Data"
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksModels")
    Call Unprotect_Worksheet(wksModeling)
    
    wks.Range(MODEL_DATA & Model & "_Row").Copy
    wksModeling.Range(Model & TAG_RANGE & TAG_FOOTER).Offset(-1).Insert
    
    Call Refresh_NamedRanges_Between_NamedRanges(wksModeling, "B", 4, 6, wksModeling.Range(Model & TAG_RANGE & TAG_HEADER), wksModeling.Range(Model & TAG_RANGE & TAG_FOOTER))
    Call New_NamedRange(Model & TAG_CURRENT_RANGE, wksModeling.Range(wksModeling.Range(Model & TAG_RANGE & TAG_HEADER).Offset(1), wksModeling.Range(Model & TAG_RANGE & TAG_FOOTER).Offset(-1)))
    
    wksModeling.Columns.AutoFit
    
    If Protect Then
        Call Protect_Worksheet(wksModeling)
    End If
    
End Sub
Sub Change_Shape_text(wksShape As Worksheet, Model As String, Text As String)
    Dim shp As Shape
    Set shp = wksShape.Shapes(Model)
    shp.TextFrame2.TextRange.Characters.Text = Text
End Sub

Function Get_Shape_text(wksShape As Worksheet, Model As String) As String
    Dim shp As Shape
    Set shp = wksShape.Shapes(Model)
    
    Get_Shape_text = shp.TextFrame2.TextRange.Characters.Text

End Function

Sub Model_Assets()
    Const SUB_NAME As String = "Model Assets"
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_Assets")
    Call Modeling_Data(MODEL_DATA_ASSETS, wks, "Assets")
End Sub
Sub Model_DealDetails()
    Const SUB_NAME As String = "Model Deal Details"
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_DealDetails")
    wks.Cells.ClearContents
    Call Modeling_Data(MODEL_DATA_DEALDETAILS, wks, "Deal Details and Asset Forecast")
End Sub
Sub Model_DeliveryAddresses()
    Const SUB_NAME As String = "Model Delivery Addresses"
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_DeliveryAddresses")
    Call Modeling_Data(MODEL_DATA_DELIVERYADDRESSES, wks, "Delivery Addresses")
End Sub
Sub Model_Report_DMS_Description()
    Const SUB_NAME As String = "Model Report DMS Description"

    Dim wks As Worksheet
    
    Set wks = Get_Worksheet_By_Codename("wksReportDMSDescription")
    
    With wks
        .Columns.AutoFit
        .Columns(1).ColumnWidth = 30
        .Columns(2).ColumnWidth = 10
        .Columns(3).ColumnWidth = 21
        .Columns(4).ColumnWidth = 35
        .Cells.WrapText = False
    End With


End Sub

Sub Model_Suppliers()
    Const SUB_NAME As String = "Model Suppliers"
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_Suppliers")
    Call Modeling_Data(MODEL_DATA_SUPPLIERS, wks, "Suppliers")
End Sub
Sub Modeling_Data(Model As String, wksModeling As Worksheet, HeaderText As String, Optional OnlyFormats As Boolean = False, Optional CanClearShapes As Boolean = False, Optional PasteType As XlPasteType = xlPasteAll)
    Const SUB_NAME As String = "Modeling Data"

    Dim rngHeader   As Range
    Dim rngFooter   As Range
    Dim ulAssets    As Long
    Dim wks As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksModels")
    
    If Model = MODEL_DATA_DEALDETAILS Then
        If OnlyFormats Then
            PasteType = xlPasteFormats
        End If
        Call Model_to_NamedRange(MODEL_DATA & Model, Model & TAG_CURRENT_RANGE, PasteType)
    Else
        wks.Range(MODEL_DATA & Model & "_Row").Copy
        wksModeling.Range(Model & TAG_CURRENT_RANGE).PasteSpecial PasteType
        Call Refresh_NamedRanges_Between_NamedRanges(wksModeling, "B", 4, 6, wksModeling.Range(Model & TAG_RANGE & TAG_HEADER), wksModeling.Range(Model & TAG_RANGE & TAG_FOOTER))
    End If

    If CanClearShapes Then
        Call Clear_Shapes_Worksheet(wksModeling)
        Call Shape_to_Range(MODEL_IMG_CHG_LOGO, wksModeling, "B2", vbNullString, 8, 5)
        Call Shape_to_Range(MODEL_LABEL_HEADER, wksModeling, "C2", HeaderText, 8, 50)
    End If

    Application.CutCopyMode = False
    wksModeling.Activate
    wksModeling.Range("A1").Select

End Sub

Public Sub Refresh_Models_Access_Named_Range()
    Const SUB_NAME As String = "Refresh Models Access Named Range"
    Dim wksM As Worksheet
    Dim aData() As Variant
    Dim i As Long
    Dim rngNamedRange As Range
    Dim jNamedRange As Long
    Dim jNamedRangeMax As Long
    Dim NamedRange As String
    Dim iCreated As Long
    
    
    Set wksM = MStructure.Get_Worksheet_By_Codename("wksModels_Access")
    aData = wksM.UsedRange
    jNamedRange = MStructure.Get_Column_Number(aData, "NamedRange")
    iCreated = 0
    
    For i = LBound(aData) + 1 To UBound(aData)
        NamedRange = aData(i, jNamedRange)
        If NamedRange <> vbNullString Then
            iCreated = iCreated + 1
            jNamedRangeMax = MStructure.Get_Last_Column(wksM, i)
            Set rngNamedRange = wksM.Cells(i, jNamedRange + 1).Resize(, jNamedRangeMax - jNamedRange)
            Call MStructure.New_NamedRange(NamedRange, rngNamedRange)
        End If
    Next i
    
    Call MInterface.Message_Information("Created " & iCreated & " Named Ranges", SUB_NAME)
End Sub
Sub Styles()
'
' Styles Macro
'

'
    Selection.Style = "CHG-Currency"
    Selection.Style = "CHG-Currency"
    With ActiveWorkbook.Styles("CHG-Currency")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
    Range("B3").Select
    Selection.Style = "CHG-Date"
    With ActiveWorkbook.Styles("CHG-Date")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
    ActiveWorkbook.Styles("CHG-Date").NumberFormat = "dd/mm/yyyy"
    Range("B4").Select
    Selection.Style = "CHG-General"
    With ActiveWorkbook.Styles("CHG-General")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
    ActiveWorkbook.Styles("CHG-General").NumberFormat = "General"
    Range("B5").Select
    Selection.Style = "CHG-Number"
    With ActiveWorkbook.Styles("CHG-Number")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
    ActiveWorkbook.Styles("CHG-Number").NumberFormat = "0.00"
    Range("B6").Select
    Selection.Style = "CHG-Number4"
    With ActiveWorkbook.Styles("CHG-Number4")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
    ActiveWorkbook.Styles("CHG-Number4").NumberFormat = "0.0000"
    Range("B7").Select
    Selection.Style = "CHG-Text"
    With ActiveWorkbook.Styles("CHG-Text")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
End Sub
Sub Styles_CHGBrazil()
    Dim st As Style
    Dim wksStyle As Worksheet
    Dim aData() As Range
    Dim i As Long
    
    Set wksStyle = MStructure.Get_Worksheet_By_Codename("wksModels_Style")
    aData = MStructure.Get_Current_Region(wksStyle.Range("A1"))
    
    For i = LBound(aData) + 1 To UBound(aData)
        Set st = ThisWorkbook.Styles(aData(i, 2))
        With st
            .IncludeNumber = True
            .IncludeFont = True
            .IncludeAlignment = True
            .IncludeBorder = True
            .IncludePatterns = True
            .IncludeProtection = True
        End With
    Next i
    
    
    
    
    
End Sub
Sub Style_Delete_And_Create(StyleName As String)
    Call Style_Delete(StyleName)
    Call Style_Create(StyleName)
End Sub
Sub Style_Delete(StyleName As String)
    ThisWorkbook.Styles(StyleName).Delete
End Sub
Sub Style_Create(StyleName As String)
        
    
    With ActiveWorkbook.Styles("Style 1")
        .IncludeNumber = True
        .IncludeFont = True
        .IncludeAlignment = True
        .IncludeBorder = True
        .IncludePatterns = True
        .IncludeProtection = True
    End With
    ActiveWorkbook.Styles("Style 1").NumberFormat = "h:mm;@"
    With ActiveWorkbook.Styles("Style 1")
        .HorizontalAlignment = xlGeneral
        .VerticalAlignment = xlCenter
        .ReadingOrder = xlLTR
        .WrapText = False
        .Orientation = 0
        .AddIndent = False
        .ShrinkToFit = False
    End With
    With ActiveWorkbook.Styles("Style 1").Borders(xlLeft)
        .LineStyle = xlContinuous
        .Color = -5658199
        .TintAndShade = 0
        .Weight = xlThin
    End With
    ActiveWorkbook.Styles("Style 1").Borders(xlRight).LineStyle = xlNone
    ActiveWorkbook.Styles("Style 1").Borders(xlTop).LineStyle = xlNone
    With ActiveWorkbook.Styles("Style 1").Borders(xlBottom)
        .LineStyle = xlContinuous
        .Color = -5658199
        .TintAndShade = 0
        .Weight = xlThin
    End With
    ActiveWorkbook.Styles("Style 1").Borders(xlDiagonalDown).LineStyle = xlNone
    ActiveWorkbook.Styles("Style 1").Borders(xlDiagonalUp).LineStyle = xlNone
    With ActiveWorkbook.Styles("Style 1").Interior
        .Pattern = xlSolid
        .PatternColorIndex = 0
        .ThemeColor = xlThemeColorAccent5
        .TintAndShade = 0.599963377788629
        .PatternTintAndShade = 0
    End With
    With ActiveWorkbook.Styles("Style 1")
        .Locked = False
        .FormulaHidden = False
    End With
End Sub
