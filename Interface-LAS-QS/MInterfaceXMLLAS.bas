Attribute VB_Name = "MInterfaceXMLLAS"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MInterfaceXMLLAS"

Sub Apply_Filters()
    Const SUB_NAME As String = "Apply Filters"
    Dim wks As Worksheet
    
    Set wks = Get_Worksheet_By_Codename("wksReport_Interface_XMLLAS")
    wks.Range("A1").Resize(, Get_Last_Column(wks, 1)).AutoFilter
    
    Set wks = Get_Worksheet_By_Codename("wksReport_SerialNumbers")
    wks.Range("A1").Resize(, Get_Last_Column(wks, 1)).AutoFilter

End Sub
Sub Apply_SerialNumbers()
    Dim aSerialNumbersData() As Variant

    Dim i As Long
    Dim ColumnSN As Long
    Dim ColumnNF As Long

    Dim aSerialNumbersReport() As Variant
    Dim SerialNumbers As Variant

    Dim dicSerialNumbers As Dictionary

    Dim key As Variant
    Dim Item As Variant
    Dim j As Long

    Dim wks As Worksheet
    Dim wksReport As Worksheet
    Set wks = Get_Worksheet_By_Codename("wksData_SerialNumbers")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_SerialNumbers")

    aSerialNumbersData = wks.Range("A1").CurrentRegion
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Report_SerialNumbers_Header")
    ColumnSN = Get_Column_Number(aSerialNumbersData, "N� de S�rie")
    ColumnNF = Get_Column_Number(aSerialNumbersData, "NF")
    Set dicSerialNumbers = New Dictionary
    
    For i = LBound(aSerialNumbersData) + 1 To UBound(aSerialNumbersData)
        ReDim aSerialNumbersReport(1 To 1, 1 To 3)

        aSerialNumbersReport(1, 1) = aSerialNumbersData(i, ColumnNF)

        SerialNumbers = Split(aSerialNumbersData(i, ColumnSN), ",")

        For j = LBound(SerialNumbers) To UBound(SerialNumbers)
            aSerialNumbersReport(1, 3) = SerialNumbers(j)
            key = aSerialNumbersReport(1, 1) & "-" & aSerialNumbersReport(1, 3)

            If Not dicSerialNumbers.Exists(key) Then
                Call dicSerialNumbers.Add(key, aSerialNumbersReport)
            End If


        Next j
    Next i

    For Each Item In dicSerialNumbers.Items
        wksReport.Range("A1").Offset(Get_Last_Row(wksReport, 1)).Resize(, UBound(Item, 2)) = Item
    Next Item


    Call Apply_Filters

End Sub
Sub Generate_Desmembering_Goods()

    Dim aHeader() As Variant
    Dim aData() As Variant
    Dim aNewData() As Variant
    Dim i As Long
    Dim iNewData As Long

    Dim j As Long

    Dim Column_Quantity As Long
    Dim TotalNewRecords As Long
    Dim DataTotalColumns As Long
    Dim NumberFormat As String

    Dim wksReport As Worksheet
    Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE")
    
    NumberFormat = MStructure.Check_Number_Format

    DataTotalColumns = MStructure.Get_Last_Column(wksReport, 1)
    aHeader = wksReport.Range("A1").Resize(, DataTotalColumns)
    aData = wksReport.Range("A1").Offset(1).Resize(MStructure.Get_Last_Row(wksReport, 1) - 1, DataTotalColumns)

    Column_Quantity = MStructure.Get_Column_Number(aHeader, "Quantity")
    TotalNewRecords = MStructure.Get_Total(aData, Column_Quantity)

    ReDim aNewData(1 To TotalNewRecords, 1 To UBound(aData, 2))
    iNewData = LBound(aData)

    For i = LBound(aData) To UBound(aData)

        If i Mod 100 = 0 Then
            DoEvents
            Application.StatusBar = "Desmembering Invoices: " & i & " / " & UBound(aData)
        End If

        Do While (aData(i, Column_Quantity) > 0)
            aData(i, Column_Quantity) = aData(i, Column_Quantity) - 1
            For j = LBound(aData, 2) To UBound(aData, 2)
                aNewData(iNewData, j) = aData(i, j)
            Next j

            aNewData(iNewData, Column_Quantity) = 1
            iNewData = iNewData + 1
            If iNewData > TotalNewRecords Then
                Exit Do
            End If
        Loop

    Next i

    Application.StatusBar = "Desmembering Invoices: Success!"
    
    
    'Desmembering!
    Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE_Desmembered")
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_NFE_Header")
    wksReport.Range("A1").Resize(, UBound(aHeader, 2)) = aHeader
    wksReport.Range("A1").Offset(1).Resize(UBound(aNewData), UBound(aNewData, 2)) = aNewData
    Call Insert_Serial_Numbers(wksReport)

End Sub
Sub Import_SerialNumbers()
    Const SUB_NAME              As String = "Import_SerialNumbers"
    On Error GoTo ErrorHandler
    
    Dim wkbFile                 As Workbook
    Dim wksFile                 As Worksheet
    
    Dim Model                   As String
    Dim ModelRX                 As String
    Dim rngModel                As Range
    Dim rngModelRX              As Range
    Dim aModel()                As Variant
    Dim aModelRX()              As Variant
    Dim aFileHeader()           As Variant
    Dim aSN()                   As Variant
    Dim SelectedFiles()         As Variant
    Dim SelectedFile            As Variant
    Dim i                       As Long
    Dim j                       As Long
    Dim wksData As Worksheet
    Dim wksTemp As Worksheet
    Dim aData() As Variant
    Dim aFileData() As Variant
    Dim Column As Long
    
    'Process
    SelectedFiles = Get_Selected_Files
    If SelectedFiles(1) = vbNullString Then GoTo Finish
    
    gTimerStart = Now
    
    Set wksData = Get_Worksheet_By_Codename("wksData_SerialNumbers")
    
    For Each SelectedFile In SelectedFiles
        
        Set wkbFile = Workbooks.Open(SelectedFile)
        
        ModelRX = "Model_Data_SerialNumbers_Header_ValidRegex"
        Set rngModelRX = Get_Model_Range(ModelRX)
        aModelRX = rngModelRX
        
        Model = "Model_Data_SerialNumbers_Header"
        Set rngModel = Get_Model_Range(Model)
        aModel = rngModel
        
        Call Generate_Worksheet_From_Model(wksData, False, Model)
        
        Set wksFile = wkbFile.Worksheets(1)
        aFileHeader = wksFile.Range("A1").Resize(, Get_Last_Column(wksFile, 1))
        aFileData = wksFile.Range("A1").Resize(Get_Last_Row(wksFile, 1), Get_Last_Column(wksFile, 1))
        If Get_Data_From_Valid_Model(aModel, aModelRX, aFileData, True) Then
            wksData.Range("A" & Get_Last_Row(wksData, 1) + 1).Resize(UBound(aFileData), UBound(aFileData, 2)) = aFileData
            Application.CutCopyMode = False
        Else
            Call MsgBox("Incorrect Layout! Check headers!" & vbNewLine & "File: " & wkbFile.Name & vbNewLine, vbCritical, SUB_NAME)
        End If
        
        wkbFile.Close (False)
        
        Set wkbFile = Nothing
    Next SelectedFile
    
    Call MInterfaceXMLLAS.Apply_SerialNumbers
    
    Call MStructure.Worksheet_Columns_Autofit("wksData_SerialNumbers")
    Call MStructure.Worksheet_Columns_Autofit("wksReport_SerialNumbers")
    
    
Finish:
    If Not wkbFile Is Nothing Then
        wkbFile.Close (False)
    End If
    
    Exit Sub
ErrorHandler:
    GoTo Finish
    
End Sub
Sub Import_XML_NFE_Cover()
    Const SUB_NAME              As String = "Import XML NFE Cover"
    On Error GoTo ErrorHandler
    
    Dim xDoc                As MSXML2.DOMDocument
    Dim xDocNode            As IXMLDOMNode
        
    Dim key                 As Variant
    Dim Item                As Variant
    
    Dim IsMainNode          As Boolean

    Dim wksReport As Worksheet
    Dim wksTemp As Worksheet
    
    Call MStructure.FilesPaths_To_Collection
    
    If gdicPaths.Count = 0 Then
        Call MsgBox("(0) Valid XML files found!", vbCritical, "Error")
        GoTo Finish
    End If
    

    'CLEAN
    Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE")
    Call Generate_Worksheet_From_Model(wksReport, False, "Model_NFE_Cover_Header")
    
    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
            
    'MODELS
    Call MXML.Generate_XML_Fields_Extra(False)
    Call MXML.Generate_XML_Fields_Array
    Call MXML.Generate_XML_Fields_Extra
    
    'IMPORT: XML
    gFileID = 0
    For Each key In gdicPaths.Keys
        gFileID = gFileID + 1
        
        'XML object
        Set xDoc = New MSXML2.DOMDocument
        xDoc.Load (key)
        If Not xDoc Is Nothing Then
            Call MXML.XML_Import(xDoc, key, , True)
        End If
    Next key


    Call MXML.XML_Generate_Temp_Data(wksReport)

    Call MStructure.Worksheet_Columns_Autofit("wksReport_XML_NFE")

Finish:
    Exit Sub
ErrorHandler:
    GoTo Finish
End Sub
Sub Import_XML_NFE()
    Const SUB_NAME              As String = "Import XML NFE"
    On Error GoTo ErrorHandler
    
    Dim xDoc                As MSXML2.DOMDocument
    Dim xDocNode            As IXMLDOMNode
        
    Dim key                 As Variant
    Dim Item                As Variant
    
    Dim IsMainNode          As Boolean

    Dim wksReport As Worksheet
    Dim wksTemp As Worksheet
    Call MStructure.FilesPaths_To_Collection
    If gdicPaths.Count = 0 Then
        Call MsgBox("(0) Valid XML files found!", vbCritical, "Error")
        GoTo Finish
    End If
    
    'TIMER: START
    gTimerStart = Now
    
    'CLEAN
    Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE")
    Set wksTemp = Get_Worksheet_By_Codename("wksTemporary")
    Call MStructure.Clean_Formats(wksReport)
    Call MSetup.Clean_Model_XML
    Call MSetup.Clean_Model_Interfaces
            
    'MODELS
    Call MXML.Generate_XML_Fields_Extra(False)
    'Call MXML.Generate_XML_Fields
    Call MXML.Generate_XML_Fields_Array
    Call MXML.Generate_XML_Fields_Extra
    
    'IMPORT: XML
    gFileID = 0
    For Each key In gdicPaths.Keys
        gFileID = gFileID + 1
        
        'XML object
        Set xDoc = New MSXML2.DOMDocument
        xDoc.Load (key)
        If Not xDoc Is Nothing Then
            Call MXML.XML_Import(xDoc, key)
        End If
    Next key
    
    Call MStructure.Clean_Worksheet(wksTemp)
    Call MXML.XML_Generate_Temp_Data
    
    Call MInterfaceXMLLAS.Validate_Models_Serial_Numbers
    
    
    Application.StatusBar = "Saving Workbook to get data!"
    ThisWorkbook.Save
    
    Call MSQL.Generate_Data_XML_NFE
    
    Call MInterfaceXMLLAS.Generate_Desmembering_Goods

    Call MStructure.Worksheet_Columns_Autofit("wksReport_XML_NFE")
    Call MStructure.Worksheet_Columns_Autofit("wksReport_XML_NFE_Desmembered")

Finish:
    Exit Sub
ErrorHandler:
    GoTo Finish
End Sub
Sub Import_XML_NFS()
    Const SUB_NAME          As String = "Import_XML_NFS"
    Dim wksReport As Worksheet
    Dim xDoc                As MSXML2.DOMDocument
    Dim xDocNode            As IXMLDOMNode
    Dim key                 As Variant
    
    'Process
    Call MStructure.FilesPaths_To_Collection
    Call MXML.Generate_XML_Fields_Extra(False)
    Call MSetup.Clean_Model_XML
    
    If gdicPaths.Count = 0 Then
        Call MsgBox("(0) Valid XML files found!", vbCritical, "Error")
        GoTo Finish
    End If

    '5 Iterate through files
    gFileID = 0
    For Each key In gdicPaths.Keys
        Application.StatusBar = "Generating NFS Data: " & key
        gFileID = gFileID + 1
        
        'XML object
        Set xDoc = New MSXML2.DOMDocument
        xDoc.Load (key)
        If Not xDoc Is Nothing Then
            Call Generate_XML_NFS_Fields(xDoc, key)
        End If
    Next key
    
    Set wksReport = Get_Worksheet_By_Codename("wksReport_XML_NFE")
    Call MInterfaceXMLLAS.Validate_Models_Serial_Numbers(wksReport)
    Call MInterfaceXMLLAS.Generate_Desmembering_Goods
    
Finish:
End Sub
Sub Insert_Serial_Numbers(wksInsert As Worksheet)
    Dim ColumnSerialNumbers As Long
    Dim ColumnValid         As Long
    Dim ColumnInvoiceNo     As Long
    Dim ColumnModel         As Long
    Dim Column_Description   As Long
    
    Dim aData()          As Variant
    Dim aSerialNumbers()    As Variant
    Dim iSerialNumbers      As Long
    Dim ColumnSN_NF         As Long
    Dim ColumnSN_SN         As Long
    Dim i                   As Long
    Dim dicValidModelsSerialNumbers     As Dictionary
    Dim key                             As Variant
    Dim MSN As ModelsSerialNumbers
    Dim wksReport As Worksheet
    
    MSN = New_Type_ModelsSerialNumbers
    Set dicValidModelsSerialNumbers = Load_Dictionary("ModelsSerialNumbers_I", , , MSN.Column_Valid)
    
    Set wksReport = Get_Worksheet_By_Codename("wksReport_SerialNumbers")
    wksReport.Cells.Interior.Pattern = xlNone
    Call Generate_Worksheet_From_Model(wksReport, False, "Model_Report_SerialNumbers_Header")
    aData = wksInsert.Range("A1").CurrentRegion
    aSerialNumbers = wksReport.Range("A1").CurrentRegion
    
    ColumnSerialNumbers = Get_Column_Number(aData, "SN")
    ColumnInvoiceNo = Get_Column_Number(aData, "InvoiceNo")
    ColumnModel = Get_Column_Number(aData, "Model")
    Column_Description = Get_Column_Number(aData, "AssetName")
    
    ColumnSN_NF = Get_Column_Number(aSerialNumbers, "NF")
    ColumnSN_SN = Get_Column_Number(aSerialNumbers, "SN")
    'SERIAL NUMBERS
    For i = LBound(aData) + 1 To UBound(aData)
        key = UCase(Trim(aData(i, Column_Description)))
        
        If dicValidModelsSerialNumbers.Exists(key) Then
        
            For iSerialNumbers = LBound(aSerialNumbers) + 1 To UBound(aSerialNumbers)
                If aSerialNumbers(iSerialNumbers, ColumnSN_NF) <> vbNullString Then
                    If CStr(aData(i, ColumnInvoiceNo)) = CStr(aSerialNumbers(iSerialNumbers, ColumnSN_NF)) Then
                        aData(i, ColumnSerialNumbers) = aSerialNumbers(iSerialNumbers, ColumnSN_SN)
                        aSerialNumbers(iSerialNumbers, ColumnSN_NF) = vbNullString
                        wksReport.Cells(iSerialNumbers, ColumnSN_SN).Interior.Color = RGB(0, 255, 0)
                        Exit For
                    End If
                End If
            Next iSerialNumbers
        End If
    Next i
    
    wksInsert.Range("A1").Resize(UBound(aData), UBound(aData, 2)) = aData
    
End Sub
Sub Interface_LAS_Assets()
    Const SUB_NAME As String = "Interface LAS Assets"
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Set wksData = Get_Worksheet_By_Codename("wksReport_XML_NFE_Desmembered")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_Interface_XMLLAS")
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Interface_LAS_Header")
    Call Interface_Layout(wksData, wksReport, "LAS_Assets_CR", 16)
    Call Apply_Filters
End Sub
Sub Interface_Layout(wksData As Worksheet, wksReport As Worksheet, LayoutName As String, ColumnUL As Long, Optional ParametersName As String = "Menu_Parameters")

    Dim aData() As Variant
    Dim aLayout() As Variant
    Dim aHeaderPurchasesInstallation() As Variant
    Dim rngPurchasesInstallation As Range
    
    Dim dicAssetsPositionPurchase As Dictionary
    Dim dicPurchasesInstallation As Dictionary
    Dim dicModelsSerial As Dictionary
    Dim dicPurnoInvoice As Dictionary
    Dim dicSupplierManufacturer As Dictionary
    Dim dicNewSupplierManufacturer As Dictionary
    Dim dicNewPurnoInvoice As Dictionary
    Dim dicDMSAddresses As Dictionary
    
    Dim aRecord() As Variant

    Dim i As Long
    Dim iRecord As Long
    Dim iLayout As Long

    Dim lay As Layout
    Dim MSN As ModelsSerialNumbers

    Dim Column_ID As Long
    Dim Column_DataName As Long
    Dim Column_DefaultValue As Long
    Dim Column_KeyModel As Long
    Dim Column_PID As Long
    Dim Column_Purchase As Long
    
    Dim Column_MSN As Long 'Model Serial Number

    Dim key As Variant
    Dim Item As Variant
    Dim PurchaseNo As Variant
    Dim NumberFormat As String
    Dim LASNumberFormat As String
    
    Dim wksList As Worksheet
    
    'PRE
    NumberFormat = Check_Number_Format
    
    aData = wksData.Range("A1").CurrentRegion
    aLayout = Get_Model_Range(LayoutName)
    lay = MSetup.New_Type_Layout(aLayout)
    LASNumberFormat = gdicGlobalParameters.Item("FORMATO VALOR")
    
    Select Case UCase(gProjectName)
        
        Case UCase(PROJECT_LAS_QS_ASSETS)
            Set dicAssetsPositionPurchase = Load_Dictionary("Assets_Position_Purchases_I", 1)
            Column_PID = Get_Column_Number(aData, "PID")
            
            Set dicPurchasesInstallation = Load_Dictionary("Purchases_IC_I", 1, 0)
            Set rngPurchasesInstallation = Get_Model_Range("Purchases_IC_I")
            aHeaderPurchasesInstallation = rngPurchasesInstallation.Resize(, Get_Last_Column(rngPurchasesInstallation.Worksheet, 1))

        Case UCase(PROJECT_XML_LAS_PUCHASES)
            Set dicDMSAddresses = Load_Dictionary("DMS_Addresses", 1, 2)
            
       Case Else
            Set dicModelsSerial = Load_Dictionary("ModelsSerialNumbers_I", 1, 0)
            Set dicPurnoInvoice = Load_Dictionary("Purchases_Invoices_I", 1, 2)
            Set dicSupplierManufacturer = Load_Dictionary("Supplier_Manufacturer_I", 1, 2)
            
            Set dicNewSupplierManufacturer = New Dictionary
            Set dicNewPurnoInvoice = New Dictionary
            
            MSN = New_Type_ModelsSerialNumbers
            Column_KeyModel = Get_Column_Number(aData, "AssetName")

    End Select


    'Identifica as colunas do layout
    For iLayout = LBound(aLayout) + 1 To UBound(aLayout)
        lay.DataOriginType = aLayout(iLayout, lay.Column_DataOriginType)
        lay.DataName = aLayout(iLayout, lay.Column_DataName)
        If InStr(1, UCase(lay.DataOriginType), "DATA") > 0 Then
            aLayout(iLayout, lay.Column_DataName) = Get_Column_Number(aData, lay.DataName)
        End If
    Next iLayout

    ReDim aRecord(1 To 1, 1 To UBound(aLayout) - 1) '-1 for there is a header

    For i = LBound(aData) + 1 To UBound(aData) 'For each record in Data

        For iLayout = LBound(aLayout) + 1 To UBound(aLayout) 'For each field in layout

            lay.DefaultValue = aLayout(iLayout, lay.Column_DefaultValue)
            lay.ID = aLayout(iLayout, lay.Column_ID)
            lay.DataOriginType = aLayout(iLayout, lay.Column_DataOriginType)
            lay.LastValue = aRecord(1, lay.ID)
            lay.CurrentValue = aRecord(1, lay.ID)
            lay.DataName = aLayout(iLayout, lay.Column_DataName)
            lay.Treatment = aLayout(iLayout, lay.Column_Treatment)

            '#1 - Default Value
            If lay.DefaultValue <> vbNullString Then
                lay.CurrentValue = lay.DefaultValue
            End If


            Select Case UCase(lay.DataOriginType)
                Case "PARAMETERS"
                    key = UCase(lay.DataName)

                    If gdicGlobalParameters.Exists(key) Then
                        Item = gdicGlobalParameters.Item(key)

                        If Regx_Match(Item, "PREENCHER") Then
                            Call Message_Error("Verificar !" & vbCrLf & "Parametro: " & key)
                            Exit Sub
                        End If

                        lay.CurrentValue = Item 'Get_Parameter_Value(aParameter, aData(i, lay.DataName))

                    End If

                Case "MODELSN"
                    key = UCase(aData(i, Column_KeyModel))

                    If dicModelsSerial.Exists(key) Then
                        Item = dicModelsSerial.Item(key)

                        Column_MSN = 0
                        Select Case UCase(lay.DataName)
                            Case "TYPE"
                                Column_MSN = MSN.Column_Type
                            Case "MODEL"
                                Column_MSN = MSN.Column_ModelLAS
                            Case "CLASS"
                                Column_MSN = MSN.Column_Class
                            Case "ASSETNAME", "DESCRIPTION LAS", "DESCRIPTION_LAS"
                                Column_MSN = MSN.Column_DescriptionLAS
                        End Select

                        If Not Column_MSN = 0 Then
                            lay.CurrentValue = Item(Column_MSN)
                        End If

                    End If
                Case "RECORD"
                    lay.CurrentValue = aRecord(1, lay.DataName)
                    
                Case "DATA"
                    If IsNumeric(lay.DataName) Then
                        If lay.DataName > 0 Then
                            lay.CurrentValue = aData(i, lay.DataName)
                        End If
                    End If

                Case "PID"
                    key = UCase(aData(i, Column_PID))
                    PurchaseNo = Trim(dicAssetsPositionPurchase.Item(key)) 'Purchase

                    Item = dicPurchasesInstallation(PurchaseNo)


                    If Not IsEmpty(Item) Then
                        Column_Purchase = MStructure.Get_Column_Number(aHeaderPurchasesInstallation, lay.DataName)

                        If Not Column_Purchase = 0 Then
                            lay.CurrentValue = Item(Column_Purchase)
                        End If
                    End If

            End Select

            If lay.Treatment <> vbNullString Then

                Select Case UCase(lay.Treatment)

                    Case "CONVERT-DATE"
                        lay.CurrentValue = MStructure.Convert_Date(lay.CurrentValue)

                    Case "CONVERT-TO-BRL"
                        lay.CurrentValue = MStructure.Convert_Number(lay.CurrentValue, "BRL")
                        If aLayout(iLayout, lay.Column_Operation) = "TEXT" Then
                            lay.CurrentValue = CStr(lay.CurrentValue)
                        End If

                    Case "CONVERT_NUMBER"
                        If IsNumeric(lay.CurrentValue) Then
                            lay.CurrentValue = Convert_Number(lay.CurrentValue, NumberFormat)
                            lay.CurrentValue = Round(CDbl(lay.CurrentValue), 2)
                        End If
                        
                    Case "DEC2"
                        If IsNumeric(lay.CurrentValue) Then
                            lay.CurrentValue = Convert_Number(lay.CurrentValue, NumberFormat)
                            lay.CurrentValue = Round(CDbl(lay.CurrentValue), 2)
                        End If
                        lay.CurrentValue = Format_Number_US(lay.CurrentValue, LASNumberFormat)

                    Case "SUPPLIER-MANUFACTURER"
                        If dicSupplierManufacturer.Exists(lay.CurrentValue) Then
                            lay.CurrentValue = dicSupplierManufacturer.Item(lay.CurrentValue)

                        Else
                            If Not dicNewSupplierManufacturer.Exists(lay.CurrentValue) Then
                                Call dicNewSupplierManufacturer.Add(lay.CurrentValue, "")
                            End If

                            lay.CurrentValue = "MANUFACTURER NOT FOUND: " & lay.CurrentValue
                        End If

                    Case "PURNO-INVOICE"
                        If dicPurnoInvoice.Exists(lay.CurrentValue) Then
                            lay.CurrentValue = dicPurnoInvoice.Item(lay.CurrentValue)

                        Else
                            If Not dicNewPurnoInvoice.Exists(lay.CurrentValue) Then
                                Call dicNewPurnoInvoice.Add(lay.CurrentValue, "")
                            End If

                            lay.CurrentValue = "INVOICE NOT FOUND: " & lay.CurrentValue
                        End If

                    Case Else
                        lay.CurrentValue = MInterfaceLASQS.Interface_Treatment(lay.CurrentValue, lay.Treatment, aLayout(iLayout, lay.Column_Operation), False)

                End Select
            End If
            
            If aLayout(iLayout, lay.Column_Operation) <> vbNullString Then
                lay.CurrentValue = Interface_Operation(lay.CurrentValue, aLayout(iLayout, lay.Column_Operation), LastValue:=lay.LastValue)
            End If
            
            If aLayout(iLayout, lay.Column_MaxLenght) <> vbNullString Then
                lay.CurrentValue = Left(CStr(lay.CurrentValue), CDbl(aLayout(iLayout, lay.Column_MaxLenght)))
            End If

            aRecord(1, lay.ID) = lay.CurrentValue

        Next iLayout

        wksReport.Range("A1").Offset(Get_Last_Row(wksReport, ColumnUL)).Resize(, UBound(aRecord, 2)) = aRecord

    Next i

    wksReport.Columns.AutoFit

    Select Case UCase(gProjectName)
        Case UCase(PROJECT_LAS_QS_ASSETS)

        Case Else

            If dicNewPurnoInvoice.Count > 0 Then
                Set wksList = Get_Worksheet_By_Codename("wksList_PurchasesInvoices")
                Call Dictionary_To_Range(dicNewPurnoInvoice, wksList.Range("A1"))
                Call MInterface.Message_Error("NEW Purchase-Invoice!", "Purchase-Invoice")
            End If

            If dicNewSupplierManufacturer.Count > 0 Then
                Set wksList = Get_Worksheet_By_Codename("wksList_Supplier_Manufacturer")
                Call Dictionary_To_Range(dicNewSupplierManufacturer, wksList.Range("A1"))
                Call MInterface.Message_Error("NEW Supplier-Manufacturer!", "Supplier-Manufacturer")
            End If

    End Select
    
    
    ThisWorkbook.RefreshAll

End Sub
Sub Interface_QS_Assets()
    Const SUB_NAME As String = "Interface QS Assets"
    Dim wksData As Worksheet
    Dim wksReport As Worksheet
    
    Set wksData = Get_Worksheet_By_Codename("wksReport_XML_NFE_Desmembered")
    Set wksReport = Get_Worksheet_By_Codename("wksReport_Interface_QS_Assets")
    
    Call Generate_Worksheet_From_Model(wksReport, True, "Model_Interface_Qualisoft_Header")
    Call Interface_Layout(wksData, wksReport, "QS_Assets_CR", 2)
End Sub
Sub Validate_Models_Serial_Numbers(Optional wksOrigin As Worksheet = Nothing)
    Dim ColumnModel             As Long
    Dim Column_Description      As Long
    Dim i                       As Long
    
    Dim aData()                 As Variant
    Dim aModelsSerialNumbers(1 To 1, 1 To 4) As Variant
    Dim key                     As Variant
    Dim Item                    As Variant
    
    Dim dicModelsSN             As Dictionary
    Dim dicNewModelsSN          As Dictionary

    Dim rng                     As Range
    
    Dim MSN                     As ModelsSerialNumbers
    
    Dim wksLayout               As Worksheet
    
    'Pre
    MSN = New_Type_ModelsSerialNumbers
    Set wksLayout = Get_Worksheet_By_Codename("wksList_Model_SerialNumber")
    Call Generate_Worksheet_From_Model(wksLayout, False, "Model_Layout_SerialNumbers_Header")
    
    If wksOrigin Is Nothing Then
        Set wksOrigin = Get_Worksheet_By_Codename("wksTemporary")
    End If
    
    If wksOrigin.Range("A1").CurrentRegion.Address = "$A$1" Then
        Call err.Raise(999, , "No DATA available")
    End If
    
    aData = wksOrigin.Range("A1").CurrentRegion
    ColumnModel = Get_Column_Number(aData, "Model")
    Column_Description = Get_Column_Number(aData, "AssetName")
    Set dicModelsSN = Load_Dictionary("ModelsSerialNumbers_I")
    Set dicNewModelsSN = New Dictionary
    
    For i = LBound(aData) + 1 To UBound(aData)
        key = UCase(Trim(aData(i, Column_Description)))
        If Not dicModelsSN.Exists(key) And key <> SEPARADOR And key <> vbNullString Then
            
            aModelsSerialNumbers(1, MSN.Column_Description) = aData(i, Column_Description)
            'aModelsSerialNumbers(1, MSN.Column_DescriptionLAS) = aData(i, Column_Description)
            'aModelsSerialNumbers(1, MSN.Column_Valid) = 0
            
            Call dicModelsSN.Add(key, aData(i, ColumnModel))
            Call dicNewModelsSN.Add(key, aModelsSerialNumbers)
            
        End If
    Next i
    
    
    For Each Item In dicNewModelsSN.Items
        wksLayout.Range("A1").Offset(Get_Last_Row(wksLayout, 1)).Resize(, UBound(Item, 2)) = Item
    Next Item
    
    wksLayout.Columns.AutoFit
    wksLayout.Cells.Locked = False
    
Finish:
End Sub
