Attribute VB_Name = "MEmail"
Option Explicit
Option Private Module
Const MODULE_NAME As String = "MEmail"

Public Type MailItems
    MailTo      As String
    MailCC      As String
    MailFrom    As String
    MailCCo     As String
    MailHTMLBody As String
    MailSubject As String
    
End Type
Sub Create_Email(MailToList As String, MailCCList As String, MailSubject As String, MailHTMLBody As String, Optional CanSend As Boolean = False)
    Const SUB_NAME As String = "Create Email"
    On Error GoTo ErrorHandler
    
    Dim OutApp As Outlook.Application
    Dim OutMail As Outlook.MailItem
    Dim Fname As String
    
    Set OutApp = New Outlook.Application
    Set OutMail = OutApp.CreateItem(0)
    
    With OutMail
         .To = MailToList
         .CC = MailCCList
         .Subject = MailSubject
         '.Attachments.Add Fname, 1, 0
         .HTMLBody = MailHTMLBody
         If CanSend Then
            .send
         Else
            .Display
         End If
         
    End With
    
Fim:
    Set OutMail = Nothing
    Set OutApp = Nothing
    
    Exit Sub
    
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Fim
    
End Sub

Public Sub DirectSendAndReceiveCall()
    Dim OutApp As Outlook.Application
    Dim ns As Outlook.Namespace
    
    Set OutApp = New Outlook.Application
    
    Set ns = OutApp.GetNamespace("MAPI")
    ns.SendAndReceive (False)
    
    Set ns = Nothing
    Set OutApp = Nothing
End Sub

Sub Supplier_Email_List()
    Const SUB_NAME As String = "Supplier Email List"
    On Error GoTo ErrorHandler
    
    Dim OutApp As Outlook.Application
    Dim OutMail As Outlook.MailItem
    
    Dim rngTemplate As Range
    Dim rngEmailList As Range
    
    Dim aDataEmailList() As Variant
    Dim iEmailList As Long
    Dim jEmailList As Long
    Dim ColumnMail1 As Long
    Dim ColumnSupplierName As Long
    Dim iTemplate As Long
    
    Dim SupplierName As String
    Dim TemplateTextOriginal As String
    
    
    Dim MI As MailItems
    
    Dim oCht As Chart
    Dim obj
    Dim originalText As String
    Dim bdayText As String
    Dim shpTemplate As Shape
    Dim wksTemplate As Worksheet
    
    Dim Fname As String
    Dim FnamePath As String
    Dim rngActive As Range
    Dim rngSelection As Range
    Dim wksReportBDay As Worksheet
    Dim wksActive As Worksheet
    Dim iApproved As Long
    
    Dim ColumnID As Long
    Dim ColumnName As Long
    Dim ColumnBirthday As Long
    
    Dim aDataTemplate() As Variant
    
    Dim oAccount    As Outlook.Account
    
    Call MGlobal.Global_Initialize
    
    'Supplier e-mail ------------------------------------------------------------------------------------------------------------------------
    
    Set OutApp = New Outlook.Application
    
    Set rngTemplate = Get_Worksheet_By_Codename("wksData_Email_Template").Range("A1")
    Set rngTemplate = rngTemplate.Resize(Get_Last_Row(rngTemplate.Worksheet, 1))
    aDataTemplate = rngTemplate
    Set rngEmailList = Get_Worksheet_By_Codename("wksData_Email_List").Range("A1")
    
    aDataEmailList = Get_Current_Region(rngEmailList)
    ColumnMail1 = Get_Column_Number(aDataEmailList, "E-mail 1")
    ColumnSupplierName = Get_Column_Number(aDataEmailList, "Supplier Name")
    TemplateTextOriginal = vbNullString
    For iTemplate = LBound(aDataTemplate) To UBound(aDataTemplate)
        TemplateTextOriginal = TemplateTextOriginal & aDataTemplate(iTemplate, 1) & "<br>"
    Next iTemplate
    
    TemplateTextOriginal = "<html>" & TemplateTextOriginal & "</html>"
    
    MI.MailSubject = gdicGlobalParameters(UCase("Mail Subject"))
    Set oAccount = Get_Outlook_Account(OutApp, "NotasFiscaisCHGBrazil")
    For iEmailList = LBound(aDataEmailList) + 1 To UBound(aDataEmailList)
        
        MI.MailTo = vbNullString
        MI.MailHTMLBody = TemplateTextOriginal
        SupplierName = Trim(aDataEmailList(iEmailList, 2))
        For jEmailList = ColumnMail1 To UBound(aDataEmailList, 2)
            If Trim(aDataEmailList(iEmailList, jEmailList)) <> vbNullString Then
                MI.MailTo = MI.MailTo & aDataEmailList(iEmailList, jEmailList) & "; "
            End If
        Next jEmailList
        
        
        MI.MailHTMLBody = Replace(MI.MailHTMLBody, "@FORNECEDOR", SupplierName)
        If MI.MailTo <> vbNullString Then
            Set OutMail = OutApp.CreateItem(0)
            With OutMail
                 .To = MI.MailTo
                 .Subject = MI.MailSubject
                 .HTMLBody = MI.MailHTMLBody
                 
                 .BCC = "bruno.rodrigues@chg-meridian.com"
                 .send
            End With
        End If
        
    Next iEmailList

    
    Call MInterface.Message_Information(MSG_SUCCESS, SUB_NAME)
    
Finish:
    Set OutMail = Nothing
    Set OutApp = Nothing
    
    Exit Sub
ErrorHandler:
    Call MInterface.Message_Error(err.Description, SUB_NAME)
    GoTo Finish
    
End Sub
Function Get_Outlook_Account(OutApp As Outlook.Application, AccountName As String) As Outlook.Account
    
 
    Dim oAccount As Outlook.Account
    Set Get_Outlook_Account = Nothing
    For Each oAccount In OutApp.Session.Accounts
 
        If oAccount.UserName = AccountName Then
            
            Set Get_Outlook_Account = oAccount
        End If
    Next oAccount
 
End Function

Public Sub DisplaySenderDetails()
 Dim Explorer As Outlook.Explorer
 Dim CurrentItem As Object
 Dim Sender As Outlook.AddressEntry
 Dim Contact As Outlook.ContactItem
 
 Dim OutApp As Outlook.Application
 Set OutApp = New Outlook.Application
 Set Explorer = OutApp.ActiveExplorer
 
 ' Check whether any item is selected in the current folder.
 If Explorer.Selection.Count Then
 
 ' Get the first selected item.
 Set CurrentItem = Explorer.Selection(1)
 
 ' Check for the type of the selected item as only the
 ' MailItem object has the Sender property.
 If CurrentItem.Class = olMail Then
 Set Sender = CurrentItem.Sender
 
 ' There is no sender if the item has not been sent yet.
 If Sender Is Nothing Then
 MsgBox "There's no sender for the current email", vbInformation
 Exit Sub
 End If
 
 Set Contact = Sender.GetContact
 
 If Not Contact Is Nothing Then
 ' The sender is stored in the contacts folder,
 ' so the contact item can be displayed.
 Contact.Display
 
 Else
 ' If the contact cannot be found, display the
 ' address entry in the properties dialog box.
 Sender.Details 0
 End If
 End If
 End If
End Sub
