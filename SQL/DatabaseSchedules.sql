SELECT LALS.LeaseSchedule
	,LALS.OldLeaseNo                                                                                                                	AS ContractNumber
	,LAIC.Deal                                                                                                                      	AS Deal
	,LALS.MLA																															AS MLA
	
	,U.UserCHG                                                                                                                         	AS SP
	,LALS.AccountManagerID                                                                                                             	AS AccountManagerID
	,U.Username                                                                                                                        	AS AccountManagerName
	,LALS.Customer                                                                                                                     	AS Account
	,LALS.Name1                                                                                                                           	AS AccountName
	,DA.IDNumber                                                                                                                       	AS CNPJ
	
	,DMSC.DealDetailGenerated                                                                                                           AS DealDetailsDate
	,LALS.MinLeaseTerm                                                                                                                  AS Term
	,LALS.ActualLeaseTerm                                                                                                               AS Installments
	,DMSC.DealDetailIndexedBy                                                                                                           AS IndexedBy
	,LALS.CalculationDueValue                                                                                                           AS PaymentType
	,DMSC.Cycle                                                                                                                         AS Cycle
	,DMSC.DealDetailModel                                                                                                               AS LeaseOption
	,DMSC.CHGPresentValuePercent                                                                                                        AS PV
	,DMSC.DealDetailCOF                                                                                                                 AS COF
	,DMSC.DealDetailTAX                                                                                                                 AS TAX
	,DMSC.LeaseFactorNoTax                                                                                                     
	,DMSC.LeaseFactor
	,DMSC.ServiceFactor
	,(DMSC.LeaseFactor + DMSC.ServiceFactor)                      																		AS TotalLRF
	,(DMSC.LeaseFactor * LAIC.InsurValOnDoc)                                                      										AS LeasePMT
	,(DMSC.LeaseFactor + DMSC.ServiceFactor) * (LAIC.InsurValOnDoc) 																	AS TotalPMT
	,DMSC.LeaseOrigination                                                                                                              AS DMSLeaseOrigination
	,LAIC.InsurValOnDoc                                                                                                                 AS LASLeaseOrigination
	,LALS.MailDateOut                                                                                                                   AS TRAFDate
	
FROM (((LAS_Adm_LeaseSchedules AS LALS
    INNER JOIN LAS_Adm_InstallationCertificate AS LAIC
			ON LALS.LeaseSchedule = LAIC.LeaseSchedule)
    LEFT JOIN Users AS U
           ON LALS.AccountManagerID = U.AccountManagerID)
    
    LEFT JOIN DMS_Calculations AS DMSC
		   ON LAIC.Deal = DMSC.Deal)
;