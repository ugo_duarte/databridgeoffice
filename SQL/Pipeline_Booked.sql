SELECT
    LALS.[LeaseSchedule]
  , LALS.[OldLeaseNo]			AS ContractNumber
  , LALS.[MLA]
  , LAIC.Deal                   AS Deal
  , LAIC.InsurValOnDoc			AS LeaseOrigination
FROM (
    LAS_Adm_LeaseSchedules LALS
	LEFT JOIN
        LAS_Adm_InstallationCertificate AS LAIC
        ON LALS.LeaseSchedule = LAIC.LeaseSchedule)
;