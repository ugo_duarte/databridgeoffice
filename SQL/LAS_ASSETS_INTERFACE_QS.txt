SELECT
LAA.[OldLeaseNo]		 AS [Schedule]
,LRPAI.[SupplierInvoice] AS [Invoice No]
,LRPAI.[InvoiceDate]     AS [Invoice Date]
,LRPAI.[Supplier]        AS [Supplier No]
,LAA.[Description]       AS [Asset No]
,LAA.[Description]       AS [Brand]
,LAA.[Description]       AS [Model]
,1						AS Qty
,""						AS [Month/Year]
,""						AS [Unit Value]
,CostCC        	AS [Total Value]
,2						AS [Insurance Type]
,3						AS [POC Type]
,0						AS [POC Value]
,1						AS [POC Type Value]
,LAA.SerialNumber        	AS Key1
,"" 						AS Key2
,"" 						AS Extras
FROM (((LAS_Adm_Assets LAA
INNER JOIN Gerar_Interface_LAS_QS_Assets AS GILQA 
ON LAA.[OldLeaseNo] = GILQA.[ContratoVigente])
INNER JOIN LAS_Report_PurchasesAllocatedToInstallationCertificate AS LRPAI
ON LRPAI.[InstallationCertificate] = LAA.[InstallationCertificate])
INNER JOIN LAS_Adm_Purchases_Subpositions AS LAPS
ON LAPS.[Purchase] = LRPAI.[Purchase]
AND LAA.[PID]= LAPS.[PID])

UNION 
SELECT
LAP.[CustomerRefNo]		 AS [Schedule]
,LAP.[SupplierInvoice] AS [Invoice No]
,LAP.[InvoiceDate]     AS [Invoice Date]
,LAP.[Supplier]        AS [Supplier No]
,"1491"       AS [Asset No]
,LAP.[Supplier]       AS [Brand]
,"SERVICO"       AS [Model]
,1						AS Qty
,""						AS [Month/Year]
,""						AS [Unit Value]
,CurrNetTotal        	AS [Total Value]
,2						AS [Insurance Type]
,3						AS [POC Type]
,0						AS [POC Value]
,1						AS [POC Type Value]
,""        				AS Key1
,"" 					AS Key2
,"" 					AS Extras
FROM ((LAS_Adm_Purchases LAP
INNER JOIN Gerar_Interface_LAS_QS_Assets AS GILQA 
ON LAP.[CustomerRefNo] = GILQA.[ContratoVigente])
LEFT JOIN LAS_Adm_Purchases_Subpositions AS LAPS
ON LAPS.[Purchase] = LAP.[Purchase])
WHERE ISNULL(LAPS.[Purchase]) = TRUE

;