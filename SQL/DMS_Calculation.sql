SELECT
	 DD.Deal
	,DD.DealName
	,DD.DealStatus
	,DD.SuccessProbability
	,DD.AddressNumber
	,DD.AddressName
	,DD.AccountManagerID
	,DD.AccountManagerName
	,DD.LeaseOriginationHardware
	,DD.LeaseOriginationSoftware
	,DD.LeaseOriginationServices
	,DD.LeaseOriginationMaintenance
	,DD.LeaseOrigination
	,DD.LeaseOriginationApprovedByTreasury
	,DD.TechnologyArea
	,DD.DealType
	,DD.Type
	,DD.ContractingPlanned
	,DD.ContractingSold
	,DD.CreateUser
	,DD.CreationDate
	,DD.UpdateUser
	,DD.UpdateDate
	,DD.HomeCurrency
	,DD.Option
--- ADDRESS -----------------------------	
	,DA.Status 		AS AddressStatus
	,DA.SP1
	,DA.ModifyDate 	AS AddressModifyDate
	,DA.IDNumber	AS CNPJ
	
--- CALCULATION -----------------------------
	,DDC.CalculationID
	,DDC.LeaseModel
	,DDC.MinimumLeaseTerm
	,DDC.Cycle
	,DDC.AdvancedArrears
	,DDC.FactorHardware
	,DDC.FactorSoftware
	,DDC.FactorService
	,DDC.FactorMaintenance
	,DDC.IndividualInterestRate
	,DDC.InterestRate

--- DEAL PRINTOUT DEAL DETAILS -----------------------------	
	,DDP.DealDetailOldLeaseNo
	,DDP.DealDetailDeal
	,DDP.DealDetailGenerated
	,DDP.DealDetailUserCHG
	,DDP.DealDetailDealType
	,DDP.DealDetailDetails
	,DDP.DealDetailCurrency
	,DDP.DealDetailGracePeriod
	,DDP.DealDetailCOF
	,DDP.DealDetailNRA
	,DDP.DealDetailIndexedBy
	,DDP.DealDetailBlindDiscount
	,DDP.DealDetailTax                                                                                              
	,DDP.DealDetailDocuSignEnabled
    ,DDP.DealDetailPeriodType
	,DDP.DealDetailPricingNo
	,DDP.DealDetailBillingUnit
	,DDP.DealDetailBillingType
	,DDP.DealDetailModel
	,DDP.DealDetailCAP
	,DDP.DealDetailBuyoutValue
	,DDP.CalculationTESMA
	,DDP.CalculationExternalFactor
	
--- Math
	,DDC.FactorHardware                      								AS LeaseFactorNoTax
	,(DDC.FactorHardware)/(1-DDP.DealDetailTAX)								AS LeaseFactor
	,(DDP.CalculationTESMA + DDP.CalculationExternalFactor)					AS ServiceFactor
	
FROM (((DMS_Deals DD
INNER JOIN DMS_Addresses DA
ON DD.AddressNumber = DA.Address)
INNER JOIN DMS_DealsCalculation DDC
ON DD.DEAL = DDC.DEAL)
INNER JOIN DMS_DealPrintout DDP
ON DD.DEAL = DDP.DEAL)
